var gateway_support_8h =
[
    [ "BackchannelState", "gateway-support_8h.html#a2bc5e15b3622a0b6321905fecbc13c27", null ],
    [ "NO_CONNECTION", "gateway-support_8h.html#abed82baf7f470b522273a3e37c24c600acad28945d500e3fc972e2d1c3b948e8e", null ],
    [ "CONNECTION_EXISTS", "gateway-support_8h.html#abed82baf7f470b522273a3e37c24c600aa7bb2abb8df669a7fed812d874146829", null ],
    [ "NEW_CONNECTION", "gateway-support_8h.html#abed82baf7f470b522273a3e37c24c600ae39dc3d2a5495264033ec8252b8054c3", null ],
    [ "CONNECTION_ERROR", "gateway-support_8h.html#abed82baf7f470b522273a3e37c24c600a933f614e4909db5914bb021329f26f8a", null ],
    [ "gatewayBackchannelStop", "gateway-support_8h.html#a8b1311884cb85081fc0c479d75ae17de", null ],
    [ "backchannelStartServer", "gateway-support_8h.html#a037df48aaf0182f952a4f6d7a1553611", null ],
    [ "backchannelStopServer", "gateway-support_8h.html#a7804f6614dd6a3285a5373c0b3942aa8", null ],
    [ "backchannelReceive", "gateway-support_8h.html#a5f84a1557aaf6e747af3c40c64d84057", null ],
    [ "backchannelSend", "gateway-support_8h.html#aaf3cf3b722d821858aab9e664b2514b5", null ],
    [ "backchannelClientConnectionCleanup", "gateway-support_8h.html#aec99a5c1b7a99c6e6cd33217c5e4cd1e", null ],
    [ "backchannelCheckConnection", "gateway-support_8h.html#ae8715d54252d80ad64e5a49c77aab87b", null ],
    [ "backchannelMapStandardInputOutputToRemoteConnection", "gateway-support_8h.html#a73b957c5fd90f78ff40b5409a236ff01", null ],
    [ "backchannelCloseConnection", "gateway-support_8h.html#ad970733846a3816957895c418dda9d48", null ],
    [ "backchannelServerPrintf", "gateway-support_8h.html#a8bdc4590a4a0d40598f51b4bf7ccbaf4", null ],
    [ "backchannelClientPrintf", "gateway-support_8h.html#acfb8b3bf80218b686310f3ca332bb91c", null ],
    [ "backchannelClientVprintf", "gateway-support_8h.html#ac46d4e4e530c0d6e4b4603bd8a202291", null ],
    [ "backchannelSupported", "gateway-support_8h.html#a56756c4099c8a1fee4d0c252371a8ca5", null ],
    [ "backchannelEnable", "gateway-support_8h.html#a4de2aeb2897c79e6518cf0c7d1808451", null ],
    [ "backchannelSerialPortOffset", "gateway-support_8h.html#a0fed22da0ec0fef66e9a58386593e69c", null ]
];