var structEmberAfTouCalendarStruct =
[
    [ "weeks", "structEmberAfTouCalendarStruct.html#a9e8b1ee2aef99046e9b1100633ae4148", null ],
    [ "normalDays", "structEmberAfTouCalendarStruct.html#a742a649a2c121637623ca056a72603b9", null ],
    [ "specialDays", "structEmberAfTouCalendarStruct.html#a62256f199c11fc04ebba9047cc2c1089", null ],
    [ "seasons", "structEmberAfTouCalendarStruct.html#af58f91ed45cfed70ba64487682c677be", null ],
    [ "providerId", "structEmberAfTouCalendarStruct.html#a9a3e440d7379eeb5c5aa4cbc7321777b", null ],
    [ "issuerEventId", "structEmberAfTouCalendarStruct.html#aeaab2a9398c25e285363aa13a5f572b1", null ],
    [ "calendarId", "structEmberAfTouCalendarStruct.html#abcbf8f42450556d857b118f7cde4501e", null ],
    [ "startTimeUtc", "structEmberAfTouCalendarStruct.html#ad30bdb41ce1266b310b7e5c15e8d0e94", null ],
    [ "name", "structEmberAfTouCalendarStruct.html#a23822decd90e6ec097cac23869ecd342", null ],
    [ "calendarType", "structEmberAfTouCalendarStruct.html#a425947ae2798e971b9416df76a71acaa", null ],
    [ "numberOfSeasons", "structEmberAfTouCalendarStruct.html#af990799f4a68bda59dd61d463378cc87", null ],
    [ "numberOfWeekProfiles", "structEmberAfTouCalendarStruct.html#a6d9649bcb063d33cfe4322afcaf65968", null ],
    [ "numberOfDayProfiles", "structEmberAfTouCalendarStruct.html#a6a2365f8573d90347ff550da79f1d992", null ],
    [ "numberOfSpecialDayProfiles", "structEmberAfTouCalendarStruct.html#a8c17683ad906c363f019e4e1670292e0", null ]
];