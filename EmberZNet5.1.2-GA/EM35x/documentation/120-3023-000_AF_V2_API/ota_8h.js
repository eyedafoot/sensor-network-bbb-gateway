var ota_8h =
[
    [ "OTA_UPGRADE_END_RESPONSE_RUN_NOW", "ota_8h.html#a775ad01edb531a72d906bdd7900ed4ee", null ],
    [ "MFG_ID_WILD_CARD", "ota_8h.html#adc123c7f31fe3b966f610b8f01f57cc2", null ],
    [ "IMAGE_TYPE_WILD_CARD", "ota_8h.html#a0f76ee16ad02adff7d864fa72ce29816", null ],
    [ "IMAGE_TYPE_SECURITY", "ota_8h.html#a5b8618ded0f944a3474b3b6b84e47657", null ],
    [ "IMAGE_TYPE_CONFIG", "ota_8h.html#a073be0c87f7382d0fe53228b877440c1", null ],
    [ "IMAGE_TYPE_LOG", "ota_8h.html#abef4bfe644c6c0bce973a7d3dc283dac", null ],
    [ "FILE_VERSION_WILD_CARD", "ota_8h.html#a8d8ad8a0e19ed802dddfc0ddba7ce056", null ],
    [ "ZIGBEE_2006_STACK_VERSION", "ota_8h.html#aff1e32d8fd9d13d7a4035d402dd76d24", null ],
    [ "ZIGBEE_2007_STACK_VERSION", "ota_8h.html#ad3882aabba4758f35ea86c80d7333fc0", null ],
    [ "ZIGBEE_PRO_STACK_VERSION", "ota_8h.html#ac4a805f357eb2bcb76f6b799382ebbfc", null ],
    [ "OTA_HW_VERSION_BIT_MASK", "ota_8h.html#a1567b3efccdbe9b849be850b1de454e9", null ],
    [ "OTA_NODE_EUI64_BIT_MASK", "ota_8h.html#acc874b2b23c388b30342368256761845", null ],
    [ "OTA_TAG_UPGRADE_IMAGE", "ota_8h.html#ad346271dc616b08b41e47ad6b199b689", null ],
    [ "OTA_TAG_ECDSA_SIGNATURE", "ota_8h.html#ac4944f19a35bd86efd7106dd9bf359dc", null ],
    [ "OTA_TAG_ECDSA_SIGNING_CERTIFICATE", "ota_8h.html#afe72959ac06ba01a30e4911d118c7402", null ],
    [ "OTA_TAG_RESERVED_START", "ota_8h.html#acca4540620f06c147b61c5c7200b575f", null ],
    [ "OTA_TAG_RESERVED_END", "ota_8h.html#a64547835f7008cdc4489c2c3fc9eb097", null ],
    [ "OTA_TAG_MANUFACTURER_SPECIFIC_START", "ota_8h.html#af739e8c2ce37086a16c490c700df32ab", null ],
    [ "EM_AF_OTA_MAX_COMMAND_ID", "ota_8h.html#ab6ef51e9528a43808dbc6b9defde9d94", null ],
    [ "otaPrintln", "ota_8h.html#ac15b869ba9050b739e5181f49c2b0b5f", null ],
    [ "otaPrint", "ota_8h.html#a8b1181f7eaac96af21359566e4f8848c", null ],
    [ "otaPrintFlush", "ota_8h.html#a95aa1c68ef17a65f880daea1315c6a07", null ],
    [ "emAfPrintPercentageSetStartAndEnd", "ota_8h.html#abf16f8e326b80a238daa2dbcd08cb7d3", null ],
    [ "emAfPrintPercentageUpdate", "ota_8h.html#a8816a02a76d352cb61ea3fba602994bd", null ],
    [ "emAfCalculatePercentage", "ota_8h.html#af13156823560495c5d1bd5b50c64ffc2", null ],
    [ "OTA_USE_OFFSET_TIME", "ota_8h.html#abc5c98fcc1211af2b80116dd6e0a035da0c0d3d695799a966ed50fc9a60325ad4", null ],
    [ "OTA_USE_UTC_TIME", "ota_8h.html#abc5c98fcc1211af2b80116dd6e0a035dada7659f948d04cc881fc14673f23da21", null ],
    [ "OTA_UPGRADE_STATUS_NORMAL", "ota_8h.html#ac36f475ca5b446f4fde4c9b90bec77c8a45f9395290b3d32c7140b50f91c8fb15", null ],
    [ "OTA_UPGRADE_STATUS_DOWNLOAD_IN_PROGRESS", "ota_8h.html#ac36f475ca5b446f4fde4c9b90bec77c8ad8aac490460448d3ba7b3e98b7f27fee", null ],
    [ "OTA_UPGRADE_STATUS_DOWNLOAD_COMPLETE", "ota_8h.html#ac36f475ca5b446f4fde4c9b90bec77c8aa46a5dddeaba278a758a2af0f54a317c", null ],
    [ "OTA_UPGRADE_STATUS_WAIT", "ota_8h.html#ac36f475ca5b446f4fde4c9b90bec77c8a367e3493f0490576ad41e8fdcea20666", null ],
    [ "OTA_UPGRADE_STATUS_COUNTDOWN", "ota_8h.html#ac36f475ca5b446f4fde4c9b90bec77c8aef1d1564d23e12cbed8397f805620311", null ],
    [ "emAfOtaCreateEmberAfOtaImageIdStruct", "ota_8h.html#a3a861967de2fe5f87465b3428199965c", null ],
    [ "emAfOtaParseImageIdFromMessage", "ota_8h.html#ad258f86b04d4338ff5fdebe27d9c3b61", null ],
    [ "upgradeServerNodeId", "ota_8h.html#a245540a04cce8d49902a1f5c8235c853", null ],
    [ "emAfOtaMinMessageLengths", "ota_8h.html#a29cb7d8f57443a0d3077ea5165272761", null ]
];