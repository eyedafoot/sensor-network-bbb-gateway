var ota_client_policy_8h =
[
    [ "EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_IMAGE_TYPE_ID", "ota-client-policy_8h.html#a7eadce52c35c0091cd53978398242880", null ],
    [ "EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_FIRMWARE_VERSION", "ota-client-policy_8h.html#aab275bfa9bbcce8365a2346ca3938ec3", null ],
    [ "SECONDS_IN_MS", "ota-client-policy_8h.html#a0fe5e144abdef3ceb4d2da347ccf8595", null ],
    [ "MINUTES_IN_MS", "ota-client-policy_8h.html#a0ca520fadede5c61db5f3ce5292b0f3b", null ],
    [ "HOURS_IN_MS", "ota-client-policy_8h.html#ad1b41f7d06c69ebcefc5714584f829d6", null ],
    [ "EMBER_AF_INVALID_HARDWARE_VERSION", "ota-client-policy_8h.html#a3f37f5a8bb27df7d7dc80870c5fa3cec", null ],
    [ "EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_HARDWARE_VERSION", "ota-client-policy_8h.html#a4a8008f9d1270519e867c10d670e9937", null ],
    [ "EmberAfOtaDownloadResult", "ota-client-policy_8h.html#aa084fe9d5dfd2ff9eec6bc75760cde28", null ],
    [ "EMBER_AF_OTA_DOWNLOAD_AND_VERIFY_SUCCESS", "ota-client-policy_8h.html#a385c44f6fb256e5716a2302a5b940388a7327c57cf71dee3f428e317962b09a29", null ],
    [ "EMBER_AF_OTA_DOWNLOAD_TIME_OUT", "ota-client-policy_8h.html#a385c44f6fb256e5716a2302a5b940388ac1ca95ad3711290687703e2b36161a84", null ],
    [ "EMBER_AF_OTA_VERIFY_FAILED", "ota-client-policy_8h.html#a385c44f6fb256e5716a2302a5b940388a7123ff3294b1ff56df137b11d283c8c5", null ],
    [ "EMBER_AF_OTA_SERVER_ABORTED", "ota-client-policy_8h.html#a385c44f6fb256e5716a2302a5b940388ae9e972a3a1b313685dae55a7dde91525", null ],
    [ "EMBER_AF_OTA_CLIENT_ABORTED", "ota-client-policy_8h.html#a385c44f6fb256e5716a2302a5b940388a7e1f44ccd4a91f9936153e2a80d39a76", null ],
    [ "EMBER_AF_OTA_ERASE_FAILED", "ota-client-policy_8h.html#a385c44f6fb256e5716a2302a5b940388ada6d428cc4cd555161cb76702e46ee86", null ]
];