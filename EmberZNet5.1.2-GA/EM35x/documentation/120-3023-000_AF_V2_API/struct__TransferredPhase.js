var struct__TransferredPhase =
[
    [ "energyPhaseId", "struct__TransferredPhase.html#a2ada89c5393ab1d70d934af247af51d3", null ],
    [ "macroPhaseId", "struct__TransferredPhase.html#aacb8f4fe1fb1fd0cc693a0fd2bcdaa5c", null ],
    [ "expectedDuration", "struct__TransferredPhase.html#a64f062c86d57d29295ea24c006628de2", null ],
    [ "peakPower", "struct__TransferredPhase.html#a5d649291947837750e7b0083c31fab75", null ],
    [ "energy", "struct__TransferredPhase.html#ae552d2d7a0870d18e16eaf60b004666b", null ],
    [ "maxActivationDelay", "struct__TransferredPhase.html#a6d088b905a79cf8ed25f70bb39505453", null ]
];