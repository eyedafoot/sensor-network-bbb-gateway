var struct__DeviceInformationRecord =
[
    [ "ieeeAddress", "struct__DeviceInformationRecord.html#ab70c668c35f4fcc49185c0545eabb616", null ],
    [ "endpointId", "struct__DeviceInformationRecord.html#abb74b4c0b1d034952f141dbe4692c951", null ],
    [ "profileId", "struct__DeviceInformationRecord.html#a9674d8c7f6944e7ed505a572565698bc", null ],
    [ "deviceId", "struct__DeviceInformationRecord.html#a3e7e226c5e2b44cdd77f0ce63775e4f1", null ],
    [ "version", "struct__DeviceInformationRecord.html#a3e6b8444875a0cff29d123717466b8e3", null ],
    [ "groupIdCount", "struct__DeviceInformationRecord.html#a202935170ee6b2e944679e05124d890e", null ],
    [ "sort", "struct__DeviceInformationRecord.html#a818104142eab0b9a2984f6f478ef52c7", null ]
];