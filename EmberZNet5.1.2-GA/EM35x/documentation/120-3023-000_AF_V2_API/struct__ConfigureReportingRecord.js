var struct__ConfigureReportingRecord =
[
    [ "direction", "struct__ConfigureReportingRecord.html#a83c0b2799b804c56e3c5d24664b20207", null ],
    [ "attributeId", "struct__ConfigureReportingRecord.html#a88f4b630edf2ae4c720bd4e49ab4d579", null ],
    [ "attributeType", "struct__ConfigureReportingRecord.html#a3c842eb15d98924da5f3ff790a1fd5e3", null ],
    [ "minimumReportingInterval", "struct__ConfigureReportingRecord.html#a3a413738ac258bb196109bcaa2572891", null ],
    [ "maximumReportingInterval", "struct__ConfigureReportingRecord.html#a9592b4f3d2652868cf73fe4d371ba29f", null ],
    [ "reportableChangeLocation", "struct__ConfigureReportingRecord.html#a43ec780b6139f7e2df402f8f5ce969cb", null ],
    [ "timeoutPeriod", "struct__ConfigureReportingRecord.html#ab2c2496cfe0d738d638dbe19224f13aa", null ]
];