var structEmberAfDoorLockScheduleEntry =
[
    [ "userID", "structEmberAfDoorLockScheduleEntry.html#a4a9ce9fe978e5e4b9a8d2bb5953a4c22", null ],
    [ "daysMask", "structEmberAfDoorLockScheduleEntry.html#a9fd21239a9e234212cb7790667eb1e40", null ],
    [ "startHour", "structEmberAfDoorLockScheduleEntry.html#a359736739f487a168f1e37e21ae00c1f", null ],
    [ "startMinute", "structEmberAfDoorLockScheduleEntry.html#a69ab4cb043c7bf58522493ad5c95b178", null ],
    [ "stopHour", "structEmberAfDoorLockScheduleEntry.html#aeb6ca7aa7d6022c610c83fb5d5319f91", null ],
    [ "stopMinute", "structEmberAfDoorLockScheduleEntry.html#a7f8c56f96a01055757e492f18149c59e", null ]
];