var standalone_bootloader_8h =
[
    [ "bootloaderMenu", "standalone-bootloader_8h.htm#gaecc98bfd2158367195157e16ef58d317", null ],
    [ "receiveImage", "standalone-bootloader_8h.htm#ga6e07e9f0beaaea7de3e68c6c6140c28c", null ],
    [ "checkDebugMenuOption", "standalone-bootloader_8h.htm#ga5720e2598cf3bb2c81354f8491ccf0dc", null ],
    [ "initOtaState", "standalone-bootloader_8h.htm#ga006f88b148a0008a3cb3f6b047d1dbcd", null ],
    [ "checkOtaStart", "standalone-bootloader_8h.htm#gae100916a4127140fe57a4e9a52d5aeee", null ],
    [ "receiveOtaImage", "standalone-bootloader_8h.htm#ga30aec9f161c3ef336a1240f55feea233", null ],
    [ "paIsPresent", "standalone-bootloader_8h.htm#ga9d1db9efe911bb71ef933e7dc3732680", null ]
];