var group__app__bootload =
[
    [ "BOOTLOADER_SEGMENT_SIZE_LOG2", "group__app__bootload.htm#ga1b810c26ece58c171edee3c8c26b58af", null ],
    [ "BOOTLOADER_SEGMENT_SIZE", "group__app__bootload.htm#gac8bf3171589663245e965b19ff895c03", null ],
    [ "BL_IMAGE_IS_VALID_CONTINUE", "group__app__bootload.htm#ga452d8a832dda9edbf9476b7318c9e064", null ],
    [ "halAppBootloaderInit", "group__app__bootload.htm#ga82d1bcaeb33862aef14ca14b10b701fd", null ],
    [ "halAppBootloaderInfo", "group__app__bootload.htm#gabe47f69fc81e9c501079c747ab139f73", null ],
    [ "halAppBootloaderShutdown", "group__app__bootload.htm#gac4dd34463e5b7f325c9f9dc78178c7ff", null ],
    [ "halAppBootloaderImageIsValidReset", "group__app__bootload.htm#ga2f52e7c1a49af57122858c83b9e3220a", null ],
    [ "halAppBootloaderImageIsValid", "group__app__bootload.htm#ga2958be5a4880e73b69c004ebd90572ef", null ],
    [ "halAppBootloaderInstallNewImage", "group__app__bootload.htm#ga747336cccba964e37e1621f70b243cb7", null ],
    [ "halAppBootloaderWriteRawStorage", "group__app__bootload.htm#ga58eee31447e1faab0962bc69d6e86dc4", null ],
    [ "halAppBootloaderReadRawStorage", "group__app__bootload.htm#ga78f28b9ef3528ebf8bca2e9ffc2d46f2", null ],
    [ "halAppBootloaderEraseRawStorage", "group__app__bootload.htm#gacddfb4435de7033eaf65029eda66db85", null ],
    [ "halAppBootloaderStorageBusy", "group__app__bootload.htm#ga9ef6b83a636cf253303488ad4e9fd5ad", null ],
    [ "halAppBootloaderReadDownloadSpace", "group__app__bootload.htm#gaf080320677083e6f094b08cd484812ec", null ],
    [ "halAppBootloaderWriteDownloadSpace", "group__app__bootload.htm#ga0670beca2a655eb5e6dddbe7b2856c07", null ],
    [ "halAppBootloaderGetImageData", "group__app__bootload.htm#ga850069557bea8da4e5ae218becb9923f", null ],
    [ "halAppBootloaderGetVersion", "group__app__bootload.htm#ga51fad55e1a1a02c65a9798afbd014926", null ],
    [ "halAppBootloaderGetRecoveryVersion", "group__app__bootload.htm#ga11cea6e09f05c4909ebd3d23f751cd6e", null ]
];