var group__diagnostics =
[
    [ "halResetWasCrash", "group__diagnostics.htm#gabee81bcaa661ace6b1a96d1b03d115fd", null ],
    [ "halGetMainStackBytesUsed", "group__diagnostics.htm#gab6ed1ef9128053e323783b5d6d912257", null ],
    [ "halPrintCrashSummary", "group__diagnostics.htm#ga7e65ea3d7e736f0589e78e91ba276931", null ],
    [ "halPrintCrashDetails", "group__diagnostics.htm#ga2a03069065659203d6b68baedf9dc340", null ],
    [ "halPrintCrashData", "group__diagnostics.htm#ga7cae87dcdeeaf98c84f73625fcbb9360", null ]
];