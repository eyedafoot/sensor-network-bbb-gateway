var ember_debug_8h =
[
    [ "NO_DEBUG", "ember-debug_8h.htm#ga424f1b989129c5519f4df8f61ad6dcaf", null ],
    [ "BASIC_DEBUG", "ember-debug_8h.htm#ga58a368314c1eab28f1a632a09b3f4324", null ],
    [ "FULL_DEBUG", "ember-debug_8h.htm#ga132c290e35b442326bf7c88815ac6d8c", null ],
    [ "emberDebugInit", "ember-debug_8h.htm#gaa90b4e5cbe1d116e26a033b44dd3835b", null ],
    [ "emberDebugAssert", "ember-debug_8h.htm#gaa0ee18c07ba9b61f0f42d8449204c101", null ],
    [ "emberDebugMemoryDump", "ember-debug_8h.htm#ga4c8016ec061d99eeb313722358a0bab2", null ],
    [ "emberDebugBinaryPrintf", "ember-debug_8h.htm#ga0e571e2c620a521441280bb092ab827e", null ],
    [ "emDebugSendVuartMessage", "ember-debug_8h.htm#ga4acfbd617a8a98faa2d04626356efbf9", null ],
    [ "emberDebugError", "ember-debug_8h.htm#gaad5a7a4b525a978ffc9f2fa93b535e33", null ],
    [ "emberDebugReportOff", "ember-debug_8h.htm#gaa612a85a6c5d1e69c56b196a7d499c7c", null ],
    [ "emberDebugReportRestore", "ember-debug_8h.htm#ga29afb66bec933db58018e7718b7b2a77", null ],
    [ "emberDebugPrintf", "ember-debug_8h.htm#ga8ed5653fc2665481496e39b5961f12af", null ]
];