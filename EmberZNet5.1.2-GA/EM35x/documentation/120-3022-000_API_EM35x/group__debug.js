var group__debug =
[
    [ "NO_DEBUG", "group__debug.htm#ga424f1b989129c5519f4df8f61ad6dcaf", null ],
    [ "BASIC_DEBUG", "group__debug.htm#ga58a368314c1eab28f1a632a09b3f4324", null ],
    [ "FULL_DEBUG", "group__debug.htm#ga132c290e35b442326bf7c88815ac6d8c", null ],
    [ "emberDebugInit", "group__debug.htm#gaa90b4e5cbe1d116e26a033b44dd3835b", null ],
    [ "emberDebugAssert", "group__debug.htm#gaa0ee18c07ba9b61f0f42d8449204c101", null ],
    [ "emberDebugMemoryDump", "group__debug.htm#ga4c8016ec061d99eeb313722358a0bab2", null ],
    [ "emberDebugBinaryPrintf", "group__debug.htm#ga0e571e2c620a521441280bb092ab827e", null ],
    [ "emDebugSendVuartMessage", "group__debug.htm#ga4acfbd617a8a98faa2d04626356efbf9", null ],
    [ "emberDebugError", "group__debug.htm#gaad5a7a4b525a978ffc9f2fa93b535e33", null ],
    [ "emberDebugReportOff", "group__debug.htm#gaa612a85a6c5d1e69c56b196a7d499c7c", null ],
    [ "emberDebugReportRestore", "group__debug.htm#ga29afb66bec933db58018e7718b7b2a77", null ],
    [ "emberDebugPrintf", "group__debug.htm#ga8ed5653fc2665481496e39b5961f12af", null ]
];