var group__mfglib =
[
    [ "mfglibStart", "group__mfglib.htm#ga935a6bfbfe3715843213fe2f6d42bbbe", null ],
    [ "mfglibEnd", "group__mfglib.htm#ga9f8862cbcc8ae5c807f95876ed1a5440", null ],
    [ "mfglibStartTone", "group__mfglib.htm#ga452440d510a7f807add100192d28398f", null ],
    [ "mfglibStopTone", "group__mfglib.htm#ga01614e57f52b7b06bbbee75e50a8be50", null ],
    [ "mfglibStartStream", "group__mfglib.htm#ga1357b9e9294375cbb19ff701c4564ad6", null ],
    [ "mfglibStopStream", "group__mfglib.htm#ga8cb47aa883904af821530f28f0633017", null ],
    [ "mfglibSendPacket", "group__mfglib.htm#gae0892ab1fdbabc1de69c813d33629f53", null ],
    [ "mfglibSetChannel", "group__mfglib.htm#ga16a831dacc5579c376aa867137cba19e", null ],
    [ "mfglibGetChannel", "group__mfglib.htm#ga70f46fb857591a4efc898372091b3b35", null ],
    [ "mfglibSetPower", "group__mfglib.htm#ga1a1251595203b349dafb98225a84d025", null ],
    [ "mfglibGetPower", "group__mfglib.htm#ga5f43c30181b21aa1f919c045e2504935", null ],
    [ "mfglibSetSynOffset", "group__mfglib.htm#gaf735218b1f69871b052414b755aaea69", null ],
    [ "mfglibGetSynOffset", "group__mfglib.htm#gaf288ac0e589f3e8b99b49f73ff536ce0", null ],
    [ "mfglibTestContModCal", "group__mfglib.htm#ga2c92b479db15b043b7e806374f1169f5", null ]
];