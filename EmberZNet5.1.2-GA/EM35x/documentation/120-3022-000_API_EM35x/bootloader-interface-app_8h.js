var bootloader_interface_app_8h =
[
    [ "BOOTLOADER_SEGMENT_SIZE_LOG2", "bootloader-interface-app_8h.htm#ga1b810c26ece58c171edee3c8c26b58af", null ],
    [ "BOOTLOADER_SEGMENT_SIZE", "bootloader-interface-app_8h.htm#gac8bf3171589663245e965b19ff895c03", null ],
    [ "BL_IMAGE_IS_VALID_CONTINUE", "bootloader-interface-app_8h.htm#ga452d8a832dda9edbf9476b7318c9e064", null ],
    [ "halAppBootloaderInit", "bootloader-interface-app_8h.htm#ga82d1bcaeb33862aef14ca14b10b701fd", null ],
    [ "halAppBootloaderInfo", "bootloader-interface-app_8h.htm#gabe47f69fc81e9c501079c747ab139f73", null ],
    [ "halAppBootloaderShutdown", "bootloader-interface-app_8h.htm#gac4dd34463e5b7f325c9f9dc78178c7ff", null ],
    [ "halAppBootloaderImageIsValidReset", "bootloader-interface-app_8h.htm#ga2f52e7c1a49af57122858c83b9e3220a", null ],
    [ "halAppBootloaderImageIsValid", "bootloader-interface-app_8h.htm#ga2958be5a4880e73b69c004ebd90572ef", null ],
    [ "halAppBootloaderInstallNewImage", "bootloader-interface-app_8h.htm#ga747336cccba964e37e1621f70b243cb7", null ],
    [ "halAppBootloaderWriteRawStorage", "bootloader-interface-app_8h.htm#ga58eee31447e1faab0962bc69d6e86dc4", null ],
    [ "halAppBootloaderReadRawStorage", "bootloader-interface-app_8h.htm#ga78f28b9ef3528ebf8bca2e9ffc2d46f2", null ],
    [ "halAppBootloaderEraseRawStorage", "bootloader-interface-app_8h.htm#gacddfb4435de7033eaf65029eda66db85", null ],
    [ "halAppBootloaderStorageBusy", "bootloader-interface-app_8h.htm#ga9ef6b83a636cf253303488ad4e9fd5ad", null ],
    [ "halAppBootloaderReadDownloadSpace", "bootloader-interface-app_8h.htm#gaf080320677083e6f094b08cd484812ec", null ],
    [ "halAppBootloaderWriteDownloadSpace", "bootloader-interface-app_8h.htm#ga0670beca2a655eb5e6dddbe7b2856c07", null ],
    [ "halAppBootloaderGetImageData", "bootloader-interface-app_8h.htm#ga850069557bea8da4e5ae218becb9923f", null ],
    [ "halAppBootloaderGetVersion", "bootloader-interface-app_8h.htm#ga51fad55e1a1a02c65a9798afbd014926", null ],
    [ "halAppBootloaderGetRecoveryVersion", "bootloader-interface-app_8h.htm#ga11cea6e09f05c4909ebd3d23f751cd6e", null ]
];