//

// This callback file is created for your convenience. You may add application code
// to this file. If you regenerate this file over a previous version, the previous
// version will be overwritten and any code you have added will be lost.

#include "app/framework/include/af.h"
#include "app/framework/util/af-main.h"

#include "hal/micro/cortexm3/diagnostic.h"

#include <hal/ece/haldrvI2C.h>
#include <hal/ece/haldrvTempSensor.h>
#include <hal/ece/haldrvAcclSensor.h>
#include <hal/ece/haldrvUtils.h>
#include <hal/ece/haldrvBattGauge.h>

#define APP_ENDPOINT 1

#define PRINT(x) emberSerialGuaranteedPrintf(APP_SERIAL, x)

#define I2C_BUS_ONBOARD     1
#define TEMP_SENSOR_ONBOARD 2
#define ACCL_SENSOR_ONBOARD 3

#define SUCCESS 0

/*
 * Make sure that non-global variables are all declared with the static keyword.  
 * This keeps the size of the symbol table down.
 *//*
 * All variables and functions that are visible outside of the file
 * should have the module name prepended to them. This makes it easy
 * to know where to look for function and variable definitions.
 *
 * Put dividers (a single-line comment consisting only of dashes)
 * between functions. 
 */


static int8u sensorsOnBoard;
static int8u sensorsInitiated;

/* forward declarations of initialization routines */

static void initAccelerometer(void);
static void initTemperatureSensor(void);

static int8u chkAccelerometer(void);
static int8u chkTemperatureSensor(void);


// Define some usefull CLI commands
/* EntryActionFormat unbCli_XXXcmd  they all will go to a file for cli options*/

static void unbCli_HLPcmd(void); // help

/* BOARD Commands*/
static void unbCli_BSDWcmd(void); // shutdown           FIXME!!!
static void unbCli_BASTcmd(void); // assertme           OK!
static void unbCli_BRSTcmd(void); // halReboot()        OK!
static void unbCli_BRSRcmd(void); // resetString        OK!
static void unbCli_BCRScmd(void); // crashinfo          OK!
static void unbCli_BGPRcmd(void)  { logPrintln("FIXME!!!"); } 

/* BUTTON Commands*/
/* LED Commands*/

/* TEMPERATURE SENSOR Commands */
static void printTempSleepMode(void);
static void unbCli_TSTAcmd(void); // status             OK!
static void unbCli_TSBYcmd(void); // StandBy            OK!
static void unbCli_TACTcmd(void); // Activate           OK!
static void unbCli_TGETcmd(void); // get temperature    OK!
static void unbCli_TI2Ccmd(void); // I2C address        OK!
static void unbCli_TRDRcmd(void); // read data register OK!
static void unbCli_TDMPcmd(void); // dump registers     OK!
static void unbCli_TGRScmd(void); // get resolution     OK!
static void unbCli_TSRScmd(void); // set resolution     OK!
static void unbCli_TGTHcmd(void); // get thresholds     OK!
static void unbCli_TSTHcmd(void); // set thresholds     OK!
static void unbCli_TGFTcmd(void); // get Fault tol      OK!
static void unbCli_TSFTcmd(void); // set Fault tol      OK!
static void unbCli_TGCFcmd(void); // showConfig         OK!
static void unbCli_TRSTcmd(void); // reset              OK!
static void unbCli_TGCTcmd(void); // get delays         OK!
static void unbCli_TSDWcmd(void); // shutdown on/off    OK!

/* ACCELEROMETER Commands */
static void printAcclSleepMode(void);
static void unbCli_ASTAcmd(void); // status             OK!
static void unbCli_AI2Ccmd(void); // I2C address        OK!
static void unbCli_ADMPcmd(void); // dump registers     OK!
static void unbCli_AWIMcmd(void); // whoAmI signature   OK!
static void unbCli_AGRVcmd(void); // get register value
static void unbCli_ASRVcmd(void); // set register value

static void unbCli_ASBYcmd(void); // StandBy            OK!
static void unbCli_AACTcmd(void); // Activate           OK!
static void unbCli_ASDWcmd(void); // shutdown on/off    OK!
static void unbCli_ARSTcmd(void); // reset              OK!
static void unbCli_ASTScmd(void); // selftest           OK!

static void unbCli_AGAXcmd(void); // axes               OK!  
static void unbCli_ANAXcmd(void); // Read axes N times  OK!
static void unbCli_ASDRcmd(void); // set sample rate 
static void unbCli_AGDRcmd(void); // get sample rate    OK!  
static void unbCli_AGSCcmd(void); // get dynamic range  OK!
static void unbCli_ASSCcmd(void); // set dynamic range  OK!
static void unbCli_ASFRcmd(void); // Fast Read Mode FIXME!!!

static void unbCli_AORTcmd(void); // Orientation        FIXME!!!
static void unbCli_AEVNcmd(void); // Event detection    FIXME!!!
static void unbCli_AGCFcmd(void); // showConfig         FIXME!!!


/* ADC Commands*/
static void unbCli_DCALcmd(void); // ADC Calibration        FIXME!!!
static void unbCli_DREFcmd(void); // get reference Voltages FIXME!!!
static void unbCli_DUNCcmd(void); // ADC reset calibration  FIXME!!!
static void unbCli_DGDRcmd(void); // ADC get sampling rate  FIXME!!!
static void unbCli_DSDRcmd(void); // ADC set sampling rate  FIXME!!!

/* Battery Gauge Commands*/
static void unbCli_GSTAcmd(void); // BATTERY GAUGE get battery status                                                                   FIXME!!!
static void unbCli_GVERcmd(void); // BATTERY GAUGE get IC Version (Default value: from 0x2717 to 0xFFFF)                                FIXME!!!
static void unbCli_GGPFcmd(void); // BATTERY GAUGE get battery Profile and Type                                                         FIXME!!!
static void unbCli_GSOCcmd(void); // BATTERY GAUGE get Relative State of Charge (RSOC value based on a 0-100 scale)                     FIXME!!!
static void unbCli_GGIEcmd(void); // BATTERY GAUGE get Indicator to Empty (remaining capacity of battery based on a 0-1000 scale)       FIXME!!!

/* PHY Commands*/
/* RADIO Commands*/
/* PLATFORM Commands*/
static void unbCli_INFOcmd(void);
static void unbCli_CRC8cmd(void); // calculate a CRC result FIXME!! to incl;ude CRC16 & CRC32!

static PGM_P TempThresholdsCmdArgs[] = { "Low Threshold", "High Threshold", NULL, };
static PGM_P TempResolutionCmdArgs[] = { "Resolution. Use 9,10,11 or 12 bits", NULL, };
static PGM_P TempFaultTolCmdArgs[] =   { "Fault Queue length. Use 1,2,4 or 6 samples", NULL};
static PGM_P OnOffCmdArgs[] =          { "1 - On; 0 - Off", NULL,};
static PGM_P AcclScaleCmdArgs[] =      { "Scale. Use 2,4, or 8 Gs ", NULL };
static PGM_P AcclsetRegistersCmdArgs[]={ "Register Address", "Register Content", NULL, };
static PGM_P AcclSRateCmdArgs[] =      { 
  "Active Sample rates (0:7), (1.56,6.25,12.5,50,100,200,400,800)", 
  "Sleep Sample rates  (0:3), (1.56,6.25,12.5,50)",NULL };
static PGM_P PLTcrcCmdArgs[] =  {"Data Byte", "Previous CRC", NULL};

/*CommandEntrySubMenu unbCli_XXXsbm*/
EmberCommandEntry unbCli_BRDsbm[] = { /*  */
  emberCommandEntryActionWithDetails("shutdown",  unbCli_BSDWcmd, "", "Put the chip in the Deep Sleep state", NULL),
  emberCommandEntryActionWithDetails("assertme",  unbCli_BASTcmd, "", "Resets the chip using assert()", NULL),
  emberCommandEntryActionWithDetails("reset",     unbCli_BRSTcmd, "", "Resets the chip using halreset()", NULL),
  emberCommandEntryActionWithDetails("rstString", unbCli_BRSRcmd, "", "Prints the ResetString", NULL),
  emberCommandEntryActionWithDetails("crashinfo", unbCli_BCRScmd, "", "Prints crash information", NULL),
//  emberCommandEntryActionWithDetails("gpioRead",  unbCli_BGPRcmd, "", "Print all GPIO state", NULL),
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_BTNsbm[] = { /*  */
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_LEDsbm[] = { /*  */
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_TMPsbm[] = { /* Temperature sensor commands */
  emberCommandEntryActionWithDetails("status",           unbCli_TSTAcmd, "",  "Indicates the device's status.", NULL),
  emberCommandEntryActionWithDetails("activate",         unbCli_TACTcmd, "",  "Activates (wake) Temperature sensor. All blocks are enabled (digital, analog)", NULL),
  emberCommandEntryActionWithDetails("standBy",          unbCli_TSBYcmd, "",  "puts the temperature sensor in shutdown mode. Only digital blocks are enabled.", NULL),
  emberCommandEntryActionWithDetails("reset",            unbCli_TRSTcmd, "",  "resets the device to its default (factory) configuration", NULL),
  emberCommandEntryActionWithDetails("shutdown",         unbCli_TSDWcmd, "u", "puts the temperature sensor in shutdown (sleep) or continuous (awake) mode.\n\r", OnOffCmdArgs),
  
  emberCommandEntryActionWithDetails("getTemperature",   unbCli_TGETcmd, "",  "displays temperature reading depending on current operating mode", NULL),
  emberCommandEntryActionWithDetails("getResolution",    unbCli_TGRScmd, "",  "gets and displays resolution of the temperature sensor.", NULL),
  emberCommandEntryActionWithDetails("setResolution",    unbCli_TSRScmd, "u", "sets resolution of the temperature sensor.", TempResolutionCmdArgs),
  emberCommandEntryActionWithDetails("getOSconvertTimes",unbCli_TGCTcmd, "u", "gets and displays typical and maximum conversion times for One shot measurements based on resolution.\n\r", TempResolutionCmdArgs),
  
  emberCommandEntryActionWithDetails("getThresholds",    unbCli_TGTHcmd, "",  "reads and displays sensor's thermostat thresholds. (Thermostat Interrupt mode is not available as ALERT bit is not connected)", NULL),
  emberCommandEntryActionWithDetails("setThresholds",    unbCli_TSTHcmd, "vv","sets sensor's thermostat thresholds (low high -both as integer numbers).", TempThresholdsCmdArgs),
  emberCommandEntryActionWithDetails("getFaultTol",      unbCli_TGFTcmd, "",  "reads and displays sensor's fault tolerance queue length.", NULL),
  emberCommandEntryActionWithDetails("setFaultTol",      unbCli_TSFTcmd, "v", "sets sensor's fault tolerance queue length(in number of samples).\n\r", TempFaultTolCmdArgs),
  
  emberCommandEntryActionWithDetails("address",          unbCli_TI2Ccmd, "",  "displays device's I2C address.", NULL),
  emberCommandEntryActionWithDetails("readDataRegister", unbCli_TRDRcmd, "",  "reads and displays sensor data register, regardless of its operating mode", NULL),
  emberCommandEntryActionWithDetails("dumpRegisters",    unbCli_TDMPcmd, "",  "dump contents of all registers in device.\n\r", NULL),
  emberCommandEntryActionWithDetails("showConfig",       unbCli_TGCFcmd, "",  "reads and displays sensor configuration register.\n\r", NULL),

  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_ACLsbm[] = { /* Accelerator commands */
  emberCommandEntryActionWithDetails("status",       unbCli_ASTAcmd, "",  "Indicates the device�s status.", NULL),
  emberCommandEntryActionWithDetails("runSelfTest",  unbCli_ASTScmd, "",  "Selftest of the device", NULL),
  emberCommandEntryActionWithDetails("activate",     unbCli_AACTcmd, "",  "Activate (wake) accelerometer. All blocks are enabled (digital, analog).", NULL),
  emberCommandEntryActionWithDetails("standBy",      unbCli_ASBYcmd, "",  "puts the accelerometer in standby mode. Only digital blocks are enabled.", NULL),
  emberCommandEntryActionWithDetails("reset",        unbCli_ARSTcmd, "",  "resets the device to its default (factory) configuration.", NULL),
  emberCommandEntryActionWithDetails("shutdown",     unbCli_ASDWcmd, "u", "puts the accelerometer in standby (sleep) or active (awake) mode.\n\r", OnOffCmdArgs),
  
  emberCommandEntryActionWithDetails("axes",         unbCli_AGAXcmd, "",  "Gets Accelerometer axes.", NULL),
  emberCommandEntryActionWithDetails("getNsamples",  unbCli_ANAXcmd, "u", "Gets Accelerometer axes N times (max 255).", NULL),
  emberCommandEntryActionWithDetails("useFastRead",  unbCli_ASFRcmd, "u", "Enables (1) or disables (0) fast read mode", OnOffCmdArgs),
  emberCommandEntryActionWithDetails("getSRate",     unbCli_AGDRcmd, "",  "gets and displays active/sleep sampling rates of the accelerometer.", NULL),
  emberCommandEntryActionWithDetails("setSRate",     unbCli_ASDRcmd, "uu","sets active/sleep sampling rate (8/4 avilable frequencies) of the accelerometer.", AcclSRateCmdArgs),
  emberCommandEntryActionWithDetails("getScale",     unbCli_AGSCcmd, "",  "gets and displays dynamic range (scale) of the accelerometer.", NULL),
  emberCommandEntryActionWithDetails("setScale",     unbCli_ASSCcmd, "u", "sets dynamic range (�2g, �4g or �8g scales) of the accelerometer.\n\r", AcclScaleCmdArgs),
  
  //emberCommandEntryActionWithDetails("event",        unbCli_AEVNcmd, "b",  "Event detection", NULL),
  emberCommandEntryActionWithDetails("orientation",  unbCli_AORTcmd, "",  "Displays device orientation.\n\r", NULL),
  
  emberCommandEntryActionWithDetails("address",      unbCli_AI2Ccmd, "",  "displays device's I2C address ", NULL),
  emberCommandEntryActionWithDetails("whoAmI",       unbCli_AWIMcmd, "",  "Gets Accelerometer's signature (whoAmI = 0x2A)", NULL),
  emberCommandEntryActionWithDetails("dumpRegisters",unbCli_ADMPcmd, "",  "dump contents of all registers in device", NULL),
  emberCommandEntryActionWithDetails("getRegister",  unbCli_AGRVcmd, "u", "reads and displays internal register content", AcclsetRegistersCmdArgs),
  emberCommandEntryActionWithDetails("setRegister",  unbCli_ASRVcmd, "uu","sets a value of an internal register.\n\r", AcclsetRegistersCmdArgs),
  emberCommandEntryActionWithDetails("showConfig",   unbCli_AGCFcmd, "",  "reads and displays device's configuration.\n\r", NULL),
 
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_ADCsbm[] = { /* ADC commands */
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_BATsbm[] = { /* Battery Gauge commands */
  emberCommandEntryActionWithDetails("status",  unbCli_GSTAcmd, "", "get battery status", NULL),
  emberCommandEntryActionWithDetails("version", unbCli_GVERcmd, "", "get IC Version (Default value: from 0x2717 to 0xFFFF)", NULL),
  emberCommandEntryActionWithDetails("profile", unbCli_GGPFcmd, "", "get battery Profile and Type", NULL),
  emberCommandEntryActionWithDetails("charge",  unbCli_GSOCcmd, "", "get Relative State of Charge (RSOC value based on a 0-100 scale)", NULL),
  emberCommandEntryActionWithDetails("toEmpty", unbCli_GGIEcmd, "", "get Indicator to Empty", NULL),
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_TOKsbm[] = { /* Token Commands */
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_PHYsbm[] = { /* Physical layer commands */
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_NWKsbm[] = { /* Network commands */
  emberCommandEntryTerminator()
};

EmberCommandEntry unbCli_RDMsbm[] = { /* Radio commands */
  emberCommandEntryTerminator()
};


EmberCommandEntry unbCli_PLTsbm[] = { /* Platform commands */
  emberCommandEntryActionWithDetails("info",  unbCli_INFOcmd, "",   "prints information", NULL),
  emberCommandEntryActionWithDetails("crc8",  unbCli_CRC8cmd, "uu", "one step CRC-8 CCITT result.\n\r", PLTcrcCmdArgs),
  emberCommandEntryTerminator()
};

 EmberCommandEntry emberCommandTable[] ={ /* The main command table*/
   emberCommandEntrySubMenu("board",        unbCli_BRDsbm, "Board commands"),
//   emberCommandEntrySubMenu("button",       unbCli_BTNsbm, "Button commands"),
//   emberCommandEntrySubMenu("led",          unbCli_LEDsbm, "Led commands"),
   emberCommandEntrySubMenu("temperature",  unbCli_TMPsbm, "Temperature sensor commands"),
   emberCommandEntrySubMenu("accelerometer",unbCli_ACLsbm, "Accelerometer commands"),
//   emberCommandEntrySubMenu("adc",          unbCli_ADCsbm, "ADC commands"),
   emberCommandEntrySubMenu("battery",      unbCli_BATsbm, "Battery gauge commands"),
//   emberCommandEntrySubMenu("token",        unbCli_TOKsbm, "Token commands"),
//   emberCommandEntrySubMenu("phy",          unbCli_PHYsbm, "PHY layer commands"),
//   emberCommandEntrySubMenu("network",      unbCli_NWKsbm, "Network form/join commands"),
//   emberCommandEntrySubMenu("Radio",        unbCli_RDMsbm, "Radio form/join commands"),
   emberCommandEntrySubMenu("platform",     unbCli_PLTsbm, "Platform commands"),
   emberCommandEntryAction("help",          unbCli_HLPcmd,  "?", "Prints application status"), 
   emberCommandEntryTerminator()
 };
 

/** @brief Main Tick
 *
 * Whenever main application tick is called, this callback will be called at
 * the end of the main tick execution.
 *
 */
static int ticCounter =0;
void emberAfMainTickCallback(void)
{

  ticCounter++;

#if !defined(ZA_CLI_MINIMAL) && !defined(ZA_CLI_FULL)
  if (emberProcessCommandInput(APP_SERIAL) || (ticCounter == 1)) {
    if (ticCounter > 1){
      resetIndent();
      logPrint("%p>", ZA_PROMPT);
      setIndent(2);
    }
  }
#endif

}

/** @brief Main Init
 *
 * This function is called from the application's main function. It gives the
 * application a chance to do any initialization required at system startup.
 * Any code that you would normally put into the top of the application's
 * main() routine should be put into this function.
        Note: No callback
 * in the Application Framework is associated with resource cleanup. If you
 * are implementing your application on a Unix host where resource cleanup is
 * a consideration, we expect that you will use the standard Posix system
 * calls, including the use of atexit() and handlers for signals such as
 * SIGTERM, SIGINT, SIGCHLD, SIGPIPE and so on. If you use the signal()
 * function to register your signal handler, please mind the returned value
 * which may be an Application Framework function. If the return value is
 * non-null, please make sure that you call the returned function from your
 * handler to avoid negating the resource cleanup of the Application Framework
 * itself.
 *
 */
void emberAfMainInitCallback(void)
{
  ticCounter = 0;
#if !defined(ZA_CLI_MINIMAL) && !defined(ZA_CLI_FULL)
  {
    logPrintln(""); setIndent(1);
    logPrintln("UNB Test Suite for Ember Sensor Board");
    logPrintln("Initializing and checking board peripherals...");
    decIndent(1);
    
#ifdef CORTEXM3
    if (halResetWasCrash()) {
      logPrintln("[ INIT ] crashinfo");
      halPrintCrashSummary(APP_SERIAL);
      halPrintCrashDetails(APP_SERIAL);
      halPrintCrashData(APP_SERIAL);
    }
#endif
    
    logPrintln("[ INIT ] Board. [%p]",
               ( (sensorsInitiated & TEMP_SENSOR_ONBOARD)!=0?"PASSED":"FAILED" ));
    logPrintln("[ INIT ] I2C. [%p]",
               ( (sensorsInitiated & I2C_BUS_ONBOARD)!=0?"PASSED":"FAILED" ));
    logPrintln("[ INIT ] Temperature Sensor. [%p]",
               ( (sensorsInitiated & TEMP_SENSOR_ONBOARD)!=0?"PASSED":"FAILED" ));
    if (sensorsInitiated & TEMP_SENSOR_ONBOARD){
      incIndent(10);
      unbCli_TDMPcmd();
      decIndent(10);
    }
    
    logPrintln("[ INIT ] Accelerometer. [%p]",
               ( (sensorsInitiated & ACCL_SENSOR_ONBOARD)!=0?"PASSED":"FAILED" ));
    if (sensorsInitiated & ACCL_SENSOR_ONBOARD){
      incIndent(10);
      unbCli_ADMPcmd();
      decIndent(10);
    }
    
    logPrintln("[ INIT ] ADC [%p]",
               ( (sensorsInitiated & TEMP_SENSOR_ONBOARD)?"PASSED":"FAILED" ));
    
    logPrintln("Type a command at the prompt (type HELP for help on commands)");
    logPrint("%p> ", ZA_PROMPT);
  }
#endif
}


/** @brief Main Start
 *
 * This function is called at the start of main after the HAL has been
 * initialized.  The standard main function arguments of argc and argv are
 * passed in.  However not all platforms have support for main() function
 * arguments.  Those that do not are passed NULL for argv, therefore argv
 * should be checked for NULL before using it.  If the callback determines
 * that the program must exit, it should return TRUE.  The value returned by
 * main() will be the value written to the returnCode pointer.  Otherwise the
 * callback should return FALSE to let normal execution continue.
 *
 * @param returnCode   Ver.: always
 * @param argc   Ver.: always
 * @param argv   Ver.: always
 */
boolean emberAfMainStartCallback(int* returnCode,
                                 int argc,
                                 char** argv)
{
  // NOTE:  argc and argv may not be supported on all platforms, so argv MUST be
  // checked for NULL before referencing it.  On those platforms without argc 
  // and argv "0" and "NULL" are passed respectively.

 /*
  * Peripheral setup and configuration. we cannot inform anything here. Stack 
  * isn't initialized yet, nor serial ports. However, sensors could be 
  * initilized here as hal has already ben initialized. 
  */
  
  sensorsOnBoard = 0;
  sensorsInitiated = 0;
  
  /*
   *
   */
  /* chek I2C InitInit. Init has been done by halInit() */
  if ( (SC1_MODE & SC_MODE_MASK) == SC1_MODE_I2C ){
    sensorsOnBoard |= I2C_BUS_ONBOARD;
    sensorsInitiated |= I2C_BUS_ONBOARD;
  }
  
  /* init and check Temperature Sensor */
  initTemperatureSensor();
  if (chkTemperatureSensor() == SUCCESS){
    sensorsOnBoard |= TEMP_SENSOR_ONBOARD;
    sensorsInitiated |= TEMP_SENSOR_ONBOARD;
  }
  
  /* init and check Accelerometer */
  initAccelerometer();
  if (chkAccelerometer() == SUCCESS){
    sensorsOnBoard |= ACCL_SENSOR_ONBOARD;
    sensorsInitiated |= ACCL_SENSOR_ONBOARD;
  }

  return FALSE;  // exit?
}

/** @brief Start Feedback
 *
 * This function is called by the Identify plugin when identification begins.
 * It informs the Identify Feedback plugin that it should begin providing its
 * implemented feedback functionality (e.g. LED blinking, buzzer sounding,
 * etc.) until the Identify plugin tells it to stop. The identify time is
 * purely a matter of informational convenience; this plugin does not need to
 * know how long it will identify (the Identify plugin will perform the
 * necessary timekeeping.)
 *
 * @param endpoint The identifying endpoint  Ver.: always
 * @param identifyTime The identify time  Ver.: always
 */
void emberAfPluginIdentifyStartFeedbackCallback(int8u endpoint,
                                                int16u identifyTime)
{
}

/** @brief Stop Feedback
 *
 * This function is called by the Identify plugin when identification is
 * finished. It tells the Identify Feedback plugin to stop providing its
 * implemented feedback functionality.
 *
 * @param endpoint The identifying endpoint  Ver.: always
 */
void emberAfPluginIdentifyStopFeedbackCallback(int8u endpoint)
{
}

/** @brief Finished
 *
 * This callback is fired when the network-find plugin is finished with the
 * forming or joining process.  The result of the operation will be returned
 * in the status parameter.
 *
 * @param status   Ver.: always
 */
void emberAfPluginNetworkFindFinishedCallback(EmberStatus status)
{
}

/** @brief Get Radio Power For Channel
 *
 * This callback is called by the framework when it is setting the radio power
 * during the discovery process. The framework will set the radio power
 * depending on what is returned by this callback.
 *
 * @param channel   Ver.: always
 */
int8s emberAfPluginNetworkFindGetRadioPowerForChannelCallback(int8u channel){
  return EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER;
}

/** @brief Join
 *
 * This callback is called by the plugin when a joinable network has been
 * found.  If the application returns TRUE, the plugin will attempt to join
 * the network.  Otherwise, the plugin will ignore the network and continue
 * searching.  Applications can use this callback to implement a network
 * blacklist.
 *
 * @param networkFound   Ver.: always
 * @param lqi   Ver.: always
 * @param rssi   Ver.: always
 */
boolean emberAfPluginNetworkFindJoinCallback(EmberZigbeeNetwork * networkFound,
                                             int8u lqi,
                                             int8s rssi)
{
  return TRUE;
}

/** @brief Poll Completed
 *
 * This function is called by the End Device Support plugin after a poll is
 * completed.
 *
 * @param status Return status of a completed poll operation  Ver.: always
 */
void emberAfPluginEndDeviceSupportPollCompletedCallback(EmberStatus status){
}

/** @brief Button Event
 *
 * This allows another module to get notification when a button is pressed and
 * released but the button joining plugin did not handle it.  This callback is
 * NOT called in ISR context so there are no restrictions on what code can
 * execute.
 *
 * @param buttonNumber The button number that was pressed.  Ver.: always
 * @param buttonPressDurationMs The length of time button was held down before
 * it was released.  Ver.: always
 */
void emberAfPluginButtonJoiningButtonEventCallback(int8u buttonNumber,
                                                   int32u buttonPressDurationMs)
{
}

/******************************************************************************/
//static void printIndent(const int8u indent){
//  int8u i;
//  for(i=0; i<indent; i++){
//    logPrint("  ");
//  }
//}//End of printIndent
///*----------------------------------------------------------------------------*/

//void unbCli_PrintCommandUsage(int8u indent, EmberCommandEntry *entry){
//  
//  if (entry->action==NULL){
//  
//    incIndent(1);
//    logPrint("---- %p ---- ", entry->name );
//    if (entry->description == NULL){
//      logPrintln("");
//    } else {
//      logPrintln("%p", entry->description);
//    }
//    unbCli_PrintCommandUsage(++indent, (EmberCommandEntry*)entry->argumentTypes);
//  
//  } else {
//  
//    PGM_P arg = entry->argumentTypes;
//    printIndent(indent);
//    logPrint("%p ", entry->name );
//    
//    while (*arg) {
//      int8u c = *arg;
//      logPrint(( c == 'u' ? " <int8u>"
//               : c == 'v' ? " <int16u>"
//               : c == 'w' ? " <int32u>"
//               : c == 's' ? " <int8s>"
//               : c == 'b' ? " <string>"
//               : c == 'n' ? " ..."
//               : c == '*' ? " *"
//               : " ?"));
//      arg += 1;
//    }
//
//    if (entry->description == NULL){
//      logPrintln("");
//    } else {
//      logPrintln("%p", entry->description);
//    }
//  
//  }
//}//End of unbCli_PrintCommandUsage
///*----------------------------------------------------------------------------*/
//
//void unbCli_PrintCommandTable(void){
//  EmberCommandEntry *entry = emberCommandTable;
//  int8u indent = 1;
//  
//  logPrintln("COMMAND\t[PARAMETERS]\t[- DESCRIPTION]");
//  logPrintln("  where: b=buffer, s1=int8s, s2=int16s, u1=int8u, u2=int16u, u4=int32u");
//  logPrintln("");
//  for (; entry->name != NULL; entry++) {
//     unbCli_PrintCommandUsage(indent, entry);
//  }
//}
///*----------------------------------------------------------------------------*/

void unbCli_HLPcmd(void){ 
  logPrintln("");
  logPrintln("------------------------------------------------------------------------------");
  logPrintln("COMMAND\t[PARAMETERS]\t[- DESCRIPTION]");
  logPrintln("  where: b=buffer, s1=int8s, s2=int16s, u1=int8u, u2=int16u, u4=int32u");
  logPrintln("");
  
  emberPrintCommandTable();
  
  logPrintln("    ** NOTE: input values in hex should be preceeded by 0x (e.g 0xA)!");
  logPrintln("    ** Commands are NOT case sensitive");
  logPrintln("");
}
/*----------------------------------------------------------------------------*/

static void printExtendedPanId(const EmberEUI64 eui64){
  logPrint("(%c)%X%X%X%X%X%X%X%X", '<',
           eui64[0], eui64[1], eui64[2], eui64[3],
           eui64[4], eui64[5], eui64[6], eui64[7]);
}//End of printExtendedPanId
/*----------------------------------------------------------------------------*/

void printNetInfo(EmberNetworkParameters * networkParameters){
  logPrint("channel 0x%x, panid 0x%2x, ", networkParameters->radioChannel,
           networkParameters->panId);
  printExtendedPanId(networkParameters->extendedPanId);
  logPrintln("");
}
/*----------------------------------------------------------------------------*/

static PGM_NO_CONST PGM_P nodeTypeStrings[] = {
  "Unknown",
  "Coordinator",
  "Router",
  "End Device",
  "Sleep End Device",
  "Mobile End Device",
};
/*----------------------------------------------------------------------------*/

/**PLATFORM********************************************************************/
void unbCli_INFOcmd(void){
 
  EmberNodeId id = emberAfGetNodeId();
  EmberNodeType type = EMBER_UNKNOWN_DEVICE;
  EmberNetworkParameters parameters;
  emberAfGetNetworkParameters(&type, &parameters);

  logPrintln("Stack Profile: %d", emberAfGetStackProfile());
  logPrintln("Configured Node Type (%d): %p",
                    emAfCurrentNetwork->nodeType,
                    nodeTypeStrings[emAfCurrentNetwork->nodeType]);
  logPrintln("Running Node Type    (%d): %p",
                    type,
                    nodeTypeStrings[type]);
  logPrintln("Tx Power:      %d dBm", parameters.radioTxPower);
  logPrintln("Channel:       %d", parameters.radioChannel);
  logPrintln("Node ID:       0x%2x", id);
  logPrintln("PAN ID:        0x%2X", parameters.panId);
  logPrint(  "Extended PAN:  ");
  printExtendedPanId(parameters.extendedPanId);
  logPrintln("");
  logPrintln("*** END OF INFO ***");
 
}//End of unbCli_INFOcmd
/*----------------------------------------------------------------------------*/

void unbCli_CRC8cmd(void){
  int8u newByte, prevResult, crc;
  
  newByte = emberUnsignedCommandArgument(0);
  prevResult = emberUnsignedCommandArgument(1);
  
  crc =  halCommonCRC8 ( newByte,  prevResult );
  
  logPrintln("CRC = %d (0x%X)", crc, crc);
}//End of unbCli_CRC8cmd
/*----------------------------------------------------------------------------*/

/**BOARD***********************************************************************/
static void unbCli_BRSTcmd(void){  
  logPrintln("Rebooting board.");
  halReboot(); 
}//End of unbCli_RSTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_BCRScmd(void){
  halPrintCrashSummary(APP_SERIAL);
  halPrintCrashDetails(APP_SERIAL);
  halPrintCrashData(APP_SERIAL);
}//end of unbCli_BCRHcmd
/*----------------------------------------------------------------------------*/  

static void unbCli_BRSRcmd(void){
  // print the reason for the reset
  logPrintln("RESET: %p-%p", 
             (PGM_P) halGetResetString(), 
             (PGM_P) halGetExtendedResetString() );
 
}//End of unbCli_BRSRcmd
/*----------------------------------------------------------------------------*/  

static void unbCli_BASTcmd(void){
  assert(FALSE);
}//End of unbCli_BASTcmd
/*----------------------------------------------------------------------------*/  

static void unbCli_BSDWcmd(void){ // board shutdown
  int8u TempMode, AcclWasActive;
  
  /* shutdown peripherals first */
  
  /* shutdown temperature sensor */
  TempMode = TMP100getMode( UNBNODE_TEMP_DEV_NUM );
  TMP100ShutDown( TRUE, UNBNODE_TEMP_DEV_NUM );
  
  /* shutdown accelerometer */
  AcclWasActive = MMA8452Q_isActive();
  MMA8452Q_setActive(FALSE);
  
  /* shutdown processor */
  logPrintln("Entering SLEEPMODE_MAINTAINTIMER via halSleep...");
  
//  halSleep(SLEEPMODE_MAINTAINTIMER);
  
  /* Waking Up */
  logPrintln("\r\n Processor Awake!\r\n");
  logPrintln("Waking up peripherals if needed...");
  
  /* put temperature Sensor back to its mode upon entry to this routine */
  TMP100setMode( TempMode, UNBNODE_TEMP_DEV_NUM );
  printTempSleepMode();
  
  /* put Aaccelerometer back to its mode upon entry to this routine */
  if (AcclWasActive){
    //  Put the device back into Active Mode
    MMA8452Q_setActive(TRUE);
  }
  printAcclSleepMode();
  
}//End of unbCli_BASTcmd

/**ACCELEROMETER***************************************************************/

static void printAcclSleepMode(void){ // print Accl Mode (SB, WK, SL)
  tAcclModes sysMode;
 /* System Mode. Default value: 00.
   * 00: STANDBY mode
   * 01: WAKE mode
   * 10: SLEEP mode */
  
  
  /* Get sysMode register */
  sysMode =  MMA8452Q_getSysMod();
 
  logPrint("SysMod = %X -> ",sysMode);
  switch (sysMode) {
  case sleep:
    logPrint("SLEEP");
    break;
  case wake:
    logPrint("WAKE");
    break;
  case standby:
    logPrint("STANDBY");
    break;
  default:
    logPrint("ERROR reading ");
  }
  logPrintln(" MODE");

}//End of printAcclSysMode
/*----------------------------------------------------------------------------*/

static void unbCli_ASTAcmd(void){ // status 
  printAcclSleepMode();
}//End of unbCli_ASTAcmd
/*----------------------------------------------------------------------------*/  

static void unbCli_AWIMcmd(void){ // get signature
  int8u whoAmI;
  /*
   * Accelerometer. get device signature (WHO_AM_I) and display it
   */  
  whoAmI = MMA8452Q_getWhoAmI();
  logPrintln("MMA8452Q signature: 0x%X", whoAmI );
  
} //End of unbCli_WAIcmd
/*----------------------------------------------------------------------------*/

static void unbCli_AI2Ccmd(void){ // get I2C address
  /*
   * Accelerometer. get device I2C Address and display it
   */
  logPrintln("Accelerometer device (MMA8452Q) I2C address: 0x%X", 
             (int8u) MMA8452Q_I2C_ADDRESS);
  
}//End of unbCli_AI2Ccmd
/*----------------------------------------------------------------------------*/

static void unbCli_AGAXcmd(void){ // get Axes
  int8u scale;
  int16s axes[3];

  /*
   * Accelerometer. Get axes
   */  
  
  scale = MMA8452Q_getScale();
  MMA8452Q_getAxes( axes );

  printAxes(axes, scale);

  if ( !MMA8452Q_isActive() ){    // device in Standby Mode
    incIndent(2);
    logPrintln("* NOTE: Accelerometer is in STANDBY MODE. Values can be outdated"); 
    decIndent(2);
  }

}//End of unbCli_AGAXcmd

/*----------------------------------------------------------------------------*/
static void unbCli_ANAXcmd(void){ // Read axes N times
  int8u  i, N;
  int16s axes[3];

  /*
   * Accelerometer. Get axes N times
   */  
  if ( MMA8452Q_isActive() ){    // device in Standby Mode
    N = emberUnsignedCommandArgument(0);
    unbCli_AGSCcmd();
    
    for(i =0; i < N; i++) {
      while (!MMA8452Q_XYZDataReady()) ;
      
      /* read accl values*/
      MMA8452Q_getAxes( axes );
      
      /* print accl*/
      logPrintln("%d,%d,%d",axes[0],axes[1],axes[2]);
    }
  } else {
    incIndent(2);
    logPrintln("* NOTE: Accelerometer is in STANDBY MODE. Change to ACTIVE MODE"); 
    decIndent(2);
  }
  
 }//End of unbCli_ANAXcmd
/*----------------------------------------------------------------------------*/

static void unbCli_AGRVcmd(void){ // get register value
  int8u rA,rV;
  /*
   * Accelerometer. display internal register content
   */

  rA = emberUnsignedCommandArgument(0);
  
  /* check command argument */
  if ( rA > MMA8452Q_RA_OFF_Z ) {
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
   
    /* get the contents */
    rV = MMA8452Q_getRegister( rA );
    
    /* print address and content */
    logPrintln("[0x%X] = 0x%X",rA, rV);
  }
  
}
/*----------------------------------------------------------------------------*/

static void unbCli_ASRVcmd(void){ // set register value
  int8u rA, rV, oldValue, newValue;
  /*
   * Accelerometer. display internal register content
   */

  /* print  header */
  logPrintln("  ADDRESS   VALUE");

  /* get desired register address from command line */
  rA = emberUnsignedCommandArgument(0);
  rV = emberUnsignedCommandArgument(1);
  
  /* check command argument */
  if ( rA > MMA8452Q_RA_OFF_Z ) {
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
 
    /* get the contents prior modificaction */
    oldValue = MMA8452Q_getRegister( rA );
    
    /* print address and previous content */
    logPrintln("[ADDR] = OLD  -> NEW");
    logPrint  ("[0x%X] = 0x%X -> ",rA, oldValue);
    
    if ( rV != oldValue ){
      MMA8452Q_setRegister( rA, rV );
    }
    
    /* get the contents after modificaction */
    newValue = MMA8452Q_getRegister( rA );
    
    /* print register's new content */
    logPrintln("0x%X", newValue);
    
    if ((newValue == oldValue) & (rV != oldValue) ){
      
      logPrintln("* Note: Register not modified. Check:");
      incIndent(2);
      logPrintln("- Read Only Register");
      if (MMA8452Q_isActive()){
        logPrintln("- register cannot be modified in ACTIVE mode");
      }
      decIndent(2);
    }
    
  }
  
}
/*----------------------------------------------------------------------------*/

static void unbCli_ADMPcmd(void){ // dump registers
  int8u rA,rV;
  /*
   * Accelerometer. dump internal register contents
   */

  /* print  header */
  logPrintln("Dumping Registers [ADDR]::CONTENT");
  
  for( rA=0; rA < MMA8452Q_RA_RESERVED06; rA++){
    
    /* get the contents */
    rV = MMA8452Q_getRegister( rA );
    
     /* print address and content */
   if ( (rA & 0x0F) == 0 ) {
     logPrint("\r\n [ %X ]::",rA);
    } 
   logPrint(" %X", rV);
  }
  logPrintln("\r\n");
}
/*----------------------------------------------------------------------------*/

static void unbCli_ARSTcmd(void){ // reset
   
  logPrint("Resetting device...");
  MMA8452Q_reset();
  logPrintln("Done. (use 'showConfig' command to view configuration values)");
 
}//End of unbCli_TRSTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_ASDWcmd(void){ // shutdown (StandBy/Wake)
  int8u enable;
  /*
   * Accelerometer. Set operating mode (Active/StandBy) of device
   * Shutdown is used to put device in StandBy mode. There are actually 3 states
   * Sleep, wake and StandBy. This function only switches between StandBy and 
   * wake states.
   */
  
  /* get desired mode from command line */
  enable = emberUnsignedCommandArgument(0);
  
  /* check command argument */
  if (enable > 1) {
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
    logPrint("Putting device into %p mode...",(enable?"sleep":"awake"));
    
    /* set device new mode */
    MMA8452Q_setActive( !enable );
    
    logPrintln("Done. (use 'showConfig' command to view configuration values)");
  }  
}
/*----------------------------------------------------------------------------*/

static void unbCli_ASBYcmd(void){ // StandBy
  
  logPrint("Shutting Down...");
  MMA8452Q_setActive( FALSE );
  printAcclSleepMode();
  
}//End of unbCli_ASBYcmd
/*----------------------------------------------------------------------------*/

static void unbCli_AACTcmd(void){ // Activate
  int8u dr; 
  int32u turnOnTimems;
  
  /* get wake-on time depending on active data rate */
  dr = MMA8452Q_getActiveDataRate ();
  turnOnTimems = MMA8452Q_getTurnOnTimeUS(dr) >> 10;
  
  logPrint("Activating (%d ms left)... ", turnOnTimems);
  
  /* activate accelerometer */
  MMA8452Q_setActive( TRUE );
  
  /* wait for the accelerometer to wake up beacuse we are goin to access 
  registers that are modified after activation*/
  waitBusy(turnOnTimems);

  /* print out current mode (should be WAKE) */
  printAcclSleepMode();
  
}//End of unbCli_AACTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_ASDRcmd(void){ // set data rate 
  int8u adr, sdr;
  int8u devActiveOnEntry;
  
  /*
  * Accelerometer. Set Sample Rate in Active and Sleep modes
  */
  
  /* get desired sample rates from command line */
  adr = emberUnsignedCommandArgument(0);
  sdr = emberUnsignedCommandArgument(1);
  if ((adr > 7) || (sdr >3)){
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
    
    /* Read and print old values */
    logPrintln("Previous values:");
    unbCli_AGDRcmd();
    
    /* Read the current device operating mode. */
    devActiveOnEntry = MMA8452Q_isActive();  
    
    /* put device in Stand By Mode */
    if ( devActiveOnEntry ){
      // Put the device into Standby Mode
      MMA8452Q_setActive(FALSE); 
    }
    
    /* set new sample rates */
    /* sort data rates as device specifications */
    adr = (~adr) & 7;
    sdr = (~sdr) & 3;
    MMA8452Q_setActiveDataRate ( adr );
    MMA8452Q_setSleepDataRate  ( sdr );
    
    /* put device back to its mode upon entry to this routine */
    if (devActiveOnEntry){
      //  Put the device back into Active Mode
      MMA8452Q_setActive(TRUE);
    }
    
   /* Read and print new values */
    logPrintln("New values:");
    unbCli_AGDRcmd();
    
  }
 }//End of unbCli_ASDRcmd
/*----------------------------------------------------------------------------*/

static void unbCli_AGDRcmd(void){ // get data rate  
  int8u adr, sdr;
  
  /*
  * Accelerometer. Get current Sample Rate in both, Active and Sleep modes
  */
 
  adr = MMA8452Q_getActiveDataRate();
  sdr = MMA8452Q_getSleepDataRate();
  
  logPrintln("Active/Sleep Sampling Rate: %p/%p Hz.",
             haldrvAcclSensor_getDRstr(adr, FALSE),
             haldrvAcclSensor_getDRstr(sdr, TRUE)
               );
  
}//End of unbCli_AGDRcmd
/*----------------------------------------------------------------------------*/

static void unbCli_AGSCcmd(void){ // get Dynamic Range
  int8u scale,nBits;
  int16u counts;
  int16u ugPerLSB;
  boolean fastReadMode;
  /*
   * Accelerometer. get scale from device
   */
  scale = MMA8452Q_getScale();
  fastReadMode = MMA8452Q_isFastRead();
  nBits = fastReadMode?8:12;
  counts = BIT( nBits - 2 - ( scale >> 2) );
  ugPerLSB = halCommonUDiv32By16(1000000L, counts); //1000000L/counts;
  logPrintln("Scale read from device: �%dG (%d counts/g = %d ug/LSB)", 
             scale, counts, ugPerLSB);

}//End of unbCli_TGSCcmd
/*----------------------------------------------------------------------------*/

static void unbCli_ASSCcmd(void){ // set Dynamic Range
  int8u scale;
  int8u devActiveOnEntry;
  /*
   * Accelerometer. set scale for measurements
   */
  
  /* get desired scale from command line */
  scale = emberUnsignedCommandArgument(0);
  
  /* check command argument */
  if ((scale != 2) & (scale != 4) & (scale != 8)){
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
    
    /* Show scale before changing it. */
    logPrint("Previous: "); 
    unbCli_AGSCcmd(); 
    
    /* Read the current device operating mode. */
    devActiveOnEntry = MMA8452Q_isActive();  
    
    /* put device in Stand By Mode */
    if ( devActiveOnEntry ){
      // Put the device into Standby Mode
      MMA8452Q_setActive(FALSE); 
    }
    
    /* set  device new scale */
    MMA8452Q_setScale ( scale );
    
    /* put device back to its mode upon entry to this routine */
    if (devActiveOnEntry){
      //  Put the device back into Active Mode
      MMA8452Q_setActive(TRUE);
    }
    
    logPrint("Changed: "); /* Show scale after changing it. */
    unbCli_AGSCcmd(); 
    
   }
  
}//End of unbCli_TSRScmd
/*----------------------------------------------------------------------------*/

static void unbCli_ASFRcmd(void){ // Fast Read Mode (On/Off)
  int8u enable;
  boolean devActiveOnEntry;
  
  /*
   * Accelerometer. Sets/Clears Fast Read Mode.
   */
  
  /* get desired mode from command line */
  enable = emberUnsignedCommandArgument(0);
  
  /* check command argument */
  if (enable > 1) {
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
    logPrint("Fast Read Mode:..."); 

    /* Read the current device operating mode. */
    devActiveOnEntry = MMA8452Q_isActive();  
    
    /* put device in Stand By Mode */
    if ( devActiveOnEntry ){
      // Put the device into Standby Mode
      MMA8452Q_setActive( FALSE ); 
    }
    
    /* set device new mode */
    MMA8452Q_setFastRead( enable );

    /* Check configuration change */
    enable = MMA8452Q_isFastRead();
    logPrintln(" %p ...",(enable?"ON":"OFF"));
    
    unbCli_AGSCcmd();
    
    /* put device back to its mode upon entry to this routine */
    if ( !devActiveOnEntry ){
      //  Put the device back into Active Mode
      MMA8452Q_setActive( FALSE );
    }

  }  
}
/*----------------------------------------------------------------------------*/

static void unbCli_ASTScmd(void){ // Self-test
  int8u status;
  int16s differences[3];
  /*
   * Accelerometer. Selftest 
   */
  logPrintln("Self-Test Output Change:");
  incIndent(2);
  logPrintln("[  X,  Y,  Z   ]");
  logPrintln("[ %d, %d, %d  ] <- datasheet values",
             SELFTEST_X_TYP_OUTPUT_CHANGE, 
             SELFTEST_Y_TYP_OUTPUT_CHANGE, 
             SELFTEST_Z_TYP_OUTPUT_CHANGE );
  
  status = exSelfTest( differences );
  logPrint("[ %d, %d, %d  ] <- values read during test",
             differences[0], differences[1], differences[2] );
  
  logPrintln(" (%p%p%p %p the test)", 
             ((status&1)?"X":""), ((status&2)?"Y":""), ((status&4)?"Z":""), 
             ((status&7)?"PASSED":"FAILED"));
  decIndent(2);
}
/*----------------------------------------------------------------------------*/

static void unbCli_AORTcmd(void){ // Orientation
  int8u devActiveOnEntry;
  int8u plStatus;
  boolean newOrient;
  
  /*
  * Accelerometer. get orientation from device
  */
  
  /* Read the current device operating mode. */
  devActiveOnEntry = MMA8452Q_isActive();  
  
  /* put device in Stand By Mode */
  if ( devActiveOnEntry ){
    // Put the device into Standby Mode
    MMA8452Q_setActive(FALSE); 
  }
  
  /* Activate orientation detection */
  MMA8452Q_detectOrientation(TRUE);
  
  /* Activate device and wait for turn-on time */
  MMA8452Q_activate();
  
  
  do{ /* Now check orientation */  
    
    /* newOrient will always be true as we are activating the device after its programming */
    newOrient = MMA8452Q_getOrientation(&plStatus);
    
    logPrint("Orientation: ");
 
    if ( plStatus & plLockout ){ /* if Z-tilt lockout */ 
      logPrint("Flat. ");
      if (plStatus & plBack){
        logPrint("Upside-Down. ");
      }
      plStatus &= ~plLockout; 
    } 
    
    plStatus &= 6;
    switch (plStatus){
    case plPortraitUp: logPrintln("Portrait Up"); break;
    case plPortraitDown: logPrintln("Portrait Down"); break;
    case plLandscapeRight: logPrintln("Landscape Right"); break;
    case plLandscapeLeft: logPrintln("Landscape Left"); break;
    }
    logPrintln("");
  } while(0);
  
  /* put device in Stand By Mode */
  MMA8452Q_setActive(FALSE); 
  
  /* Deactivate orientation */
  MMA8452Q_detectOrientation(FALSE);
  
  /* put device back to its mode upon entry to this routine */
  if (devActiveOnEntry){
    //  Put the device back into Active Mode
    MMA8452Q_setActive(TRUE);
  }
  
}//End of unbCli_AORTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_AEVNcmd(void){ // Event detection
  logPrintln("");
}//End of unbCli_AEVNcmd
/*----------------------------------------------------------------------------*/

static void unbCli_AGCFcmd(void){ // showConfig         FIXME!!!
  MMA8452Q_showConfig();
}//End of unbCli_AGCFcmd
/*----------------------------------------------------------------------------*/


/**TEMPERATURE SENSOR**********************************************************/

static void printTemperature(int16s rawT){
  int16u I,F;
  
    /* print results in HEX */
  logPrint("Temperature = HEX: 0x%2X,", rawT);
 
  /* use one of the differente funtions used for presenting results in Celsius */
  rawT = rawT2100sC(rawT); /* convert to 1/100s og degrees */
  
  /* Temperature will be presented in I.F �C (Integer.Fraction) format */
  I = rawT/100L; 
  F = rawT<0? toBCD( (int16u)(-rawT%100L) ): toBCD( (int16u) rawT%100L );

  logPrintln("  ( %d.%X �C )", I, F);
 
}//End of printTemperature
/*----------------------------------------------------------------------------*/

static void unbCli_TSTAcmd(void){ // status 
  printTempSleepMode();
}//End of unbCli_ASTAcmd
/*----------------------------------------------------------------------------*/  

static void unbCli_TSBYcmd(void){ // StandBy
  
  logPrint("  Shutting Down...");
  TMP100ShutDown( TRUE, UNBNODE_TEMP_DEV_NUM );
  printTempSleepMode();
  
}//End of unbCli_TSBYcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TACTcmd(void){ // Activate
   
  logPrint("  Activating... ");
  TMP100ShutDown( FALSE, UNBNODE_TEMP_DEV_NUM );
  printTempSleepMode();
  
}//End of unbCli_TACTcmd
/*----------------------------------------------------------------------------*/

static void printTempSleepMode(void){ // print temp Mode (SHUTDOWN, WAKE)
  int8u sysMode;
 /* System Mode. Default value: 0.
   * 0: CONTINUOUS (WAKE) mode
   * 1: SHUTDOWN (SLEEP) mode
   */ 
  
  /* Get sysMode register */
  sysMode =  TMP100getMode( UNBNODE_TEMP_DEV_NUM );
 
  logPrint("SysMod = %X -> ",sysMode);
  switch (sysMode) {
  case TMP100_MODE_SHUTDOWN:
    logPrint("SLEEP (SHUTDOWN)");
    break;
  case TMP100_MODE_CONTINUOUS:
    logPrint("WAKE (CONTINUOUS)");
    break;
  }
  logPrintln(" MODE");

}//End of printAcclSysMode
/*----------------------------------------------------------------------------*/

static void unbCli_TGFTcmd(void){ // get Fault tolerance
  int8u tolerance;
  /*
   * Temperature Sensor. get Fault tolerance queue length in number of samples
   */
  
  /* read the value from configuration register */
  tolerance = TMP100getTTolerance( UNBNODE_TEMP_DEV_NUM );
  
  /* print the value */
  logPrintln("Thermostat's fault queue length: %d samples", tolerance);
  
}//End of unbCli_TGFTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TSFTcmd(void){ // set Fault tolerance
  int8u tolerance;
  
  /*
  * Temperature Sensor. Set Fault tolerance queue length in number of samples
  */
  
  /* get desired fault queue length from command line */
  tolerance = emberUnsignedCommandArgument(0);
  
  /* check command argument 
  * Tolerance values can only be 1,2,4,6 (0,1,2,3 if right shift one bit)
  */
  if ( (tolerance >> 1) > 3){
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
   } else {
    /* Show previous value before changing it. */
    logPrint("Previous"); 
    unbCli_TGFTcmd(); 
    
    /* set device Tolerance */
    TMP100setTTolerance ( tolerance, UNBNODE_TEMP_DEV_NUM );
    
    logPrint("  Changed"); /* Show resolution before changing it. */
    unbCli_TGFTcmd(); 
    
  }
  
}//End of unbCli_TSFTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TI2Ccmd(void){ // get I2C address
  int8u devAdr;
  /*
   * Temperature Sensor. get device I2C Address and display it
   */
  
  /* read the value based on device number used in board configuration */
  devAdr = (int8u) TMP100_I2C_ADDR(UNBNODE_TEMP_DEV_NUM);
  
  /* print the value */
  logPrintln("Temperature device I2C address: 0x%X", devAdr);
  
}//End of unbCli_TADRcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TRDRcmd(void){ // read data register
  int16s rawT;
   
  /*
   * Temperature Sensor. read device's data register and display contents both 
   * in HEX and Celsius.
   */
  
  /* directly access data register using non-blocking function */
  rawT = readTemperatureRawNonBlocking( UNBNODE_TEMP_DEV_NUM );
  
  /* Once conversion is finished, present results */
  printTemperature( rawT );

}//End of unbCli_RTDRcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TGRScmd(void){ // get device resolution
  int8u resolution;
  /*
   * Temperature Sensor. get resolution in number of bits from device
   */
  
  /* read the value from configuration register */
  resolution =  TMP100getResolution( UNBNODE_TEMP_DEV_NUM );
  
  /* print the value */
  logPrintln("Resolution: %d bits (8.%df format)", resolution, resolution-8);
  
}//End of unbCli_TGRScmd
/*----------------------------------------------------------------------------*/

static void unbCli_TSRScmd(void){ // set device resolution
  int8u resolution;
  /*
   * Temperature Sensor. Set resolution in number of bits to device
   */
  
  /* get desired resolution from command line */
  resolution = emberUnsignedCommandArgument(0);
  
  /* check command argument */
  if ((resolution < 9) | (resolution > 12)){
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
    
    logPrint("Previous"); 
    
    /* Show resolution before changing it. */
    unbCli_TGRScmd(); 
    
    /* set device resolution */
    TMP100setResolution ( resolution, UNBNODE_TEMP_DEV_NUM );
    
    logPrint("Changed"); 
    
    /* Show resolution after changing it. */
    unbCli_TGRScmd(); 
    
   }
  
}//End of unbCli_TSRScmd
/*----------------------------------------------------------------------------*/

/*
static void unbCli_RESLcmd(void){
  int8u argc = emberCommandArgumentCount();
  int8u resolution =  TMP100getResolution( UNBNODE_TEMP_DEV_NUM );
  logPrintln("  Resolution %d bits (8.%df format)", resolution, resolution-8);
  if (argc > 0){
    if (argc > 1){
      logPrintln("Invalid number of arguments");
    } else {
         resolution = emberUnsignedCommandArgument(0);
         if ((resolution < 9) | (resolution > 12)){
           logPrintln("  %d bits! Invalid resolution", resolution);
         } else {
           TMP100setResolution ( resolution, UNBNODE_TEMP_DEV_NUM );
           resolution =  TMP100getResolution( UNBNODE_TEMP_DEV_NUM );
           logPrintln("  Resolution changed to %d bits (8.%df format)", resolution, resolution-8);
         }
    }
  }
}
*/

static void unbCli_TDMPcmd(void){ // dump internal registers
  int8u devAddr;
  /*
   * Temperature Sensor. dump registers
   */
  
  /* read device address depending on device number used in board configuration */
  devAddr = (int8u) TMP100_I2C_ADDR(UNBNODE_TEMP_DEV_NUM);

  logPrintln("Data Register:            0x%2X", TMP100_RD_DATA( devAddr ));
  logPrintln("Configuration Register:   0x%X", TMP100_RD_CONFIG( devAddr ));
  logPrintln("Thermostat Low Register:  0x%2X", TMP100_RD_TLOW( devAddr ));
  logPrintln("Thermostat High Register: 0x%2X", TMP100_RD_THIGH( devAddr ));
}
/*----------------------------------------------------------------------------*/

static void unbCli_TGTHcmd(void){ // get thermostat thresholds
  int16s TLo, THi;
  
  /*
  * Temperature Sensor. get thermostat thresholds. Display them both in Hex and 
  * in degrees (Celsius)
  */
  
  /* get values from internar registers */
  TLo = TMP100getTLowThresholdRaw( UNBNODE_TEMP_DEV_NUM );
  THi = TMP100getTHighThresholdRaw( UNBNODE_TEMP_DEV_NUM );
  
  /* print the retrieved values */
  logPrintln("Thermostat Low  Threshold: %d.%d �C", 
             HIGH_BYTE(TLo), rawTFrac(TLo, TMP100_MAX_RESOLUTION));
  
  logPrintln("Thermostat High Threshold: %d.%d �C", 
             HIGH_BYTE(THi), rawTFrac(THi, TMP100_MAX_RESOLUTION));
  
}//End of unbCli_TGTHcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TSTHcmd(void){ // set thermostat thresholds
  int16s TLo, THi;
  
  /*
  * Temperature Sensor. get thermostat thresholds. Display them both in Hex and 
  * in degrees (Celsius)
  */
  
  /* Read and print old values */
  logPrintln("Previous values:");
  unbCli_TGTHcmd();
  
  /* get desired thresholds from command line */
  TLo = emberUnsignedCommandArgument(0);
  THi = emberUnsignedCommandArgument(1);
 
  /* write them to device */
  TMP100setTLowThresholdRaw( TLo, UNBNODE_TEMP_DEV_NUM );
  TMP100setTHighThresholdRaw(THi, UNBNODE_TEMP_DEV_NUM );
  

  /* Read and print new values */
  logPrintln("New values:");
  unbCli_TGTHcmd();
  
}//End of unbCli_TSTHcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TGCTcmd(void){ // get one-shot convertion times
  int8u resolution;
  int16u typ, max;
  /*
  * Temperature Sensor. get typical and maximum conversion times for One Shot 
  * measurements 
  */
    
  /* get desired resolution from command line */
  resolution = emberUnsignedCommandArgument(0);
  
  /* check command argument */
  if ((resolution < 9) | (resolution > 12)){
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {

    typ = OS_TYP_CONV_TIME(resolution);
    max = OS_MAX_CONV_TIME(resolution);
    
    logPrintln("OneShot conversion takes: typ = %d msec. max = %d msec, when resolution is %d bits", 
             typ, max, resolution);
  }

}//End of unbCli_TGCTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TRSTcmd(void){ // reset configuration
 
  /*
  * Temperature Sensor. Reset the device 
  */
  
  logPrint("Resetting device...");
  
  TMP100Reset( UNBNODE_TEMP_DEV_NUM );
  
  logPrintln("Done. (use 'showConfig' command to view configuration values)");

  
}//End of unbCli_TRSTcmd
/*----------------------------------------------------------------------------*/

static void unbCli_TSDWcmd(void){ // shutdown
  int8u enable;
  /*
   * Temperature Sensor. Shutdown/WakeUp device. In ShutDown Mode, temperature 
   * measurements are only made triggering one-shot sampling. In WakeUp 
   * (Continuous) mode, device continuously samples temperature.
   */
  
  /* get desired resolution from command line */
  enable = emberUnsignedCommandArgument(0);
  
  /* check command argument */
  if (enable > 1) {
    emberCommandErrorHandler(EMBER_CMD_ERR_ARGUMENT_OUT_OF_RANGE);
  } else {
    /* set device new mode */
    logPrint("Putting device into %p mode...",(enable?"SLEEP":"WAKE"));
    
    TMP100ShutDown( enable, UNBNODE_TEMP_DEV_NUM );
    
    logPrintln("Done. (use 'showConfig' command to view configuration values)");
  }  
}
/*----------------------------------------------------------------------------*/

static void unbCli_TGCFcmd(void){ // showConfig
  /*
   * Temperature Sensor. Show Configuration
   */
  
  TMP100showConfig( UNBNODE_TEMP_DEV_NUM ); 
}
/*----------------------------------------------------------------------------*/

static void unbCli_TGETcmd(void){ // get Temperature reading
  int16s rawT;
  int8u mode;
 
  /*
   * Temperature Sensor. get Temperature reading
   */
  
  /*  let the user know the mode the device is in */
  mode = TMP100getMode(UNBNODE_TEMP_DEV_NUM);
  logPrintln("%p",(mode? "Device will wake up for a moment to sample temperature"
                       : "Device is awake. Measurement taken from data register"));
  
  /* Read temperature depending on the device mode */
  rawT = getTemperatureRaw( UNBNODE_TEMP_DEV_NUM );
  
  /* Once conversion is finished, present results */
  printTemperature( rawT );

}//End of unbCli_RTDRcmd
/*----------------------------------------------------------------------------*/

/** BATTERY GAUGE *************************************************************/

static void unbCli_GSTAcmd(void){ // BATTERY GAUGE get battery status                                                                   FIXME!!!
  int16u 
    cellVoltage,         // cell voltage in millivolts
    cellTemperature,     // cell temperature in Kelvin (0 Celsius = 0xAAC = 273.15K, 25 Celsius = 0xBA6 = 298.2 K)
    relStateOfCharge,    // RSOC value based on a 0-100 scale
    indicatorToEmpty,    // RSOC value based on a 0-1000 scale
    alarmLowRSOC,        // RSOC threshold to generate an alarm signal 
    alarmLowCellVoltage, // Voltage threshold to generate an alarm signal
    thermistorB,         // B-constant of the thermistor to be measured (units:1K)
    parasiticImpedance,  // Parasitic Impedance adjustment in MOhms
    ICVersion,           // ID number of the LC709203F 
    statusBit,           // Temperature obtaining method (0 ->I2C or 1->Thermistor mode)
    cotp, pn;
  
  int16s 
    currentDirection,    // Direction of the current 1->charging, -1 ->Discgharging, 0 ->Auto
    cellTemperatureCelsius;
  
  tICGaugeMode 
    icPwrMode; /* Gauge Mode. TEST, OPERATIONAL or SLEEP mode */
  
  int8u  
    *pn_Buffer, 
    batteryType;
  
  boolean
    devicePresent, cellPresent, isNegative;
  
  /* Check if device is present */
   devicePresent = TRUE;
   cellPresent = TRUE;
   icPwrMode = LC709203F_getPowerMode();
   ICVersion = LC709203F_getICVersion();
   if (( (int8u)icPwrMode == 0xFF) && ICVersion == 0xFFFF){
     // Apparently there is no Battery cell
     cellPresent = FALSE;
   }
 
   if (devicePresent)
     if (cellPresent){
       /* Get device information */
       cellVoltage         = LC709203F_getBattVoltage();
       cellTemperature     = LC709203F_getBattTemperature();
       relStateOfCharge    = LC709203F_getRSOC();
       indicatorToEmpty    = LC709203F_getIndicatorEmpty();
       alarmLowRSOC        = LC709203F_getAlarmLowRSOC();
       alarmLowCellVoltage = LC709203F_getAlarmLowVoltage();
       thermistorB         = LC709203F_getThermistorB();
       parasiticImpedance  = LC709203F_getAPA();
       currentDirection    = LC709203F_getCurrentDirection(); 
       statusBit           = LC709203F_getThermistorMode();
       
       cotp = LC709203F_getProfileSelect(); // change of the parameter
       pn   = LC709203F_getProfileNumber(); // number of the parameter
       pn_Buffer = (int8u*)&pn;
       pn =  NTOHS(pn);
       batteryType = pn_Buffer[cotp];
       
       char divLine[] = 
         "+------------------------------------------------------------------------------+";
       logPrintln("");
       logPrintln("%s",divLine);
       logPrint("| IC VERSION: %2X  | OPERATING MODE: ", ICVersion);
       
       switch (icPwrMode) {
       case eTest:
         logPrint("TEST");
         break;
       case eOperational:
         logPrint("OPERATIONAL");
         break;
       case eSleep:
         logPrint("SLEEP");
         break;
       default:
         logPrint("ERROR reading ");
       }
       logPrintln(" |");
       
       logPrint("|  CELL TYPE: Li-ion/polymer (Type %d)| CHARGING MODE: ", batteryType);
       switch (currentDirection){
       case 1:
         logPrintln("CHARGING     |");    
         break;
       case -1:
         logPrintln("DISCHARGING  |");    
         break;
       case 0:
         logPrintln("AUTO         |");
         break;
       default:
         logPrintln("ERROR reading|");
       }
       logPrintln("%s",divLine);
       logPrintln("|      CELL VOLTAGE       |  State of Charge  |  Temperature  |    Parasitic   |");
       logPrintln("| NOMINAL | CHARGE | READ | 0-100%% | 0-100.0%% | using | Value | Impedance(MOhm)|");
       
       switch (batteryType){
       case 1:
         logPrint("|  3.7v   |  4.2v  |");
         break;
       case 3:
         logPrint("|  3.8v   |  4.35v |");
         break;
       case 4:
         logPrint("|  3.8v   |  4.35v |");
         logPrint("UR18650ZY-PANASONIC|");
         break;
       case 5:
         logPrint("|ICR18650 - SAMSUNG|");
         break;
       default:
         logPrint("| ERROR reading    | ");
       }
       if (cellVoltage < 1000) logPrint(" ");
       if (cellVoltage < 100) logPrint(" ");
       if (cellVoltage < 10) logPrint(" ");
       print_uXtoX (cellVoltage, 1000);     logPrint("v|");
       
       if (relStateOfCharge < 100) logPrint(" ");
       if (relStateOfCharge < 10) logPrint(" ");
       logPrint("   %d%% | ", relStateOfCharge);
       
       if (indicatorToEmpty < 1000) logPrint(" ");
       if (indicatorToEmpty < 100) logPrint(" ");
       if (indicatorToEmpty < 10) logPrint(" ");
       print_uXtoX (indicatorToEmpty, 10);     logPrint("%%  |");
       
       logPrint("  I2C  |");
       
       cellTemperatureCelsius = KELVIN_TO_CELSIUS(cellTemperature);
       
       isNegative = FALSE;
       if (cellTemperatureCelsius < 0){
         cellTemperatureCelsius = -cellTemperatureCelsius;
         isNegative = TRUE;
       }
       if (cellTemperatureCelsius < 1000) logPrint(" ");
       if (cellTemperatureCelsius < 100) logPrint(" ");
       if (cellTemperatureCelsius < 10) logPrint(" ");
       if (isNegative) logPrint("-");else logPrint(" ");
       
       print_uXtoX (cellTemperatureCelsius, 10);
       logPrint(" |");
       
       logPrintln(" %d     |", parasiticImpedance);
       logPrintln("%s",divLine);
       
     } else {
       logPrintln("LC709203F: Battery cell not present or not connected on board");
     }
   else logPrintln("LC709203F: Battery Gauge LC709203F not present or not connected on board");

}//End of unbCli_GSTAcmd
/*----------------------------------------------------------------------------*/

static void unbCli_GVERcmd(void){ // BATTERY GAUGE get IC Version (Default value: from 0x2717 to 0xFFFF)                                FIXME!!!
  LC709203F_initBasic(200, 1, 0, 0, 0);//TEMPORARILY TO CHECK IF THE DEVICE IS WORKING
  int16u ver = LC709203F_getICVersion();
  logPrintln("IC Battery Gauge IC version = HEX: 0x%2X,", ver);
  logPrintln("Done. BATTERY GAUGE get IC Version (Default value: from 0x2717 to 0xFFFF).");
}//End of unbCli_GVERcmd
/*----------------------------------------------------------------------------*/
static void unbCli_GGPFcmd(void){ // BATTERY GAUGE get battery Profile and Type                                                         FIXME!!!
  logPrintln("Done. BATTERY GAUGE get battery Profile and Type.");
}//End of unbCli_GGPFcmd
/*----------------------------------------------------------------------------*/
static void unbCli_GSOCcmd(void){ // BATTERY GAUGE get Relative State of Charge (RSOC value based on a 0-100 scale)                     FIXME!!!
  logPrintln("Done. BATTERY GAUGE get Relative State of Charge (RSOC value based on a 0-100 scale).");
}//End of unbCli_GSOCcmd
/*----------------------------------------------------------------------------*/
static void unbCli_GGIEcmd(void){ // BATTERY GAUGE get Indicator to Empty (remaining capacity of battery based on a 0-1000 scale)       FIXME!!!
  logPrintln("Done. BATTERY GAUGE get Indicator to Empty (remaining capacity of battery based on a 0-1000 scale)");
}//End of unbCli_GGIEcmd
/*----------------------------------------------------------------------------*/

/**INITIALIZATION**************************************************************/

static void initAccelerometer(void){
  MMA8452Q_reset();
  logPrintln("Accelerometer MMA8452Q %p found",(MMA8452Q_isPresent()?"":"NOT"));
}//End of initAccelerometer
/*----------------------------------------------------------------------------*/

static void initTemperatureSensor(void){
  /* Device should be in sleep mode after board initialization 
   * let's reset the device and leave it in wake (continuous) mode 
   */
 TMP100Reset( UNBNODE_TEMP_DEV_NUM );
}//End of initTemperatureSensor
/*----------------------------------------------------------------------------*/

static int8u chkTemperatureSensor(void){
  int8u error;
  int8u resetMode, sysMode;
  
  error = 0;
  
  resetMode =  TMP100getMode( UNBNODE_TEMP_DEV_NUM );

  TMP100ShutDown( TRUE, UNBNODE_TEMP_DEV_NUM );
  sysMode =  TMP100getMode( UNBNODE_TEMP_DEV_NUM );
  
  if (sysMode != TMP100_MODE_SHUTDOWN) error++;
  
  TMP100ShutDown( FALSE, UNBNODE_TEMP_DEV_NUM );
  if (sysMode != TMP100_MODE_CONTINUOUS) error++;

  if (error == 0){
    TMP100ShutDown( (resetMode == TMP100_MODE_SHUTDOWN), UNBNODE_TEMP_DEV_NUM );
  }


   
  return error;
}//End of chkAccelerometer
/*----------------------------------------------------------------------------*/

static int8u chkAccelerometer(void){
  int8u error;
  
  error = !MMA8452Q_isPresent();
   
  return error;
}//End of chkAccelerometer
/*----------------------------------------------------------------------------*/


