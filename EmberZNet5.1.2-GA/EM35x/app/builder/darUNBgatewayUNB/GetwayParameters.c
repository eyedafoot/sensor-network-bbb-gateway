
#include "app/framework/include/af.h"



#define EPType_count 8  // number of elements that can be mirroring, 
                        // numbers of elements of listDeviceFId 
												
	
	
#define Node_count 6	// Number of node on the network					


// *******************************************************************
// Node EUI64 address list available in the network, eui is backwards on memory, MSB first 

EmberEUI64 ListEuiNode[Node_count] = { 		
  {0xF1, 0x03, 0x60, 0x01, 0x00, 0x6F, 0x0D, 0x00},  	// KIT 
  {0x3C, 0xE4, 0xD0, 0x00, 0x00, 0x6F, 0x0D, 0x00},  	// KIT
  {0xB2, 0x5D, 0xD5, 0x00, 0x00, 0x6F, 0x0D, 0x00},	// KIT
  {0x77, 0x80, 0xDB, 0x00, 0x00, 0x6F, 0x0D, 0x00},	// UNB												
  {0x2A, 0x9B, 0x0B, 0x02, 0x00, 0x6F, 0x0D, 0x00},  	// UNB
  {0x2F, 0x9B, 0x0B, 0x02, 0x00, 0x6F, 0x0D, 0x00}   	// UNB												
};




// Devices FullID List available to be bind on the network 
// Full Id means 32 bits combination of Profile ID (2-MSB) and Device ID (2-LSB)
// Their index means their Endpoint number to be assigned  
// The first element will never be used because corresponds to EP=0 is an especial EP (ZDO) at any device.
// for that reason the first element will always be initialized with zero but this value doesn't means a FullID.
 
int32u 	listDeviceFId [EPType_count] = {0x00000000, 0x01040000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000};
	
