
// This application will work with its sensor counterpart in order to retrieve the values of several variables on the network.
// It will collect the values of the variables from the nodes listed in the table "ListEuiNode"
// It will collect from the Endpoint types listed on the table "EPType_count"
// For each type of Endpoint listed on  "EPType_count" there should be an Endpoint created here at the gateway
// The Endpoint numbers here at the gateway most be the index at the table "EPType_count"
// When the messages arrives they will be displayed specifying the node that sends it.
// The node number used to display the messages are the index of the table "ListEuiNode"

#include "app/framework/include/af.h"
#include <hal/ece/haldrvUtils.h>
/*#include "hal/ece/driverUtils.h"
*/
// *******************************************************************
// Application specific constants and globals

#define LED BOARDLED1
#define PJOIN_DURATION_S 60
#define EmberNodeId_Size 2
#define Application_ProfileId_Size 2
#define Application_DeviceId_Size 2

#define APP_SERIAL   0  // serial port

#define EPType_count 8 	// number of elements that can be mirroring, 
			// numbers of elements of listDeviceFId 
												
	
	
#define Node_count 31	// Number of node on the network	(JC: Why?)


// Event control struct declarations
EmberEventControl buttonEventControl;

// *******************************************************************
// Forward declarations
void 	ZDOBroadcastMessage	(EmberEUI64 EuiNode);
void 	EndpointRequest		(int8u* NetwidBNode, EmberNodeId NetwidNode);
void 	SimpleDescriptorRequest	(int8u* NetwidBNode, EmberNodeId NetwidNode, int8u EP);
void 	printEUI64		(int8u port, EmberEUI64* eui);
void 	ChecklistDeviceFId 	(int32u DeviceFid, int8u* Fidcount, int8u* FidIndex); 
int8u 	CheckListEuiNode	(EmberEUI64* eui);
void 	CheckNetworkByEuiTable	(void); 
int16s  MergeBytes 		(int8u LSB, int8u MSB);


//EmberStatus emberSendDeviceAnnouncement ( void )

// *******************************************************************
// Node EUI64 address list available in the network, eui is backwards on memory, MSB first 

EmberEUI64 ListEuiNode[Node_count] = { 		
  {0xF1, 0x03, 0x60, 0x01, 0x00, 0x6F, 0x0D, 0x00},  	// KIT 
  {0x3C, 0xE4, 0xD0, 0x00, 0x00, 0x6F, 0x0D, 0x00},  	// KIT
  {0xB2, 0x5D, 0xD5, 0x00, 0x00, 0x6F, 0x0D, 0x00},	// KIT
  {0x77, 0x80, 0xDB, 0x00, 0x00, 0x6F, 0x0D, 0x00},	// UNB BOARD V1											
  {0x2A, 0x9B, 0x0B, 0x02, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V1
  {0x2F, 0x9B, 0x0B, 0x02, 0x00, 0x6F, 0x0D, 0x00},   	// UNB BOARD V1												
  {0x13, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #01
  {0x15, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #02
  {0x16, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #03
  {0x18, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #04
  {0x19, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #05
  {0x1A, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #06
  {0x1B, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #07
  {0x1C, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #08
  {0x1D, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #09
  {0x1E, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #10
  {0x1F, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #11
  {0x20, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #12
  {0x21, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #13
  {0x22, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #14
  {0x23, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #15
  {0x24, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #16
  {0x25, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #17
  {0x26, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #18
  {0x27, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #19
  {0x28, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #20
  {0x29, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #21
  {0x2A, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #22
  {0x2B, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #23
  {0x2C, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00},  	// UNB BOARD V2 #24
  {0x2F, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00}  	// UNB BOARD V2 #25
};


// Devices FullID List available to be bind on the network 
// Full Id means 32 bits combination of Profile ID (2-MSB) and Device ID (2-LSB)
// Their index means their Endpoint number to be assigned  
// The first element will never be used because corresponds to EP=0 is an especial EP (ZDO) at any device.
// for that reason the first element will always be initialized with zero but this value doesn't means a FullID.
 
int32u 	listDeviceFId [EPType_count] = 
{0x00000000, 0x01040000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000};
	


// Global variables 
// these variables are design to mark the stage of the messages exchanged
// and they are used to share their values among the messages when they arrive

// Node data
EmberEUI64 eui;      	// long network address
EmberNodeId netwid;     // short network address in 16 bits, this is also used as flag for the process of Binding for one Node
int8u netwidB[2];	// short network address as an array of two bytes
int8u REPcount;		// # of endpoints at the remote Node  
int8u REPlist[240];     // The endpoint list at one Node 
	

// accelerometer variables
int16s x,y,z ;

// *******************************************************************
// Begin main application loop


void buttonEventHandler(void){
  // On a press-and-hold button event, form a network if we don't have one or
  // permit joining if we have a network. 
  // If we form, we'll automatically permit joining when it comes up.
  
  emberEventControlSetInactive(buttonEventControl);
  
  if (halButtonState(BUTTON0) == BUTTON_PRESSED) {
    if (emberNetworkState() == EMBER_NO_NETWORK) {
      emberFormAndJoinIsScanning();
      emberAfFindUnusedPanIdAndForm();
    } 
    else{
      
      emberPermitJoining(PJOIN_DURATION_S);
      netwid = 0X0000;
      
    }	
    
    
  } 	
  else 
    if (halButtonState(BUTTON1) == BUTTON_PRESSED) {
    
      halInternalSysReset (0x0601 );  // Extended Reset info: 0x0601 (RBT) 
    
    }
  
}

/** @brief Stack Status
 *
 * This function is called by the application framework from the stack status
 * handler.  This callbacks provides applications an opportunity to be
 * notified of changes to the stack status and take appropriate action.  The
 * application should return TRUE if the status has been handled and should
 * not be handled by the application framework.
 *
 * @param status   Ver.: always
 */
boolean emberAfStackStatusCallback(EmberStatus status){
  return FALSE;
}

/** @brief emberAfHalButtonIsrCallback
 *
 *
 */
// Hal Button ISR Callback
// This callback is called by the framework whenever a button is pressed on the
// device. This callback is called within ISR context.
void emberAfHalButtonIsrCallback(int8u button, int8u state){
  if ((button == BUTTON0 || button == BUTTON1) && state == BUTTON_PRESSED) {
    emberEventControlSetActive(buttonEventControl);
  }
}

/** @brief Main Init
 *
 * This function is called from the application�s main function. It gives the
 * application a chance to do any initialization required at system startup.
 * Any code that you would normally put into the top of the application�s
 * main() routine should be put into this function.
        Note: No callback
 * in the Application Framework is associated with resource cleanup. If you
 * are implementing your application on a Unix host where resource cleanup is
 * a consideration, we expect that you will use the standard Posix system
 * calls, including the use of atexit() and handlers for signals such as
 * SIGTERM, SIGINT, SIGCHLD, SIGPIPE and so on. If you use the signal()
 * function to register your signal handler, please mind the returned value
 * which may be an Application Framework function. If the return value is
 * non-null, please make sure that you call the returned function from your
 * handler to avoid negating the resource cleanup of the Application Framework
 * itself.
 *
 */
void emberAfMainInitCallback(void) {
 
  if (emberNetworkState() == EMBER_NO_NETWORK) {
    emberFormAndJoinIsScanning();
    emberAfFindUnusedPanIdAndForm();
  } 
  else{
    
    emberPermitJoining(PJOIN_DURATION_S);
    netwid = 0X0000;
    
  }	
  
}


/** @brief Broadcast Sent
 *
 * This function is called when a new MTORR broadcast has been successfully
 * sent by the concentrator plugin.
 *
 */
void emberAfPluginConcentratorBroadcastSentCallback(void){

// will check the Eui table to open the network if any node is not part of the network
netwid = 0X0000;
CheckNetworkByEuiTable();

}

//

/** @brief Pre ZDO Message Received
 *
 * This function passes the application an incoming ZDO message and gives the
 * appictation the opportunity to handle it. By default, this callback returns
 * FALSE indicating that the incoming ZDO message has not been handled and
 * should be handled by the Application Framework.
 *
 * @param emberNodeId   Ver.: always
 * @param apsFrame   Ver.: always
 * @param message   Ver.: always
 * @param length   Ver.: always
 */
boolean emberAfPreZDOMessageReceivedCallback(EmberNodeId emberNodeId, 
                                             EmberApsFrame* apsFrame, 
                                             int8u* message, 
                                             int16u length)
{
  // Declarations
  
  
  // Endpoint Data
  int16u AppProfId;	// The Aplication Profile ID of the Endpoint
  int16u AppDevId;	// The Device ID of the Endpoint 
  int32u DeviceFid;	// the combination of Aplication Profile ID and Device ID in 32bits
			// DeviceFid (MS 16bits) AppDevId (LS 16 bits)
  
   
  // Information about the List of ID here on the getway
  
  int8u FIdcount;		// counting how many mirrors exist at the getway for an especific Endpoint
  int8u FIdIndex;		// the position of the last one on the Endpoint list here at the getway
  
  EmberStatus  status;
  
  // Going for the network ID
  // this message must be NETWORK_ADDRESS_RESPONSE and have 12 bytes minimum
  
  if (((apsFrame->clusterId) == END_DEVICE_ANNOUNCE)&&(netwid==0)) {
    
    // collecting the message and display it
    emberSerialPrintf(APP_SERIAL, 
                      "RX END_DEVICE_ANNOUNCE from profile 0x%2x culster 0x%2x of length 0x%x\r\n",
                      apsFrame->profileId,
                      apsFrame->clusterId,
                      length);
    MEMCOPY(&eui, (message+3), EUI64_SIZE);// eui is backwards on memory, MSB first 
        
    //If this Node is part of the table then:
        
    if ((CheckListEuiNode (&eui))!=0xFF) {
      
      MEMCOPY(&netwid, (message+1), EmberNodeId_Size);
      MEMCOPY(&netwidB, (message+1), EmberNodeId_Size);
      printEUI64(APP_SERIAL, &eui);
      emberSerialPrintf(APP_SERIAL,"\r\n");
      emberSerialPrintf(APP_SERIAL, 
                        "network ID1 0x%2x \r\n",netwid);
      
      //  find the list of endpoints supported in the announcing Node.  
      
      
      // status = emberActiveEndpointsRequest (netwid,EMBER_APS_OPTION_ENABLE_ADDRESS_DISCOVERY );	
      
      EndpointRequest(netwidB,netwid) ;   
      //emberSerialPrintf(APP_SERIAL,
      //"TX [Send ZDO EndpointRequest], status 0x%x length 0x%x \r\n", 
      //status );
      
    }
        
    return FALSE; //the stack handle the message 
    
  }		
  // Going for the Endpoint list inside. 
  // make sure this is a valid packet ACTIVE_ENDPOINTS_RESPONSE
  
  else if ((apsFrame->clusterId == ACTIVE_ENDPOINTS_RESPONSE) && (emberNodeId==netwid)){
    
    // collecting the message and display it
    
    emberSerialPrintf(APP_SERIAL, 
                      "RX ACTIVE_ENDPOINTS_RESPONSE from profile 0x%2x culster 0x%2x of length 0x%x\r\n",
                      apsFrame->profileId,
                      apsFrame->clusterId,
                      length);
    
    MEMCOPY(&REPcount, (message+4), 1);
    
    
    emberSerialPrintf(APP_SERIAL, 
                      "List of 0x%2x Endpoints ID of the node 0x%2x \r\n",REPcount, netwid);
    
    // populating the Endpoint list and display it 
    
    for (int i=0; i<REPcount; i++ ) {
      
      MEMCOPY(&REPlist[i], (message+5+i), 1); 
      
      emberSerialPrintf(APP_SERIAL, 
                        "%u Remote EndPointID  0x%x \r\n", 
                        i+1,
                        REPlist[i]);
    }					
    
    // start asking for the last remote EP	
             SimpleDescriptorRequest( netwidB,netwid, REPlist[REPcount-1]);
           }	
  
  // Going for the Application ProfileId and Application_DeviceId of each endpoint
  // make sure this is a valid packet SIMPLE_DESCRIPTOR_RESPONSE			
  
  else if ((apsFrame->clusterId == SIMPLE_DESCRIPTOR_RESPONSE) && (emberNodeId==netwid)){
    // collecting the message and display it
    
    emberSerialPrintf(APP_SERIAL, 
                      "RX SIMPLE_DESCRIPTOR_RESPONSE from profile 0x%2x culster 0x%2x of length 0x%x \r\n",
                      apsFrame->profileId,
                      apsFrame->clusterId,
                      length);
    
    REPcount--; // to update the index at REPlist table  of the EP we are dealing with
    
    MEMCOPY(&AppProfId, (message+6), Application_ProfileId_Size);
    MEMCOPY(&AppDevId, (message+8), Application_DeviceId_Size);
    emberSerialPrintf(APP_SERIAL, 
                      "Application ProfileId 0x%2x Application DeviceId 0x%2x \r\n",
                      AppProfId,
                      AppDevId);
    
    // to combine AppProfId and AppDevId in 32 bits
    DeviceFid = 0x00000000;
    DeviceFid = AppProfId << 16;
    DeviceFid = DeviceFid | AppDevId;
    
    emberSerialPrintf(APP_SERIAL, 
                      "EndPoint 0x%x has a Device Full ID 0x%4x  \r\n",
                      REPlist[REPcount], DeviceFid);
    
    
    
    // this will determine the index of this device ID and how many are in the table "listDeviceFId"
    // the index is the Endpoint number of this DeviceID, here at the gateway 
    // Tipically there is only one DeviceID but the function will give us how many just in case of future expansions
    ChecklistDeviceFId (DeviceFid, &FIdcount, &FIdIndex);
    
    emberSerialPrintf(APP_SERIAL, 
                      "There are 0x%2x Devices and the Endpoint number of the last one 0x%2x \r\n",
                      FIdcount,
                      FIdIndex);
    
    if (FIdcount==0 ){
      
      emberSerialPrintf(APP_SERIAL, 
                        "The Remote Endpoint 0x%2x is not on the list for reporting to the Gateway \r\n",
                        REPcount);
      
      if (REPcount!=0){ // Ask for another EP on the Remote Node 
        SimpleDescriptorRequest( netwidB,netwid, REPlist[REPcount-1]);
        
      }
      
    }else { //Create a Binding record in the table of the remote node
      
      emberAfFillCommandIdentifyClusterIdentifyQuery();
      emberAfSetCommandEndpoints(FIdIndex, REPlist[REPcount]);
      emberAfSendCommandUnicast(EMBER_OUTGOING_DIRECT, netwid); // using network id to send the unicast
      
      if (REPcount!=0){ // Ask for another EP on the Remote Node
        
        SimpleDescriptorRequest( netwidB,netwid, REPlist[REPcount-1]);
      }
      
    }	
    
    
    
    
  }		
  
  
  return FALSE; //the stack handle the message 
  
  
}


/** @brief Pre Command Received
 *
 * This callback is the second in the Application Framework�s message
 * processing chain. At this point in the processing of incoming over-the-air
 * messages, the application has determined that the incoming message is a ZCL
 * command. It parses enough of the message to populate an
 * EmberAfClusterCommand struct. The Application Framework defines this struct
 * value in a local scope to the command processing but also makes it
 * available through a global pointer called emberAfCurrentCommand, in
 * app/framework/util/util.c. When command processing is complete, this
 * pointer is cleared.
 *
 * @param cmd   Ver.: always
 */
boolean emberAfPreCommandReceivedCallback(EmberAfClusterCommand* cmd){
  
  int8u LSB, MSB, AttrId1, AttrId2, AttrId3 ;
  int8u NodeIdTable;
  EmberEUI64 euip;      	// long network address for printing on the reporting messages 
  EmberStatus status;
  int16s val; 
  
  status = emberLookupEui64ByNodeId ( cmd->source, euip ); // to get the EUI address of the message
  
  if (status == EMBER_SUCCESS){
    NodeIdTable = CheckListEuiNode(&euip);
    
  } else if (status==EMBER_ERR_FATAL){// if there is a problem and the address is not on the table euip=00000000...
    // any way do it so NodeIdTable will be 0xFF meaning that the address is not on the table
    
    int8u* p = (int8u*)euip;
    
    for (int i=8; i>0; i--) {
      p[i-1]=0x00;
    }
    
    NodeIdTable=CheckListEuiNode(&euip);
    
  }
  
  /* 1I*/
  // Checking for all the reporting messages
  if (cmd->commandId == ZCL_REPORT_ATTRIBUTES_COMMAND_ID ){
    
//    x = (int16s)NTOHS( (int16u)(cmd->buffer[7]) );
    x = *((int16s*)(cmd->buffer + 6));
    
    switch (cmd->apsFrame->clusterId){
      
    case ZCL_ILLUM_MEASUREMENT_CLUSTER_ID:
      if (cmd->bufLen != 8) emberSerialPrintf(APP_SERIAL, "Buffer Length:%d\n", cmd->bufLen);
      else{
        emberSerialPrintfLine(APP_SERIAL, "-A- Node %d AC value: %d mArms -%2x-", NodeIdTable, x, x );
      }
      break;
      
    case ZCL_TEMP_MEASUREMENT_CLUSTER_ID: 
      emberSerialPrintfLine(APP_SERIAL, "-T- Node %d Temp: %d.%d �C -%2x-", NodeIdTable, x/100, x%100, x);
      break;
      
    case ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID:
      
      AttrId1 = cmd->buffer[3];
      switch (AttrId1){
      case 0:
        emberSerialPrintf(APP_SERIAL, "-X- Node %d Acceleration X: (0x%2x) ", NodeIdTable, x);
        break;
      case 1:
        emberSerialPrintf(APP_SERIAL, "-Y- Node %d Acceleration Y: (0x%2x) ", NodeIdTable, x);
        break;
      case 2:
        emberSerialPrintf(APP_SERIAL, "-Z- Node %d Acceleration Z: (0x%2x) ", NodeIdTable, x);
        break;
      }
      val = x;
      if (val < 0){
        emberSerialPrintf(APP_SERIAL, "-");
        val = -val;
      }
      emberSerialPrintf(APP_SERIAL, "%d.", getAxisIntegerPart( val, 2 ));
      val = getAxisFractionPart( val, 2 );
      if (val < 1000) emberSerialPrintf(APP_SERIAL, "0");
      if (val < 100) emberSerialPrintf(APP_SERIAL, "0");
      if (val < 10) emberSerialPrintf(APP_SERIAL, "0");
      emberSerialPrintfLine(APP_SERIAL, "%d ", val);
      
      break;
    }
  }
 /* 1E*/
  
//  if ((cmd->commandId == ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) && 
//      (cmd->apsFrame->clusterId == ZCL_ILLUM_MEASUREMENT_CLUSTER_ID) && 
//        (cmd->bufLen == 8)){
//          
//          MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
//          MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
//          
//          //emberAfAppPrintln("\nTemp:0x%2x C", MergeBytes(LSB, MSB));
//          emberSerialPrintf(APP_SERIAL, "\n Node %d AC value: %d mArms", NodeIdTable, MergeBytes(LSB, MSB) );
//          emberSerialPrintf(APP_SERIAL, "\n");
//        }
  
//  if ((cmd->commandId==ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) && 
//      (cmd->apsFrame->clusterId== ZCL_TEMP_MEASUREMENT_CLUSTER_ID) && 
//        (cmd->bufLen==8)){
//          
//          MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
//          MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
//          
//          
//          //emberAfAppPrintln("\nTemp:0x%2x C", MergeBytes(LSB, MSB));
//          emberSerialPrintf(APP_SERIAL, "\n Node %d Temp: %d.%d C", NodeIdTable, MergeBytes(LSB, MSB)/ 100, MergeBytes(LSB, MSB) % 100 );
//          emberSerialPrintf(APP_SERIAL, "\n");
//        }
  
//  if ((cmd->commandId == ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) && 
//      (cmd->apsFrame->clusterId == ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) && 
//        (cmd->bufLen==8)){
//          
//          MEMCOPY(&AttrId1, ((cmd->buffer)+3), 1);// attribute ID
//          
//          switch (AttrId1) {
//      
//          case 0x00 : //ZCL_ACCELERATION_X_ATTRIBUTE_ID in 1 byte
//            
//            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
//            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
//            x=MergeBytes(LSB, MSB);
//            
//            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, ", NodeIdTable, x);
//            emberSerialPrintf(APP_SERIAL, "\n");
//            break;
//            
//          case 0x01 : //ZCL_ACCELERATION_Y_ATTRIBUTE_ID in 1 byte
//            
//            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
//            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
//            y=MergeBytes(LSB, MSB);
//            
//            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Y: %d mg, ", NodeIdTable, y);
//            emberSerialPrintf(APP_SERIAL, "\n");
//            break;
//            
//          case 0x02 : //ZCL_ACCELERATION_Z_ATTRIBUTE_ID in 1 byte
//            
//            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
//            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
//            z=MergeBytes(LSB, MSB);
//            
//            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Z: %d mg, ", NodeIdTable, z);
//            emberSerialPrintf(APP_SERIAL, "\n");
//            break;
//          }
//          
//          //emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, Y: %d mg, Z: %d mg", NodeIdTable, x, y, z);
//          //emberSerialPrintf(APP_SERIAL, "\n");	
//          
//        }
  
  if ((cmd->commandId==ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) && 
      (cmd->apsFrame->clusterId== ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) && 
        (cmd->bufLen==13)) {
          
          MEMCOPY(&AttrId1, ((cmd->buffer)+3), 1);// attribute ID
          MEMCOPY(&AttrId2, ((cmd->buffer)+8), 1);// attribute ID
          
          switch (AttrId1) {
            
          case 0x00 : //ZCL_ACCELERATION_X_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
            x=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, ", NodeIdTable, x);
            emberSerialPrintf(APP_SERIAL, "\n");
            
            break;
            
          case 0x01 : //ZCL_ACCELERATION_Y_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
            y=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Y: %d mg, ", NodeIdTable, y);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x02 : //ZCL_ACCELERATION_Z_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
            z=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Z: %d mg, ", NodeIdTable, z);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
          }
          
          switch (AttrId2) {
            
          case 0x00 : //ZCL_ACCELERATION_X_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+11), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+12), 1);// Second byte MSB
            x=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, ", NodeIdTable, x);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x01 : //ZCL_ACCELERATION_Y_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+11), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+12), 1);// Second byte MSB
            y=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Y: %d mg, ", NodeIdTable, y);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x02 : //ZCL_ACCELERATION_Z_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+11), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+12), 1);// Second byte MSB
            z=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Z: %d mg, ", NodeIdTable, z);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
          }
          
          //emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, Y: %d mg, Z: %d mg", NodeIdTable, x, y, z);
          //emberSerialPrintf(APP_SERIAL, "\n");	
        }	
  
  
  if ((cmd->commandId==ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) && 
      (cmd->apsFrame->clusterId== ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) && 
        (cmd->bufLen==18)) {
          
          
          MEMCOPY(&AttrId1, ((cmd->buffer)+3), 1);// attribute ID
          MEMCOPY(&AttrId2, ((cmd->buffer)+8), 1);// attribute ID
          MEMCOPY(&AttrId3, ((cmd->buffer)+13), 1);// attribute ID
          
          emberSerialPrintf(APP_SERIAL, "\nAttrId1-> 0x%2x, AttrId2-> 0x%2x, AttrId3-> 0x%2x", AttrId1, AttrId2, AttrId3);
          
          switch (AttrId1) {
            
          case 0x00 : //ZCL_ACCELERATION_X_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
            x=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, ", NodeIdTable, x);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x01 : //ZCL_ACCELERATION_Y_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
            y=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Y: %d mg, ", NodeIdTable, y);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x02 : //ZCL_ACCELERATION_Z_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+6), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+7), 1);// Second byte MSB
            z=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration z: %d mg, ", NodeIdTable, z);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
          }
          
          switch (AttrId2) {
          case 0x00 : //ZCL_ACCELERATION_X_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+11), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+12), 1);// Second byte MSB
            x=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, ", NodeIdTable, x);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x01 : //ZCL_ACCELERATION_Y_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+11), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+12), 1);// Second byte MSB
            y=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Y: %d mg, ", NodeIdTable, y);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x02 : //ZCL_ACCELERATION_Z_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+11), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+12), 1);// Second byte MSB
            z=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Z: %d mg, ", NodeIdTable, z);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
          }
          
          switch (AttrId3) {
          case 0x00 : //ZCL_ACCELERATION_X_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+16), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+17), 1);// Second byte MSB
            x=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, ", NodeIdTable, x);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x01 : //ZCL_ACCELERATION_Y_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+16), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+17), 1);// Second byte MSB
            y=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Y: %d mg, ", NodeIdTable, y);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
            
          case 0x02 : //ZCL_ACCELERATION_Z_ATTRIBUTE_ID in 1 byte
            
            MEMCOPY(&LSB, ((cmd->buffer)+16), 1);// first byte LSB 
            MEMCOPY(&MSB, ((cmd->buffer)+17), 1);// Second byte MSB
            z=MergeBytes(LSB, MSB);
            
            emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration Z: %d mg, ", NodeIdTable, z);
            emberSerialPrintf(APP_SERIAL, "\n");
            break;
          }
          
          //emberSerialPrintf(APP_SERIAL, "\n Node %d Acceleration X: %d mg, Y: %d mg, Z: %d mg", NodeIdTable, x, y, z);
          //emberSerialPrintf(APP_SERIAL, "\n");
          
        }
  
  if ((cmd->source==netwid) && 
      (cmd->apsFrame->clusterId==ZCL_IDENTIFY_CLUSTER_ID) &&
        (cmd->commandId==ZCL_IDENTIFY_QUERY_COMMAND_ID)		&&
          (cmd->type==EMBER_INCOMING_UNICAST)){ 

            emberAfCorePrintln("RX OK from EP 0x%2x to EP 0x%2x profile 0x%2x culster 0x%2x of source 0x%2x Command ID 0x%2x\r\n",
                               cmd->apsFrame->sourceEndpoint,
                               cmd->apsFrame->destinationEndpoint,
                               cmd->apsFrame->profileId,
                               cmd->apsFrame->clusterId,
                               cmd->source,
                               cmd->commandId);
            
            if (REPcount==0) {
              //the Binding process have finished
              
              emberSerialPrintf(APP_SERIAL, 
                                "The process of creating the records on the binding table of 0x%2x have finished, EUI address \r\n",
                                netwid);	
              printEUI64(APP_SERIAL, &eui);
              emberSerialPrintf(APP_SERIAL,"\r\n");
              
              
              netwid = 0X0000;						//this reset will indicates the process of Binding to one Node have finished
            }
          }

  return FALSE;	 // Does it always return FALSE???
}

/** @brief Configured
 *
 * This callback is called by the Reporting plugin whenever a reporting entry
 * is configured, including when entries are deleted or updated.  The
 * application can use this callback for scheduling readings or measurements
 * based on the minimum and maximum reporting interval for the entry.  The
 * application should return EMBER_ZCL_STATUS_SUCCESS if it can support the
 * configuration or an error status otherwise.  Note: attribute reporting is
 * required for many clusters and attributes, so rejecting a reporting
 * configuration may violate ZigBee specifications.
 *
 * @param entry   Ver.: always
 */
EmberAfStatus emberAfPluginReportingConfiguredCallback(const EmberAfPluginReportingEntry * entry){
  return EMBER_ZCL_STATUS_SUCCESS;
}

/** @brief Finished
 *
 * This callback is fired when the network-find plugin is finished with the
 * forming or joining process.  The result of the operation will be returned
 * in the status parameter.
 *
 * @param status   Ver.: always
 */
void emberAfPluginNetworkFindFinishedCallback(EmberStatus status){
  CheckNetworkByEuiTable();
}

/** @brief Get Radio Power For Channel
 *
 * This callback is called by the framework when it is setting the radio power
 * during the discovery process. The framework will set the radio power
 * depending on what is returned by this callback.
 *
 * @param channel   Ver.: always
 */
int8s emberAfPluginNetworkFindGetRadioPowerForChannelCallback(int8u channel){
  return EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER;
}



/** @brief Join
 *
 * This callback is called by the plugin when a joinable network has been
 * found.  If the application returns TRUE, the plugin will attempt to join
 * the network.  Otherwise, the plugin will ignore the network and continue
 * searching.  Applications can use this callback to implement a network
 * blacklist.
 *
 * @param networkFound   Ver.: always
 * @param lqi   Ver.: always
 * @param rssi   Ver.: always
 */
boolean emberAfPluginNetworkFindJoinCallback(EmberZigbeeNetwork *networkFound, int8u lqi, int8s rssi){
  return TRUE;
}



// *********************************
// utility for printing EUI64 addresses
// *********************************
void printEUI64(int8u port, EmberEUI64* eui) {
  int8u i;
  int8u* p = (int8u*)eui;
  for (i=8; i>0; i--) {
    emberSerialPrintf(port, "%x", p[i-1]);
  }
}

void EndpointRequest(int8u* NetwidBNode, EmberNodeId NetwidNode)  {
  
  EmberStatus status;
  EmberApsFrame apsFrame;
  EmberMessageBuffer buffer = 0;
  int8u length;
  
  int8u TXsequence[1] = {0x01}; 	// Transaction sequence number arbitrary choosen (0x01) 
  
  int8u localBuffer[3];	

  
  
  // the data - the network address of the Node only
  
  MEMCOPY(&(localBuffer[0]), TXsequence, 1);
  MEMCOPY(&(localBuffer[1]), NetwidBNode, EmberNodeId_Size);
  

  // copy the data into a packet buffer
  buffer = emberFillLinkedBuffers((int8u*) localBuffer, EmberNodeId_Size + 1);
	
  length = emberMessageBufferLength(buffer);
	emberSerialPrintf(APP_SERIAL,
       "buffer length 0x%x \r\n", 
	    length);
		
		
  // check to make sure a buffer is available
  if (buffer == EMBER_NULL_MESSAGE_BUFFER) {
    emberSerialPrintf(APP_SERIAL,
        "TX ERROR [ZDO EndpointRequest], OUT OF BUFFERS\r\n");
    return;
  }
  
  apsFrame.profileId = EMBER_ZDO_PROFILE_ID;          	// 
  apsFrame.clusterId = ACTIVE_ENDPOINTS_REQUEST; 				// message type
  apsFrame.sourceEndpoint = EMBER_ZDO_ENDPOINT;       						// ZDO endpoint
  apsFrame.destinationEndpoint = EMBER_ZDO_ENDPOINT;  						// ZDO endpoint
  apsFrame.sequence = 0;                    					// use seq of 0

  // send the message
  status =  emberSendUnicast (	EMBER_OUTGOING_DIRECT,  // using network id to send the unicast
								NetwidNode, 			// the address
								&apsFrame,
										// Parameters for the message
								buffer );				// message to send 	

    

  emberSerialPrintf(APP_SERIAL,
       "TX [Send ZDO EndpointRequest], status 0x%x length 0x%x \r\n", 
	   status );
	   
   emberReleaseMessageBuffer(buffer);
}

void SimpleDescriptorRequest(int8u* NetwidBNode, EmberNodeId NetwidNode, int8u EP)  {
  
  EmberStatus status;
  EmberApsFrame apsFrame;
  EmberMessageBuffer buffer = 0;
  int8u length;
  
  int8u TXsequence[1] = {0x01}; 	// Transaction sequence number arbitrary choosen (0x01) 
  
  int8u localBuffer[100];	

  
  
  // the data - the network address of the Node only
  
  MEMCOPY(&(localBuffer[0]), TXsequence, 1);
  MEMCOPY(&(localBuffer[1]), NetwidBNode, EmberNodeId_Size);
  MEMCOPY(&(localBuffer[3]), &EP, 1);
  

  // copy the data into a packet buffer
  buffer = emberFillLinkedBuffers((int8u*) localBuffer,
                                  EmberNodeId_Size+2);
	
	length = emberMessageBufferLength(buffer);
	emberSerialPrintf(APP_SERIAL,
       "buffer length 0x%x \r\n", 
	    length);
		
		
  // check to make sure a buffer is available
  if (buffer == EMBER_NULL_MESSAGE_BUFFER) {
    emberSerialPrintf(APP_SERIAL,
        "TX ERROR [ZDO SimpleDescriptorRequest], OUT OF BUFFERS\r\n");
    return;
  }
  
  apsFrame.profileId = EMBER_ZDO_PROFILE_ID;          	// 
  apsFrame.clusterId = SIMPLE_DESCRIPTOR_REQUEST; 				// message type
  apsFrame.sourceEndpoint = EMBER_ZDO_ENDPOINT;       						// ZDO endpoint
  apsFrame.destinationEndpoint = EMBER_ZDO_ENDPOINT;  						// ZDO endpoint
  apsFrame.sequence = 0;                    					// use seq of 0

  // send the message
  status =  emberSendUnicast (	EMBER_OUTGOING_DIRECT,  // using network id to send the unicast
								NetwidNode, 			// the address
								&apsFrame,  			// Parameters for the message
								buffer );				// message to send 	

  length = emberMessageBufferLength(buffer);
  // done with the packet buffer
  

  emberSerialPrintf(APP_SERIAL,
       "TX [Send ZDO SimpleDescriptorRequest], status 0x%x length 0x%x \r\n", 
	   status,
	   length);
	   
   emberReleaseMessageBuffer(buffer);
}

// will check the list to get the information of this type of endpoint into the list
// the FidIndex is the position in the table listDeviceFId 
// and the Fidcount is the # of times it is repeted

void ChecklistDeviceFId (int32u DeviceFid, int8u* Fidcount, int8u* FidIndex) {


int8u count=0;
int8u index=0;
//sizeof(listDeviceFId);

	for (int8u i=0; i!=EPType_count; i++ ){

		if (listDeviceFId[i]== DeviceFid){
		count=count+1;
		index=i;
		}
	}	
*Fidcount = count; // number of mirrors
*FidIndex = index; // index of the last one
} 



//This function will return the index of an EUI address fron the table ListEuiNode
// if the address is not on the table, will return FF, this shouldn't happen but just in case.
int8u CheckListEuiNode (EmberEUI64* EUI){

int8u* 			p;
int8u* 			q;
boolean 		flag = FALSE;
EmberEUI64*		temp; 



for (int8u i=0; i<(Node_count); i++ ){

	temp = &ListEuiNode[i];
	
	p= (int8u*)EUI;
	q= (int8u*)temp;
	
	flag = TRUE;
	
	for (int8u j=0; j<8; j++) {
					
		if (p[j]!=q[j]){
				flag = FALSE;
				}
	}
		
if (flag==TRUE){
		return i;// this value will indicate the index on the table
	}
}		
return 0xff; // this value will indicate there is no match on the table
}





// This Will open the network if any node from the EUI table is not part of the network.
void 	CheckNetworkByEuiTable	(void) {

	for (int8u i=0; i<(Node_count); i++ ){
				
		//emberSerialPrintf(APP_SERIAL,"CheckNetworkByEuiTable node 0x%x NodeIdTemp 0x%x \r\n", i, emberLookupNodeIdByEui64(ListEuiNode[i]));
			
		if ((emberLookupNodeIdByEui64(ListEuiNode[i])) == EMBER_NULL_NODE_ID){
			emberPermitJoining(PJOIN_DURATION_S);
			//emberSerialPrintf(APP_SERIAL,"PermitJoining because node 0x%x   \r\n", i);
			break;
		}
			
		
		
	}

}


// To combine two bytes unsigned in one word 16 signed
int16s  MergeBytes (int8u LSB, int8u MSB){

int16u 	temp;
int16s 	MSBLSB;

						temp = 0x0000;
						temp = MSB << 8;
						temp = temp | LSB;
						MSBLSB = (int16s)temp;

return MSBLSB;

}