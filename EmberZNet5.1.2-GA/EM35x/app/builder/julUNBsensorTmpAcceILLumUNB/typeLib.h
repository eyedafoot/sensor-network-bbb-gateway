#ifndef TYPES_LIB_H
#define TYPES_LIB_H

#ifndef uint8_t
	typedef unsigned char uint8_t;
#endif

#ifndef uint16_t
	typedef unsigned short uint16_t;
#endif

#ifndef uint32_t
	typedef unsigned int uint32_t;
#endif

#ifndef CompressedStream
    typedef struct compressedStream{
		uint8_t * data; 	// data without header
		int length;      	// number of data bits within stream
	    int byteCount;      // number of bytes of data
    } CompressedStream; 	
#endif

#ifndef DecompressedStream
    typedef struct decompressedStream{
		short * data; 	// data without header
		int length;      // number of data bits within stream
	} DecompressedStream; 	
#endif



#endif //TYPES_LIB_H
