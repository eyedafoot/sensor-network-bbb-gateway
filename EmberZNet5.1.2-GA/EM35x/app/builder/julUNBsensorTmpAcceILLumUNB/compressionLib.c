#include "compressionLib.h"
#include "typeLib.h"

#ifndef ABS
	#define ABS(x) ((x)<0 ? -(x) : (x))
#endif
#ifndef LARGE_MASK
	#define LARGE_MASK 0xFFFFFFFF
#endif
#ifndef MASK
	#define MASK 0xFFFF
#endif
#ifndef SMALL_MASK
	#define SMALL_MASK 0xFF
#endif


/*dictionary is based on the Huffman table IF CHANGED BE SURE TO CORRECT: findIndex*/
static uint16_t dictionary[] = {0, 2, 3, 4, 5, 6, 14, 30, 62, 126, 254, 510, 1022, 2046, 4094};
static uint8_t dBits[] = {2, 3, 3, 3, 3, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

static const char LogTable256[256] = 
{
#define LT(n) n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n
    -1, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,
    LT(4), LT(5), LT(5), LT(6), LT(6), LT(6), LT(6),
    LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7)
};

//http://graphics.stanford.edu/~seander/bithacks.html#IntegerLogLookup
int logbase2(int v){
	int r;
	register unsigned int t, tt;

	if (tt = v >> 16)
	{
		r = (t = tt >> 8) ? 24 + LogTable256[t] : 16 + LogTable256[tt];
	}
	else 
	{
		r = (t = v >> 8) ? 8 + LogTable256[t] : LogTable256[v];
	}
	return r;
}

uint32_t deltaCompress(short delta,uint32_t * bitCount){
	(*bitCount) = 0;//clear bit count
	uint32_t result = 0;
	int ni;
	if(!delta){
		ni = 0;
	}else{
		ni = logbase2(ABS(delta)) + 1; //int based rounding
	}
	result = dictionary[ni];
	(*bitCount) += dBits[ni] + ni;
	if(delta > 0){
		result = result << ni; //make room for new bits
		result |= (delta & (MASK >> (16 - ni))); //mask to be sure not to harm other information
	}
	else if(delta < 0){
		result = result << ni;
		result |= ((delta-1) & (MASK >> (16 - ni)));
	}
	return result;
}

//DICTIONARY SPECIFIC if you change the dictionary you must change this too!
int findIndex(unsigned int target, int start){
    int index = 0;
    int found = 0;
    int temp = 0;
    //exposes only the first 2 bits
    if (!(target >> (start - 2))){ 
        index = 0;     
    }//exposes only the first 3 bits
    else if((temp = target >> (start - 3)) <= 6){
        index = temp - 1;           
    }else{
        //now there is expected to be a string of ones so keep moving until one is found
        temp = 4;
        do{
           if(!((target >> (start - temp))&1))
           {
               found = 1;
               index = temp + 2;
           }
           temp += 1;
        }while(!found);
    }
    return index;    
}

short extractDelta(int index, unsigned int target, int firstBitI){
    short result = 0;
    if(index){
        //mask over the start 
        target = (LARGE_MASK >> (32-firstBitI+dBits[index])) & target;
        //shift LSB into position 0
        target = target >> (firstBitI - (dBits[index]+index));
        //target holds the last digits of the delta
        if(target>>(index-1)){
             //positive number
             result = (short)target;        
        }else{
             //negative number 
             target = (~target) & (LARGE_MASK >> (32-index));
             result = (short)(-1*target);
        }
    }
    return result;
}

int deltaDecompress(CompressedStream cStream, DecompressedStream dStream){
	int count = 0;
    //ensure that each stream is functioning and no null pointers
    if(cStream.data != 0 && dStream.data != 0){
        int index = 0;
        int extractIndex;
        unsigned int data = 0;
        int i = 0;
        //iterate over each bit/byte
        while(i < cStream.length){
            //clear old data
            data = 0;
            //move msb to make room for extra information
            data |= ((SMALL_MASK>>(i%8)) & cStream.data[(i>>3)]) << (16);
            //as long as there is room add the 2nd byte
            if(((i>>3)+1) < (cStream.byteCount)){
                 data |= cStream.data[(i>>3)+1] << (8);
            }
            //as long as there is room add the 3rd byte
            if(((i>>3)+2) < (cStream.byteCount)){
                 data |= cStream.data[(i>>3)+2];
            }
            //find what type this is
            extractIndex = 24-i%8;
            index = findIndex(data, extractIndex);
            if(dStream.length > count){
                 if((index + dBits[index]) > (24-i%8)){
                      //need more bits
                      data = data << 8;
                      data |= cStream.data[(i>>3)+3];
                      extractIndex += 8;
                      //still need more but this has to be the last
                      if((index + dBits[index]) > (32-i%8)){
                           data = data << 8;
                           data |= cStream.data[(i>>3)+4];
                           extractIndex += 8;
                      }   
                 }
                 dStream.data[count] = extractDelta(index,data,extractIndex);
                 count += 1;
            }
            else{
                 //WARN USER THERE IS NOT ENOUGH SPACE
                 count = -1;
            }
            //move the bit counter pasted extracted sample
            i += index + dBits[index];
        }
    }
    return count;
}
/*
Author: Ryan MacDonald
Description: packData takes a data point of bit length nBit and inserts the data 
  into compression stream

INPUTS:
    data                 compressed signal held within the lease significant bits
    nBit                 number of useful bits within data
    cStream              local stream that the data is getting compressed into
    
OUTPUTS:
    status of operation  (0 success, -1 compression buffer not long enough)
    
WARNINGS: the data will be input up to the point where it no longer fits into the buffer.
  This being said the caller should handle this case differently and if at all possible
  there should always be extra space so this case does not happen    
*/
int packData(uint32_t data, int nBit, CompressedStream * cStream){
    int status = 0;
    int sampleN = (cStream->length)>>3; //find byte number of last sample
    int bitN = (cStream->length)%8; //find the bit number currently within the index
    uint8_t temp = 0;
    
    cStream->length += nBit; //add the number of bits to the stream count
    if(nBit > (8-bitN)){
        while(nBit>0){
            //if there is a requirement for more bytes than allocated return the
            //error
            if(sampleN > cStream->byteCount){
                status = -1;
                //remove the bits that were not encoded
                cStream->length -= nBit;
                break;       
            }              
            if((bitN) || (nBit>8)){
                //handle part chars
                temp = data >> (nBit - 8 + bitN);
                cStream->data[sampleN] |= temp;
                nBit = nBit - 8 + bitN;
                data = data & (LARGE_MASK>>(32-nBit));
                //move to the next sample
                sampleN++;
                bitN = 0;
            }else{
                //move the bits into position
                temp = (data << (8-nBit))&((LARGE_MASK >> (32-nBit))<<(8-nBit));
                //put the sample into the data stream
                cStream->data[sampleN] |= temp; 
                break;   
            }
        }  
    }else{
        //move the bits into position
        temp = data<<(8-bitN-nBit);
        //put the sample into the data stream
        cStream->data[sampleN] |= temp;
    }    
    return status;
}

/******************TEST SCRIPTS**********************/
/*
Author: Ryan MacDonald
Description: testDeltaDecompress builds a mock data stream and is directly
  dependent on the dictionary! Then it decompresses the data and checks it against
  know results.

INPUTS:
    verbose               indicates if the user wishes to see printouts of status
    
OUTPUTS:
    number of incorrect decompressoins (NOTE: will be -1 if it attempted to 
                          extract more than dStream.length)
*/
int testDeltaDecompress(int verbose){
    int result = 0; //final result for the caller
    static int samples = 13;
    CompressedStream cStream;
    //compressed data for decompression
    short originalData[] =  {0,-4,0,3,2,-5,-16, 111, 511, -2000, 0, -1, 6};
    uint8_t stream[] = {0x23, 0x1E, 0xE8, 0xB3, 0xFD, 0xBF, 0xF7, 0xFF, 0xFC, 0x0B, 0xC4, 0x98};
    cStream.data = stream;
    cStream.length = 94; //number of bits total
    cStream.byteCount = 12;
    
    //pre-decompression
    DecompressedStream dStream;
    short output[13];
    //ensure data is all zeros
    int q=0;
    for(;q<samples;q++){
        output[q] = 0;        
    }
    dStream.data = output;
    dStream.length = samples;
    
    int dCount = deltaDecompress(cStream, dStream);
    
    for(q=0;q<samples;q++){
        if(originalData[q] != dStream.data[q]){
            result += 1;                    
        }
        if(verbose){
            //printf("original: %hd decompression: %hd\r\n",originalData[q],dStream.data[q]);            
        }        
    }
    if(dCount != samples){
        result = -1;
        if(verbose){
            //printf("samples: %d actualCount: %d (if -1 tried for too many)",samples,dCount);  
        }        
    }
    return result;
}
/*
Author: Ryan MacDonald
Description: testDeltaCompress inputs a known delta checks it against a known
  response! Then it checks the # of bits known to have to be sure there is no 
  extra redundancy.

INPUTS:
    verbose               indicates if the user wishes to see printouts of status
    
OUTPUTS:
    number of incorrect compressions (1 for compression, 1 for bit length)
*/
int testDeltaCompress(int verbose){
    int errorCount = 0;
    short tempDelta = -2;
    unsigned int bCount = 0;
    if(deltaCompress(tempDelta,&bCount)!=13){
        errorCount++;
    }
    if(bCount!=5){
        errorCount++;
    }
    tempDelta = -4444;
    if(deltaCompress(tempDelta,&bCount)!=16764579){
        errorCount++;
    }
    if(bCount!=24){
        errorCount++;
    }
    tempDelta = 0;
    if(deltaCompress(tempDelta,&bCount)!=0){
        errorCount++;
    }
    if(bCount!=2){
        errorCount++;
    }
    tempDelta = 16;
    if(deltaCompress(tempDelta,&bCount)!=208){
        errorCount++;
    }
    if(bCount!=8){
        errorCount++;
    }
    tempDelta = 16383;
    if(deltaCompress(tempDelta,&bCount)!=67092479){
        errorCount++;
    }
    if(bCount!=26){
        errorCount++;
    }
    tempDelta = -16382;
    if(deltaCompress(tempDelta,&bCount)!=67076097){
        errorCount++;
    }
    if(bCount!=26){
        errorCount++;
    }    
    return errorCount;
}

int systemCompressionTest(int verbose){
    uint32_t cData;
    uint32_t count = 0;
    int status = 0;
    short originalData[] =  {0,-4,0,3,2,-5,-16, 111, 511, -2000, 0, -1, 6};
    //uint8_t stream[] = {0x23, 0x1E, 0xE8, 0xB3, 0xFD, 0xBF, 0xF7, 0xFF, 0xFC, 0x0B, 0xC4, 0x98};
    int i = 0;
    
    //define the compression stream
    CompressedStream cStream;
    uint8_t outStream[50];
    //force space to be zeros
    for(;i<50;i++){
        outStream[i] = 0;
    }
    cStream.data = outStream; //point compress stream to data buffer
    cStream.byteCount = 50;
    cStream.length = 0;
    
    //define the decompression stream
    DecompressedStream dStream;
    short inStream[15];
    dStream.data = inStream;
    dStream.length = 15;
    
    //pack compressed data
    for(i=0;i<13;i++){
        cData = deltaCompress(originalData[i],&count);     
        packData(cData, count, &cStream);           
        if(verbose){
            //printf("compressed: %d len: %d   stream len: %d\r\n",cData,count,cStream.length);            
        }         
    }
    if(verbose){
        //printf("\r\n");
        for(i=0;i < ((cStream.length>>3)+1);i++){
           //printf("%x ",cStream.data[i]);                                  
        }            
        //printf("\r\n");
    }
    //SEND HERE emulation
    
    //decompression
    int samples = deltaDecompress(cStream,dStream);
    
    //examine data post decompression
    if(verbose){
        //printf("samples decompressed: %d expected: %d\r\n",samples,13);            
    }
    if(samples != 13){
        status += 1; //error with data             
    }
    for(i=0;i<samples;i++){
        if(dStream.data[i] != originalData[i]){
            status += 1;                                     
        }
        if(verbose){
            //printf("before: %d after: %d\r\n",originalData[i],dStream.data[i]);            
        }                       
    }  
    return status;
}
