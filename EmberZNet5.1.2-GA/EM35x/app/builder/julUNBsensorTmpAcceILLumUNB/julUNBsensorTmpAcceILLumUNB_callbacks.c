// example using the UNB node reporting its atributes (temp accelerometer)
// There is a restriction about using an Endpoint # 2, another Endpoint should have a number different from 2.
// This is due to the problem of the stack to create one Endpoint which a number different than Zero

#include "app/framework/include/af.h"
#include <parameters.c>
#include "hal/unb/temprature.h"
#include "hal/unb/acceleration.h"
#include "hal/micro/cortexm3/adc.h"
#include "hal/ece/haldrvADC.h"
#include <hal/ece/haldrvTempSensor.h>
#include <hal/ece/haldrvAcclSensor.h>
#include <hal/ece/haldrvUtils.h>

#ifndef __TEMPERATURE_SENSOR_H__
  #error no Temp Sensor defined
#endif

// Event control struct declarations
EmberEventControl buttonEventControl;

// Each Node needs an event to control the Annunce for start the Binding process
 EmberEventControl AnnunceEventControl;

// Each EP needs an event to control the reporting 
EmberEventControl EP1configureReportingControl;



#define LED1 BOARDLED1
#define LED0 BOARDLED0


#define APP_SERIAL        0     // serial port
#define EmberNodeId_Size  2     // size of network ID

#define EP1         1
#define EP2         2
#define EP5         5
#define EP3         3

// *******************************************************************

// Forward declarations

void printEUI64(int8u port, EmberEUI64* eui);
void createBinding(int8u LocalEP, int8u RemoteEP, int16u ezmodeClientCluster, int8u *address );


static int16u EP1clusterIds[] = {ZCL_TEMP_MEASUREMENT_CLUSTER_ID, ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID, ZCL_ILLUM_MEASUREMENT_CLUSTER_ID};

EmberEUI64 GetwayEUIAddress ; 

int8u netwidB[2]={0x00, 0x00};
int8u EPBinding;

int16s tempVal;
boolean LastButtonState = TRUE;
int16u ILLUM_MEASUREMENT;
boolean availADC = FALSE;
boolean owADC = FALSE;

struct AcclConfig{
  int8u maxG;           // �2g is the default mode
  int8u ActiveDataRate;
  int8u SleepDataRate;
  boolean LowNoiseFlag; // Set LNOISE_BIT (clearing it, as in default mode, saves power)
  boolean FastModeFlag; // Set Fast read mode to read 8 bits per xyz instead of 12bits
  boolean OrientFlag;   // Set if orientation detection is also needed
  tOSMode OSmode;      // Set oversample mode
  tOSMode sOSmode;     // Set Sleep oversample mode
} acclCfg;

EmberStatus initAccelerator(int8u verbose);

// *******************************************************************
// Begin main application loop




/** @brief External Attribute Write
 *
 * This function is called whenever the Application Framework needs to write
 * an attribute which is not stored within the data structures of the
 * Application Framework itself. One of the new features in Version 2 is the
 * ability to store attributes outside the Framework. This is particularly
 * useful for attributes that do not need to be stored because they can be
 * read off the hardware when they are needed, or are stored in some central
 * location used by many modules within the system. In this case, you can
 * indicate that the attribute is stored externally. When the framework needs
 * to write an external attribute, it makes a call to this callback.
       
 * This callback is very useful for host micros which need to store attributes
 * in persistent memory. Because each host micro (used with an Ember NCP like
 * the EM260) has its own type of persistent memory storage, the Application
 * Framework does not include the ability to mark attributes as stored in
 * flash the way that it does for Ember SoCs like the EM35x. On a host micro,
 * any attributes that need to be stored in persistent memory should be marked
 * as external and accessed through the external read and write callbacks. Any
 * host code associated with the persistent storage should be implemented
 * within this callback.
        All of the important information about the
 * attribute itself is passed as a pointer to an EmberAfAttributeMetadata
 * struct, which is stored within the application and used to manage the
 * attribute. A complete description of the EmberAfAttributeMetadata struct is
 * provided in app/framework/include/af-types.h.
        This function assumes
 * that the application is able to write the attribute and return immediately.
 * Any attributes that require a state machine for reading and writing are not
 * candidates for externalization at the present time. The Application
 * Framework does not currently include a state machine for reading or writing
 * attributes that must take place across a series of application ticks.
 * Attributes that cannot be written immediately should be stored within the
 * Application Framework and updated occasionally by the application code from
 * within the emberAfMainTickCallback.
        If the application was
 * successfully able to write the attribute, it returns a value of
 * EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the application
 * was not able to write the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeWriteCallback(int8u endpoint,
                                                    EmberAfClusterId clusterId,
                                                    EmberAfAttributeMetadata * attributeMetadata,
                                                    int16u manufacturerCode,
                                                    int8u * buffer)
{
  return EMBER_ZCL_STATUS_FAILURE;
}


/** @brief External Attribute Read
 *
 * Like emberAfExternalAttributeWriteCallback above, this function is called
 * when the framework needs to read an attribute that is not stored within the
 * Application Framework�s data structures.
        All of the important
 * information about the attribute itself is passed as a pointer to an
 * EmberAfAttributeMetadata struct, which is stored within the application and
 * used to manage the attribute. A complete description of the
 * EmberAfAttributeMetadata struct is provided in
 * app/framework/include/af-types.h
        This function assumes that the
 * application is able to read the attribute, write it into the passed buffer,
 * and return immediately. Any attributes that require a state machine for
 * reading and writing are not really candidates for externalization at the
 * present time. The Application Framework does not currently include a state
 * machine for reading or writing attributes that must take place across a
 * series of application ticks. Attributes that cannot be read in a timely
 * manner should be stored within the Application Framework and updated
 * occasionally by the application code from within the
 * emberAfMainTickCallback.
        If the application was successfully able
 * to read the attribute and write it into the passed buffer, it should return
 * a value of EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the
 * application was not able to read the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeReadCallback(int8u endpoint,
                                                   EmberAfClusterId clusterId,
                                                   EmberAfAttributeMetadata * attributeMetadata,
                                                   int16u manufacturerCode,
                                                   int8u * buffer)
                           
{
  int16s rawT;          // Temperature read from sensor
  int16s axes[3];       // Acceleration read from sensor
  int32s delay_ms;      // Time (in ms) to wait for sensors to respond
  
  EmberAfAttributeId attributeId = attributeMetadata->attributeId;
  
  switch (clusterId) {
    // Temprature measurements from on-board sensor
  case ZCL_TEMP_MEASUREMENT_CLUSTER_ID:
    
    if (attributeId == ZCL_TEMP_MEASURED_VALUE_ATTRIBUTE_ID) {
#ifndef UNBNODE_USE_DRIVER_V2      
      // This is Tristan's function
      rawT = readTemprature();
      emberAfCorePrintln("Temperature(v1): %d.%d �C", rawT/100L, rawT%100L);
#else
      logPrint("Reading temperature ... ");
      rawT = readTemperatureRawSleeping(UNBNODE_TEMP_DEV_NUM );
      logPrintln("Done.");
      
      logPrint("Temperature(v2): 0x%2x = ", rawT );
      
      logPrintln(" %d.%d �C", rawTInt(rawT), rawTFrac(rawT,  TMP100getResolution(UNBNODE_TEMP_DEV_NUM)) );
      
      // int16s fpT = rawT2fpoint(rawT);
      // emberAfCorePrint(" %d.%d oC", HIGH_BYTE(fpT), LOW_BYTE(fpT));
      
      rawT = rawT2100sC(rawT);
      // emberAfCorePrintln(" %d.%d oC", rawT/100, rawT%100);
      
#endif
     
      *(int16s*)buffer = rawT;
      
      return EMBER_ZCL_STATUS_SUCCESS;
    }
    break;
    
    // Acceleration measurements from on-board sensor
  case ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID:
    logPrintln("******************** Reading Acceleration ************************************");
  
    /* Device back to activation */
    MMA8452Q_setActive( TRUE );
    
    /* Calculate Turn on Time from standby to active mode in msecs. */
    delay_ms = MMA8452Q_getTurnOnTimeUS( acclCfg.ActiveDataRate )/1000L ;
     
    /* Wait Turn on Time */
    logPrint("Waiting %d ms to ensure accelerometer sensor readiness... ", delay_ms);
    waitSleeping( delay_ms );
    logPrintln("Done.");
    
    
    // Check if there is any available data
    if ( MMA8452Q_XYZDataReady() ){
      MMA8452Q_getAxes( axes );
      logPrint("Active data ready. ");
    }
    else{
      axes[0] = axes[1] = axes[2] = 0;
      logPrint("NO Active data ready. ");
    }
 
    logPrintln("Disabling device to save power...");
 
    // Disable device to save power. Actual sampling will be determined by master device
    MMA8452Q_setActive(FALSE); 

    printAxes(axes, acclCfg.maxG);
    
    if (acclCfg.OrientFlag){
      int8u plStatus;
      MMA8452Q_getOrientation(&plStatus);
      logPrint(" Orientation: ");
      printOrientation (plStatus);
      logPrintln("");
    }
    
    logPrintln("***************** End of Reading Acceleration ********************************");
        
    /******************End of debugging part***********************************/

    switch (attributeId) {
      
    case ZCL_ACCELERATION_X_ATTRIBUTE_ID:
      *(int16s*)buffer = axes[0];
      break;
      
    case ZCL_ACCELERATION_Y_ATTRIBUTE_ID:
      *(int16s*)buffer = axes[1];
      break;
      
    case ZCL_ACCELERATION_Z_ATTRIBUTE_ID:
      *(int16s*)buffer = axes[2];
      break;
    }
    return EMBER_ZCL_STATUS_SUCCESS;
    
  case ZCL_ILLUM_MEASUREMENT_CLUSTER_ID:
    
    if (attributeId == ZCL_ILLUM_MEASURED_VALUE_ATTRIBUTE_ID) {
        int16s tempVal = readAdcOneShot();
        logPrint("-A- ADC Measurement: %d mA (0x%2x), ", convertToMilliAmps(tempVal), tempVal);
        return EMBER_ZCL_STATUS_SUCCESS;
    }
    break;
  }
  
  return EMBER_ZCL_STATUS_FAILURE;
}

/** @brief Cluster Init
 *
 * This function is called when a specific cluster is initialized. It gives
 * the application an opportunity to take care of cluster initialization
 * procedures. It is called exactly once for each endpoint where cluster is
 * present.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 */
void emberAfClusterInitCallback(int8u endpoint,
                                EmberAfClusterId clusterId)
{
  EmberStatus iniStatus;
  //int16s vdd, vRef2, vReg2, vRef2_uV, vReg2_uV;
  //int32s vBatt;
  
  switch (clusterId){
  case ZCL_ILLUM_MEASUREMENT_CLUSTER_ID:
    halInternalInitAdc();
    logPrintln("ADC before Configuration");
    adcConfigStruct AdcCfg = getAdcConfig();
    logPrintln("Using 1 MHz Clock: %d, Significant Bits: %d, P-Channel: 0x%2x, N-Channel: 0x%2x",
               AdcCfg.sigBits, AdcCfg.sigBits, AdcCfg.inputP, AdcCfg.inputN);
    
    logPrintln("********Calibrating ADC**************");

    logPrint("ADC will be calibrated....");
    calibrateAdc();
    logPrintln("Done");

    setAdcConfig(TRUE, 14, VREF2, GND); 
    tempVal = readAdcOneShot();
    logPrint("nGND = 0x%2x == %d volts", tempVal, convertToSignedMillivolts(tempVal));

    setAdcConfig(TRUE, 14, GND, VREF2); 
    tempVal = readAdcOneShot();
    logPrint("GND = 0x%2x == %d volts", tempVal, convertToSignedMillivolts(tempVal));

    setAdcConfig(TRUE, 14, VREF2, VREF); 
    tempVal = readAdcOneShot();
    logPrint("nVREF = 0x%2x == %d volts", tempVal, convertToSignedMillivolts(tempVal));

    setAdcConfig(TRUE, 14, VREF, VREF2); 
    tempVal = readAdcOneShot();
    logPrint("VREF = 0x%2x == %d volts", tempVal, convertToSignedMillivolts(tempVal));

    setAdcConfig(TRUE, 14, VREF2, VREG2); 
    tempVal = readAdcOneShot();
    logPrint("nVDD = 0x%2x == %d volts", tempVal, convertToSignedMillivolts(tempVal));

    setAdcConfig(TRUE, 14, VREG2, VREF2); 
    tempVal = readAdcOneShot();
    logPrint("VDD = 0x%2x == %d volts", tempVal, convertToSignedMillivolts(tempVal));
    
    logPrintln("********End of Calibrating ADC**************");
    
    break;
    
  case ZCL_TEMP_MEASUREMENT_CLUSTER_ID:
 #ifndef UNBNODE_USE_DRIVER_V2     
    emberAfCorePrintln("Temperature Sensor Conversion Time= %d ms",OS_MAX_CONV_TIME(TMP100_MAX_RESOLUTION));
    initTemprature();
#else
    TMP100initDevice (UNBNODE_TEMP_DEV_NUM);
    //TMP100setResolution (TMP100_MAX_RESOLUTION, UNBNODE_TEMP_DEV_NUM );
    
    /******************debugging part******************************************/
    TMP100showConfig (UNBNODE_TEMP_DEV_NUM);
    TMP100showMeasDelay (UNBNODE_TEMP_DEV_NUM);
    /******************End of debugging part***********************************/
#endif
   break;
    
  case ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID:
    //    initAcceleration(); // this is Tristan's initialization routine
    
    acclCfg.maxG = 2;  // �2g is the default mode
    acclCfg.ActiveDataRate = ACTIVE_DR_800HZ;
    acclCfg.SleepDataRate = SLEEP_DR_1P56HZ;
    acclCfg.LowNoiseFlag = FALSE; // Set LNOISE_BIT (clearing it, as in default mode, saves power)
    acclCfg.FastModeFlag = FALSE; // Set Fast read mode to read 8 bits per xyz instead of 12bits
    acclCfg.OrientFlag = TRUE;    // Set Orientation detection
    acclCfg.OSmode = Normal;      // Set oversample mode
    acclCfg.sOSmode = Normal;     // Set Sleep oversample mode

    iniStatus = initAccelerator(TRUE); // Initialie the device (verbosely)
    logPrint("Sensor MMA8452Q (Acceleration) ");
    if (iniStatus == EMBER_SUCCESS) 
      logPrintln("CONFIGURED!");
    else
      logPrintln("either NOT FOUND or could NOT be CONFIGURED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    break;
    
  }//End of switch(ClusterId)
  
  
  
  if (clusterId == ZCL_TEMP_MEASUREMENT_CLUSTER_ID){
  }
  else if (clusterId == ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID){

  }
}


void emberAfMainTickCallback(void){

  EmberNetworkStatus state = emberNetworkState();
  
  if (state == EMBER_NO_NETWORK) {
    
    emberAfStartSearchForJoinableNetwork();
  }   
  
  
}

void AnnunceEvent(void){
  
  EmberNetworkStatus state = emberNetworkState();
  EmberBindingTableEntry candidate;
  
  // if there is no network wait until there is a network to start annunce
  if (state != EMBER_JOINED_NETWORK) 
    emberEventControlSetDelayQS(AnnunceEventControl,10);
  
  
  // if there is nothig at the binding table annunce yourself
  else if ((state == EMBER_JOINED_NETWORK) && (emberGetBinding(0, &candidate) == EMBER_SUCCESS)  && (candidate.type == EMBER_UNUSED_BINDING)){ 
    emberSendDeviceAnnouncement ( );
    emberEventControlSetDelayQS(AnnunceEventControl,10);
  } 
  // if there is a binding table stop this event
  else if ((state == EMBER_JOINED_NETWORK) && (emberGetBinding(0, &candidate) == EMBER_SUCCESS)  && (candidate.type == EMBER_UNICAST_BINDING)){
    
    emberEventControlSetInactive(AnnunceEventControl);
  }
  
}




/** @brief Stack Status
 *
 * This function is called by the application framework from the stack status
 * handler.  This callbacks provides applications an opportunity to be
 * notified of changes to the stack status and take appropriate action.  The
 * application should return TRUE if the status has been handled and should
 * not be handled by the application framework.
 *
 * @param status   Ver.: always
 */
boolean emberAfStackStatusCallback(EmberStatus status){
  
  EmberNetworkStatus state = emberNetworkState();
  EmberBindingTableEntry candidate;
  
  if (status == EMBER_NETWORK_UP)  {
    
    halSetLed(LED0);
    
    
    if (emberGetBinding(0, &candidate) == EMBER_SUCCESS  && candidate.type == EMBER_UNUSED_BINDING){ 
      emberAfCorePrintln("Start Annunce Event and the candidate.type is 0x%2x\r\n",
                         candidate.type);
      
      emberEventControlSetDelayQS(AnnunceEventControl,1);
    }
    
  } else if (status == EMBER_NETWORK_DOWN) {
    
    halClearLed(LED0);
    
    // Clear the binding table before trying to start the binding process
    
    
    emberClearBindingTable ( );
    
    // Clear the reporting table also
    
    emberAfClearReportTableCallback();  
    
    // Restart the state of the reporting flag for On Off operation
    LastButtonState = TRUE;
    
  } 
  
  return FALSE;
}



void buttonEventHandler(void)
{
  
  // Botton 0
  // If not joined, join the network
  // If already joined it will create a binding of the cluster specified in clusterIds[] 
  // This is only for the Primary Endpoint
  
  emberEventControlSetInactive(buttonEventControl);
  
  if (halButtonState(BUTTON0) == BUTTON_PRESSED) {
    EmberNetworkStatus state = emberNetworkState();
    if (state == EMBER_JOINED_NETWORK) {
      
      if (LastButtonState == FALSE) {
        
         emberEventControlSetDelayQS(EP1configureReportingControl,1);
        
        LastButtonState = TRUE;
        
      }else {
        
        emberAfClearReportTableCallback();  
        LastButtonState = FALSE;  
      }
      
    } else if (state == EMBER_NO_NETWORK) {
      
      emberAfStartSearchForJoinableNetwork();
      
    }
    
  }
  
  if (halButtonState(BUTTON1) == BUTTON_PRESSED) {
    
    emberLeaveNetwork ();
    
  }
}


void EP1configureReportingEvent (void)
{
  
  // Now configuring the automatic report for the temperature attribute
  // if already reporting stop it.
  
  
  
  emberEventControlSetInactive(EP1configureReportingControl);
  
  
  EmberAfPluginReportingEntry reportingEntry;
  
  
  
  
  
  
  reportingEntry.direction = EMBER_ZCL_REPORTING_DIRECTION_REPORTED;
  reportingEntry.endpoint = 1;
  reportingEntry.clusterId = ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID;
  reportingEntry.attributeId = ZCL_ACCELERATION_X_ATTRIBUTE_ID;
  reportingEntry.mask = CLUSTER_MASK_SERVER;
  reportingEntry.manufacturerCode = EMBER_AF_NULL_MANUFACTURER_CODE;
  reportingEntry.data.reported.minInterval = EP1MIN_ACCELX_INTERVAL_S;
  reportingEntry.data.reported.maxInterval = EP1MAX_ACCELX_INTERVAL_S;
  reportingEntry.data.reported.reportableChange = 0; // unused
  
  emberAfPluginReportingConfigureReportedAttribute(&reportingEntry);
  
  
  reportingEntry.direction = EMBER_ZCL_REPORTING_DIRECTION_REPORTED;
  reportingEntry.endpoint = 1;
  reportingEntry.clusterId = ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID;
  reportingEntry.attributeId = ZCL_ACCELERATION_Y_ATTRIBUTE_ID;
  reportingEntry.mask = CLUSTER_MASK_SERVER;
  reportingEntry.manufacturerCode = EMBER_AF_NULL_MANUFACTURER_CODE;
  reportingEntry.data.reported.minInterval = EP1MIN_ACCELY_INTERVAL_S;
  reportingEntry.data.reported.maxInterval = EP1MAX_ACCELY_INTERVAL_S;
  reportingEntry.data.reported.reportableChange = 0; // unused
  
  emberAfPluginReportingConfigureReportedAttribute(&reportingEntry);
  
  reportingEntry.direction = EMBER_ZCL_REPORTING_DIRECTION_REPORTED;
  reportingEntry.endpoint = 1;
  reportingEntry.clusterId = ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID;
  reportingEntry.attributeId = ZCL_ACCELERATION_Z_ATTRIBUTE_ID;
  reportingEntry.mask = CLUSTER_MASK_SERVER;
  reportingEntry.manufacturerCode = EMBER_AF_NULL_MANUFACTURER_CODE;
  reportingEntry.data.reported.minInterval = EP1MIN_ACCELZ_INTERVAL_S;
  reportingEntry.data.reported.maxInterval = EP1MAX_ACCELZ_INTERVAL_S;
  reportingEntry.data.reported.reportableChange = 0; // unused
  
  emberAfPluginReportingConfigureReportedAttribute(&reportingEntry);
  
  reportingEntry.direction = EMBER_ZCL_REPORTING_DIRECTION_REPORTED;
  reportingEntry.endpoint = 1;
  reportingEntry.clusterId = ZCL_TEMP_MEASUREMENT_CLUSTER_ID;
  reportingEntry.attributeId = ZCL_TEMP_MEASURED_VALUE_ATTRIBUTE_ID;
  reportingEntry.mask = CLUSTER_MASK_SERVER;
  reportingEntry.manufacturerCode = EMBER_AF_NULL_MANUFACTURER_CODE;
  reportingEntry.data.reported.minInterval = EP1MIN_TEMP_INTERVAL_S;
  reportingEntry.data.reported.maxInterval = EP1MAX_TEMP_INTERVAL_S;
  reportingEntry.data.reported.reportableChange = 0; // unused
  
  emberAfPluginReportingConfigureReportedAttribute(&reportingEntry);
  
  
  reportingEntry.direction = EMBER_ZCL_REPORTING_DIRECTION_REPORTED;
  reportingEntry.endpoint = 1;
  reportingEntry.clusterId = ZCL_ILLUM_MEASUREMENT_CLUSTER_ID;
  reportingEntry.attributeId = ZCL_ILLUM_MEASURED_VALUE_ATTRIBUTE_ID;
  reportingEntry.mask = CLUSTER_MASK_SERVER;
  reportingEntry.manufacturerCode = EMBER_AF_NULL_MANUFACTURER_CODE;
  reportingEntry.data.reported.minInterval = EP1MIN_ILLUM_INTERVAL_S;
  reportingEntry.data.reported.maxInterval = EP1MAX_ILLUM_INTERVAL_S;
  reportingEntry.data.reported.reportableChange = 0; // unused
  
  emberAfPluginReportingConfigureReportedAttribute(&reportingEntry);
  
  
}




/** @brief emberAfHalButtonIsrCallback
 *
 *
 */
// Hal Button ISR Callback
// This callback is called by the framework whenever a button is pressed on the
// device. This callback is called within ISR context.
void emberAfHalButtonIsrCallback(int8u button, int8u state)
{
  if ((button == BUTTON0 || button == BUTTON1) && state == BUTTON_PRESSED) {
    emberEventControlSetActive(buttonEventControl);
  }
}

/** @brief Server Init
 *
 * On/off cluster, Server Init
 *
 * @param endpoint Endpoint that is being initialized  Ver.: always
 */
void emberAfTempMeasurementClusterServerInitCallback(int8u endpoint)
{
  // At startup, trigger a read of the attribute to check for the temperature limit 
 // this can be used for testing
  
  emberAfTempMeasurementClusterServerAttributeChangedCallback(endpoint,
                                                    ZCL_TEMP_MEASURED_VALUE_ATTRIBUTE_ID);
}

/** @brief Server Attribute Changed
 *
 * On/off cluster, Server Attribute Changed
 *
 * @param endpoint Endpoint that is being initialized  Ver.: always
 * @param attributeId Attribute that changed  Ver.: always
 */
 void emberAfTempMeasurementClusterServerAttributeChangedCallback(int8u endpoint,
                                                       EmberAfAttributeId attributeId)
{
  // When the temperature attribute changes, it is compared to a value and set the LED appropriately. 
  
  if (attributeId == ZCL_TEMP_MEASURED_VALUE_ATTRIBUTE_ID) {
    int16u attribute_value;
    if (emberAfReadServerAttribute(endpoint,
                                   ZCL_TEMP_MEASUREMENT_CLUSTER_ID,
                                   ZCL_TEMP_MEASURED_VALUE_ATTRIBUTE_ID,
                                   (int8u *)&attribute_value,
                                   sizeof(attribute_value))
        == EMBER_ZCL_STATUS_SUCCESS) {
      if (attribute_value==0x000) {
        halSetLed(LED1);
      } else {
        halClearLed(LED1);
      }
    }
  }
}


/** @brief Pre Command Received
 *
 * This callback is the second in the Application Framework�s message
 * processing chain. At this point in the processing of incoming over-the-air
 * messages, the application has determined that the incoming message is a ZCL
 * command. It parses enough of the message to populate an
 * EmberAfClusterCommand struct. The Application Framework defines this struct
 * value in a local scope to the command processing but also makes it
 * available through a global pointer called emberAfCurrentCommand, in
 * app/framework/util/util.c. When command processing is complete, this
 * pointer is cleared.
 *
 * @param cmd   Ver.: always
 */
boolean emberAfPreCommandReceivedCallback(EmberAfClusterCommand* cmd){

int8u i;
 
if ((cmd->source==0x0000) && 
    (cmd->apsFrame->clusterId==ZCL_IDENTIFY_CLUSTER_ID) &&
      (cmd->commandId==ZCL_IDENTIFY_QUERY_COMMAND_ID) &&
  (cmd->type==EMBER_INCOMING_UNICAST)){ 
          
          
          
          emberAfCorePrintln("RX OK from EP 0x%2x to EP 0x%2x profile 0x%2x culster 0x%2x of source 0x%2x Command ID 0x%2x\r\n",
                             cmd->apsFrame->sourceEndpoint,
                             cmd->apsFrame->destinationEndpoint,
                             cmd->apsFrame->profileId,
                             cmd->apsFrame->clusterId,
                             cmd->source,
                             cmd->commandId);
            
          switch (cmd->apsFrame->destinationEndpoint) {
            
          case EP1 : // Write the binding table for all the clusterIds that we have on the table                  
            for ( i=0; i<(COUNTOF(EP1clusterIds)); i++) { 
              
              emberLookupEui64ByNodeId ( 0x0000, GetwayEUIAddress );
              createBinding(cmd->apsFrame->destinationEndpoint, cmd->apsFrame->sourceEndpoint, EP1clusterIds[i], GetwayEUIAddress);
            } 
            
            emberEventControlSetDelayQS(EP1configureReportingControl,1);
            
            emberAfFillCommandIdentifyClusterIdentifyQuery();
            emberAfSetCommandEndpoints(cmd->apsFrame->destinationEndpoint, cmd->apsFrame->sourceEndpoint); // inverse the destination with source to send it back 
            emberAfSendCommandUnicast(EMBER_OUTGOING_DIRECT, cmd->source); //// using network id of the getway to send the unicast
            
            break;
            
          case EP2 : // This Endpoint is fake, is not pretend to be used. it is here because of the limitation of EP 0  
            
            
            
            emberAfFillCommandIdentifyClusterIdentifyQuery();
            emberAfSetCommandEndpoints(cmd->apsFrame->destinationEndpoint, cmd->apsFrame->sourceEndpoint); // inverse the destination with source to send it back 
            emberAfSendCommandUnicast(EMBER_OUTGOING_DIRECT, cmd->source); // // using network id of the getway to send the unicast
            
            break;  
            
            
          }
          
          
  }

return FALSE; 

}






/** @brief Get Group Name
 *
 * This function returns the name of a group with the provided group ID,
 * should it exist.
 *
 * @param endpoint Endpoint  Ver.: always
 * @param groupId Group ID  Ver.: always
 * @param groupName Group Name  Ver.: always
 */
void emberAfPluginGroupsServerGetGroupNameCallback(int8u endpoint,
                                                   int16u groupId,
                                                   int8u * groupName)
{
}

/** @brief Set Group Name
 *
 * This function sets the name of a group with the provided group ID.
 *
 * @param endpoint Endpoint  Ver.: always
 * @param groupId Group ID  Ver.: always
 * @param groupName Group Name  Ver.: always
 */
void emberAfPluginGroupsServerSetGroupNameCallback(int8u endpoint,
                                                   int16u groupId,
                                                   int8u * groupName)
{
}

/** @brief Group Names Supported
 *
 * This function returns whether or not group names are supported.
 *
 * @param endpoint Endpoint  Ver.: always
 */
boolean emberAfPluginGroupsServerGroupNamesSupportedCallback(int8u endpoint)
{
  return FALSE;
}

/** @brief Finished
 *
 * This callback is fired when the network-find plugin is finished with the
 * forming or joining process.  The result of the operation will be returned
 * in the status parameter.
 *
 * @param status   Ver.: always
 */
void emberAfPluginNetworkFindFinishedCallback(EmberStatus status)
{
  
}

/** @brief Get Radio Power For Channel
 *
 * This callback is called by the framework when it is setting the radio power
 * during the discovery process. The framework will set the radio power
 * depending on what is returned by this callback.
 *
 * @param channel   Ver.: always
 */
int8s emberAfPluginNetworkFindGetRadioPowerForChannelCallback(int8u channel)
{
  return EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER;
}

/** @brief Join
 *
 * This callback is called by the plugin when a joinable network has been
 * found.  If the application returns TRUE, the plugin will attempt to join
 * the network.  Otherwise, the plugin will ignore the network and continue
 * searching.  Applications can use this callback to implement a network
 * blacklist.
 *
 * @param networkFound   Ver.: always
 * @param lqi   Ver.: always
 * @param rssi   Ver.: always
 */
boolean emberAfPluginNetworkFindJoinCallback(EmberZigbeeNetwork *networkFound,
                                             int8u lqi,
                                             int8s rssi)
{
  return TRUE;
}

/** @brief Configured
 *
 * This callback is called by the Reporting plugin whenever a reporting entry
 * is configured, including when entries are deleted or updated.  The
 * application can use this callback for scheduling readings or measurements
 * based on the minimum and maximum reporting interval for the entry.  The
 * application should return EMBER_ZCL_STATUS_SUCCESS if it can support the
 * configuration or an error status otherwise.  Note: attribute reporting is
 * required for many clusters and attributes, so rejecting a reporting
 * configuration may violate ZigBee specifications.
 *
 * @param entry   Ver.: always
 */
EmberAfStatus emberAfPluginReportingConfiguredCallback(const EmberAfPluginReportingEntry * entry)
{
  return EMBER_ZCL_STATUS_SUCCESS;
}

/** @brief Poll Completed
 *
 * This function is called by the End Device Support plugin after a poll is
 * completed.
 *
 * @param status Return status of a completed poll operation  Ver.: always
 */
void emberAfPluginEndDeviceSupportPollCompletedCallback(EmberStatus status)
{

}


void printEUI64(int8u port, EmberEUI64* eui) {
  int8u i;
  int8u* p = (int8u*)eui;
  for (i=8; i>0; i--) {
    emberSerialPrintf(port, "%x", p[i-1]);
  }
}

void createBinding(int8u LocalEP, int8u RemoteEP, int16u ClientCluster, int8u *address ){
  // create binding
  int8u i = 0;
  EmberBindingTableEntry candidate;
  EmberStatus status;
    
  // first look for a duplicate binding, we should not add duplicates
  for (i = 0; i < EMBER_BINDING_TABLE_SIZE; i++)
  {
     status = emberGetBinding(i, &candidate);

     if ((status == EMBER_SUCCESS)
           && (candidate.type == EMBER_UNICAST_BINDING)
           && (candidate.local == LocalEP )
           && (candidate.clusterId == ClientCluster )
           && (candidate.remote == RemoteEP )
           && (MEMCOMPARE(candidate.identifier, 
                 address, 
                 EUI64_SIZE) == 0))
     {
   emberAfCorePrintln("< node bounded >");
       
     return;
     }
   }
  
   for (i = 0; i < EMBER_BINDING_TABLE_SIZE; i++) {
     if (emberGetBinding(i, &candidate) == EMBER_SUCCESS
         && candidate.type == EMBER_UNUSED_BINDING) {
       candidate.type = EMBER_UNICAST_BINDING;
       candidate.local = LocalEP;
       candidate.remote = RemoteEP;
       candidate.clusterId = ClientCluster;
       MEMCOPY(candidate.identifier, address, EUI64_SIZE);
       status = emberSetBinding(i, &candidate);
      
 if (status == EMBER_SUCCESS) {
         emberAfCorePrintln("<bounded at the table LocalEP--0x%2x to RemoteEP--0x%2x with ClientCluster--0x%4x  >",
              LocalEP, RemoteEP, ClientCluster);
       
         break;
       }
     }
   }
}



EmberStatus initAccelerator(int8u verbose){
  if (verbose) logPrintln("Resetting Accelerator...");
  MMA8452Q_reset(); // Reset the device to ensure known configuration
  
  if ( MMA8452Q_isPresent() == FALSE ){
    if (verbose) logPrintln("Accelerator NOT found");
    return EMBER_ERR_FATAL;
  }
  
  // Must be in standby to change registers
  // in STANDBY already after RESET
  //MMA8452Q_setActive(FALSE);

  // Set up the full scale range to 2, 4, or 8g.
  if (verbose) logPrintln("Setting scale �%dg", acclCfg.maxG);
  MMA8452Q_setScale(acclCfg.maxG);
    
  // setup CTRL_REG1
  
  // Print user configuration
  int32s dr_usec = MMA8452Q_getDataRateInUS(acclCfg.ActiveDataRate);
  if (verbose) logPrintln("Setting Active data rate %d.%dms", dr_usec/1000L, dr_usec%1000L);
  
  //Set device's data rate while in wake mode
  MMA8452Q_setActiveDataRate (acclCfg.ActiveDataRate);

  
  // Print user configuration
  dr_usec = MMA8452Q_getDataRateInUS(acclCfg.SleepDataRate);
  if (verbose) logPrintln("Setting Sleep data rate (sampling period) %d.%dms", dr_usec/1000L, dr_usec%1000L);
  
  //Set device's data rate while in sleep mode
  MMA8452Q_setSleepDataRate (acclCfg.SleepDataRate);

  // Set LNOISE_BIT 
  MMA8452Q_setLowNoise(acclCfg.LowNoiseFlag);
    
  // Set Fast/Normal read mode to read 8/12bits per xyz instead of 12bits
  MMA8452Q_setFastRead(acclCfg.FastModeFlag);
  
    
  // setup CTRL_REG2
  // set Oversample mode in wake mode
  MMA8452Q_setOSMode (acclCfg.OSmode);

  // set Oversample mode in sleep
  MMA8452Q_setSOSMode (acclCfg.sOSmode);

  
  // Enable/Disable Auto-SLEEP
  MMA8452Q_setAutoSleep(FALSE);
  
   
  // setup CTRL_REG3
  //No other configuration needed in this application

  // Enable Disable Orientation detection
  MMA8452Q_detectOrientation(acclCfg.OrientFlag);
  
  // Activate device for configuration to take effect
  MMA8452Q_setActive(TRUE);
  
  int8u odr = MMA8452Q_getActiveDataRate();
    
  //Wait Turn on Time
  int32s dr_msec = MMA8452Q_getTurnOnTimeUS( odr )/1000L ;
  if (verbose) logPrint("Waiting for %d ms ... ", dr_msec);
  waitSleeping( dr_msec );
  if (verbose) logPrintln("Done.");
  
  // Print configuration
  dr_usec = MMA8452Q_getDataRateInUS( odr );
  if (verbose) logPrintln("Active data rate has been set to (sampling period): %d.%dms", dr_usec/1000L, dr_usec%1000L);
  
  // Check configuration
  // To Do... or change
  MMA8452Q_showRegister (MMA8452Q_getRegister(MMA8452Q_RA_CTRL_REG1), MMA8452Q_RA_CTRL_REG1);
  MMA8452Q_showRegister (MMA8452Q_getRegister(MMA8452Q_RA_XYZ_DATA_CFG),MMA8452Q_RA_XYZ_DATA_CFG);
  
  // Disable device to save power. Actual sampling will be determined by master device
  MMA8452Q_setActive(FALSE);
  if (verbose) logPrintln("Accelerator has been disabled to save power. It will be activated when needed.");
  return EMBER_SUCCESS;
}//End of InitAccelerator