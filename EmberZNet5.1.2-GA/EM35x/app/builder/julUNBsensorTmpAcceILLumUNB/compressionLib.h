#ifndef COMPRESSION_LIB_H
#define COMPRESSION_LIB_H
#include "typeLib.h"

/*
Author: Ryan MacDonald
Description: deltaCompress uses Francesco Marcelloni's and Massimo 
Vecchio's paper on simple compression. It combines delta coding with
Huffman.

INPUTS:
	delta 				the difference between the previous sample and the current sample
	bitCount			returned as the number of bits found within the encoded delta
	
OUTPUTS:
	encoded data
*/
uint32_t deltaCompress(short delta,uint32_t *bitCount);

/*
Author: Ryan MacDonald
Description: deltaDecompress uses Francesco Marcelloni's and Massimo 
Vecchio's paper on simple compression. It reverses the compression done by deltaCompress

INPUTS:
    cStream				stream data unpacked from the packet
    dStream       	    		location of the decompressed samples
	
OUTPUTS:
    number of samples extracted (NOTE: will be -1 if it attempted to extract more than dStream.length)
*/
int deltaDecompress(CompressedStream cStream, DecompressedStream dStream);

/*
Author: Ryan MacDonald
Description: packData takes a data point of bit length nBit and inserts the data 
  into compression stream

INPUTS:
    data                 compressed signal held within the lease significant bits
    nBit                 number of useful bits within data
    cStream              local stream that the data is getting compressed into
    
OUTPUTS:
    status of operation  (0 success, -1 compression buffer not long enough)
    
WARNINGS: the data will be input up to the point where it no longer fits into the buffer.
  This being said the caller should handle this case differently and if at all possible
  there should always be extra space so this case does not happen  
*/
int packData(uint32_t data, int nBit, CompressedStream * cStream);

#endif //COMPRESSION_LIB_H
