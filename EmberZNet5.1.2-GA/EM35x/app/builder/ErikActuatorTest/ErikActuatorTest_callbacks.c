//

// This callback file is created for your convenience. You may add application code
// to this file. If you regenerate this file over a previous version, the previous
// version will be overwritten and any code you have added will be lost.

#include "app/framework/include/af.h"
#include "hal/ece/haldrvI2C.h"
#include "hal/ece/haldrvRelay.h"
#include "hal/ece/haldrvUtils.h"

int16u mains = 0;

// Custom event stubs. Custom events will be run along with all other events in the
// application framework. They should be managed using the Ember Event API
// documented in stack/include/events.h

// Event control struct declarations
EmberEventControl EP1configureReportingControl;
EmberEventControl buttonEventControl;
EmberEventControl AnnunceEventControl;

// Event function forward declarations
void EP1configureReportingEvent(void);
void buttonEventHandler(void);
void AnnunceEvent(void);
void adcEvent(void);

// Event function stubs
void EP1configureReportingEvent(void) { }
void buttonEventHandler(void) { }
void AnnunceEvent(void) { }

/** @brief Main Tick
 *
 * Whenever main application tick is called, this callback will be called at
 * the end of the main tick execution.
 *
 */
void emberAfMainTickCallback(void)
{
  // Stall before running
  if (mains < 100) {
    mains++;
    return;
  } else if (mains > 100) {
    return;
  }
  
  if (mains==100) {
    logPrintln("MAIN");
    halCommonDelayMilliseconds(1000); halResetWatchdog();

    logPrintln("reset");
    resetRelay();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();

    logPrintln("enable all");
    enableAllRelays();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();

    logPrintln("disable all");
    disableAllRelays();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();

    logPrintln("set A and F on");
    setRelay(relayA, TRUE, 0);
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    setRelay(relayF, TRUE, 0);
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();

    logPrintln("set A and F off");
    setRelay(relayA, FALSE, 0);
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    setRelay(relayF, FALSE, 0);
    halCommonDelayMilliseconds(1000); halResetWatchdog();
    halCommonDelayMilliseconds(1000); halResetWatchdog();

    logPrintln("Turn on B, C, D, E, G, H");
    multRelay relays[] = { relayB, relayC, relayD, 
                           relayE, relayG, relayH };
    turnOnRelays(relays,6);
    mains++;
  }
}


/** @brief emberAfHalButtonIsrCallback
 *
 *
 */
// Hal Button ISR Callback
// This callback is called by the framework whenever a button is pressed on the 
// device. This callback is called within ISR context.
void emberAfHalButtonIsrCallback(int8u button, int8u state) {
}


/** @brief Cluster Init
 *
 * This function is called when a specific cluster is initialized. It gives
 * the application an opportunity to take care of cluster initialization
 * procedures. It is called exactly once for each endpoint where cluster is
 * present.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 */
void emberAfClusterInitCallback(int8u endpoint,
                                EmberAfClusterId clusterId)
{
}

/** @brief External Attribute Write
 *
 * This function is called whenever the Application Framework needs to write
 * an attribute which is not stored within the data structures of the
 * Application Framework itself. One of the new features in Version 2 is the
 * ability to store attributes outside the Framework. This is particularly
 * useful for attributes that do not need to be stored because they can be
 * read off the hardware when they are needed, or are stored in some central
 * location used by many modules within the system. In this case, you can
 * indicate that the attribute is stored externally. When the framework needs
 * to write an external attribute, it makes a call to this callback.
       
 * This callback is very useful for host micros which need to store attributes
 * in persistent memory. Because each host micro (used with an Ember NCP) has
 * its own type of persistent memory storage, the Application Framework does
 * not include the ability to mark attributes as stored in flash the way that
 * it does for Ember SoCs like the EM35x. On a host micro, any attributes that
 * need to be stored in persistent memory should be marked as external and
 * accessed through the external read and write callbacks. Any host code
 * associated with the persistent storage should be implemented within this
 * callback.
        All of the important information about the attribute
 * itself is passed as a pointer to an EmberAfAttributeMetadata struct, which
 * is stored within the application and used to manage the attribute. A
 * complete description of the EmberAfAttributeMetadata struct is provided in
 * app/framework/include/af-types.h.
        This function assumes that the
 * application is able to write the attribute and return immediately. Any
 * attributes that require a state machine for reading and writing are not
 * candidates for externalization at the present time. The Application
 * Framework does not currently include a state machine for reading or writing
 * attributes that must take place across a series of application ticks.
 * Attributes that cannot be written immediately should be stored within the
 * Application Framework and updated occasionally by the application code from
 * within the emberAfMainTickCallback.
        If the application was
 * successfully able to write the attribute, it returns a value of
 * EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the application
 * was not able to write the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeWriteCallback(int8u endpoint,
                                                    EmberAfClusterId clusterId,
                                                    EmberAfAttributeMetadata * attributeMetadata,
                                                    int16u manufacturerCode,
                                                    int8u * buffer)
{
  return EMBER_ZCL_STATUS_FAILURE;
}

/** @brief External Attribute Read
 *
 * Like emberAfExternalAttributeWriteCallback above, this function is called
 * when the framework needs to read an attribute that is not stored within the
 * Application Framework's data structures.
        All of the important
 * information about the attribute itself is passed as a pointer to an
 * EmberAfAttributeMetadata struct, which is stored within the application and
 * used to manage the attribute. A complete description of the
 * EmberAfAttributeMetadata struct is provided in
 * app/framework/include/af-types.h
        This function assumes that the
 * application is able to read the attribute, write it into the passed buffer,
 * and return immediately. Any attributes that require a state machine for
 * reading and writing are not really candidates for externalization at the
 * present time. The Application Framework does not currently include a state
 * machine for reading or writing attributes that must take place across a
 * series of application ticks. Attributes that cannot be read in a timely
 * manner should be stored within the Application Framework and updated
 * occasionally by the application code from within the
 * emberAfMainTickCallback.
        If the application was successfully able
 * to read the attribute and write it into the passed buffer, it should return
 * a value of EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the
 * application was not able to read the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeReadCallback(int8u endpoint,
                                                   EmberAfClusterId clusterId,
                                                   EmberAfAttributeMetadata * attributeMetadata,
                                                   int16u manufacturerCode,
                                                   int8u * buffer)
{
  return EMBER_ZCL_STATUS_FAILURE;
}

/** @brief Finished
 *
 * This callback is fired when the network-find plugin is finished with the
 * forming or joining process.  The result of the operation will be returned
 * in the status parameter.
 *
 * @param status   Ver.: always
 */
void emberAfPluginNetworkFindFinishedCallback(EmberStatus status)
{
}

/** @brief Get Radio Power For Channel
 *
 * This callback is called by the framework when it is setting the radio power
 * during the discovery process. The framework will set the radio power
 * depending on what is returned by this callback.
 *
 * @param channel   Ver.: always
 */
int8s emberAfPluginNetworkFindGetRadioPowerForChannelCallback(int8u channel)
{
  return EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER;
}

/** @brief Join
 *
 * This callback is called by the plugin when a joinable network has been
 * found.  If the application returns TRUE, the plugin will attempt to join
 * the network.  Otherwise, the plugin will ignore the network and continue
 * searching.  Applications can use this callback to implement a network
 * blacklist.
 *
 * @param networkFound   Ver.: always
 * @param lqi   Ver.: always
 * @param rssi   Ver.: always
 */
boolean emberAfPluginNetworkFindJoinCallback(EmberZigbeeNetwork * networkFound,
                                             int8u lqi,
                                             int8s rssi)
{
  return TRUE;
}

/** @brief Configured
 *
 * This callback is called by the Reporting plugin whenever a reporting entry
 * is configured, including when entries are deleted or updated.  The
 * application can use this callback for scheduling readings or measurements
 * based on the minimum and maximum reporting interval for the entry.  The
 * application should return EMBER_ZCL_STATUS_SUCCESS if it can support the
 * configuration or an error status otherwise.  Note: attribute reporting is
 * required for many clusters and attributes, so rejecting a reporting
 * configuration may violate ZigBee specifications.
 *
 * @param entry   Ver.: always
 */
EmberAfStatus emberAfPluginReportingConfiguredCallback(const EmberAfPluginReportingEntry * entry)
{
  return EMBER_ZCL_STATUS_SUCCESS;
}

/** @brief Poll Completed
 *
 * This function is called by the End Device Support plugin after a poll is
 * completed.
 *
 * @param status Return status of a completed poll operation  Ver.: always
 */
void emberAfPluginEndDeviceSupportPollCompletedCallback(EmberStatus status)
{
}


