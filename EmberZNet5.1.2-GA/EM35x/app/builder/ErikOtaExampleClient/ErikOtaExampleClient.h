// This file is generated by AppBuilder. Please do not edit manually.
// 
//

// Enclosing macro to prevent multiple inclusion
#ifndef __APP_ERIKOTAEXAMPLECLIENT_H__
#define __APP_ERIKOTAEXAMPLECLIENT_H__


/**** Included Header Section ****/

/**** ZCL Section ****/
#define ZA_PROMPT "ErikOtaExampleClient"
#define ZCL_USING_BASIC_CLUSTER_SERVER
#define ZCL_USING_OTA_BOOTLOAD_CLUSTER_CLIENT
#define ZCL_USING_KEY_ESTABLISHMENT_CLUSTER_CLIENT
#define ZCL_USING_KEY_ESTABLISHMENT_CLUSTER_SERVER
#define EMBER_AF_DEFAULT_RESPONSE_POLICY_ALWAYS

/**** Cluster endpoint counts ****/
#define EMBER_AF_BASIC_CLUSTER_SERVER_ENDPOINT_COUNT 1
#define EMBER_AF_OTA_BOOTLOAD_CLUSTER_CLIENT_ENDPOINT_COUNT 1
#define EMBER_AF_KEY_ESTABLISHMENT_CLUSTER_CLIENT_ENDPOINT_COUNT 1
#define EMBER_AF_KEY_ESTABLISHMENT_CLUSTER_SERVER_ENDPOINT_COUNT 1
#ifndef EMBER_AF_GENERATE_CLI
#define EMBER_AF_GENERATE_CLI
#endif // EMBER_AF_GENERATE_CLI



/**** Security Section ****/
#define EMBER_KEY_TABLE_SIZE 6
#define EMBER_AF_HAS_SECURITY_PROFILE_SE_TEST

/**** Network Section ****/
#define EMBER_SUPPORTED_NETWORKS 1
#define EMBER_AF_NETWORK_INDEX_PRIMARY 0
#define EMBER_AF_DEFAULT_NETWORK_INDEX EMBER_AF_NETWORK_INDEX_PRIMARY
#define EMBER_AF_HAS_ROUTER_NETWORK
#define EMBER_AF_HAS_RX_ON_WHEN_IDLE_NETWORK
#define EMBER_AF_TX_POWER_MODE EMBER_TX_POWER_MODE_USE_TOKEN

/*** Bindings section ****/
#define EMBER_BINDING_TABLE_SIZE 2

/**** LED configuration ****/
#define EMBER_AF_HEARTBEAT_ENABLE
#define EMBER_AF_HEARTBEAT_LED BOARDLED1

/**** HAL Section ****/
#define ZA_CLI_FULL

/**** Callback Section ****/
#define EMBER_CALLBACK_UNUSED_PAN_ID_FOUND
#define EMBER_CALLBACK_JOINABLE_NETWORK_FOUND
#define EMBER_CALLBACK_SCAN_ERROR
#define EMBER_CALLBACK_FIND_UNUSED_PAN_ID_AND_FORM
#define EMBER_CALLBACK_START_SEARCH_FOR_JOINABLE_NETWORK
#define EMBER_CALLBACK_GET_FORM_AND_JOIN_EXTENDED_PAN_ID
#define EMBER_CALLBACK_SET_FORM_AND_JOIN_EXTENDED_PAN_ID
#define EMBER_CALLBACK_INITIATE_KEY_ESTABLISHMENT
#define EMBER_CALLBACK_INITIATE_INTER_PAN_KEY_ESTABLISHMENT
#define EMBER_CALLBACK_PERFORMING_KEY_ESTABLISHMENT
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_KEY_ESTABLISHMENT_CLUSTER_SERVER_INIT
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_KEY_ESTABLISHMENT_CLUSTER_SERVER_TICK
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_SERVER_COMMAND_RECEIVED
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_INITIATE_KEY_ESTABLISHMENT_REQUEST
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_EPHEMERAL_DATA_REQUEST
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_CONFIRM_KEY_DATA_REQUEST
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_TERMINATE_KEY_ESTABLISHMENT
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_KEY_ESTABLISHMENT_CLUSTER_SERVER_MESSAGE_SENT
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_CLIENT_COMMAND_RECEIVED
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_INITIATE_KEY_ESTABLISHMENT_RESPONSE
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_EPHEMERAL_DATA_RESPONSE
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_CONFIRM_KEY_DATA_RESPONSE
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_KEY_ESTABLISHMENT_CLUSTER_CLIENT_MESSAGE_SENT
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_KEY_ESTABLISHMENT_CLUSTER_CLIENT_DEFAULT_RESPONSE
#define EMBER_CALLBACK_KEY_ESTABLISHMENT_CLUSTER_KEY_ESTABLISHMENT_CLUSTER_SERVER_DEFAULT_RESPONSE
#define EMBER_CALLBACK_ZIGBEE_KEY_ESTABLISHMENT
#define EMBER_APPLICATION_HAS_ZIGBEE_KEY_ESTABLISHMENT_HANDLER
#define EMBER_CALLBACK_EZSP_ZIGBEE_KEY_ESTABLISHMENT
#define EZSP_APPLICATION_HAS_ZIGBEE_KEY_ESTABLISHMENT_HANDLER
#define EMBER_CALLBACK_INITIATE_PARTNER_LINK_KEY_EXCHANGE
#define EMBER_CALLBACK_PARTNER_LINK_KEY_EXCHANGE_REQUEST
#define EMBER_CALLBACK_PARTNER_LINK_KEY_EXCHANGE_RESPONSE
#define EMBER_CALLBACK_REGISTRATION_START
#define EMBER_CALLBACK_REGISTRATION_ABORT
#define EMBER_CALLBACK_KEY_ESTABLISHMENT
#define EMBER_CALLBACK_OTA_BOOTLOAD
#define EMBER_CALLBACK_OTA_BOOTLOAD_CLUSTER_OTA_BOOTLOAD_CLUSTER_CLIENT_INIT
#define EMBER_CALLBACK_OTA_BOOTLOAD_CLUSTER_OTA_BOOTLOAD_CLUSTER_CLIENT_TICK
#define EMBER_CALLBACK_OTA_CLIENT_INCOMING_MESSAGE_RAW
#define EMBER_CALLBACK_OTA_CLIENT_START
#define EMBER_CALLBACK_OTA_BOOTLOAD_CLUSTER_OTA_BOOTLOAD_CLUSTER_CLIENT_DEFAULT_RESPONSE
#define EMBER_CALLBACK_OTA_CLIENT_VERSION_INFO
#define EMBER_CALLBACK_OTA_CLIENT_DOWNLOAD_COMPLETE
#define EMBER_CALLBACK_OTA_CLIENT_BOOTLOAD
#define EMBER_CALLBACK_OTA_CLIENT_CUSTOM_VERIFY_CALLBACK
#define EMBER_CALLBACK_OTA_STORAGE_INIT
#define EMBER_CALLBACK_OTA_STORAGE_GET_COUNT
#define EMBER_CALLBACK_OTA_STORAGE_SEARCH
#define EMBER_CALLBACK_OTA_STORAGE_ITERATOR_FIRST
#define EMBER_CALLBACK_OTA_STORAGE_ITERATOR_NEXT
#define EMBER_CALLBACK_OTA_STORAGE_CLEAR_TEMP_DATA
#define EMBER_CALLBACK_OTA_STORAGE_WRITE_TEMP_DATA
#define EMBER_CALLBACK_OTA_STORAGE_GET_FULL_HEADER
#define EMBER_CALLBACK_OTA_STORAGE_GET_TOTAL_IMAGE_SIZE
#define EMBER_CALLBACK_OTA_STORAGE_READ_IMAGE_DATA
#define EMBER_CALLBACK_OTA_STORAGE_CHECK_TEMP_DATA
#define EMBER_CALLBACK_OTA_STORAGE_FINISH_DOWNLOAD
#define EMBER_CALLBACK_OTA_STORAGE_DRIVER_INIT
#define EMBER_CALLBACK_OTA_STORAGE_DRIVER_READ
#define EMBER_CALLBACK_OTA_STORAGE_DRIVER_WRITE
#define EMBER_CALLBACK_OTA_STORAGE_DRIVER_DOWNLOAD_FINISH
#define EMBER_CALLBACK_OTA_STORAGE_DRIVER_INVALIDATE_IMAGE
#define EMBER_CALLBACK_OTA_STORAGE_DRIVER_RETRIEVE_LAST_STORED_OFFSET
#define EMBER_CALLBACK_OTA_STORAGE_DRIVER_PREPARE_TO_RESUME_DOWNLOAD
#define EMBER_CALLBACK_EEPROM_INIT
#define EMBER_CALLBACK_EEPROM_NOTE_INITIALIZED_STATE
#define EMBER_CALLBACK_EEPROM_SHUTDOWN
/**** Debug printing section ****/

// Global switch
#define EMBER_AF_PRINT_ENABLE
// Individual areas
#define EMBER_AF_PRINT_CORE 0x0001
#define EMBER_AF_PRINT_APP 0x0002
#define EMBER_AF_PRINT_ATTRIBUTES 0x0004
#define EMBER_AF_PRINT_OTA_BOOTLOAD_CLUSTER 0x0008
#define EMBER_AF_PRINT_BITS { 0x0F }
#define EMBER_AF_PRINT_NAMES { \
  "Core",\
  "Application",\
  "Attributes",\
  "Over the Air Bootloading",\
  NULL\
}
#define EMBER_AF_PRINT_NAME_NUMBER 4


#define EMBER_AF_SUPPORT_COMMAND_DISCOVERY
/**** App plugins section ****/
#define EMBER_AF_PLUGIN_ADDRESS_TABLE
// User options for plugin Address Table
#define EMBER_AF_PLUGIN_ADDRESS_TABLE_SIZE 2
#define EMBER_AF_PLUGIN_ADDRESS_TABLE_TRUST_CENTER_CACHE_SIZE 2
#define EMBER_AF_PLUGIN_NETWORK_FIND
#define EMBER_AF_DISABLE_FORM_AND_JOIN_TICK
// User options for plugin Network Find
#define EMBER_AF_PLUGIN_NETWORK_FIND_CHANNEL_MASK 0x0318C800UL
#define EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER 3
#define EMBER_AF_PLUGIN_NETWORK_FIND_EXTENDED_PAN_ID { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#define EMBER_AF_PLUGIN_NETWORK_FIND_DURATION 5
#define EMBER_AF_PLUGIN_NETWORK_FIND_JOINABLE_SCAN_TIMEOUT_MINUTES 1
#define EMBER_AF_PLUGIN_KEY_ESTABLISHMENT
#define EZSP_APPLICATION_HAS_CBKE_HANDLERS
#define EMBER_AF_PLUGIN_PARTNER_LINK_KEY_EXCHANGE
// User options for plugin Partner Link Key Exchange
#define EMBER_AF_PLUGIN_PARTNER_LINK_KEY_EXCHANGE_TIMEOUT_SECONDS 5
#define EMBER_AF_PLUGIN_SMART_ENERGY_REGISTRATION
// User options for plugin Smart Energy Registration
#define EMBER_AF_PLUGIN_SMART_ENERGY_REGISTRATION_ESI_REDISCOVERY
#define EMBER_AF_PLUGIN_SMART_ENERGY_REGISTRATION_ESI_DISCOVERY_PERIOD 3
#define EMBER_AF_PLUGIN_OTA_BOOTLOAD
#define EMBER_AF_PLUGIN_OTA_CLIENT
// User options for plugin OTA Bootload Cluster Client
#define EMBER_AF_PLUGIN_OTA_CLIENT_AUTO_START
#define EMBER_AF_PLUGIN_OTA_CLIENT_SET_IMAGE_STAMP
#define EMBER_AF_PLUGIN_OTA_CLIENT_QUERY_DELAY_MINUTES 5
#define EMBER_AF_PLUGIN_OTA_CLIENT_QUERY_ERROR_THRESHOLD 10
#define EMBER_AF_PLUGIN_OTA_CLIENT_DOWNLOAD_DELAY_MS 0
#define EMBER_AF_PLUGIN_OTA_CLIENT_DOWNLOAD_ERROR_THRESHOLD 10
#define EMBER_AF_PLUGIN_OTA_CLIENT_UPGRADE_WAIT_THRESHOLD 10
#define EMBER_AF_PLUGIN_OTA_CLIENT_SERVER_DISCOVERY_DELAY_MINUTES 10
#define EMBER_AF_PLUGIN_OTA_CLIENT_RUN_UPGRADE_REQUEST_DELAY_MINUTES 10
#define EMBER_AF_PLUGIN_OTA_CLIENT_PAGE_REQUEST_SIZE 1024
#define EMBER_AF_PLUGIN_OTA_CLIENT_PAGE_REQUEST_TIMEOUT_SECONDS 5
#define EMBER_AF_PLUGIN_OTA_CLIENT_VERIFY_DELAY_MS 10
#define EMBER_AF_PLUGIN_OTA_CLIENT_SIGNER_EUI0 { 0x00, 0x0D, 0x6F, 0x00, 0x00, 0x19, 0x8B, 0x36 }
#define EMBER_AF_PLUGIN_OTA_CLIENT_SIGNER_EUI1 { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#define EMBER_AF_PLUGIN_OTA_CLIENT_SIGNER_EUI2 { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#define EMBER_AF_PLUGIN_OTA_CLIENT_POLICY
#define CUSTOMER_APPLICATION_VERSION EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_FIRMWARE_VERSION
// User options for plugin OTA Bootload Cluster Client Policy
#define EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_IMAGE_TYPE_ID 0
#define EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_FIRMWARE_VERSION 1
#define EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_HARDWARE_VERSION 0
#define EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_EBL_VERIFICATION
#define EMBER_AF_PLUGIN_OTA_CLIENT_POLICY_DELETE_FAILED_DOWNLOADS
#define EMBER_AF_PLUGIN_OTA_COMMON
#define EMBER_AF_PLUGIN_OTA_STORAGE_COMMON
#define EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE
#define EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE_EEPROM
// User options for plugin OTA Simple Storage EEPROM Driver
#define EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE_EEPROM_SOC_BOOTLOADING_SUPPORT
#define EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE_EEPROM_STORAGE_START 0
#define EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE_EEPROM_STORAGE_END 204800
#define EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE_EEPROM_READ_MODIFY_WRITE_SUPPORT
#define EMBER_AF_PLUGIN_OTA_STORAGE_SIMPLE_EEPROM_DOWNLOAD_OFFSET_SAVE_RATE 1024
#define EMBER_AF_PLUGIN_EEPROM
// User options for plugin EEPROM
#define EMBER_AF_PLUGIN_EEPROM_PARTIAL_WORD_STORAGE_COUNT 2


// Custom macros
#ifdef APP_SERIAL
#undef APP_SERIAL
#endif
#define APP_SERIAL 0
#ifdef EMBER_ASSERT_SERIAL_PORT
#undef EMBER_ASSERT_SERIAL_PORT
#endif
#define EMBER_ASSERT_SERIAL_PORT 0
#ifdef EMBER_AF_BAUD_RATE
#undef EMBER_AF_BAUD_RATE
#endif
#define EMBER_AF_BAUD_RATE 19200
#ifdef EMBER_SERIAL0_MODE
#undef EMBER_SERIAL0_MODE
#endif
#define EMBER_SERIAL0_MODE EMBER_SERIAL_FIFO
#ifdef EMBER_SERIAL0_RX_QUEUE_SIZE
#undef EMBER_SERIAL0_RX_QUEUE_SIZE
#endif
#define EMBER_SERIAL0_RX_QUEUE_SIZE 128
#ifdef EMBER_SERIAL0_TX_QUEUE_SIZE
#undef EMBER_SERIAL0_TX_QUEUE_SIZE
#endif
#define EMBER_SERIAL0_TX_QUEUE_SIZE 128
#ifdef EMBER_SERIAL0_BLOCKING
#undef EMBER_SERIAL0_BLOCKING
#endif
#define EMBER_SERIAL0_BLOCKING


#endif // __APP_ERIKOTAEXAMPLECLIENT_H__
