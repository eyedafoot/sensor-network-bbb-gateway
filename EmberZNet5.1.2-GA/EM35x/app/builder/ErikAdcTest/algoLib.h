#ifndef ALGO_LIB_H
#define ALGO_LIB_H

#ifndef ARM_MATH_CM3
  #define ARM_MATH_CM3
#endif
#include "arm_math.h"

/*
Author: Ryan MacDonald
Description: initPLL starts the required standings of the PI and I controllers

INPUTS:
	pi_kP, pi_kI		constant for the proportional and the integral constants of the PI controller

OUTPUTS:

*/
void initPLL(q31_t pi_kP, q31_t pi_kI);

/*
Author: Ryan MacDonald
Description: initPLL starts the required standings of the PI and I controllers

INPUTS:
	phase1, phase2			array of size 2 with the present time 3 phase values
	prevCosWT, prevSinWT	cos and sin of previous wt
	currentWT				returns the output of the PLL or the current wt

OUTPUTS:
	status 		1 incorrect input, 0 OK, and all other errors
*/
int wtPLL(q15_t phase1, q15_t phase2, q31_t prevCosWT, q31_t prevSinWT, q31_t *currentWT);

/*
Author: Ryan MacDonald
Description: dqTransform takes two or three phases of current, voltage etc. and computes the dq transform if there is only two phases 
	the third phase is found by using KVL and KCL:
		i1 + i2 + i3 = 0   AND   v1 + v2 + v3 = 0

INPUTS:
	threePhaseValues	array of size 2 or 3 with the current 3 phase values
	bufferSize			number of phases read
	wt					current phase and time
	
OUTPUTS:
*/
void dqTransformPowInv(q31_t wt, q31_t *prevSinWT, q31_t *prevCosWT, q31_t p1, q31_t p2, q31_t *pD, q31_t *pQ);

/*
*/
void clarkeTransform(q31_t p1, q31_t p2, q31_t *alpha, q31_t *beta);

/*
*/
void dqTransformPowVar(q31_t wt, q31_t *prevSinWT, q31_t *prevCosWT, q31_t alpha, q31_t beta, q31_t *pD, q31_t *pQ);

/*
Author: Ryan MacDonald
Description: phaseCalculator finds the current samples cos and sin of the phases
PRIVATE FUNCTION

INPUTS:
	phase1, phase2		array of size 2 with the present time 3 phase values
	sinG				returned result for the angle over current samples
	cosG				returned result for the angle over current samples

OUTPUTS:
	status 		1 incorrect input, 0 OK, and all other errors
*/
int phaseCalculator(q15_t phase1, q15_t phase2, q31_t *sinG, q31_t *cosG);

/*
Author: Ryan MacDonald
Description: initFaultDetection establishes the algorithms parameters and starts
transform initialization

INPUTS:
					
	
OUTPUTS:
*/
arm_status initFaultDetection(arm_rfft_instance_q15 *pRFFT_q15, arm_cfft_radix4_instance_q15 *pCFFT_q15, uint32_t N, uint32_t ifftFlagR, uint32_t bitReverseFlag);

/*
Author: Ryan MacDonald
Description: detectFault takes n samples of the dq transform and returns if a fault is present

INPUTS:
	dqTransformData 	power signal in the dq domain
	
OUTPUTS:
	1 if fault is found, 0 if no fault, else error
*/
int detectFault(q31_t *dqTransformData,int dataLength);

#endif