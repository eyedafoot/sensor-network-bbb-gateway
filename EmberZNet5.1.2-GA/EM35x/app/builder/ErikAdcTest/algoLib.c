#ifndef ARM_MATH_CM3
  #define ARM_MATH_CM3
#endif
#include "arm_math.h"
#include "algoLib.h"
/*for full description please use the header file*/
//scale factor of 1/(2*sqrt(8)) from equation reduction for beta
#define PHASE_SCALE_Q15_COS		0x16A1
//scale factor of sqrt(3)/(2*sqrt(8)) from equation reduction for alpha
#define PHASE_SCALE_Q15_SIN  	0x2731
//phase shift that moves the current angle by 2pi/3 (DQ transform requires)
#define PHASE_SHIFT_Q31 0x55555555
//factor of sqrt(2/3) required by DQ transform
#define DQ_SCALE_Q31 0x6882F5C0
#define BETA_FACTOR_Q15 0x49E7
//global objects
arm_pid_instance_q31 pIControl;
q31_t controlAccum = 0;
q63_t acc;

void dqTransformPowInv(q31_t wt, q31_t *prevSinWT, q31_t *prevCosWT, q31_t p1, q31_t p2, q31_t *pD, q31_t *pQ){
    q31_t tempBuff[4], p3;
    q63_t tempDQBuffer;  
    q31_t cosWTUp, sinWTUp;
    q31_t cosWTDown, sinWTDown;
    
    
    //find the angles for the DQ transform
    arm_sin_cos_q31(wt,prevSinWT,prevCosWT);
    arm_sin_cos_q31(wt+PHASE_SHIFT_Q31,&sinWTUp,&cosWTUp);
    arm_sin_cos_q31(wt-PHASE_SHIFT_Q31,&sinWTDown,&cosWTDown);
    
    
    //calculate DQ format with buffer of 2 bits due to extra adds
    p1 = (p1)<<15;
    p2 = (p2)<<15;
    p3 = -(p1 + p2);
    tempBuff[0] = (*prevCosWT)>>1;
    tempBuff[1] = cosWTDown>>1;
    tempBuff[2] = cosWTUp>>1;
    tempDQBuffer = (q63_t)(tempBuff[0])*(p1);
    tempDQBuffer += (q63_t)(tempBuff[1])*(p2);
    tempDQBuffer += (q63_t)(tempBuff[2])*(p3);
    tempDQBuffer = (tempDQBuffer>>31)*(DQ_SCALE_Q31>>2);
    (*pD) = ((q31_t)(tempDQBuffer>>31u));
    (*pD) = (*pD)*9 + (q31_t)(((q63_t)(*pD)*(0x662DB61C))>>31);
    tempBuff[0] = (*prevSinWT)>>1;
    tempBuff[1] = sinWTDown>>1;
    tempBuff[2] = sinWTUp>>1;
    tempDQBuffer = (q63_t)(tempBuff[0])*(p1);
    tempDQBuffer += (q63_t)(tempBuff[1])*(p2);
    tempDQBuffer += (q63_t)(tempBuff[2])*(p3);
    tempDQBuffer = (tempDQBuffer>>31)*(DQ_SCALE_Q31>>2);
    (*pQ) = (-(q31_t)(tempDQBuffer>>31u));
    (*pQ) = (*pQ)*9 + (q31_t)(((q63_t)(*pQ)*(0x662DB61C))>>31);
}

void initPLL(q31_t pi_kP, q31_t pi_kI){
  //ensure the gains are initialized
  pIControl.Kp = pi_kP;
  pIControl.Ki = pi_kI;
  pIControl.Kd = 0;
  arm_pid_init_q31(&pIControl,1);
}

q31_t satVal_q31(q31_t value,q31_t maxValue){
  if(value > maxValue) return maxValue;
  else if(value < (-maxValue)) return -maxValue;
  return value;
}

q31_t satAdd_q31(q31_t x, q31_t y){
  q31_t z = x + y;
  if(x>=0 && y>=0 && z<0) return 0x7FFFFFFF;
  else if(x<=0 && y<=0 && z>0) return -(0x7FFFFFFF);
  return z;
}
  
q63_t satVal_q63(q63_t value, q63_t maxValue){
  if(value > maxValue) return maxValue;
  else if(value < (-maxValue)) return (-maxValue);
  return value;
}

int wtPLL(q15_t phase1, q15_t phase2, q31_t prevCosWT, q31_t prevSinWT, q31_t *currentWT){
	q63_t temp;
        q31_t sinG, cosG, controllerInput, prevState;
	int status = 0;
	
	//find the input to the PI controller
	status = phaseCalculator(phase1,phase2,&sinG,&cosG);
	
        //create controller input of q30 and saturate it before the input
        temp = satVal_q63((q63_t)(sinG)*(prevCosWT),0x3FFFFFFFFFFFFFFF) - satVal_q63((q63_t)(cosG)*(prevSinWT),0x3FFFFFFFFFFFFFFF);
        controllerInput = (q31_t)(temp>>32u); //one extra bit shift THIS IS THE ERROR TO THE CONTROLLER
    
        //input the values through the PI and I controllers
        prevState = controllerInput;
        // acc = A0 * x[n]
        acc = (q63_t) pIControl.A0 * controllerInput;
        // acc += A1 * x[n-1]
        acc += (q63_t) pIControl.A1 * pIControl.state[0];
        // convert output to 1.31 format to add y[n-1]
        controllerInput = (q31_t) (acc >> 31u);
        // out += y[n-1]
        controllerInput = satAdd_q31(pIControl.state[2],controllerInput);
        // Update state
        pIControl.state[0] = prevState;
        pIControl.state[2] = controllerInput;        

	//iController is unbounded and this is corrected simply by the cyclical pattern of wt
        controlAccum += controllerInput;
        (*currentWT) = controlAccum;
        
	//update the caller on the status
	return status;
}

/*
Author: Ryan MacDonald
Description: phaseCalculator finds the current samples cos and sin of the phases
PRIVATE FUNCTION

INPUTS:
	phase1, phase2		array of size 2 with the present time 3 phase values
	sinG				returned result for the angle over current samples
	cosG				returned result for the angle over current samples

OUTPUTS:
	status 		1 incorrect input, 0 OK, and all other errors
*/
int phaseCalculator(q15_t phase1, q15_t phase2, q31_t *sinG, q31_t *cosG){
	//start status as okay and change when there is an error
	int status = 0; 
	q31_t denominator;
	
	//move the phase values down to q14 (divide by 2) due to the bit limit and load into q31_t
	q31_t p1 = phase1 >> 1;
	q31_t p2 = phase2 >> 1;
	
        if(arm_sqrt_q31(p1*p1 + p1*p2 + p2*p2,&denominator) != ARM_MATH_SUCCESS){
		status = -1;
	}
	else{
                (*sinG) = satVal_q31(((p1<<17)/(denominator>>15))*PHASE_SCALE_Q15_SIN,0x3FFFFFFF);
		(*cosG) = satVal_q31((((p1 + (p2<<1))<<15)/(denominator>>17))*PHASE_SCALE_Q15_COS,0x3FFFFFFF);
                (*sinG) = (*sinG) << 1; 
		(*cosG) = (*cosG) << 1;
	}
	
	return status;
}

arm_status initFaultDetection(arm_rfft_instance_q15 *pRFFT_q15, arm_cfft_radix4_instance_q15 *pCFFT_q15, uint32_t N, uint32_t ifftFlagR, uint32_t bitReverseFlag){
  arm_status status = arm_rfft_init_q15(pRFFT_q15,pCFFT_q15,N,ifftFlagR,bitReverseFlag);
}

int detectFault(q31_t *dqTransformData,int dataLength){
  int hasFault = 0;
	
  return hasFault;
}

//PASS IN PHASE AS q15_t cast to q31_t (note that the alpha and beta is in q1.30_t)
void clarkeTransform(q31_t p1, q31_t p2, q31_t *alpha, q31_t *beta){
  (*alpha) = (p1<<15);
  (*beta) = (p1 + (p2<<1))*BETA_FACTOR_Q15;
}

//NOT TESTED YET
void dqTransformPowVar(q31_t wt, q31_t *prevSinWT, q31_t *prevCosWT, q31_t alpha, q31_t beta, q31_t *pD, q31_t *pQ){
    //find the angles for the DQ transform
    arm_sin_cos_q31(wt,prevSinWT,prevCosWT);
    (*pD) = (q31_t)(((q63_t)((*prevCosWT)*(alpha)) + ((q63_t)(*prevSinWT)*(beta)))>>31u);
    (*pQ) = (q31_t)(((q63_t)(-(*prevSinWT)*(alpha)) + ((q63_t)(*prevCosWT)*(beta)))>>31u);
}

int testDQTransform(){
  int status = 0;
  q31_t DQ_pD,DQ_pQ;
  q31_t pSin, pCos;
  
  //take transform
  dqTransform(876741811,&pSin,&pCos,12076<<2,-2892<<2,&DQ_pD,&DQ_pQ);
  
  //display result against expected
  logPrintln("Expected: 1359570946 & -2073325274 Received:D %d &Q %d\r\n",DQ_pD,DQ_pQ);  
              
  dqTransform(1117900772,&pSin,&pCos,12516<<2,-6880<<2,&DQ_pD,&DQ_pQ);
  
  logPrintln("Expected: -299769884 & -2446496966 Received:D %d &Q %d\r\n",DQ_pD,DQ_pQ);  
  
  dqTransform(-1403066516,&pSin,&pCos,-11080<<2,568<<2,&DQ_pD,&DQ_pQ);
  
  logPrintln("Expected: 2009653412 & -1407441554 Received:D %d &Q %d\r\n",DQ_pD,DQ_pQ); 
  
  return status;
}

int testPhaseCalculator(){
	int result = 0;
	//  pi/4 sin & cos = 0x5A82799A   		p1=0x5A82   p2=0x2120  
	//  pi/5 sin = 0x4B3C8C12 & cos = 0x678DDE6E   	p1=0x4B3C   p2=0x340F
	// -pi/3 sin = 0xEED9EBA1 & cos = 0x40000000	p1=0xEED9   p2=0x6ED9
	// 2pi/3 sin = 0x6ED9EBA1 & cos = 0xC0000000	p1=0x6ED9   p2=0xEED9
	q31_t cosTemp, sinTemp;
	//pi/4
	if(phaseCalculator(0x5A82,0x2120,&sinTemp,&cosTemp)){
		return -1;
	}
	if(0x5A8279!=(sinTemp>>8)||0x5A8279!=(cosTemp>>8)){
                logPrintln("Expected: 1518500250 & 1518500250 Received:S %d &C %d\r\n",sinTemp,cosTemp);
		result++;
	}
	//pi/5
	if(phaseCalculator(0x4B3C,0x340F,&sinTemp,&cosTemp)){
		return -1;
	}
	if(0x4B3C!=(sinTemp>>16)||0x678D!=(cosTemp>>16)){
                logPrintln("Expected: 1262259218 & 1737350766 Received:S %d &C %d\r\n",sinTemp,cosTemp);
		result++;
	}
	//-pi/3
	if(phaseCalculator(0x9127,0x6ED9,&sinTemp,&cosTemp)){
		return -1;
	}
	if(0xEED9!=(sinTemp>>16)||0x4000!=(cosTemp>>16)){
                logPrintln("Expected: -1859775393 & 1073741824 Received:S %d &C %d\r\n",sinTemp,cosTemp);
		result++;
	}
	//2pi/3
	if(phaseCalculator(0x6ED9,0x9127,&sinTemp,&cosTemp)){
		return -1;
	}
	if(0x6ED9!=(sinTemp>>16)||0xC000!=(cosTemp>>16)){
                logPrintln("Expected: 1859775393 & -1073741824 Received:S %d &C %d\r\n",sinTemp,cosTemp);
		result++;
	}
	
	// if result 
	return result;
}