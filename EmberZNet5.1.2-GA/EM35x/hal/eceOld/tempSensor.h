/** @file hal/ece/tempSensor.h
 * @brief Header for TMP100 temperature sensor.
 *
 * See @ref temperature for documentation.
 *
 * <!-- University of New Brunswick. 2014 -->
 */
#ifndef __TEMPERATURE_SENSOR_H__
#define __TEMPERATURE_SENSOR_H__

   
/* Comment out the following line inf you wanted to use Tristan's drivers */
#define UNBNODE_USE_DRIVER_V2   

   
/* TMP100 allows up to 8 devices in a single I2C bus
  -- First  Device I2C Address = 0x90
  -- Second Device I2C Address = 0x92
  -- ...
  -- Last   Device I2C Address = 0x9E
*/
// I2C address of the temperature sensor
#define TMP100_I2C_ADDR_0  0x90 //default
#define TMP100_I2C_ADDR_1  0x92
#define TMP100_I2C_ADDR_2  0x94
#define TMP100_I2C_ADDR_3  0x96
#define TMP100_I2C_ADDR_4  0x98
#define TMP100_I2C_ADDR_5  0x9A
#define TMP100_I2C_ADDR_6  0x9C
#define TMP100_I2C_ADDR_7  0x9E

#define TMP100_DEFAULT_ADDRESS   TMP100_I2C_ADDR_0
#define TMP100_I2C_ADDR(devNum)  (((devNum & 0x0F) << 1) | 0x90)
#define TMP100_INVALID_ADDR(devAddr) (((devAddr&0xFE) < TMP100_I2C_ADDR_0) || ((devAddr&0xFE) > TMP100_I2C_ADDR_7))

#define UNBNODE_TEMP_DEV_NUM 0 // Only TMP100 DEVICE ACTIVE ON UNB BOARD
   

typedef int8u TMP100_address;

/* I2C General Call
The TMP100 and TMP101 respond to the I2C General Call address (0000000)
if the eighth bit is 0. The device acknowledges the general call address and 
responds to commands in the second byte. 
-- If the second byte is 00000100, the TMP100 and TMP101 latch the status of 
their address pins, but will not reset. 
-- If the second byte is 00000110, the TMP100 and TMP101 latch the status of 
their address pins and reset their internal registers.

The general call can be used for: 
1. Checking the presence of TMP100 devices on the bus
2. Reseting the device
*/
#define TMP100_I2C_GENERAL_CALL 0x00

/** TMP100/TMP101 Internal Register addresses
Internal registers:
Address | Register      | Access     | Description
0x00    | Data          | READ ONLY  | Temperature Register (two bytes)
0x01    | Configuration | READ WRITE | Configuration Register (one Byte)
0x02    | TLow          | READ WRITE | Low Temperature Threshold (two bytes)
0x03    | THigh         | READ WRITE | High Temperature Threshold (two bytes) 

TMP100/101  Registers Addresses                                               */
#define TMP100_RA_DATA   0x00
#define TMP100_RA_CFG    0x01
#define TMP100_RA_TLOW   0x02
#define TMP100_RA_THIGH  0x03

/* Macros for Reading/writing internal registers*/
#define TMP100_RD_DATA(devAddr)       I2CREAD16 (devAddr, TMP100_RA_DATA)

#define TMP100_RD_CONFIG(devAddr)     I2CREAD08 (devAddr, TMP100_RA_CFG)
#define TMP100_WR_CONFIG(devAddr,val) I2CWRITE08(devAddr, TMP100_RA_CFG, val)

#define TMP100_RD_TLOW(devAddr)       I2CREAD16 (devAddr, TMP100_RA_TLOW)
#define TMP100_WR_TLOW(devAddr,val)   I2CWRITE16(devAddr, TMP100_RA_TLOW, val)

#define TMP100_RD_THIGH(devAddr)      I2CREAD16 (devAddr, TMP100_RA_THIGH)
#define TMP100_WR_THIGH(devAddr,val)  I2CWRITE16(devAddr, TMP100_RA_THIGH, val)



 /** 
* @brief Register configuration bits.
+--------+--------+--------+--------+--------+--------+--------+--------+
|OS/ALERT|   R1   |   R0   |   F1   |   F0   |   POL  |   TM   |   SD   |
+--------+--------+--------+--------+--------+--------+--------+--------+
  100/101  100/101  100/101  100/101  100/101    101      101    100/101
 
  OS/ALERT -- One Shot (W) or Alert bit (R in TMP101)
  R0/R1    -- Resolution bits (00-11 0.5-0.0625, 9-12 bits)
              BBBB BBBB.FFFF 0000 (B - integer part, F Fractional part)
           --                     | Conversion Times  | Conversion Rates
              R0/R1  |  Precision | Typical | Maximum | Rate
               00    |   9 bits   |   40 ms |   75 ms | 25 s/s
               01    |  10 bits   |   80 ms |  150 ms | 12 s/s
               10    |  11 bits   |  160 ms |  300 ms |  6 s/s
               11    |  12 bits   |  320 ms |  600 ms |  3 s/s
  SD       -- 1- Shutdown Mode (< 1uA consumption, Longer convertion times)
              0- Normal (continuous mode)
  TM       -- Thermostat Mode (TMP101 only). 0-Comparator Mode; 1-Interrupt Mode
  POL      -- Polarity bit (TMP101 only). ALERT pin polarity. 0-Active Low
  FAULT QUEUE (F1/F0) - To avoid noise to trigger an alert condition. 

Note: The Power-Up/Reset value of the configuration Register is: 0x80
*/

#define TMP100_CFG_BIT_SD    0    
#define TMP100_CFG_BIT_TM    1    
#define TMP100_CFG_BIT_POL   2
#define TMP100_CFG_BIT_F0    3
#define TMP100_CFG_BIT_F1    4
#define TMP100_CFG_BIT_R0    5
#define TMP100_CFG_BIT_R1    6
#define TMP100_CFG_BIT_OS    7
#define TMP100_CFG_BIT_ALERT 7

#define TMP100_CFG_SET_SD    0x01
#define TMP100_CFG_SET_TM    0x02
#define TMP100_CFG_SET_POL   0x04
#define TMP100_CFG_SET_F0    0x08
#define TMP100_CFG_SET_F1    0x10
#define TMP100_CFG_SET_R0    0x20
#define TMP100_CFG_SET_R1    0x40
#define TMP100_CFG_SET_OS    0x80

#define TMP100_MODE_CONTINUOUS 0x00  
#define TMP100_MODE_ONESHOT    0x01 // default
#define TMP100_CFG_MODE_MASK   0x01
  
#define TMP100_COMP_POL_ACTIVE_LOW  0x00  // default
#define TMP100_COMP_POL_ACTIVE_HIGH 0x01    
#define TMP100_COMP_POL_MASK   0x04    



/**
 * @brief Mask for changing or reading resolution info.
 */
#define RESOL_MASK       (TMP100_CFG_SET_R0|TMP100_CFG_SET_R1)

/**
 * @brief Mask for changing or reading fault queue info.
 */
#define FAULT_MASK       (TMP100_CFG_SET_F0|TMP100_CFG_SET_F1)

/**
 * @brief Macros to get resolution R0R1-code or value.
 */
#define TMP100_MIN_RESOLUTION 9  //default
#define TMP100_MAX_RESOLUTION 12
#define TMP100_DEFAULT_RESOLUTION TMP100_MIN_RESOLUTION

#define RESOLUTION_BITS(val) ((val-TMP100_MIN_RESOLUTION)<<TMP100_CFG_BIT_R0)
#define RESOLUTION(cfg)      (((cfg&RESOL_MASK)>>TMP100_CFG_BIT_R0)+TMP100_MIN_RESOLUTION)
#define DFLT_RESOL_BITS      RESOLUTION_BITS(TMP100_DEFAULT_RESOLUTION)

/**
 * @brief Macros to get fault tolerance F0/F1-code or value.
 */
#define TMP100_DEFAULT_TOL   1
#define FAULT_TOL_BITS(val)  (((val)>>1)<<TMP100_CFG_BIT_F0)
#define FAULT_TOL(cfg)       ((cfg&FAULT_MASK)>>TMP100_CFG_BIT_F0)
#define DFLT_TOL              FAULT_TOL(0)

/**
 * @brief Macro to extract the Polarity of ALERT Bit.
 */
#define ALERT_ACTIVE_LOW(cfg)  ((cfg&TMP100_CFG_SET_POL)==TMP100_COMP_POL_ACTIVE_LOW)

/**
 * @brief Typical conversion times for One Shot measurements.
 */
#define OS_TYP_CONV_TIME_9_BITS  40 // 40  milliseconds
#define OS_TYP_CONV_TIME_10_BITS (OS_TYP_CONV_TIME_9_BITS*2)  // 80 msec  
#define OS_TYP_CONV_TIME_11_BITS (OS_TYP_CONV_TIME_10_BITS*2) // 160 msec
#define OS_TYP_CONV_TIME_12_BITS (OS_TYP_CONV_TIME_11_BITS*2) // 320 msec

/**
 * @brief Macro to get typical conversion time for One Shot measurements.
 */
#define OS_TYP_CONV_TIME(resol) ((1<<(resol-9))*OS_TYP_CONV_TIME_9_BITS)

/**
 * @brief Maximum conversion times for One Shot measurements.
 */
#define OS_MAX_CONV_TIME_9_BITS  75 // 75  milliseconds
#define OS_MAX_CONV_TIME_10_BITS (OS_MAX_CONV_TIME_9_BITS*2)  // 150 msec  
#define OS_MAX_CONV_TIME_11_BITS (OS_MAX_CONV_TIME_10_BITS*2) // 300 msec
#define OS_MAX_CONV_TIME_12_BITS (OS_MAX_CONV_TIME_11_BITS*2) // 600 msec

/**
 * @brief Macro to get Maximum conversion time for One Shot measurements.
 */
#define OS_MAX_CONV_TIME(resol) ((1<<(resol-9))*OS_MAX_CONV_TIME_9_BITS)


/******************************************************************************/
/* Reading Temperature Functions                                              */
/******************************************************************************/
/** @brief Reads Temperature in float regardless TMP100 Operating Mode.  
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as a float number
 */
float getTemperature( int8u devNum );

/** @brief Reads Temperature in units of 1/100 oC regardless TMP100 Operating Mode
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an integer number containig the tempearture in 1/100 oC number. 
 * This format facilitates printing out temperature values and it is easier to interpret. 
 *   E.g.
 *   T = getTemperatureIn100sC(0);
 *   emberAfCorePrintln(" %d.%d oC", T/100, T%100);
 */
int16s getTemperatureIn100sC( int8u devNum );

/** @brief Reads Temperature in fixed point format regardless TMP100 Operating Mode.
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as a fixed point number. The High Byte holds the integer part
 * The Low Byte holds the fractional part. This format facilitates printing out 
 * the temperature values. E.g.
 *   fpT = getTemperatureFpoint(0);
 *   emberAfCorePrintln(" %d.%d oC", HIGH_BYTE(fpT), LOW_BYTE(fpT));
 */
int16s getTemperatureFpoint( int8u devNum );

/** @brief Reads Temperature in TMP100 format regardless TMP100 Operating Mode.
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s getTemperatureRaw( int8u devNum );

/** @brief Issues a OneShot temperature measurement and waits in sleep mode. 
 * 
 * The processor commands a measurement to the TMP100 device and goes to sleep 
 * mode until the measurement is ready. It uses the function 
 * halCommonIdleForMilliseconds(&ms) from Ember to go to sleep the waiting time
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s readTemperatureRawSleeping( int8u devNum );

/** @brief Issues a OneShot temperature measurement and waits in a busy waiting loop. 
 * 
 * The processor commands a measurement to the TMP100 device and waits awake, in  
 * busy waiting loop until the measurement is ready. It uses the function 
 * halCommonDelayMilliseconds(ms) from Ember to wait the required amount of time
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s readTemperatureRawBlocking( int8u devNum );

/** @brief Read temperature in the continuous mode of operation.
 * 
 * This is a Non-blocking function as the TMP100 device is in countinuous mode
 * rather than the oneshot mode. It only reads the TMP100 data register.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s readTemperatureRawNonBlocking( int8u devNum );


/******************************************************************************/
/*   Old (deprecated) Functions                                               */
/******************************************************************************/
#ifndef _TEMPERATURE_SENSOR_H

/** @brief Initialize Temprature Sensor
 *
 * Configure the onboard temprature sensor via the I2C bus.
 * This function should be called when the Temprature Sensor cluster is being
 * initialized.
 */
#define initTemprature() TMP100initDevice (UNBNODE_TEMP_DEV_NUM)

/** @brief Read Temprature
 *
 * This function returns the node's temprature in units of 1/100 C
 * This function is left here for compatibility with previous version of the driver
 */
#define readTemprature() getTemperatureIn100sC(UNBNODE_TEMP_DEV_NUM)

#endif


/******************************************************************************/
/*  Device Configuration Functions                                            */
/******************************************************************************/
void TMP100initDevice( int8u devNum );

int8u TMP100getConfig( int8u devNum );

int8u TMP100getMode( int8u devNum );

void TMP100setMode( int8u mode, int8u devNum );

void TMP100setContinuousMode( int8u devNum );

void TMP100setOneShotMode( int8u devNum );


void TMP100setResolution ( int8u Resolution, int8u devNum );

int8u TMP100getResolution( int8u devNum );


void TMP100setTTolerance( int8u Tolerance, int8u devNum );

int8u TMP100getTTolerance( int8u devNum );

int16s TMP100getTLowThresholdRaw( int8u devNum );

int8s TMP100getTLowThresholdRoundC( int8u devNum );

void TMP100setTLowThresholdRaw( int16s threshold, int8u devNum );

int8s TMP100getTHighThresholdRoundC( int8u devNum );


void TMP100setTHighThresholdRaw(int16s threshold, int8u devNum );

void TMP100setTConfig(int16s TLow, int16s THigh, int8u AlertPol, int8u devNum  );

void TMP100setConfig(int8u devConf, int16s TLow, int16s THigh, int8u devNum  );

/******************************************************************************/
/* Low level functions for trigering a reading an for directly read the data register*/
/******************************************************************************/
void TMP100triggerOneShotMeasurement( int8u devNum );

int16s TMP100getData(int8u devNum );



/******************************************************************************/
/*  Debug Functions                                                           */
/******************************************************************************/
/** @brief Shows the current TMP100 Configuration. 
 *  
 * This function displays the contents of the configuration register and the 
 * threshold registers using the function emberAfAppPrintln
 */
void TMP100showConfig ( int8u devNum );

/** @brief Shows the resolution (in bits) set in the configuration register. 
 *  
 * This function displays the number of bits (9, 10,11, or 12) of reslution used 
 * by the deviced. It displays the results with the help of the function 
 * emberAfAppPrintln
 */
void TMP100showMeasResol ( int8u devNum );

/** @brief Shows the delay  (in bits) set in the configuration register. 
 *  
 * This function displays the resolution in number of bits(9, 10,11, or 12) used 
 * by the device. It displays the results with the help of the function 
 * emberAfAppPrintln
 */
void TMP100showMeasDelay ( int8u devNum );


#endif
