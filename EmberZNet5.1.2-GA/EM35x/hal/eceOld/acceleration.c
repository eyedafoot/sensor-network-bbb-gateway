#include "app/framework/include/af.h"
#include "i2c.h"
#include "acceleration.h"

// I2C address of the acceleration sensor
#define ACCEL_ADDRESS       0x38

// Regester addresses
#define ACCEL_STATUS_REG    0x00
#define ACCEL_OUT_X_REG     0x01
#define ACCEL_OUT_Y_REG     0x03
#define ACCEL_OUT_Z_REG     0x05
#define ACCEL_WHO_AM_I_REG  0x0d
#define ACCEL_CTRL_1_REG    0x2a
#define ACCEL_CTRL_2_REG    0x2b

// Regester configuration bits
#define ACCEL_XDR     0x01
#define ACCEL_YDR     0x02
#define ACCEL_ZDR     0x04
#define ACCEL_ZYXDR   0x08
#define ACCEL_ACTIVE  0x01
#define ACCEL_RST     0x40

void initAcceleration(void)
{
  // Reset the sensor
  halI2CWriteint8u(ACCEL_ADDRESS, ACCEL_CTRL_2_REG, ACCEL_RST);
}

EmberStatus readAcceleration(int16s * x, int16s * y, int16s * z)
{
  // Activate the sensor
  halI2CWriteint8u(ACCEL_ADDRESS, ACCEL_CTRL_1_REG, ACCEL_ACTIVE);
  
  // Wait for new data
  int8s count = 16;
  int8u status = 0;
  while (--count > 0) {
    halCommonDelayMilliseconds(10);
    status = halI2CReadint8u(ACCEL_ADDRESS, ACCEL_STATUS_REG);
    if (status & ACCEL_ZYXDR) break;
  }
  if (!(status & ACCEL_ZYXDR)) return EMBER_ERR_FATAL;
  
  // Retrive current values from on on-board sensor
  int16s data[3];
  halI2CRead(ACCEL_ADDRESS, ACCEL_OUT_X_REG, (int8u*)&data, 6);
  if (x != NULL) *x = data[0] >> 4;
  if (y != NULL) *y = data[1] >> 4;
  if (z != NULL) *z = data[2] >> 4;
  
  // Turn the sensor back off;
  halI2CWriteint8u(ACCEL_ADDRESS, ACCEL_CTRL_1_REG, 0);
  return EMBER_SUCCESS;
}
