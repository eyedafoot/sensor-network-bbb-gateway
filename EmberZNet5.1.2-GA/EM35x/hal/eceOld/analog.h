#ifndef _ANALOG_SENSOR_H
#define _ANALOG_SENSOR_H

#define BURDEN_RESISTOR_VALUE 297L
#define DMA_BUFFER_SIZE 130 // ~ 4 cycles of a 60 Hz AC wave

/** @brief Initialize EM357 ADC
 *
 * Configure and calibrate the internal ADC.
 */
void halInternalInitAdc(void);

/** @brief Read Battery Voltage
 *
 * Returns the battery voltage by measuring it via a voltage divider circuit
 * attached to PB7 (ADC2). The resistor values for the voltage divider are
 *   R1 = 9.83 kOHM
 *   R2 = 5.14 kOHM
 *
 * Return value is in units of mV
 */
int16s readBattVoltage(void);

/** @brief Read AC Current
 *
 * Measure an AC current. For this function to work properly a current
 * transformer needs to be connected to ADC4 and ADC5. Voltage measurements
 * are taken diffrentially across the CT's burden resistor and then converted
 * to Irms.
 *
 * Because the time required to take all of the measurements will most likely
 * exceed the rundown time of the watchdog timer, this function returns
 * immidiatly. When the measurement operation is complete, the callback function
 * provided by the user will be called and passed the value.
 *
 * @param callback
 */
void readACCurrent(void (*callback)(int16u));

/** @brief Integer Square Root
 *
 * Calculated the square root of the provided value using integer arithmetic.
 * The return value will be rounded to the nearest integer.
 *
 * @param value
 */
int32u squareRoot(int32u value);

#endif
