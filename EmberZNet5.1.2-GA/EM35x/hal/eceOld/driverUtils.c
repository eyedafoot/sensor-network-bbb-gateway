#include "app/framework/include/af.h"
#include "i2c.h"
#include "driverUtils.h"


void waitBusy(int32u ms){

  halCommonDelayMilliseconds(ms);

}//End of waitBusyLoop

void waitSleeping(int32u ms){
  int32u timeMs;
  timeMs = ms;
 // while (timeMs > 0) halSleepForMilliseconds(&timeMs); /* This function is crashing the processor. Should investigate why*/
  while (timeMs > 0) halCommonIdleForMilliseconds(&timeMs);
}//End of waitSleeping

/******************************************************************************/
/*   Utility Functions                                                        */
/******************************************************************************/
float rawT2float (int16s rawT){
  return rawT/256.0f;
}//End of rawT2float

int16s rawT2100sC(int16s rawT){
  // Convert to units of 1/100 deg C
  return (int16s)(((int32s)(rawT >> 4) * 50L + 4) >> 3);
}// End of rawT2100sC

int16s rawT2fpoint (int16s rawT){
  int16s intT, fracT;
  intT = rawT & 0xFF00;    // get the  integer part of the raw temperature

  fracT = (rawT < 0)? -rawT  : rawT;  //the fraction wil always be a positive number
  
  // Make the conversion. 
  fracT = (((fracT >> 4) & 0x0F) * 50 + 4) >> 3; 
  
  // the integer part is in the MSB and the fraction is in the LSB
  return (intT | fracT);
  
 }// End of raw2fpoint

int8s rawT2int8s(int16s rawT){
  // int (T + 0.5). Q8 fornmat is used: 8 fractional bits and 8 integer bits
  return (int8s)((rawT + 128) >> 8); 
}// End of rawT2int8s

int16u rawTFrac( int16s rawT, int8s Resolution ){
  
  int16u fracT;
  fracT = (rawT < 0)? -rawT : rawT;  //the fraction will always be a positive number
  
  switch (Resolution) {
  case 12: 
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 5000L ) >> 3);
  case 11: 
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 500L ) >> 3);
  case 10:
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 50L ) >> 3);
  default: // 9 bits of resolution
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 5L ) >> 3);
  }
}

int8s rawTInt ( int16s rawT ){
  return HIGH_BYTE(rawT);
}

/******************************************************************************/
/*   Utility Functions for converting Accelerometer values from raw measuerements */
/******************************************************************************/
float rawAxis2float(int16s raw, int8u Scale){
  int8u FSx = (Scale & 0xF) >> 2; 
  return ((float)raw /(10.0 - FSx));
}//End of rawAxis2float

void rawAxes2float(const int16s rawAxes[], int8u Scale, float Axes[]){
  int8u FSx = (Scale & 0xF) >> 2; 
  Axes[0] = ((float)rawAxes[0] /(10.0 - FSx));
  Axes[1] = ((float)rawAxes[1] /(10.0 - FSx));
  Axes[2] = ((float)rawAxes[2] /(10.0 - FSx));
}//End of rawAxis2float

int16s getAxisIntegerPart (int16s rawAxis, int8u Scale){
  int8u FSx = (Scale & 0xF) >> 2; 
  return (rawAxis >> (10 - FSx));
}//End of getDataIntegerPart

int16u getAxisFractionPart (int16s rawAxis, int8u Scale){
  
  int8u FSx = (Scale & 0x0F) >> 2;
  int32u lastIntBit = BIT(10 - FSx);
  
  if (rawAxis & 0x8000) //negative number
    rawAxis = ~rawAxis + 1; // Two's complement
  
  // Extract the fractional part
  int32u tmp = (rawAxis & (lastIntBit - 1));
  
  // put in decimal values with 4 significant places after decimal point
  // 0x.800 => .5000
  tmp = (tmp *5000L)/(lastIntBit >> 1L);
  
  return (int16u)tmp;
  
}//End of getDataFractionPart

void print_uXtoX (int32s value, int32u partsOf){
  
  int32s fraction = value%partsOf;
  logPrint("%d.",value/10000);
  while (partsOf > 10){
    partsOf /= 10;
    if (fraction < partsOf) logPrint("0");
  }
  logPrint("%d",fraction);
}