/** @file hal/micro/cortexm3/em35x/board/ecEZNode.h
 * See @ref board for detailed documentation.
 *
 * <!-- Copyright 2013 Silicon Laboratories, Inc.                        *80*-->
 */

/** @addtogroup board
 *  @brief Functions and definitions specific to the breakout board.
 *
 * @note The file ecEZNode.h customized from dev0680.h and eznode.h files
 * for UNB's Ember sensor hardware.
 *
 * The file ecEZNode.h is the default BOARD_HEADER file used with the breakout
 * board of the development kit.
 *
 * The EM35x on a eceEZNode BoB has the following example GPIO configuration.
 * This board file and the default HAL setup reflects this configuration.
 * - PA0 - SC2MOSI
 * - PA1 - SC2MISO
 * - PA2 - SC2SCLK
 * - PA3 - SC2nSSEL
 * - PA4 - ADC4
 * - PA5 - ADC5
 * - PA6 - NC
 * - PA7 - Button 1 (IRQD pointed to PA7)
 *
 * - PB0 - Accelerometer Interrupt INT2 (IRQA pointed to PB0)
 * - PB1 - SC1SDA  (I2C data Line)
 * - PB2 - SC1SCL  (I2C clock Line)
 * - PB3 - Button 0 (IRQC pointed to PB3)
 * - PB4 - FLASHGNDCTRL (Dataflash EN)
 * - PB5 - LED 0. ACTIVITY
 * - PB6 - LED 1. HEARTBEAT LED
 * - PB7 - ADC2
 *
 * - PC0 - JTAG (JRST) INSIGHT PIN
 * - PC1 - ADC3
 * - PC2 - JTAG (JTDO) / SWO / TRACEDATA0
 * - PC3 - JTAG (JTDI) / TRACECLK
 * - PC4 - JTAG (JTMS) / SWDIO
 * - PC5 - RF FEM (CTX)
 * - PC6 - RF FEM (CPS)
 * - PC7 - RF FEM (CSD)
 *
 * The PCB has a general-purpose connector available. The pin connections to
 * the EM357 chip are illustrated in the following diagram. In addition, a
 * orientation reference is provided to illustrate acceleration sensor alignment
 *
 * /-------------  <- board edge
 * | [ GND  ]      <- connector pins
 * | [ ADC4 ]
 * | [ ADC5 ]
 * | [ ADC2 ]
 * | [ ADC3 ]       Acceleration measurement orientation
 * | [ SCL  ]       X <-O Z (+ve points up out of board)
 * | [ SDA  ]           |
 * | [ VCC  ]           v
 *                      Y
 *@{
 */

#ifndef __BOARD_H__
#define __BOARD_H__

/** @name Custom Baud Rate Definitions
 *
 * The following define is used with defining a custom baud rate for the UART.
 * This define provides a simple hook into the definition of
 * the baud rates used with the UART.  The baudSettings[] array in uart.c
 * links the BAUD_* defines with the actual register values needed for
 * operating the UART.  The array baudSettings[] can be edited directly for a
 * custom baud rate or another entry (the register settings) can be provided
 * here with this define.
 */
//@{
/**
 * @brief This define is the register setting for generating a baud of
 * 921600.  Refer to the EM35x datasheet's discussion on UART baud rates for
 * the equation used to derive this value.
 */
#define EMBER_SERIAL_BAUD_CUSTOM  13
//@} //END OF CUSTOM BAUD DEFINITIONS

/** @name LED Definitions
 *
 * The following are used to aid in the abstraction with the LED
 * connections.  The microcontroller-specific sources use these
 * definitions so they are able to work across a variety of boards
 * which could have different connections.  The names and ports/pins
 * used below are intended to match with a schematic of the system to
 * provide the abstraction.
 *
 * The ::HalBoardLedPins enum values should always be used when manipulating the
 * state of LEDs, as they directly refer to the GPIOs to which the LEDs are
 * connected.
 *
 * \b Note: LEDs 0 and 1 are on the sensor board.
 *
 * \b Note: LEDs 2 and 3 are not implemented in this board and simply redirect 
 * to LEDs 0 and 1 respectively
 */
//@{

/**
 * @brief Assign each GPIO with an LED connected to a convenient name.
 * ::BOARD_ACTIVITY_LED and ::BOARD_HEARTBEAT_LED provide a further layer of
 * abstraction on top of the 3 LEDs for verbose coding.
 */
enum HalBoardLedPins {
  BOARDLED0 = PORTB_PIN(5),
  BOARDLED1 = PORTB_PIN(6),
  BOARDLED2 = BOARDLED0,
  BOARDLED3 = BOARDLED1,
  BOARD_ACTIVITY_LED  = BOARDLED0,
  BOARD_HEARTBEAT_LED = BOARDLED1
};

/** @} END OF LED DEFINITIONS  */

/** @name Button Definitions
 *
 * The following are used to aid in the abstraction with the Button
 * connections.  The microcontroller-specific sources use these
 * definitions so they are able to work across a variety of boards
 * which could have different connections.  The names and ports/pins
 * used below are intended to match with a schematic of the system to
 * provide the abstraction.
 *
 * The BUTTONn macros should always be used with manipulating the buttons
 * as they directly refer to the GPIOs to which the buttons are connected.
 *
 * @note The GPIO number must match the IRQ letter
 */
//@{
/**
 * @brief The actual GPIO BUTTON0 is connected to.  This define should
 * be used whenever referencing BUTTON0.
 */
#define BUTTON0             PORTB_PIN(3)
/**
 * @brief The GPIO input register for BUTTON0.
 */
#define BUTTON0_IN          GPIO_PBIN
/**
 * @brief Point the proper IRQ at the desired pin for BUTTON0.
 */
#define BUTTON0_SEL()       do { GPIO_IRQCSEL = BUTTON0; } while(0)
/**
 * @brief The interrupt service routine for BUTTON0.
 */
#define BUTTON0_ISR         halIrqCIsr
/**
 * @brief The interrupt configuration register for BUTTON0.
 */
#define BUTTON0_INTCFG      GPIO_INTCFGC
/**
 * @brief The interrupt enable bit for BUTTON0.
 */
#define BUTTON0_INT_EN_BIT  INT_IRQC
/**
 * @brief The interrupt flag bit for BUTTON0.
 */
#define BUTTON0_FLAG_BIT    INT_IRQCFLAG
/**
 * @brief The missed interrupt bit for BUTTON0.
 */
#define BUTTON0_MISS_BIT    INT_MISSIRQC

/**
 * @brief The actual GPIO BUTTON1 is connected to.  This define should
 * be used whenever referencing BUTTON1.
 */
#define BUTTON1             PORTA_PIN(7)
/**
 * @brief The GPIO input register for BUTTON1.
 */
#define BUTTON1_IN          GPIO_PAIN
/**
 * @brief Point the proper IRQ at the desired pin for BUTTON1.
  */
#define BUTTON1_SEL()       do { GPIO_IRQDSEL = BUTTON1; } while(0)
/**
 * @brief The interrupt service routine for BUTTON1.
 */
#define BUTTON1_ISR         halIrqDIsr
/**
 * @brief The interrupt configuration register for BUTTON1.
 */
#define BUTTON1_INTCFG      GPIO_INTCFGD
/**
 * @brief The interrupt enable bit for BUTTON1.
 */
#define BUTTON1_INT_EN_BIT  INT_IRQD
/**
 * @brief The interrupt flag bit for BUTTON1.
 */
#define BUTTON1_FLAG_BIT    INT_IRQDFLAG
/**
 * @brief The missed interrupt bit for BUTTON1.
 */
#define BUTTON1_MISS_BIT    INT_MISSIRQD
//@} //END OF BUTTON DEFINITIONS


/** @name Packet Trace
 *
 * When ::PACKET_TRACE is defined, ::GPIO_PACFGH will automatically be setup by
 * halInit() to enable Packet Trace support on PA4 and PA5,
 * in addition to the configuration specified below.
 *
 * @note This define will override any settings for PA4 and PA5.
 */
//@{
/**
 * @brief This define does not equate to anything.  It is used as a
 * trigger to enable Packet Trace support on the breakout board (ecEZNode).
 */
#define PACKET_TRACE  // We do have PACKET_TRACE support
//@} //END OF PACKET TRACE DEFINITIONS

/** @name ENABLE_OSC32K
 *
 * When ENABLE_OSC32K is defined, halInit() will configure system
 * timekeeping to utilize the external 32.768 kHz crystal oscillator
 * rather than the internal 1 kHz RC oscillator.
 *
 * @note ENABLE_OSC32K is mutually exclusive with
 * ENABLE_ALT_FUNCTION_NTX_ACTIVE since they define conflicting
 * usage of GPIO PC6.
 *
 * On initial powerup the 32.768 kHz crystal oscillator will take a little
 * while to start stable oscillation. This only happens on initial powerup,
 * not on wake-from-sleep, since the crystal usually stays running in deep
 * sleep mode.
 *
 * When ENABLE_OSC32K is defined the crystal oscillator is started as part of
 * halInit(). After the crystal is started we delay for
 * OSC32K_STARTUP_DELAY_MS (time in milliseconds).  This delay allows the
 * crystal oscillator to stabilize before we start using it for system timing.
 *
 * If you set OSC32K_STARTUP_DELAY_MS to less than the crystal's startup time:
 *   - The system timer won't produce a reliable one millisecond tick before
 *     the crystal is stable.
 *   - You may see some number of ticks of unknown period occur before the
 *     crystal is stable.
 *   - halInit() will complete and application code will begin running, but
 *     any events based on the system timer will not be accurate until the
 *     crystal is stable.
 *   - An unstable system timer will only affect the APIs in system-timer.h.
 *
 * Typical 32.768 kHz crystals measured by Ember take about 400 milliseconds
 * to stabilize. Be sure to characterize your particular crystal's stabilization
 * time since crystal behavior can vary.
 */
//@{
#define OSC32K_STARTUP_DELAY_MS (0)

#if OSC32K_STARTUP_DELAY_MS > MAX_INT16U_VALUE
#error "OSC32K_STARTUP_DELAY_MS must fit in 16 bits."
#endif

/**
 * @brief This define does not equate to anything.  It is used as a
 * trigger to enable 32.768 kHz XTAL oscillator on the RCM plugged
 * into the Breakout board (dev0680).
 * Default is to disable 32.768 kHz XTAL and use 1 kHz RC oscillator instead.
 */
//#define ENABLE_OSC32K  // Enable 32.768 kHz osc instead of 1 kHz RC osc
//@} //END OF ENABLE OSC32K DEFINITIONS

/** @name DISABLE_SLEEPTMR_DEEPSLEEP
 *
 * When DISABLE_SLEEPTMR_DEEPSLEEP is defined, halSleep() will turn off the
 * internal 10kHz SlowRC used for the system-timer when entering deep sleep.
 * The result of doing this is called DeepSleep2 and will yield a lower current
 * consumption than with the 10kHz SlowRC enabled (which is called DeepSleep1).
 *
 * To still be able to do timed sleep operations and maintain the system-timer
 * across deep sleep while DISABLE_SLEEPTMR_DEEPSLEEP is defined, ENABLE_OSC32K
 * must also be defined causing the system-timer to switch to the external
 * 32kHz XTAL.
 */
//@{
/**
 * @brief This define does not equate to anything.  It is used as a
 * trigger to disable the internal 10kHz SlowRC oscillator.  The default is to
 * keep the 10kHz SlowRC enabled.
 */
//#define DISABLE_SLEEPTMR_DEEPSLEEP  // Disable internal 10kHz SlowRC
//@} //END OF DISABLE SLOWRC DEFINITIONS

/** @name DISABLE_OSC24M_BIAS_TRIM
 *
 * When DISABLE_OSC24M_BIAS_TRIM is defined, halInit() will disable adjusting
 * the 24 MHz oscillator's bias trim based on its corresponding Manufacturing
 * token, and instead utilize full bias at power up and deep sleep wakeups.
 * This should be utilized only if the Manufacturing token value proves to be
 * unreliable.
 */
//@{
/**
 * @brief This define does not equate to anything.  It is used as a
 * trigger to disable bias adjustment of the 24 MHz crystal oscillator on
 * the RCM plugged into the Breakout board (ecEZNode).
 * Default is to enable bias adjustment.
 */
//#define DISABLE_OSC24M_BIAS_TRIM  // Disable 24 MHz bias trim adjustment
//@} //END OF DISABLE OSC24M BIAS TRIM DEFINITIONS


/** @name ENABLE_ALT_FUNCTION_REG_EN
 *
 * When ENABLE_ALT_FUNCTION_REG_EN is defined, halInit() will enable the REG_EN
 * alternate functionality on PA7.  REG_EN is the special signal provided
 * by the EM35x's internal power controller which can be used to tell an
 * external power regulator when the EM35x is in deep sleep or not and as such
 * signal the external regulator to cut power.  This signal will override all
 * GPIO configuration and use of PA7.  When the alternate functionality is
 * not enabled, PA7 can be operated as a standard GPIO.
 */
//@{
/**
 * @brief This define does not equate to anything.  It is used as a
 * trigger to enable the REG_EN alternate function on PA7.
 * Default is to not enable REG_EN functionality on PA7.
 */
//#define ENABLE_ALT_FUNCTION_REG_EN
//@} //END OF ENABLE_ALT_FUNCTION_REG_EN DEFINITIONS


/** @name EEPROM_USES_SHUTDOWN_CONTROL
 *
 * When EEPROM_USES_SHUTDOWN_CONTROL is defined, logic is enabled in the
 * EEPROM driver which drives PB4 high upon EEPROM initialization.  In
 * Ember reference designs, PB4 acts as an EEPROM enable pin and therefore
 * must be driven high in order to use the EEPROM.  This option is intended
 * to be enabled when running app-bootloader on designs based on current
 * Ember reference designs.
 */
//@{
/**
 * @brief This define does not equate to anything.  It is used as a
 * trigger to enable the logic that drives PB4 high in the EEPROM driver.
 * Default is to leave this logic disabled.
 */
#define EEPROM_USES_SHUTDOWN_CONTROL
//@} //END OF EEPROM_USES_SHUTDOWN_CONTROL DEFINITIONS


/** @name DISABLE_INTERNAL_1V8_REGULATOR
 *
 * When DISABLE_INTERNAL_1V8_REGULATOR is defined, the internal regulator for
 * the 1.8 V supply (VREG_1V8) is disabled.  Disabling of VREG_1V8 will
 * prevent excess current draw.
 *
 * @note Disabling VREG_1V8 on devices that are not externally powered will
 * prevent the device from operating normally.
 *
 * The disabling occurs early in the board power up sequence so that current
 * consumption is always minimized and the configuration will be applied
 * across all power modes.
 *
 */
//@{
/**
 * @brief This define does not equate to anything.  It is used as a
 * trigger to disable the 1.8V regulator.
 */
//#define DISABLE_INTERNAL_1V8_REGULATOR
//@} //END OF DISABLE_INTERNAL_1V8_REGULATOR DEFINITION

/**
 * @brief External regulator enable/disable macro.
 */
#ifdef DISABLE_INTERNAL_1V8_REGULATOR
  #define CONFIGURE_VREG_1V8_DISABLE()                                         \
    VREG = ( ( VREG & (~(VREG_VREG_1V8_EN_MASK | VREG_VREG_1V8_TEST_MASK)) ) | \
           (0 << VREG_VREG_1V8_EN_BIT)                                       | \
           (1 << VREG_VREG_1V8_TEST_BIT)                                     )
#else
  #define CONFIGURE_VREG_1V8_DISABLE()
#endif


/** @name GPIO Configuration Definitions
 *
 * The following are used to specify the GPIO configuration to establish
 * when Powered (POWERUP_), and when Deep Sleeping (POWERDOWN_).  The reason
 * for separate Deep Sleep settings is to allow for a slightly different
 * configuration that minimizes power consumption during Deep Sleep.  For
 * example, inputs that might float could be pulled up or down, and output
 * states chosen with care, e.g. to turn off LEDs or other devices that might
 * consume power or be unnecessary when Deep Sleeping.
 */
//@{


/** @name Packet Trace Configuration Defines
 *
 * Provide the proper set of pin configuration for when the Packet
 * Trace is enabled (look above for the define which enables it).  When Packet
 * Trace is not enabled, leave the two PTI pins in their default configuration.
 * If Packet Trace is not being used, feel free to set the pin configurations
 * as desired.  The config shown here is simply the Power On Reset defaults.
 *@{
 */
/**
 * @brief Give the packet trace configuration a friendly name.
 */
#ifdef  PACKET_TRACE
  #define PWRUP_CFG_PTI_EN    GPIOCFG_OUT_ALT
  #define PWRUP_OUT_PTI_EN    0
  #define PWRDN_CFG_PTI_EN    GPIOCFG_IN_PUD
  #define PWRDN_OUT_PTI_EN    GPIOOUT_PULLDOWN
  #define PWRUP_CFG_PTI_DATA  GPIOCFG_OUT_ALT
  #define PWRUP_OUT_PTI_DATA  1
  #define PWRDN_CFG_PTI_DATA  GPIOCFG_IN_PUD
  #define PWRDN_OUT_PTI_DATA  GPIOOUT_PULLUP
#else//!PACKET_TRACE
  #define PWRUP_CFG_PTI_EN    GPIOCFG_IN
  #define PWRUP_OUT_PTI_EN    0
  #define PWRDN_CFG_PTI_EN    GPIOCFG_IN
  #define PWRDN_OUT_PTI_EN    0
  #define PWRUP_CFG_PTI_DATA  GPIOCFG_IN
  #define PWRUP_OUT_PTI_DATA  0
  #define PWRDN_CFG_PTI_DATA  GPIOCFG_IN
  #define PWRDN_OUT_PTI_DATA  0
#endif//PACKET_TRACE
//@} END OF Packet Trace Configuration Defines

//Use Button1 configuration
#define PWRUP_CFG_BUTTON0  GPIOCFG_IN_PUD
#define PWRUP_CFG_BUTTON1  GPIOCFG_IN_PUD 
#define PWRUP_OUT_BUTTON0  GPIOOUT_PULLUP /* Button needs a pullup */
#define PWRUP_OUT_BUTTON1  GPIOOUT_PULLUP /* Button needs a pullup */
#define PWRDN_CFG_BUTTON0  GPIOCFG_IN_PUD
#define PWRDN_CFG_BUTTON1  GPIOCFG_IN_PUD 



#define PWRUP_CFG_LED0  GPIOCFG_OUT
#define PWRUP_OUT_LED0  1  /* LED default off */
#define PWRUP_CFG_LED1  GPIOCFG_OUT
#define PWRUP_OUT_LED1  1  /* LED default off */


/**
 * @brief Give GPIO SC1 TXD and nRTS configurations friendly names.
 */
#if     SLEEPY_IP_MODEM_UART
  #define PWRUP_CFG_SC1_TXD  GPIOCFG_OUT     // Let UART driver manage OUT_ALT
  #define PWRDN_OUT_SC1_nRTS 0               // Let peer send data to wake
#else//!SLEEPY_IP_MODEM_UART
  #define PWRUP_CFG_SC1_TXD  GPIOCFG_OUT_ALT // Pre-set for UART operation
  #define PWRDN_OUT_SC1_nRTS 1               // Deassert nRTS when sleeping
#endif//SLEEPY_IP_MODEM_UART


/** @name GPIO Configuration Macros
 *
 * These macros define the GPIO configuration and initial state of the output
 * registers for all the GPIO in the powerup and powerdown modes.
 *@{
 */


//Each pin has 4 cfg bits.  There are 3 ports with 2 cfg registers per
//port since the cfg register only holds 2 pins (16bits).  Therefore,
//the cfg arrays need to be 6 entries of 16bits.
extern int16u gpioCfgPowerUp[6];
extern int16u gpioCfgPowerDown[6];
//Each pin has 1 out bit.  There are 3 ports with 1 out register per
//port (8bits).  Therefore, the out arrays need to be 3 entries of 8bits.
extern int8u gpioOutPowerUp[3];
extern int8u gpioOutPowerDown[3];
//A single mask variable covers all 24 GPIO.
extern int32u gpioRadioPowerBoardMask;


/**
 * @brief Define the mask for GPIO relevant to the radio in the context
 * of power state.  Each bit in the mask indicates the corresponding GPIO
 * which should be affected when invoking halStackRadioPowerUpBoard() or
 * halStackRadioPowerDownBoard().
 */
#define DEFINE_GPIO_RADIO_POWER_BOARD_MASK_VARIABLE() \
int32u gpioRadioPowerBoardMask = 0xc00000;

/**
 * @brief Initialize GPIO powerup configuration variables.
 */
#define DEFINE_POWERUP_GPIO_CFG_VARIABLES()     \
int16u gpioCfgPowerUp[6] = {                    \
                              ((GPIOCFG_OUT_ALT    <<PA0_CFG_BIT)|             \
                               (GPIOCFG_IN         <<PA1_CFG_BIT)|             \
                               (GPIOCFG_OUT_ALT    <<PA2_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PA3_CFG_BIT)),            \
                              ((GPIOCFG_ANALOG     <<PA4_CFG_BIT)|             \
                               (GPIOCFG_ANALOG     <<PA5_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PA6_CFG_BIT)|             \
                               (PWRUP_CFG_BUTTON1  <<PA7_CFG_BIT)),            \
                              ((GPIOCFG_IN         <<PB0_CFG_BIT)|             \
                               (GPIOCFG_OUT_ALT_OD <<PB1_CFG_BIT)|             \
                               (GPIOCFG_OUT_ALT_OD <<PB2_CFG_BIT)|             \
                               (PWRUP_CFG_BUTTON0  <<PB3_CFG_BIT)),            \
                              ((GPIOCFG_OUT        <<PB4_CFG_BIT)|             \
                               (PWRUP_CFG_LED0     <<PB5_CFG_BIT)|             \
                               (PWRUP_CFG_LED1     <<PB6_CFG_BIT)|             \
                               (GPIOCFG_ANALOG     <<PB7_CFG_BIT)),            \
                              ((GPIOCFG_IN         <<PC0_CFG_BIT)|             \
                               (GPIOCFG_ANALOG     <<PC1_CFG_BIT)|             \
                               (GPIOCFG_OUT_ALT    <<PC2_CFG_BIT)|             \
                               (GPIOCFG_IN         <<PC3_CFG_BIT)),            \
                              ((GPIOCFG_IN         <<PC4_CFG_BIT)|             \
                               (GPIOCFG_OUT_ALT    <<PC5_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PC6_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PC7_CFG_BIT))             \
                             }

/**
 * @brief Initialize GPIO powerup output variables.
 */
#define DEFINE_POWERUP_GPIO_OUTPUT_DATA_VARIABLES()                       \
int8u gpioOutPowerUp[3] = {                                               \
                           ((0                  <<PA0_BIT)|               \
                            (0                  <<PA1_BIT)|               \
                            (0                  <<PA2_BIT)|               \
                            (0                  <<PA3_BIT)|               \
                            (0                  <<PA4_BIT)|               \
                            (0                  <<PA5_BIT)|               \
                            (0                  <<PA6_BIT)|               \
                            (PWRUP_OUT_BUTTON1  <<PA7_BIT)),              \
                           ((0                  <<PB0_BIT)|               \
                            (0                  <<PB1_BIT)|               \
                            (0                  <<PB2_BIT)|               \
                            (PWRUP_OUT_BUTTON0  <<PB3_BIT)|               \
                            (0                  <<PB4_BIT)|               \
                            (PWRUP_OUT_LED0     <<PB5_BIT)|               \
                            (PWRUP_OUT_LED1     <<PB6_BIT)|               \
                            (0                  <<PB7_BIT)),              \
                           ((0                  <<PC0_BIT)|               \
                            (0                  <<PC1_BIT)|               \
                            (1                  <<PC2_BIT)|               \
                            (0                  <<PC3_BIT)|               \
                            (0                  <<PC4_BIT)|               \
                            (0                  <<PC5_BIT)|               \
                            (1                  <<PC6_BIT)|               \
                            (1                  <<PC7_BIT))               \
                          }

/**
 * @brief Initialize powerdown GPIO configuration variables.
 */
#define DEFINE_POWERDOWN_GPIO_CFG_VARIABLES()                                  \
int16u gpioCfgPowerDown[6] = {                                                 \
                              ((GPIOCFG_OUT        <<PA0_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PA1_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PA2_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PA3_CFG_BIT)),            \
                              ((GPIOCFG_OUT        <<PA4_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PA5_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PA6_CFG_BIT)|             \
                               (GPIOCFG_IN_PUD     <<PA7_CFG_BIT)),            \
                              ((GPIOCFG_IN         <<PB0_CFG_BIT)|             \
                               (GPIOCFG_OUT_OD     <<PB1_CFG_BIT)|             \
                               (GPIOCFG_OUT_OD     <<PB2_CFG_BIT)|             \
                               (GPIOCFG_IN_PUD     <<PB3_CFG_BIT)),            \
                              ((GPIOCFG_OUT        <<PB4_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PB5_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PB6_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PB7_CFG_BIT)),            \
                              ((GPIOCFG_IN_PUD     <<PC0_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PC1_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PC2_CFG_BIT)|             \
                               (GPIOCFG_IN_PUD     <<PC3_CFG_BIT)),            \
                              ((GPIOCFG_IN_PUD     <<PC4_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PC5_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PC6_CFG_BIT)|             \
                               (GPIOCFG_OUT        <<PC7_CFG_BIT))             \
                             }

/**
 * @brief Initialize powerdown GPIO output variables.
 */
#define DEFINE_POWERDOWN_GPIO_OUTPUT_DATA_VARIABLES()                       \
int8u gpioOutPowerDown[3] = {                                               \
                             ((0                  <<PA0_BIT)|               \
                              (0                  <<PA1_BIT)|               \
                              (0                  <<PA2_BIT)|               \
                              (0                  <<PA3_BIT)|               \
                              (0                  <<PA4_BIT)|               \
                              (0                  <<PA5_BIT)|               \
                              (0                  <<PA6_BIT)|               \
                              (GPIOOUT_PULLUP     <<PA7_BIT)),              \
                             ((0                  <<PB0_BIT)|               \
                              (0                  <<PB1_BIT)|               \
                              (0                  <<PB2_BIT)|               \
                              (GPIOOUT_PULLUP     <<PB3_BIT)|               \
                              (0                  <<PB4_BIT)|               \
                              (1                  <<PB5_BIT)|               \
                              (1                  <<PB6_BIT)|               \
                              (0                  <<PB7_BIT)),              \
                             ((GPIOOUT_PULLUP     <<PC0_BIT)|               \
                              (0                  <<PC1_BIT)|               \
                              (1                  <<PC2_BIT)|               \
                              (GPIOOUT_PULLUP     <<PC3_BIT)|               \
                              (GPIOOUT_PULLUP     <<PC4_BIT)|               \
                              (0                  <<PC5_BIT)|               \
                              (0                  <<PC6_BIT)|               \
                              (0                  <<PC7_BIT))               \
                            }


/**
 * @brief Set powerup GPIO configuration registers.
 */
#define SET_POWERUP_GPIO_CFG_REGISTERS() \
  GPIO_PACFGL = gpioCfgPowerUp[0];       \
  GPIO_PACFGH = gpioCfgPowerUp[1];       \
  GPIO_PBCFGL = gpioCfgPowerUp[2];       \
  GPIO_PBCFGH = gpioCfgPowerUp[3];       \
  GPIO_PCCFGL = gpioCfgPowerUp[4];       \
  GPIO_PCCFGH = gpioCfgPowerUp[5];


/**
 * @brief Set powerup GPIO output registers.
 */
#define SET_POWERUP_GPIO_OUTPUT_DATA_REGISTERS() \
  GPIO_PAOUT = gpioOutPowerUp[0];                \
  GPIO_PBOUT = gpioOutPowerUp[1];                \
  GPIO_PCOUT = gpioOutPowerUp[2];


/**
 * @brief Set powerdown GPIO configuration registers.
 */
#define SET_POWERDOWN_GPIO_CFG_REGISTERS() \
  GPIO_PACFGL = gpioCfgPowerDown[0];       \
  GPIO_PACFGH = gpioCfgPowerDown[1];       \
  GPIO_PBCFGL = gpioCfgPowerDown[2];       \
  GPIO_PBCFGH = gpioCfgPowerDown[3];       \
  GPIO_PCCFGL = gpioCfgPowerDown[4];       \
  GPIO_PCCFGH = gpioCfgPowerDown[5];


/**
 * @brief Set powerdown GPIO output registers.
 */
#define SET_POWERDOWN_GPIO_OUTPUT_DATA_REGISTERS() \
  GPIO_PAOUT = gpioOutPowerDown[0];                \
  GPIO_PBOUT = gpioOutPowerDown[1];                \
  GPIO_PCOUT = gpioOutPowerDown[2];



/**
 * @brief External regulator enable/disable macro.
 */
#ifdef ENABLE_ALT_FUNCTION_REG_EN
  #define CONFIGURE_EXTERNAL_REGULATOR_ENABLE()  GPIO_DBGCFG |= GPIO_EXTREGEN;
#else
  #define CONFIGURE_EXTERNAL_REGULATOR_ENABLE()  GPIO_DBGCFG &= ~GPIO_EXTREGEN;
#endif
//@} END OF GPIO Configuration Macros


/** @name GPIO Wake Source Definitions
 *
 * A convenient define that chooses if this external signal can
 * be used as source to wake from deep sleep.  Any change in the state of the
 * signal will wake up the CPU.
 */
 //@{
/**
 * @brief TRUE if this GPIO can wake the chip from deep sleep, FALSE if not.
 */
#define WAKE_ON_PA0   FALSE
#define WAKE_ON_PA1   FALSE
#define WAKE_ON_PA2   FALSE
#define WAKE_ON_PA3   FALSE
#define WAKE_ON_PA4   FALSE
#define WAKE_ON_PA5   FALSE
#define WAKE_ON_PA6   FALSE
#define WAKE_ON_PA7   TRUE		// BUTTON 1
#define WAKE_ON_PB0   TRUE		// ACCELEROMETER INTERRUPT
#define WAKE_ON_PB1   FALSE
#ifdef SLEEPY_EZSP_UART  // SC1RXD
  #define WAKE_ON_PB2   TRUE
#else
  #define WAKE_ON_PB2   FALSE
#endif
#define WAKE_ON_PB3   TRUE		// BUTTON 0
#define WAKE_ON_PB4   FALSE
#define WAKE_ON_PB5   FALSE
#define WAKE_ON_PB6   FALSE
#define WAKE_ON_PB7   FALSE
#define WAKE_ON_PC0   FALSE
#define WAKE_ON_PC1   FALSE
#define WAKE_ON_PC2   FALSE
#define WAKE_ON_PC3   FALSE
#define WAKE_ON_PC4   FALSE
#define WAKE_ON_PC5   FALSE
#define WAKE_ON_PC6   FALSE
#define WAKE_ON_PC7   FALSE
//@} //END OF GPIO Wake Source Definitions

//@} //END OF GPIO Configuration Definitions

#define UNBNODE_USE_DRIVER_V2   

/** @name TEMPERATURE SENSOR Configuration Definitions
 *
 * A convenient define that chooses if this external signal can
 * be used as source to wake from deep sleep.  Any change in the state of the
 * signal will wake up the CPU.
 */
 //@{
/**
 * @brief 0 is the only TMP101 temperature sensor on board.
 */
#define UNBNODE_TEMP_DEV_NUM 0 // Only TMP100 DEVICE ACTIVE ON UNB BOARD
//@} //END OF TEMPERATURE SENSOR Configuration Definitions

/** @name ACCELEROMETER Configuration Definitions
 *
 * A convenient define that chooses if this external signal can
 * be used as source to wake from deep sleep.  Any change in the state of the
 * signal will wake up the CPU.
 */
 //@{
/**
 * @brief 0 is the only TMP101 temperature sensor on board.
 */
#define MMA8452Q_I2C_ADDRESS (0x1C << 1) // This is the address of the device
//@} //END OF ACCELEROMETER Configuration Definitions
  
  
/** @name Board Specific Functions
 *
 * The following macros exist to aid in the initialization, power up from sleep,
 * and power down to sleep operations.  These macros are responsible for
 * either initializing directly, or calling initialization functions for any
 * peripherals that are specific to this board implementation.  These
 * macros are called from halInit, halPowerDown, and halPowerUp respectively.
 */
 //@{
  extern void halI2CInit( void );               // I2C bus hardware configuration
  extern void TMP100initDevice( int8u devNum ); // Temp Sensor in shutdown (one-shot measurements) mode
  extern void MMA8452Q_initDevice( void );      // Accelerometer in shutdown (StandBy) mode

 /**
 * @brief Initialize the board.  This function is called from ::halInit().
 */
  #define halInternalInitBoard()                    \
          do {                                      \
            halInternalPowerUpBoard();              \
            halInternalRestartUart();               \
            halInternalInitButton();                \
            halI2CInit();                           \
            TMP100initDevice(UNBNODE_TEMP_DEV_NUM); \
            MMA8452Q_initDevice();                  \
          } while(0)

/**
 * @brief Power down the board.  This function is called from
 * ::halPowerDown().
 */
#define halInternalPowerDownBoard()                   \
        do {                                          \
          /* Board peripheral deactivation */         \
          /* halInternalSleepAdc(); */                \
          SET_POWERDOWN_GPIO_OUTPUT_DATA_REGISTERS()  \
          SET_POWERDOWN_GPIO_CFG_REGISTERS()          \
        } while(0)

/**
 * @brief Power up the board.  This function is called from
 * ::halPowerUp().
 */
#define halInternalPowerUpBoard()                                  \
        do {                                                       \
          CONFIGURE_VREG_1V8_DISABLE();                            \
          SET_POWERUP_GPIO_OUTPUT_DATA_REGISTERS()                 \
          SET_POWERUP_GPIO_CFG_REGISTERS()                         \
          /*The radio GPIO should remain in the powerdown state */ \
          /*until the stack specifically powers them up. */        \
          halStackRadioPowerDownBoard();                           \
          CONFIGURE_EXTERNAL_REGULATOR_ENABLE()                    \
          /* Board peripheral reactivation */                      \
          halInternalInitAdc();                                    \
          halI2CInit();                                            \
        } while(0)
        
          
//@} //END OF BOARD SPECIFIC FUNCTIONS

#endif //__BOARD_H__

/** @} END Board Specific Functions */

/** @} END addtogroup */
