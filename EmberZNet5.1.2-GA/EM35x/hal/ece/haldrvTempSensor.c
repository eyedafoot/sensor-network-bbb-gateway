#include "app/framework/include/af.h"
#include <haldrvI2C.h>
#include <haldrvUtils.h>
#include <haldrvTempSensor.h>

/******************************************************************************/
/*  Device Configuration Functions                                            */
/******************************************************************************/
void TMP100Reset(int8u devNum){
  /* The following code puts the device in its default configuration, as 
  suggested in datasheet 
  * Default configuration mode is Continuous Mode(SD=0), 
  * Default resolution is 9 bits.R0/R1=00 
  * Default thermostat mode is Comparator Mode (TM = 0)
  * Default Alert (Comparator polarity is 0 samples (F1F0 = 00)
  * The rest of the configuration bits are set to 0 as they don't care
  * OS/ALERT is set to 0 as this is for reading ALERT in Continuous Mode (SD = 0)
  * +--------+--------+--------+--------+--------+--------+--------+--------+
  * |OS/ALERT|   R1   |   R0   |   F1   |   F0   |   POL  |   TM   |   SD   |
  * +--------+--------+--------+--------+--------+--------+--------+--------+
  *     0         0        0        0        0        0        0        0
*/
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
                   
  TMP100_WR_CONFIG(devAddr, TMP100_DEFAULT_CONFIG );
  TMP100_WR_TLOW(devAddr, TMP100_DEFAULT_TLOW);
  TMP100_WR_THIGH(devAddr, TMP100_DEFAULT_THIGH);
  
  // TMP100setMode (TMP100_DEFAULT_MODE, devNum);
  // TMP100setResolution (TMP100_DEFAULT_RESOLUTION, UNBNODE_TEMP_DEV_NUM);
  // TMP100setTConfig(TMP100_DEFAULT_TMODE, TMP100_DEFAULT_COMP_POL, TMP100_DEFAULT_TOL, TMP100_DEFAULT_TLOW, TMP100_DEFAULT_THIGH, UNBNODE_TEMP_DEV_NUM );
}//End of Reset
/*----------------------------------------------------------------------------*/

void TMP100ShutDown( boolean enable, int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);
    
  // clear previous mode from configuration register
  devConf ^= (devConf & TMP100_CFG_MODE_MASK);
  
  if (enable){
    // Set new mode in configuration register
    devConf |= TMP100_CFG_SET_SD;
  }
  
  // write new configuration back to device
  TMP100_WR_CONFIG(TMP100_I2C_ADDR(devNum), devConf);

}//End of setShutDownMode
/*----------------------------------------------------------------------------*/

void TMP100initDevice(int8u devNum){
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
   
  // Default configuration mode is Shutdown Mode(SD=1), 
  // Default resolution is 9 bits.R0/R1=00 
  // The rest of the configuration bits are set to 0 as they don't care
  // +--------+--------+--------+--------+--------+--------+--------+--------+
  // |OS/ALERT|   R1   |   R0   |   F1   |   F0   |   POL  |   TM   |   SD   |
  // +--------+--------+--------+--------+--------+--------+--------+--------+
  //     0         0        0        0        0        0        0        1
  TMP100_WR_CONFIG(devAddr, TMP100_MODE_ONESHOT );
  
  /* The following code is a longer, but also a more descriptive way of device initialization
  TMP100setMode (TMP100_MODE_ONESHOT, devNum);
  TMP100setResolution (TMP100_MIN_RESOLUTION, devNum);
  TMP100setThermostatMode (TMP100_TMODE_COMPARATOR, devNum)
  TMP100setComparatorPolarity (TMP100_COMP_POL_ACTIVE_LOW, devNum);
  TMP100setComparatorQueueLength(1, devNum);
  */ 
 }// End of initDevice
/*----------------------------------------------------------------------------*/

int8u TMP100getConfig(int8u devNum ){
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  
  // read new configuration from device
  return TMP100_RD_CONFIG(devAddr);

}// End of getConfig
/*----------------------------------------------------------------------------*/

int8u TMP100getMode(int8u devNum ){
  return (TMP100getConfig(devNum) & TMP100_CFG_MODE_MASK);
}//End of getMode
/*----------------------------------------------------------------------------*/

void TMP100setMode(int8u mode, int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);
    
  // clear previous mode from configuration register
  devConf ^= (devConf & TMP100_CFG_MODE_MASK);
  
  // Set new mode in configuration register
  devConf |= mode & TMP100_CFG_MODE_MASK;
    
  // write new configuration back to device
  TMP100_WR_CONFIG(TMP100_I2C_ADDR(devNum), devConf);

}//End of setMode
/*----------------------------------------------------------------------------*/

void TMP100setContinuousMode(int8u devNum ){
  TMP100ShutDown( FALSE, devNum );

}//End of setContinuousMode
/*----------------------------------------------------------------------------*/

void TMP100setOneShotMode( int8u devNum ){
  TMP100ShutDown( TRUE, devNum );
}//End of setOneShotMode
/*----------------------------------------------------------------------------*/

void TMP100setThermostatMode (int8u mode , int8u devNum){
// Read device current configuration
  int8u devConf = TMP100getConfig(devNum);
  
  // check if desired resolution matches TMP100 options
  if ( mode > TMP100_TMODE_INTERRUPT )
    mode = TMP100_DEFAULT_TMODE;
  
  // clear old thermostat mode from configuration register
  devConf ^= (devConf & TMP100_CFG_TMODE_MASK);
  
  // Set new thermostat mode in configuration register
  devConf |= (mode << TMP100_CFG_BIT_TM);
    
  // write new configuration back to device
  TMP100_WR_CONFIG(TMP100_I2C_ADDR(devNum), devConf);
  
}//End of setThermostatMode
/*----------------------------------------------------------------------------*/

void TMP100setResolution (int8u Resolution, int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);
  
  // check if desired resolution matches TMP100 options
  if ((Resolution < 9) | (Resolution > 12))
    Resolution = TMP100_DEFAULT_RESOLUTION;
  
  // clear old resolution from configuration register
  devConf ^= (devConf & RESOL_MASK);
  
  // Set new resolution in configuration register
  devConf |= RESOLUTION_BITS(Resolution);
    
  // write new configuration back to device
  TMP100_WR_CONFIG(TMP100_I2C_ADDR(devNum), devConf);
  
}// End of setResolution
/*----------------------------------------------------------------------------*/

int8u TMP100getResolution(int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);

  //return resolution in number of bits
  return RESOLUTION(devConf);
  
 }// End of getResolution
/*----------------------------------------------------------------------------*/

void TMP100setTTolerance(int8u Tolerance, int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);
  
  // check if desired tolerance matches TMP100 options
  // Tolerance values can only be 1,2,4,6 (0,1,2,3 if right shift one bit)
  if ( (Tolerance >> 1) > 3)
    Tolerance = TMP100_DEFAULT_TOL;
  
  // clear old resolution from configuration register
  devConf ^= (devConf & FAULT_MASK);
  
  // Set new fault tolerance in configuration register
  devConf |= FAULT_TOL_BITS(Tolerance);
  
  // write new configuration back to device
  TMP100_WR_CONFIG(TMP100_I2C_ADDR(devNum), devConf);
  
}// End of setTolerance
/*----------------------------------------------------------------------------*/

int8u TMP100getTTolerance(int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);

  //return Thermostat's fault queue length in number of samples (1, 2, 4 or 6)
  int8u cfgFault = FAULT_TOL(devConf) << 1;
  if (cfgFault == 0) cfgFault = 1;
  
  return cfgFault;
  
 }// End of getTTolerance
/*----------------------------------------------------------------------------*/

int16s TMP100getTLowThresholdRaw(int8u devNum ){
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  
  //return register value as read from device
  return TMP100_RD_TLOW(devAddr);
 }// End of getLowThresholdRaw
/*----------------------------------------------------------------------------*/

int8s TMP100getTLowThresholdRoundC(int8u devNum ){
  // read raw register
  int16s TLo = TMP100getTLowThresholdRaw(devNum);
  
  // int (TLo + 0.5). Q8 fornmat is used: 8 fractional bits and 8 integer bits
  TLo = (TLo + 128) >> 8;
  
  return (int8s)TLo;
}// End of getTLowThresholdRoundC
/*----------------------------------------------------------------------------*/

void TMP100setTLowThresholdRaw(int16s threshold, int8u devNum ){
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  
  //return register value as read from device
  TMP100_WR_TLOW(devAddr, (threshold + 0x8) & 0xFFF0);
}// End of setLowThresholdRaw
/*----------------------------------------------------------------------------*/

int16s TMP100getTHighThresholdRaw(int8u devNum ){
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  
  //return register value as read from device
  return TMP100_RD_THIGH(devAddr);
}// End of getHighThresholdRaw
/*----------------------------------------------------------------------------*/

int8s TMP100getTHighThresholdRoundC(int8u devNum ){
  // read raw register
  int16s THi = TMP100getTHighThresholdRaw(devNum);
  
  // int (THi + 0.5). Q8 fornmat is used: 8 fractional bits and 8 integer bits
  THi = (THi + 128) >> 8;
  
  return (int8s)THi;
}// End of getTHighThresholdRoundC
/*----------------------------------------------------------------------------*/

int16s TMP100getData(int8u devNum ){

  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  
  //return register value as read from device
  return TMP100_RD_DATA(devAddr);
  
}//End of getData
/*----------------------------------------------------------------------------*/

void TMP100setTHighThresholdRaw(int16s threshold, int8u devNum ){
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  
  //return register value as read from device
  TMP100_WR_THIGH(devAddr, (threshold + 0x8) & 0xFFF0);
}// Endo of setHighThresholdRaw
/*----------------------------------------------------------------------------*/
 
void TMP100setTConfig(int8u tmode,  int8u AlertPol, int8u tol, int16s TLow, int16s THigh,int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);
  
  // clear old thermostat mode in configuration register
  devConf ^= (devConf & TMP100_CFG_TMODE_MASK);
  
  // Set new thermostat mode bit in configuration register
  if ( tmode == TMP100_TMODE_INTERRUPT )
    devConf |= TMP100_CFG_SET_TM;

  // clear old polarity setting in configuration register
  devConf ^= (devConf & TMP100_CFG_SET_POL);
  
  // Set Polarity of Alert bit in configuration register
  if (AlertPol > 0) devConf |= TMP100_CFG_SET_POL;
  
  // check if desired tolerance matches TMP100 options
  // Tolerance values can only be 1,2,4,6 (0,1,2,3 if right shift one bit)
  if ( (tol >> 1) > 3)
    tol = TMP100_DEFAULT_TOL;
  
  // clear old resolution from configuration register
  devConf ^= (devConf & FAULT_MASK);
  
  // Set new fault tolerance in configuration register
  devConf |= FAULT_TOL_BITS(tol);
  
  // write new configuration to the device
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  TMP100_WR_CONFIG(devAddr, devConf);
  
  // Write Thermostat low and high thresholds with rounding to TMP100 precision
  TMP100_WR_TLOW(devAddr, (int16u)((TLow + 0x0008) & 0xFFF0));
  TMP100_WR_THIGH(devAddr, (int16u)((THigh + 0x0008) & 0xFFF0));
  
}// End of setThermostat
/*----------------------------------------------------------------------------*/

void TMP100setConfig(int8u devConf, int16s TLow, int16s THigh, int8u devNum ){
  // Get device I2C address
  int8u devAddr = TMP100_I2C_ADDR(devNum);
  
  // write new configuration to the device
  TMP100_WR_CONFIG(devAddr, devConf);
  
  // Write Thermostat low and high thresholds with rounding to TMP100 precision
  TMP100_WR_TLOW(devAddr, (int16u)((TLow + 0x0008) & 0xFFF0));
  TMP100_WR_THIGH(devAddr, (int16u)((THigh + 0x0008) & 0xFFF0));

} // End of setConfig
/*----------------------------------------------------------------------------*/

void TMP100triggerOneShotMeasurement( int8u devNum ){
  // Note: this function should only be called when devioce is in ShutDown Mode
  
  // Read device current configuration
  int8u devConf = TMP100getConfig(devNum);
    
  // Set OneShot bit in configuration register
  SETBIT(devConf, TMP100_CFG_BIT_OS); //devConf |= TMP100_CFG_SET_OS;
    
  // write new configuration back to device
  TMP100_WR_CONFIG(TMP100_I2C_ADDR(devNum), devConf);

}//End of triggerSingleShotMeasurement
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/*  Debug Functions                                                           */
/******************************************************************************/
void TMP100showConfig( int8u devNum ){
  int8u devConf = TMP100getConfig(devNum);
  int16s TLo = TMP100getTLowThresholdRaw(devNum);
  int16s THi = TMP100getTHighThresholdRaw(devNum);
    
  logPrintln("TMP100 Configuration Register");
  logPrintln("  Single Shot Mode Enabled:         %p", (READBIT(devConf, TMP100_CFG_BIT_SD) >> TMP100_CFG_BIT_SD?"YES":"NO"));   //devConf & TMP100_MODE_ONESHOT);
  logPrintln("  Thermostat Interrupt Mode Enabled:%p", (READBIT(devConf, TMP100_CFG_BIT_TM) >> TMP100_CFG_BIT_TM?"YES":"NO"));   //devConf && TMP100_CFG_SET_TM);
  logPrintln("  Comparator Polarity Active Low:   %p", (ALERT_ACTIVE_LOW(devConf)?"YES":"NO"));
  logPrintln("  Number of consecutive faults:     %d samples", TMP100getTTolerance( devNum ));
  logPrintln("  Resolution:                       %d bits", RESOLUTION(devConf));
  logPrintln("  Alert bit is Active:              %p", (((READBIT(devConf, TMP100_CFG_BIT_ALERT) >> TMP100_CFG_BIT_ALERT) ^ ALERT_ACTIVE_LOW(devConf))?"YES":"NO") );     //(devConf >> TMP100_CFG_BIT_ALERT) ^ ALERT_ACTIVE_LOW(devConf));
  logPrintln("                                       ");
  logPrintln("  Thermostat Low  Threshold: %d.%d �C", HIGH_BYTE(TLo), rawTFrac(TLo, TMP100_MAX_RESOLUTION));
  logPrintln("  Thermostat High Threshold: %d.%d �C", HIGH_BYTE(THi), rawTFrac(THi, TMP100_MAX_RESOLUTION));
  logPrintln("--- TMP100 End of Configuration  --");
  
}// End of showConfig
/*----------------------------------------------------------------------------*/

void TMP100showMeasResol(int8u devNum){
  logPrintln("Temperature Sensor Resolution= %d bits", TMP100getResolution(devNum));
}
/*----------------------------------------------------------------------------*/

void TMP100showMeasDelay( int8u devNum ){
  logPrintln("Temperature Sensor Conversion Time= %d ms",OS_MAX_CONV_TIME(TMP100getResolution(devNum)));
}
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* Reading Temperature Functions                                              */
/******************************************************************************/

float getTemperature(int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig( devNum );
  
  if (READBIT(devConf, TMP100_CFG_BIT_SD) == TMP100_MODE_CONTINUOUS){
    return rawT2float(readTemperatureRawNonBlocking( devNum ));
  }
  else{
    return rawT2float(readTemperatureRawSleeping( devNum ));
  }
}//End of getTemperature
/*----------------------------------------------------------------------------*/

int16s getTemperatureIn100sC( int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig( devNum );
  
  if (READBIT(devConf, TMP100_CFG_BIT_SD) == TMP100_MODE_CONTINUOUS){
    return rawT2100sC(readTemperatureRawNonBlocking( devNum ));
  }
  else{
    return rawT2100sC(readTemperatureRawSleeping( devNum ));
  }
}//End of getTemperatureIn100oC
/*----------------------------------------------------------------------------*/

int16s getTemperatureFpoint(int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig( devNum );
  
  if (READBIT(devConf, TMP100_CFG_BIT_SD) == TMP100_MODE_CONTINUOUS){
    return rawT2fpoint(readTemperatureRawNonBlocking( devNum ));
  }
  else{
    return rawT2fpoint(readTemperatureRawSleeping( devNum ));
  }
}//End of getTemperatureFpoint
/*----------------------------------------------------------------------------*/

int16s getTemperatureRaw(int8u devNum ){
  // Read device current configuration
  int8u devConf = TMP100getConfig( devNum );
  
  if (READBIT(devConf, TMP100_CFG_BIT_SD) == TMP100_MODE_CONTINUOUS){
    return readTemperatureRawNonBlocking( devNum );
  }
  else{
    return readTemperatureRawSleeping( devNum );
  }
}//End of getTemperatureRaw
/*----------------------------------------------------------------------------*/

int16s readTemperatureRawNonBlocking(int8u devNum ){
  return TMP100getData(devNum);
}
/*----------------------------------------------------------------------------*/

int16s readTemperatureRawBlocking(int8u devNum ){
  
  // get device resolution to determine maximum conversion delay
  int8u resolution = TMP100getResolution(devNum);
  
  // trigger one shot temperature measurement
  TMP100triggerOneShotMeasurement(devNum);
  
  // actively wait for conversion to finish
  waitBusy(OS_MAX_CONV_TIME(resolution));
  
  // Finally we can read tha data register
  return TMP100getData(devNum);
  
}//End of readTemperatureRawBlocking
/*----------------------------------------------------------------------------*/

int16s readTemperatureRawSleeping(int8u devNum ){
  
  // get device resolution to determine maximum conversion delay
  int8u resolution = TMP100getResolution(devNum);
  
  // set the amount of time the device requires to finish conversion
  int32u duration = OS_MAX_CONV_TIME(resolution);
    
  // trigger one shot temperature measurement
  TMP100triggerOneShotMeasurement(devNum);
  
  // Put EZnode into sleep mode in order to wait for conversion to finish
  waitSleeping(duration);

  // Finally we can read tha data register
  return TMP100getData(devNum);
  
}//End of readTemperatureRawSleeping
/*----------------------------------------------------------------------------*/


