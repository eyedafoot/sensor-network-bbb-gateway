/** @file hal/ece/haldrvADC.h
 * @brief Header for EM35x A/D converter on unb PCB
 *
 * See @ref electrical for documentation.
 * Author: Erik Hatfield
 *
 * <!-- University of New Brunswick. 2015 -->
 */
#ifndef __ELECTRIC_SENSOR_H__
#define __ELECTRIC_SENSOR_H__

/*
Note: 
	The adc stays on after a conversion by default.
	It can be shut off by calling the disable function.
	Once disabled, the adc does not need to manually be enabled
	as this is done when calling a read function, however it  
	can be done enabled preemptively to avoid startup lag on the adc.
*/

/* STRUCTURES AND ENUMS */
 
typedef enum { SingleBuff, DoubleBuff, MultiCh } adcMode;
 
typedef enum {
  /*PB5     ,PB6     ,PB7     ,PC1     ,PA4     ,PA5 */
	ADC0=0x0,ADC1=0x1,ADC2=0x2,ADC3=0x3,ADC4=0x4,ADC5=0x5,
  /*0.0V   ,0.6V     ,1.2V    ,0.9V		*/
	GND=0x8,VREF2=0x9,VREF=0xA,VREG2=0xB
} adcInputChEnum;

typedef struct {
	boolean is1MHzClock;
	int16s sigBits;
	adcInputChEnum inputP, inputN;
} adcConfigStruct;

typedef struct {
	adcInputChEnum *pSetPtr, *nSetPtr;
	int16u configPerSet, setsPerBuffer;
} adcMultiCfgStruct;


/* METHODS */ 

/** @brief: Enable the adc */
void enableAdc(void);

/** @brief: Disable the adc */
void disableAdc(void);

/** @brief: Convert a measurement to a voltage when calibrated */
int16s convertToSignedMillivolts(int16s adcdata);

/** @brief: Converts an adc measurement into a current value in mA
            Used only with the ECS1030 Split Core Current Transformer*/
// UNTESTED
int16s convertToMilliAmps(int16s adcdata);

/** @brief Configures the Adc to have the desired behavior 
 *
 * Setup how the ADC will be used. This function is used to set the clock rate,
 * number of significant bits (from 7 to 14), and the positive and negative channels.
 * Note that this function should be used to configure multi-channel reading as well.
 * In this case, all the specified configurations are used during sampling, except the 
 * P and P inputs which are changed back to the ones set here once multi-sampling is done.
 */
void setAdcConfig(boolean is1MHzClock, int16s sigBits, adcInputChEnum inputP, adcInputChEnum inputN);
    
	
/** @brief Get the current adc configuration
 *
 * Fetches current adc configuration information, and organizes it into a structure.
 * Useful for making of important configurations. Used internally as well. 
 */
adcConfigStruct getAdcConfig(void);


/** @brief Initialize the ADC
 *
 * Resets any ADC calibration and sets the ADC to have its default behavior.
 * Default behaviour is single ended reading on ADC0 with fast clock and 14 bits precision
 */
void halInternalInitAdc(void);
 
 
 /** @brief Read a value from the current configurations (BLOCKING)
 *
 * Reads a single sample from the adc with the current configuration settings.
 * Note that this ties up the CPU as it is used for polling.
 */
int16s readAdcOneShot(void);


 /** @brief Set the adc to measure from -2^sigBits to 2^sigBits 
 *
 * Calibrates the ADC such that the measured values use all of the levels
 * for the given range. Reading the adc after calibration allows for the 
 * recorded level(s) to converted into millivolts. 
 */
void calibrateAdc(void);


 /** @brief Determine if the adc is calibrated to measure absolute voltage
 *
 * Returns true if the adc is calibrated, false otherwise.
 */
boolean isCalibrated(void);


 /** @brief Reads values into a single buffer (NON-BLOCKING)
 *
 * Read a given number of samples into a specified buffer using the DMA. The buffer can be any 
 * size desired by the user so long as the size parameter is specified as sizeof(buff)/sizeof(buff[0]).
 * The function is non-blocking as it uses the DMA to interrupt the CPU when the samples have been read.
 * Once the ISR has been run, an event is scheduled which invokes the given callback with a pointer to
 * the data buffer, and its total size.
 */
void readAdcSingleBuffer(int16s* buff, int16u buffSize, void (*callback)(int16s*,int16u));


 /** @brief Reads values with a double buffer configuration (NON-BLOCKING)
 *
 * Reads a given number of samples into buffer1 using the DMA. Once the buffer is full, the ISR 
 * changes pointers such that buffer2 references the data and buffer1 is set to read new samples.
 * By doing this, samples can be continually read into a DMA buffer, while completed conversions
 * are handled else where in a process buffer. This is useful when the data is being streamed and/or 
 * requires some post processing.
 */
void readAdcDoubleBufferStream(int16s* buff1, int16s* buff2, int16u buffSize, void (*callback)(int16s*,int16u));


 /** @brief Reads values from multiple channels passed as an array, by writing interleaved values into a buffer.
 *
 * Sequentially reads samples from each of the channel configurations specified in the given sets of positive 
 * and negative inputs. Once each channel configuration is read into the buffer, the process begins again, with
 * the new samples being concatenated to the end of the previous samples. In this way, multiple channel configurations
 * can be read with some latency between samples. Please ensure that the latency between each sample at the given 
 * sampling rate is not sufficient enough to cause issues. For a means of illustration, consider the following example:
 *
 *			INPUTS:							           OUTPUT:

 *      ----------------                            ----------------------------------------------------------
 *    ->| P1 | P2 | P3 |   [configPerSet:3]         | sample | sample | sample | sample | sample | sample | 
 *      ----------------                      ==>   | [ P1 ] | [ P2 ] | [ P3 ] | [ P1 ] | [ P2 ] | [ P3 ] |    * * * * *
 *      ----------------                            | [ N1 ] | [ N2 ] | [ N3 ] | [ N1 ] | [ N2 ] | [ N3 ] |
 *    ->| N1 | N2 | N3 |   ->[callback]             |        |        |        |        |        |        |
 *      ----------------                            -----------------------------------------------------------
 *		
 *	  ->[Buffer]  [setsPerBuffer:5]                 [adcMultiCfgStruct: pSetPtr, nSetPtr, configPerSet, setsPerBuffer]
 *
 * Here, three different channel configurations are sampled 5 times. Thin arrows represent pointers.
 * The conversion process occurs until the buffer is full. At this point, the provided callback is invoked
 * with the output shown above which includes the input set pointers, the # of channel configurations per sets, and 
 * the # of sets per buffer collected into a structure. This information is used to index the buffer. Any particular 
 * sample can be accessed from the buffer with the following method: sample = bufferPtr[setNumber*configPerSet+configNumber]
 */
void readAdcMultiConfig(adcInputChEnum* pSetPtr, adcInputChEnum* nSetPtr, int16u configPerSet, 
								int16s* arrayBufferPtr, int16u setsPerBuffer, void (*callback)(int16s*,adcMultiCfgStruct));


#endif // __ELECTRIC_SENSOR_H__
