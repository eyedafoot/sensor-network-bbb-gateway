#include "app/framework/include/af.h"
#include <haldrvI2C.h>
#include <haldrvUtils.h>

static PGM_P PGM strYesNo[] = {//Generic YES/NO strings
  "NO", "YES"
};

PGM_P getStrYESNO(int8u yes){ // gets "YES"(1) or "NO"(0) string
  
  return strYesNo[yes&1];
  
}//End of getStrYESNO

static int8s indent = 0;
static boolean StartOfLine = TRUE;

PGM_P PGM indents[] =
{
  "",           // 0
  " ",          // 1
  "  ",         // 2
  "   ",        // 3
  "    ",       // 4
  "     ",      // 5
  "      ",     // 6
  "       ",    // 7
  "        ",   // 8
  "         ",  // 9
  "          ", // 10
};

void resetIndent(void){ indent = 0; }
void setIndent(int8s newIndent){ 
  indent = newIndent; 
  if (indent <0) {
    indent = 0; 
  } 
// if (indent >10) {
//    indent = 10;
//  }
}
void incIndent(int8u by){ 
  indent += by; 
//  if (indent>10) indent=10; 
}
void decIndent(int8u by){ indent -= by; if (indent<0) indent=0;  }
int8u getIndent(void){ return indent; }

static void printIndent(void){
  int8u i, P, Q;
  
  if (StartOfLine){
    P= indent/10; Q = indent % 10;
    for(i=0; i<P; i++){
      emberSerialPrintf(EMBER_AF_PRINT_OUTPUT,  "%p", indents[10]);
    }
    if (Q>0){
      emberSerialPrintf(EMBER_AF_PRINT_OUTPUT,  "%p", indents[Q]);
    }
  }
}

#if defined(EMBER_AF_PRINT_OUTPUT)
EmberStatus logPrint(PGM_P formatString, ...) {
  EmberStatus stat;
  va_list ap;

  printIndent();  
  StartOfLine = FALSE;

  va_start (ap, formatString);
  stat = emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, formatString, ap);
  va_end (ap);
  return stat;
}

EmberStatus logPrintln(PGM_P formatString, ...) {
  EmberStatus stat;
  va_list ap;
  
  printIndent();  
  StartOfLine = TRUE;
  
  va_start (ap, formatString);
  stat = emberSerialPrintfVarArg(EMBER_AF_PRINT_OUTPUT, formatString, ap);
  va_end (ap);
  emberSerialPrintCarriageReturn(EMBER_AF_PRINT_OUTPUT);
  return stat;
}
#else
#define logPrintln(...) 
#define logPrint(...)   
#endif

void waitBusy(int32u ms){

  halCommonDelayMilliseconds(ms);

}//End of waitBusyLoop

void waitSleeping(int32u ms){
  int32u timeMs;
  timeMs = ms;
 // while (timeMs > 0) halSleepForMilliseconds(&timeMs); /* This function is crashing the processor. Should investigate why*/
  while (timeMs > 0) halCommonIdleForMilliseconds(&timeMs);
}//End of waitSleeping

/******************************************************************************/
/*   Utility Functions                                                        */
/******************************************************************************/

int16u toBCD(int16u number)
{
  int8u numBuff[3];
  int8u i;

  for(i = 0; i < 3; i++) {
    numBuff[i] = number % 10;
    number /= 10;
  }
  number = (number << 4) + numBuff[2];
  number = (number << 4) + numBuff[1];
  number = (number << 4) + numBuff[0];

  return number;
}

float rawT2float (int16s rawT){
  return rawT/256.0f;
}//End of rawT2float

int16s rawT2100sC(int16s rawT){
  // Convert to units of 1/100 deg C
  return (int16s)(((int32s)(rawT >> 4) * 50L + 4) >> 3);
}// End of rawT2100sC

int16s rawT2fpoint (int16s rawT){
  int16s intT, fracT;
  intT = rawT & 0xFF00;    // get the  integer part of the raw temperature

  fracT = (rawT < 0)? -rawT  : rawT;  //the fraction wil always be a positive number
  
  // Make the conversion. 
  fracT = (((fracT >> 4) & 0x0F) * 50 + 4) >> 3; 
  
  // the integer part is in the MSB and the fraction is in the LSB
  return (intT | fracT);
  
 }// End of raw2fpoint

int8s rawT2int8s(int16s rawT){
  // int (T + 0.5). Q8 fornmat is used: 8 fractional bits and 8 integer bits
  return (int8s)((rawT + 128) >> 8); 
}// End of rawT2int8s

int16u rawTFrac( int16s rawT, int8s Resolution ){
  
  int16u fracT;
  fracT = (rawT < 0)? -rawT : rawT;  //the fraction will always be a positive number
  
  switch (Resolution) {
  case 12: 
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 5000L ) >> 3);
  case 11: 
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 500L ) >> 3);
  case 10:
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 50L ) >> 3);
  default: // 9 bits of resolution
    return (int16u)(( (int32u)((fracT >> 4) & 0x0F) * 5L ) >> 3);
  }
}

int8s rawTInt ( int16s rawT ){
  return HIGH_BYTE(rawT);
}

/******************************************************************************/
/*   Utility Functions for converting Accelerometer values from raw measuerements */
/******************************************************************************/
float rawAxis2float(int16s raw, int8u Scale){
  int8u FSx = (Scale & 0xF) >> 2; 
  return ((float)raw /(10.0 - FSx));
}//End of rawAxis2float

void rawAxes2float(const int16s rawAxes[], int8u Scale, float Axes[]){
  int8u FSx = (Scale & 0xF) >> 2; 
  Axes[0] = ((float)rawAxes[0] /(10.0 - FSx));
  Axes[1] = ((float)rawAxes[1] /(10.0 - FSx));
  Axes[2] = ((float)rawAxes[2] /(10.0 - FSx));
}//End of rawAxis2float

int16s getAxisIntegerPart (int16s rawAxis, int8u Scale){
  int8u FSx = (Scale & 0xF) >> 2; 
  return (rawAxis >> (10 - FSx));
}//End of getDataIntegerPart

int16u getAxisFractionPart (int16s rawAxis, int8u Scale){
  
  int8u FSx = (Scale & 0x0F) >> 2;
  int32u lastIntBit = BIT(10 - FSx);
  
  if (rawAxis & 0x8000) //negative number
    rawAxis = ~rawAxis + 1; // Two's complement
  
  // Extract the fractional part
  int32u tmp = (rawAxis & (lastIntBit - 1));
  
  // put in decimal values with 4 significant places after decimal point
  // 0x.800 => .5000
  tmp = (tmp *5000L)/(lastIntBit >> 1L);
  
  return (int16u)tmp;
  
}//End of getDataFractionPart

void print_uXtoX (int32s value, int32s partsOf){
  if (value<0) {
    value = -value;
    logPrint("-");
  }
  
  int32s fraction = value%partsOf;
  logPrint("%d.",value/partsOf);
  while (partsOf > 10){
    partsOf /= 10;
    if (fraction < partsOf) logPrint("0");
  }
  logPrint("%d",fraction);
}