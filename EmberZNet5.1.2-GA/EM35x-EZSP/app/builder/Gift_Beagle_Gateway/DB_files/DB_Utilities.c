#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Need to make sure mysql libraries are installed in compiling system.
//Compile: gcc -o DB_tempQueries DB_tempQueries.c -I/usr/include/mysql –lmysqlclient

//Compile without main: gcc -c DB_tempQueries.c -I/usr/include/mysql -lmysqlclient

//Path to mysql on BBB: /usr/include/mysql

#include <mysql.h>

#include "mysql-connector.h"

 void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  return;      
}

 void Insert_Accel(int nodeid, int xAxis, int yAxis, int zAxis)
 {
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}   
	
	char buffer[100]; 
	sprintf(buffer,"INSERT INTO accel (nodeid, xAxis, yAxis, zAxis) VALUES (%d,%d,%d,%d)",nodeid,xAxis,yAxis,zAxis);
 
	//mysql DB query, if it doesn't work return error
	if (mysql_query(con, &buffer[0]))
	{
		finish_with_error(con);
	}
	
	mysql_close(con);
 }

 void Insert_Temp(int nodeid, float temp)
 {
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}   
 
	char buffer[100];
	sprintf(buffer,"INSERT INTO temp (nodeid, temp) VALUES (%d, %f)",nodeid,temp);
	
	//mysql DB query, if it doesn't work return error
	if (mysql_query(con, &buffer[0]))
	{
		finish_with_error(con);
	}

	mysql_close(con);
 }
 
  void Insert_Cur(int nodeid, int cur)
 {
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}   
 
	char buffer[100];
	sprintf(buffer,"INSERT INTO cur (nodeid, cur) VALUES (%d, %d)",nodeid,cur);
	
	//mysql DB query, if it doesn't work return error
	if (mysql_query(con, &buffer[0]))
	{
		finish_with_error(con);
	}

	mysql_close(con);
 }
 
 void Clear_Table(char* tableName)
 {
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}
	
	char query[100];
	sprintf(query, "TRUNCATE TABLE %s",tableName);
	
	//mysql DB query, if it doesn't work return error
	if (mysql_query(con, query))
	{
		finish_with_error(con);
	}

	mysql_close(con);
 }
 
 int Get_Total_Inserts()
 {
	MYSQL_RES *res;
	MYSQL_ROW row;
	
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}
	
	char query[100];
	sprintf(query, "SELECT SUM(TABLE_ROWS) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '%s'", SQL_DATABASE_NAME);
	
	//getting the columns from info
	if (mysql_query(con, &query[0]))
	{
		finish_with_error(con);
	}
	
	res = mysql_use_result(con);
	while ((row = mysql_fetch_row(res)) != NULL)
		return atoi(row[0]);
 }