DROP DATABASE if exists test;

CREATE DATABASE test;

USE test;

CREATE	TABLE temp(
				id int auto_increment not null,
				nodeid int,
				temp float(8,4),
				time timestamp DEFAULT current_timestamp not null,
				PRIMARY KEY (id)
				);
				
CREATE TABLE accel(
				id int auto_increment not null,
				nodeid int,
				xAxis int,
				yAxis int,
				zAxis int,
				time timestamp DEFAULT current_timestamp not null,
				PRIMARY KEY (id)
				);
				
CREATE TABLE cur (
				id int auto_increment not null,
				nodeid int,
				cur int,
				time timestamp DEFAULT current_timestamp not null,
				PRIMARY KEY (id)
				);

DROP USER'user'@'localhost';

CREATE USER 'user'@'localhost' IDENTIFIED BY '';

GRANT ALL PRIVILEGES ON test.* TO 'user'@'localhost';
FLUSH PRIVILEGES;
