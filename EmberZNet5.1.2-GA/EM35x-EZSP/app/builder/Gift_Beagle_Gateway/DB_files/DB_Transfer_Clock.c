//Compile: gcc -o DB_Transfer_Clock DB_Transfer_Clock.c -lpthread
//Compile: gcc -o DB_Transfer_Clock DB_Transfer_Clock.c -lpthread

#include <time.h>
#include <stdio.h>
#include <pthread.h>
#include <mysql.h>

#include "mysql-connector.h"

int check_t1, check_t2, dif_t, wait_dif;
struct tm* info;

int Transfer_Status = 1;

/*global shutDown of Thread*/
int DBTransferClockThreadContinue=1;

void initTimer()
{
	time_t rawtime;
	time(&rawtime);
	
	info = gmtime(&rawtime);
}

void start()
{	
	check_t1 = info->tm_min;
	
	//printf("Start: %d\n", start_t);
}

void *check_time_loop(void *minutes)
{
	while (DBTransferClockThreadContinue){
		initTimer();
		check_t2 = info->tm_min;
		dif_t += abs(check_t2 - check_t1);
		
		check_t1 = check_t2;
		
		if (dif_t >= (int)minutes)
		{
			//printf("Send and reset database: \n");

			DO_TCP_Transfer("accel");
			
			DO_TCP_Transfer("temp");
			
			DO_TCP_Transfer("cur");
			
			dif_t = 0;
			start();
		}
		
		if(minutes<MAX_THREAD_SLEEP_TIME_MIN)
		{
			wait_dif = (int)minutes;
			sleep((int)minutes*60);
		}
		else
		{
			wait_dif = wait_dif - MAX_THREAD_SLEEP_TIME_MIN;
			sleep((int)MAX_THREAD_SLEEP_TIME_MIN*60);
		}
	}
	pthread_exit(&Transfer_Status);
}

pthread_t Timer_Thread(int minutes)
{	
	initTimer();
	start();
	wait_dif = minutes;
	pthread_t thread;
	int thr = pthread_create(&thread, NULL, &check_time_loop, (void*)minutes);
	if (thr) 
		printf("failed to create thread.\n");
	
	return thread;
}