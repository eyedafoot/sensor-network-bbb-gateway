// Offset between the ZigBee epoch and Unix epoch
#define ZIGBEE_EPOCH_OFFSET 946684800

// How frequently (in seconds) we should initiate a sensor service discovery
#define SENSOR_FIND_FREQUENCY 30

#define SENSOR_TEMP   1
#define SENSOR_ACCEL  2

typedef struct {
  EmberNodeId nodeId;
  int8u index;
  int8u sensors;
  int16s temp;
  int16s xaccel;
  int16s yaccel;
  int16s zaccel;
} AppSensor;

void appReadSensor(int16u cluster, int8u * attributes, int8u len, int8u index);
void appSensorAddressFound(const EmberAfServiceDiscoveryResult *result);
void appAddTempSensor(const EmberAfServiceDiscoveryResult *result);
