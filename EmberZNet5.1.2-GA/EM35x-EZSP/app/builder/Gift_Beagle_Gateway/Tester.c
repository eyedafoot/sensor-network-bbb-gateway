/* compile: gcc Tester.c DB_Utilities.c DB_Table_2_CSV.c FTP_connection.c DB_Memory_Checker.c DB_Transfer_Clock.c -I/usr/include/mysql -lmysqlclient -I/usr/include/curl -lcurl -lpthread */

//DB tables store here: /var/mysql/


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "mysql-connector.h"

int main ()
{		
	//pthread_t timerThread = Timer_Thread(2);
	
	pthread_t memoryThread = Memory_Check(2,20000);
	
	while (1)
	{
		Insert_Temp(1, 30.93);
		Insert_Accel(1, 20, 100, -30);
		Insert_Accel(1, 60, 150, 10);
		Insert_Cur(1, 20);
	
		Insert_Temp(2, 29.78);
		Insert_Accel(2, -100, 60, 90);
		Insert_Cur(2, 3);
	}
	
	
	exit(0);
}