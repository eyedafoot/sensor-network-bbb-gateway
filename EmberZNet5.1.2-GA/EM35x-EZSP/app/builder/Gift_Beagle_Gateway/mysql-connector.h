#ifndef HEADER_FILE
#define HEADER_FILE
#include <pthread.h>

#define SQL_HOST_IP_ADRESS "127.0.0.1"
#define SQL_USERNAME "user"
#define SQL_PASSWORD ""
#define SQL_DATABASE_NAME "test"

#define FTP_SERVER "ftp://matthew:Basketball4@[131.202.94.142]:80/"

#define MAX_THREAD_SLEEP_TIME_MIN 30

/*Thread shutdowns*/
extern int DBTransferClockThreadContinue;
extern int DBMemoryCheckerThreadContinue;

//puts commas in between each column in the table when turning into files, so fileName should be a .csv
void Table_2_CSV(char* fileName, char* tableName);
void Delete_File(char* fileName);

//Calls to the local mysql database.
void Insert_Temp(int nodeid, float temp);
void Insert_Accel(int nodeid, int xAxis, int yAxis, int zAxis);
void Insert_Cur(int nodeid, int cur);
void Clear_Table(char* tableName);

//give the table name and this method will send that table as a CSV to the FTP_SERVER
void DO_TCP_Transfer(char* tableName);

//checks if memory is reaching full capacity.
//INPUT: minutes, the interval at which to check memory.
//		 num_DB_Inserts, the minimum number of inserts before pushing.
//OUTPUT: a pthread object so that the thread can be terminated if necessary.
pthread_t Memory_Check(int minutes, int num_DB_Inserts);

//Create a timer thread that keeps track of the time sense last upload to main server.
//INPUT: minutes, is the number of minutes between each upload.
//OUTPUT: a pthread object so that the thread can be terminated if necessary.
pthread_t Timer_Thread(int minutes);

#endif