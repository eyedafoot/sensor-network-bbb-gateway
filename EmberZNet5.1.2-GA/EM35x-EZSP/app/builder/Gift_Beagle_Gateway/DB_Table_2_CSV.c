#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//Need to make sure mysql libraries are installed in compiling system.
//Compile: gcc -o DB-2-CSV DB-2-CSV.c -I/usr/include/mysql –lmysqlclient

//Compile without main: gcc -c DB_Temp_2_CSV.c -I/usr/include/mysql -lmysqlclient

//Path to mysql on BBB: /usr/include/mysql

#include <mysql.h>

#include "mysql-connector.h"

void file_write_csv(MYSQL_RES *res, int num_cols_in_table_info, char* file)
{
	FILE *f = fopen(file, "a");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return;
	}
	
	fprintf (f, "\n");
	
	MYSQL_ROW row;
	
	while ((row = mysql_fetch_row(res)) != NULL)
	{
		int i;
		for (i = 0; i < num_cols_in_table_info ; i++)
		{
			fprintf (f, "%s,", row[i]);
		}
	}
	
	
	fclose(f);
	return;
}

void file_add_headers_csv(MYSQL_ROW row, char* file)
{	
	FILE *f = fopen(file, "a");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return;
	}
	
	fprintf (f, "%s,", row[0]);
	
	fclose(f);
	return;
}

void file_clear_csv(char * file)
{
	FILE *f = fopen(file, "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return;
	}
	fclose(f);
}

void Table_2_CSV(char* fileName, char* tableName)
{	
	MYSQL_RES *res;
	MYSQL_ROW row;
	
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}   
	
	char query[100];
	sprintf(query,"SHOW COLUMNS FROM %s",tableName);
	
	//getting the columns from info
	if (mysql_query(con, &query[0]))
	{
		finish_with_error(con);
	}
	
	file_clear_csv(fileName);
	
	res = mysql_use_result(con);
	while ((row = mysql_fetch_row(res)) != NULL)
		file_add_headers_csv(row, fileName);
		
	//sets static global num_cols_in_table_info to the number of columns in table info.
	int num_cols_in_table_info = mysql_num_rows(res);
	mysql_free_result(res);
	
	sprintf(query,"SELECT * FROM %s",tableName);
	
	//getting the values from the table info
	if (mysql_query(con, &query[0])) 
	{
		finish_with_error(con);
	}
   
	res = mysql_use_result(con);
	file_write_csv(res, num_cols_in_table_info, fileName);

	mysql_free_result(res);
	
	mysql_close(con);
	return;
}

void Delete_File(char* fileName)
{
	unlink(fileName);
}