//

// This callback file is created for your convenience. You may add application code
// to this file. If you regenerate this file over a previous version, the previous
// version will be overwritten and any code you have added will be lost.

#include "app/framework/include/af.h"

// Custom event stubs. Custom events will be run along with all other events in the
// application framework. They should be managed using the Ember Event API
// documented in stack/include/events.h

// Event control struct declarations
EmberEventControl findSensorEventControl;
EmberEventControl querrySensorEventControl;
EmberEventControl printEventControl;

// Event function forward declarations
void findSensorEvent(void);
void querrySensors(void);
void printEventFunction(void);

// Event function stubs
void findSensorEvent(void) { }
void querrySensors(void) { }
void printEventFunction(void) { }

/** @brief Main Init
 *
 * This function is called from the application's main function. It gives the
 * application a chance to do any initialization required at system startup.
 * Any code that you would normally put into the top of the application's
 * main() routine should be put into this function.
        Note: No callback
 * in the Application Framework is associated with resource cleanup. If you
 * are implementing your application on a Unix host where resource cleanup is
 * a consideration, we expect that you will use the standard Posix system
 * calls, including the use of atexit() and handlers for signals such as
 * SIGTERM, SIGINT, SIGCHLD, SIGPIPE and so on. If you use the signal()
 * function to register your signal handler, please mind the returned value
 * which may be an Application Framework function. If the return value is
 * non-null, please make sure that you call the returned function from your
 * handler to avoid negating the resource cleanup of the Application Framework
 * itself.
 *
 */
void emberAfMainInitCallback(void)
{
}

/** @brief Get Current Time
 *
 * This callback is called when device attempts to get current time from the
 * hardware. If this device has means to retrieve exact time, then this method
 * should implement it. If the callback can't provide the exact time it should
 * return 0 to indicate failure. Default action is to return 0, which
 * indicates that device does not have access to real time.
 *
 */
int32u emberAfGetCurrentTimeCallback(void)
{
  return 0;
}

/** @brief Read Attributes Response
 *
 * This function is called by the application framework when a Read Attributes
 * Response command is received from an external device.  The application
 * should return TRUE if the message was processed or FALSE if it was not.
 *
 * @param clusterId The cluster identifier of this response.  Ver.: always
 * @param buffer Buffer containing the list of read attribute status records. 
 * Ver.: always
 * @param bufLen The length in bytes of the list.  Ver.: always
 */
boolean emberAfReadAttributesResponseCallback(EmberAfClusterId clusterId,
                                              int8u * buffer,
                                              int16u bufLen)
{
  return FALSE;
}

/** @brief External Attribute Write
 *
 * This function is called whenever the Application Framework needs to write
 * an attribute which is not stored within the data structures of the
 * Application Framework itself. One of the new features in Version 2 is the
 * ability to store attributes outside the Framework. This is particularly
 * useful for attributes that do not need to be stored because they can be
 * read off the hardware when they are needed, or are stored in some central
 * location used by many modules within the system. In this case, you can
 * indicate that the attribute is stored externally. When the framework needs
 * to write an external attribute, it makes a call to this callback.
       
 * This callback is very useful for host micros which need to store attributes
 * in persistent memory. Because each host micro (used with an Ember NCP) has
 * its own type of persistent memory storage, the Application Framework does
 * not include the ability to mark attributes as stored in flash the way that
 * it does for Ember SoCs like the EM35x. On a host micro, any attributes that
 * need to be stored in persistent memory should be marked as external and
 * accessed through the external read and write callbacks. Any host code
 * associated with the persistent storage should be implemented within this
 * callback.
        All of the important information about the attribute
 * itself is passed as a pointer to an EmberAfAttributeMetadata struct, which
 * is stored within the application and used to manage the attribute. A
 * complete description of the EmberAfAttributeMetadata struct is provided in
 * app/framework/include/af-types.h.
        This function assumes that the
 * application is able to write the attribute and return immediately. Any
 * attributes that require a state machine for reading and writing are not
 * candidates for externalization at the present time. The Application
 * Framework does not currently include a state machine for reading or writing
 * attributes that must take place across a series of application ticks.
 * Attributes that cannot be written immediately should be stored within the
 * Application Framework and updated occasionally by the application code from
 * within the emberAfMainTickCallback.
        If the application was
 * successfully able to write the attribute, it returns a value of
 * EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the application
 * was not able to write the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeWriteCallback(int8u endpoint,
                                                    EmberAfClusterId clusterId,
                                                    EmberAfAttributeMetadata * attributeMetadata,
                                                    int16u manufacturerCode,
                                                    int8u * buffer)
{
  return EMBER_ZCL_STATUS_FAILURE;
}

/** @brief External Attribute Read
 *
 * Like emberAfExternalAttributeWriteCallback above, this function is called
 * when the framework needs to read an attribute that is not stored within the
 * Application Framework's data structures.
        All of the important
 * information about the attribute itself is passed as a pointer to an
 * EmberAfAttributeMetadata struct, which is stored within the application and
 * used to manage the attribute. A complete description of the
 * EmberAfAttributeMetadata struct is provided in
 * app/framework/include/af-types.h
        This function assumes that the
 * application is able to read the attribute, write it into the passed buffer,
 * and return immediately. Any attributes that require a state machine for
 * reading and writing are not really candidates for externalization at the
 * present time. The Application Framework does not currently include a state
 * machine for reading or writing attributes that must take place across a
 * series of application ticks. Attributes that cannot be read in a timely
 * manner should be stored within the Application Framework and updated
 * occasionally by the application code from within the
 * emberAfMainTickCallback.
        If the application was successfully able
 * to read the attribute and write it into the passed buffer, it should return
 * a value of EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the
 * application was not able to read the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeReadCallback(int8u endpoint,
                                                   EmberAfClusterId clusterId,
                                                   EmberAfAttributeMetadata * attributeMetadata,
                                                   int16u manufacturerCode,
                                                   int8u * buffer)
{
  return EMBER_ZCL_STATUS_FAILURE;
}

/** @brief Broadcast Sent
 *
 * This function is called when a new MTORR broadcast has been successfully
 * sent by the concentrator plugin.
 *
 */
void emberAfPluginConcentratorBroadcastSentCallback(void)
{
}

/** @brief Finished
 *
 * This callback is fired when the network-find plugin is finished with the
 * forming or joining process.  The result of the operation will be returned
 * in the status parameter.
 *
 * @param status   Ver.: always
 */
void emberAfPluginNetworkFindFinishedCallback(EmberStatus status)
{
}

/** @brief Get Radio Power For Channel
 *
 * This callback is called by the framework when it is setting the radio power
 * during the discovery process. The framework will set the radio power
 * depending on what is returned by this callback.
 *
 * @param channel   Ver.: always
 */
int8s emberAfPluginNetworkFindGetRadioPowerForChannelCallback(int8u channel)
{
  return EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER;
}

/** @brief Join
 *
 * This callback is called by the plugin when a joinable network has been
 * found.  If the application returns TRUE, the plugin will attempt to join
 * the network.  Otherwise, the plugin will ignore the network and continue
 * searching.  Applications can use this callback to implement a network
 * blacklist.
 *
 * @param networkFound   Ver.: always
 * @param lqi   Ver.: always
 * @param rssi   Ver.: always
 */
boolean emberAfPluginNetworkFindJoinCallback(EmberZigbeeNetwork * networkFound,
                                             int8u lqi,
                                             int8s rssi)
{
  return TRUE;
}

/** @brief Select File Descriptors
 *
 * This function is called when the Gateway plugin will do a select() call to
 * yield the processor until it has a timed event that needs to execute.  The
 * function implementor may add additional file descriptors that the
 * application will monitor with select() for data ready.  These file
 * descriptors must be read file descriptors.  The number of file descriptors
 * added must be returned by the function (0 for none added).
 *
 * @param list A pointer to a list of File descriptors that the function
 * implementor may append to  Ver.: always
 * @param maxSize The maximum number of elements that the function implementor
 * may add.  Ver.: always
 */
int emberAfPluginGatewaySelectFileDescriptorsCallback(int* list,
                                                      int maxSize)
{
  return 0;
}


