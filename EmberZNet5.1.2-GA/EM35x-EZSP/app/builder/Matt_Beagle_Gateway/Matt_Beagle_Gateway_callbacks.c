// This callback file is created for your convenience. You may add application code
// to this file. If you regenerate this file over a previous version, the previous
// version will be overwritten and any code you have added will be lost.

#include "app/framework/include/af.h"
//#include "stack/include/ember.h"
#include "app/util/common/form-and-join.h"
#include "app/framework/plugin/ezmode-commissioning/ez-mode.h"
#include "Matt_Beagle_Gateway.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "DataTransferUtil/DataTransferUtil.h"
#include "GeneralUtil/DataTypes.h"

#include "NodeConfiguration/configurationTypes.h"
#include "NodeConfiguration/configurationEncoding.h"

// This callback file is created for your convenience. You may add application code
// to this file. If you regenerate this file over a previous version, the previous
// version will be overwritten and any code you have added will be lost.

//Gateway MasterKey
static const char *masterKey = "15205a82cb86361a";

//UNB Sensor Network URL constants
static const char *host = "http://192.168.2.19:3000";

//Gateway Id
static const uint8_t gatewayId = 4;

#define Node_count 29

//key value pair for xml parsing
typedef enum keys {
    NODE_ID = 0,
    ADC_MOD = 1,
    ADC_BITS = 2,
    ADC_CLK = 3,
    ADC_PERIOD = 4,
    ADC_C0_P = 5,
    ADC_C0_N = 6,
    ADC_C1_P = 7,
    ADC_C1_N = 8,
    ADC_C2_P = 9,
    ADC_C2_N = 10,
    ADC_C3_P = 11,
    ADC_C3_N = 12,
    TEMP_PERIOD = 13,
    ACCL_MAXG = 14,
    ACCL_PERIOD = 15,
    GET_CONFIG = 16,
    SET_CONFIG = 17,
    API_KEY = 18,
    NONE = -1
} Key;

static const char *values[] = {
        "node",
        "adc_mode",
        "adc_bits",
        "adc_clock",
        "adc_seconds",
        "adc_cfg0_positive",
        "adc_cfg0_negative",
        "adc_cfg1_positive",
        "adc_cfg1_negative",
        "adc_cfg2_positive",
        "adc_cfg2_negative",
        "adc_cfg3_positive",
        "adc_cfg3_negative",
        "temp_seconds",
        "acc_maxG",
        "acc_seconds",
        "Get_Node_Configuration",
        "Set_Node_Configuration",
        "api_key"
};

// Event control struct declaration
EmberEventControl findSensorEventControl;
EmberEventControl querrySensorEventControl;
EmberEventControl printEventControl;

// Globals
int16u numSensors = 0;
#define PJOIN_DURATION_S 20
int my_test = 0;
int des = -1;
cellular = 0;
int test = 1;

// Offset between the ZigBee epoch and Unix epoch
#define ZIGBEE_EPOCH_OFFSET 946684800
int valuem = 0;
// How frequently (in seconds) we should initiate a sensor service discovery
#define SENSOR_FIND_FREQUENCY 30

#define SENSOR_TEMP 1
#define SENSOR_ACCEL 2
struct timeval wait_time;
int waiting = 1;

typedef struct {
    EmberNodeId nodeId;
    int8s nodeNumber;
    int8u index;
    int8u sensors;
    int16s temp;
    int16s cur;
    int16s xaccel;
    int16s yaccel;
    int16s zaccel;
    int8u config[15];
    char apiKey[18];
    int8s nodeRole;
    int8s nodeParent;
}
        AppSensor;

//list to hold 0:nodeId, 1:tempFieldId, 2:x-accelFieldId, 3:y-accelFieldId, 4:z-accelFieldId
// -1 for an id will mean field not setup
int16s ListNodeField[Node_count][5] = {
        {1,1,3,6,7},
        {2,-1,-1,-1,-1}
};

/*
 * appReadSensor, TODO
 *
 * cluster = TODO
 * attributes = TODO
 * len = TODO
 * index = TODO
 */
void appReadSensor(int16u cluster, int8u *attributes, int8u len, int8u index);
/*
 * appSensorAddressFound, TODO
 *
 * result = TODO
 */
void appSensorAddressFound(const EmberAfServiceDiscoveryResult *result);
/*
 * appAddTempSensor, TODO
 *
 * result = TODO
 */
void appAddTempSensor(const EmberAfServiceDiscoveryResult *result);
/*
 * config_value, TODO
 *
 * data_buff = TODO
 * commandID = TODO
 */
int config_value(char *buf, Key key, AcclConfiguration *accl, TempConfiguration *temp, AdcConfiguration *adc, char *apiKeyBuffer);
/*
 * sendDataToServer_Cellular, used to send data to server using cellular network
 *
 * data = TODO
 * fdes = TODO
 * commandID = TODO
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
void requestToJoinNetwork(EmberEUI64 address, int fdes);
/*
 * checkServerForNodeList, used to request device list from server and to update the list of allowed devices accordingly.
 *                         This function also updates the list of fieldIds for nodes
 *
 * fdes = TODO
 */
void checkServerForNodeList(int fdes);
/*
 * UpdateNodeSensorSettings, TODO
 *
 * data_buff = TODO
 * commandID = TODO
 * fdes = TODO
 */
EmberStatus UpdateNodeSensorSettings(char *data_buff, int *commandID, int fdes);
/*
 * prepare_message, used to send messages to nodes (IE. commands for turning on/off sensors and polling rates)
 *
 *
 * nodeId = TODO
 * send_buf = TODO
 * send_len = TODO
 * apiKeyBuffer = TODO
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus prepare_message(int nodeId, int8u *send_buf, int send_len, char *apiKeyBuffer);
/*
 * getNameAndId, parses command XML for commandId and command name
 *
 * buf = char pointer containing the XML to be parsed
 * commandID = pointer to be assigned the command Id
 * name = pointer to be assigned the command name
 */
void getNameAndId(char *buf, int *commandID, Key *name);
/*
 * prepare_nodeinfo, TODO (Is this even used??)
 *
 * filename = TDOD
 */
//void prepare_nodeinfo(char *filename);
/*
 * ElapsedTime_m, TODO
 *
 * PreviousTime = TODO
 */
unsigned long int ElapsedTime_m(struct timeval PreviousTime);
/*
 * getFieldId, used to get fieldId for a node
 *
 * nodeId = the nodeId of the node you want the fieldId for
 * col = the col of the sensor type (1 = temp, 2 = x-accel, 3 = y-accel, 4 = z-accel)
 *
 * return = fieldId
 */
int16s getFieldId(int16s nodeId, int8u col);
/*
 * updateFieldId, used to update a fieldId for a node
 *
 * nodeId = the nodeId of the node you want to update the fieldId of
 * col = the col of the sensor type (1 = temp, 2 = x-accel, 3 = y-accel, 4 = z-accel)
 * fieldId = new fieldId value
 */
void updateFieldId(int16s nodeId, int8u col, int16s fieldId);
/*
 * resetFieldIds, used to reset the fieldIds to -1 for a node
 *
 * nodeId = the nodeId of the node you want to fieldIds for
 */
void resetFieldIds(int16s nodeId);
/*
 * CheckListEuiNode, used to check is an address is in the list of allowed addresses
 *
 * EUI = address of the node being check if access is allowed
 *
 * return = If found: Index node was found at (nodeId)
 *      If not found: 0xFF
 */
int8u CheckListEuiNode (EmberEUI64* EUI);


// *******************************************************************
// Node EUI64 address list available in the network, eui is backwards on memory, MSB first

EmberEUI64 ListEuiNode[Node_count] = {
        { 0x77, 0x80, 0xDB, 0x00, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V1 (OLD NODE)
        { 0x1A, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #01
        { 0x21, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #02
        { 0x18, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #04
        { 0x2C, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #24
        { 0x23, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #05
        { 0x26, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #06
        { 0xB2, 0x5D, 0xD5, 0x00, 0x00, 0x6F, 0x0D, 0x00 }, // KIT
        { 0x1B, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #08
        { 0x29, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #09
        { 0x27, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #10
        { 0x1D, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #11
        //{ 0x22, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #12
        { 0x2A, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #13
        { 0x28, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #14
        { 0x2F, 0x9B, 0x0B, 0x02, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V1 (OLD NODE)
        { 0x15, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #02
        { 0x16, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #03
        { 0x19, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #05
        { 0x1E, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #10
        { 0x20, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #12
        { 0x25, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #17
        { 0x2B, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #23
        { 0x1C, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #03
        { 0x2F, 0x1F, 0xEE, 0x04, 0x00, 0x6F, 0x0D, 0x00 }, // UNB BOARD V2 #25
        { 0x3C, 0xE4, 0xD0, 0x00, 0x00, 0x6F, 0x0D, 0x00 }, // KIT
        { 0xF1, 0x03, 0x60, 0x01, 0x00, 0x6F, 0x0D, 0x00 } // KIT
};

AppSensor Sensors[EMBER_AF_PLUGIN_ADDRESS_TABLE_SIZE];

#define EmberNodeId_Size 2
#define Application_ProfileId_Size 2
#define Application_DeviceId_Size 2

EmberEUI64 eui;      	// long network address
EmberNodeId netwid;     // short network address in 16 bits, this is also used as flag for the process of Binding for one Node
int8u netwidB[2];	// short network address as an array of two bytes
int8u REPcount;		// # of endpoints at the remote Node
int8u REPlist[240];     // The endpoint list at one Node

/** @brief Pre ZDO Message Received
 *
 * This function passes the application an incoming ZDO message and gives the
 * appictation the opportunity to handle it. By default, this callback returns
 * FALSE indicating that the incoming ZDO message has not been handled and
 * should be handled by the Application Framework.
 *
 * @param emberNodeId   Ver.: always
 * @param apsFrame   Ver.: always
 * @param message   Ver.: always
 * @param length   Ver.: always
 */
boolean emberAfPreZDOMessageReceivedCallback(EmberNodeId emberNodeId,
                                             EmberApsFrame* apsFrame,
                                             int8u* message,
                                             int16u length)
{
    if (((apsFrame->clusterId) == END_DEVICE_ANNOUNCE)&&(netwid==0)) {
        printf("RX END_DEVICE_ANNOUNCE from profile 0x%4x culster 0x%4x of length 0x%x\r\n",
               apsFrame->profileId,
               apsFrame->clusterId,
               length);

        //TODO: Check table list
        MEMCOPY(&netwid, (message+1), EmberNodeId_Size);
        MEMCOPY(&netwidB, (message+1), EmberNodeId_Size);

//        EmberStatus status;
//        status = emberAfEzmodeServerCommission(emberAfPrimaryEndpoint());
//        EndpointRequest(netwidB,netwid);
        printf("EmberNodeId: %X\n", emberNodeId);
        printf("netwid: %X\n", netwid);
        printf("netwidB: %X\n", netwidB);
        // Nothing to do if we already know about this node
//        int i;
//        for (i = 0; i < numSensors; i++)
//            if (Sensors[i].nodeId == emberNodeId) return FALSE;
//
//        // If it's a new sensor, add it to the list
//        AppSensor *s = &(Sensors[numSensors++]);
//        s -> nodeId = emberNodeId;
//        s -> nodeNumber = -1;
//        s -> sensors = SENSOR_TEMP;
//        s -> index = EMBER_NULL_ADDRESS_TABLE_INDEX;
//        s -> temp = 0;
//        s -> cur = 0;
//        s -> xaccel = 0;
//        s -> yaccel = 0;
//        s -> zaccel = 0;
//        s -> nodeParent = 1;
//        s -> nodeRole = 0;

        return FALSE;
    }
    else if ((apsFrame->clusterId == ACTIVE_ENDPOINTS_RESPONSE) && (emberNodeId==netwid)){
        printf("RX ACTIVE_ENDPOINTS_RESPONSE from profile 0x%4x culster 0x%4x of length 0x%x\r\n",
               apsFrame->profileId,
               apsFrame->clusterId,
               length);

        MEMCOPY(&REPcount, (message+4), 1);
        printf("List of 0x%4x Endpoints ID of the node 0x%4x \r\n",REPcount, netwid);

        // populating the Endpoint list and display it
        int i;
        for (i=0; i<REPcount; i++ ) {
            MEMCOPY(&REPlist[i], (message+5+i), 1);
            printf("%u Remote EndPointID  0x%x \r\n",
                   i+1,
                   REPlist[i]);
        }

        // start asking for the last remote EP
//        SimpleDescriptorRequest( netwidB,netwid, REPlist[REPcount-1]);
    }
    else if ((apsFrame->clusterId == SIMPLE_DESCRIPTOR_RESPONSE) && (emberNodeId==netwid)){
        printf("RX SIMPLE_DESCRIPTOR_RESPONSE from profile 0x%4x culster 0x%4x of length 0x%x \r\n",
                          apsFrame->profileId,
                          apsFrame->clusterId,
                          length);

        // Endpoint Data
        int16u AppProfId;	// The Aplication Profile ID of the Endpoint
        int16u AppDevId;	// The Device ID of the Endpoint
        int32u DeviceFid;	// the combination of Aplication Profile ID and Device ID in 32bits
        // DeviceFid (MS 16bits) AppDevId (LS 16 bits)

        // to update the index at REPlist table  of the EP we are dealing with
        REPcount--;

        MEMCOPY(&AppProfId, (message+6), Application_ProfileId_Size);
        MEMCOPY(&AppDevId, (message+8), Application_DeviceId_Size);

        printf("Application ProfileId 0x%4x Application DeviceId 0x%4x \r\n",
               AppProfId,
               AppDevId);

        // to combine AppProfId and AppDevId in 32 bits
        DeviceFid = 0x00000000;
        DeviceFid = AppProfId << 16;
        DeviceFid = DeviceFid | AppDevId;

        printf("EndPoint 0x%x has a Device Full ID 0x%4x  \r\n",
               REPlist[REPcount], DeviceFid);

        //assumes DeciceFid == 0x01040000 if not should ask for another EP
        //Create a Binding record in the table of the remote node
//        int8u FIdIndex = 2;
//        emberAfFillCommandIdentifyClusterIdentifyQuery();
//        emberAfSetCommandEndpoints(FIdIndex, REPlist[REPcount]);
//        emberAfSendCommandUnicast(EMBER_OUTGOING_DIRECT, netwid); // using network id to send the unicast
//
//        if (REPcount!=0){ // Ask for another EP on the Remote Node
//
//            SimpleDescriptorRequest( netwidB,netwid, REPlist[REPcount-1]);
//        }
    }
    else {
        printf("RX from profile 0x%4x culster 0x%4x of length 0x%x\r\n",
               apsFrame->profileId,
               apsFrame->clusterId,
               length);
    }
    return FALSE;
}

/** @brief Pre Command Received
 *
 * This callback is the second in the Application Framework’s message
 * processing chain. At this point in the processing of incoming over-the-air
 * messages, the application has determined that the incoming message is a ZCL
 * command. It parses enough of the message to populate an
 * EmberAfClusterCommand struct. The Application Framework defines this struct
 * value in a local scope to the command processing but also makes it
 * available through a global pointer called emberAfCurrentCommand, in
 * app/framework/util/util.c. When command processing is complete, this
 * pointer is cleared.
 *
 * @param cmd   Ver.: always
 */
boolean emberAfPreCommandReceivedCallback(EmberAfClusterCommand* cmd){
    if (cmd->commandId == ZCL_REPORT_ATTRIBUTES_COMMAND_ID ){

    }
    if ((cmd->commandId==ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) &&
        (cmd->apsFrame->clusterId== ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) &&
        (cmd->bufLen==13)) {

    }
    else if ((cmd->commandId==ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) &&
        (cmd->apsFrame->clusterId== ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) &&
        (cmd->bufLen==18)) {

    }
    else if ((cmd->source==netwid) &&
        (cmd->apsFrame->clusterId==ZCL_IDENTIFY_CLUSTER_ID) &&
        (cmd->commandId==ZCL_IDENTIFY_QUERY_COMMAND_ID)		&&
        (cmd->type==EMBER_INCOMING_UNICAST)){
        printf("RX OK from EP 0x%2x to EP 0x%2x profile 0x%2x culster 0x%2x of source 0x%2x Command ID 0x%2x\r\n",
               cmd->apsFrame->sourceEndpoint,
               cmd->apsFrame->destinationEndpoint,
               cmd->apsFrame->profileId,
               cmd->apsFrame->clusterId,
               cmd->source,
               cmd->commandId);

        if (REPcount==0) {
            //this reset will indicates the process of Binding to one Node have finished
            netwid = 0X0000;
        }
    }
    return FALSE;
}

// *******************************************************************
// Event functions
void findSensorEvent(void) {
    emberAfEventControlSetDelay( &findSensorEventControl, 1000);

    if (cellular == 1) {
        des = cell_init();
        cellular = 0;
    }

    // Start a service discovery for the sensors we want to monitor
    static int8u sensorDiscoveryCountdown;
    if (sensorDiscoveryCountdown-- <= 0) {
        sensorDiscoveryCountdown = SENSOR_FIND_FREQUENCY;
        emberAfAppPrintln("Running Discovery");
        if (emberNetworkState() != EMBER_NO_NETWORK) {
            emberPermitJoining(PJOIN_DURATION_S);
            netwid = 0X0000;
        }
//        emberAfFindDevicesByProfileAndCluster(EMBER_RX_ON_WHEN_IDLE_BROADCAST_ADDRESS,
//                                              0x0104,
//                                              ZCL_TEMP_MEASUREMENT_CLUSTER_ID,
//                                              EMBER_AF_SERVER_CLUSTER_DISCOVERY,
//                                              appAddTempSensor);
        EmberStatus status;
        status = emberAfEzmodeServerCommission(emberAfPrimaryEndpoint());
    } else {
        // Make sure all of our sensors are in the address table
        int i;
        for (i = 0; i < numSensors; i++) {
            if (Sensors[i].index == EMBER_NULL_ADDRESS_TABLE_INDEX) {
                printf("Sensor[%d]: %X\n", i, Sensors[i].index);
                emberAfFindIeeeAddress(Sensors[i].nodeId, appSensorAddressFound);
                my_test = 1;
                break;
            }
        }
    }
}

/** @brief Pre Message Received
 *
 * This callback is the first in the Application Framework's message
 * processing chain. The Application Framework calls it when a message has
 * been received over the air but has not yet been parsed by the ZCL
 * command-handling code. If you wish to parse some messages that are
 * completely outside the ZCL specification or are not handled by the
 * Application Framework's command handling code, you should intercept them
 * for parsing in this callback.
        This callback returns a Boolean
 * value indicating whether or not the message has been handled. If the
 * callback returns a value of TRUE, then the Application Framework assumes
 * that the message has been handled and it does nothing else with it. If the
 * callback returns a value of FALSE, then the application framework continues
 * to process the message as it would with any incoming message.
        Note:
 *  This callback receives a pointer to an incoming message struct. This
 * struct allows the application framework to provide a unified interface
 * between both Host devices, which receive their message through the
 * ezspIncomingMessageHandler, and SoC devices, which receive their message
 * through emberIncomingMessageHandler.
 *
 * @param incomingMessage   Ver.: always
 */
boolean emberAfPreMessageReceivedCallback(EmberAfIncomingMessage *incomingMessage) {
    if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_TEMP_MEASUREMENT_CLUSTER_ID) {
        int i;
        int16u msgLen = (incomingMessage -> msgLen);
        printf("Node Id %X\n", incomingMessage -> source);
        int8u msb = (incomingMessage -> message)[msgLen - 1];
        int8u lsb = (incomingMessage -> message)[msgLen - 2];
        int16_t temp = (msb << 8) | lsb;
        printf("msb is %X\n", msb);
        printf("lsb is %X\n", lsb);
        int16s dataValue = (int16s) temp;//(incomingMessage -> message));
        int16s fixPart = dataValue / 100;
        int16s fractPart = dataValue - fixPart *100;
        printf("Temperature is %d or %d.%d\n", temp, fixPart, fractPart);
        for (i = 0; i < numSensors; i++) {
            if (Sensors[i].nodeId == incomingMessage -> source) {

                printf("Sending temperature data to server................. \n");
                printf("Node Id %d\n", Sensors[i].nodeNumber);
                printf("Temperature is %d.%d\n", fixPart, fractPart);

                int16s fieldId = getFieldId(Sensors[i].nodeNumber, 1);

                if (fieldId != -1) {
                    char urlBuf[110];

                    sprintf(urlBuf, "%s%s?master_key=%s&field_id=%u&value=%d.%d", host, DATA_PATH, masterKey, fieldId, fixPart, fractPart);
                    EmberStatus status;
                    int commandID = -2;
                    //printf("%s\n",urlBuf);

                    RestMethod method = POST;
                    if (des < 0) {
                        status = sendDataToServer(urlBuf, (char)0, method);
                    } else {
                        status = sendDataToServer_Cellular(urlBuf, (char)0, method, des);
                    }
                }
            }
        }
//        return TRUE;
        return FALSE;
    }

    else if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) {
        int i;
        int16s *dataValue = ((int16s *)(incomingMessage -> message));
        int16s Xvalue = dataValue[0];
        int16s Yvalue = dataValue[1];
        int16s Zvalue = dataValue[2];
        for (i = 0; i < numSensors; i++) {
            if (Sensors[i].nodeId == incomingMessage -> source) {

                printf("Sending accelerometer data to server................. \n");
                printf("Node Id %d\n", Sensors[i].nodeNumber);

                printf("xvalue is %d \n", Xvalue);
                printf("Yvalue is %d \n", Yvalue);
                printf("Zvalue is %d \n", Zvalue);

                EmberStatus status;
                int commandID = -2;
                char urlBuf[110];
                RestMethod method = POST;

                int16s fieldId = getFieldId(Sensors[i].nodeNumber, 2);
                if (fieldId != -1) {
                    sprintf(urlBuf, "%s%s?master_key=%s&field_id=%u&value=%d", host, DATA_PATH, masterKey, fieldId, Xvalue);
                    if (des < 0) {
                        status = sendDataToServer(urlBuf, (char)0, method);
                    } else {
                        status = sendDataToServer_Cellular(urlBuf, (char)0, method, des);
                    }
                }

                fieldId = getFieldId(Sensors[i].nodeNumber, 3);
                if (fieldId != -1) {
                    sprintf(urlBuf, "%s%s?master_key=%s&field_id=%u&value=%d", host, DATA_PATH, masterKey, fieldId, Yvalue);
                    if (des < 0) {
                        status = sendDataToServer(urlBuf, (char)0, method);
                    } else {
                        status = sendDataToServer_Cellular(urlBuf, (char)0, method, des);
                    }
                }

                fieldId = getFieldId(Sensors[i].nodeNumber, 4);
                if (fieldId != -1) {
                    sprintf(urlBuf, "%s%s?master_key=%s&field_id=%u&value=%d", host, DATA_PATH, masterKey, fieldId, Zvalue);
                    if (des < 0) {
                        status = sendDataToServer(urlBuf, (char)0, method);
                    } else {
                        status = sendDataToServer_Cellular(urlBuf, (char)0, method, des);
                    }
                }
            }

        }

        return TRUE;
    }

    else if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_ILLUM_MEASUREMENT_CLUSTER_ID) {
        int i;
        int16s dataValue = *((int16s *)(incomingMessage -> message));
        //int16s fixPart = dataValue / 1000;
        // int16s fractPart = dataValue - fixPart*1000 ;
        for (i = 0; i < numSensors; i++) {
            if (Sensors[i].nodeId == incomingMessage -> source) {

                printf("Sending electric current data to server................. \n");
                printf("Node Id %d\n", Sensors[i].nodeNumber);
                printf("RMS current value is %d \n", dataValue);

                //TODO: Implement this field into the ListNodeField table
                char urlBuf[110];
                sprintf(urlBuf, "%s%s?master_key=%s&field_id=9&value=%d", host, DATA_PATH, masterKey, dataValue);
                EmberStatus status;
                int commandID = -2;
                RestMethod method = POST;

                if (des < 0) {
                    status = sendDataToServer(urlBuf, (char)0, method);
                } else {
                    status = sendDataToServer_Cellular(urlBuf, (char)0, method, des);
                }

            }
        }
        return TRUE;
    }

//    else if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_IDENTIFY_CLUSTER_ID) {
//        int8u nodeId = incomingMessage -> source;
//
//        int8u buf[2]; //timeout
//        buf[0] = 0xFF;
//        buf[1] = 0xFF;
//
//        EmberApsFrame apsFrame;
//        EmberStatus status;
//        apsFrame.profileId = 0x0104;
//        apsFrame.clusterId = ZCL_IDENTIFY_CLUSTER_ID;
//        apsFrame.sourceEndpoint = 1;
//        apsFrame.destinationEndpoint = 2;
//        apsFrame.options = EMBER_APS_OPTION_NONE;
//        apsFrame.groupId = 0;
//        apsFrame.sequence = 1;
//
//        printf("sending uniCast\n");
//        status = emberAfSendUnicast(EMBER_OUTGOING_VIA_ADDRESS_TABLE, nodeId, &apsFrame, sizeof(buf), buf);
//
//        if (status != EMBER_SUCCESS) {
//            printf("sending Message failed \n");
//        }
//        return TRUE;
//    }

//    else {
        printf("Address: %X\n", incomingMessage -> source);
        printf("profileId: %X\n", incomingMessage -> apsFrame -> profileId);
        //if (incomingMessage -> apsFrame -> profileId == 0x0104) incomingMessage -> apsFrame -> profileId = 0x0;
        printf("clusterId: %X\n", incomingMessage -> apsFrame -> clusterId);
        //if (incomingMessage -> apsFrame -> clusterId == ZCL_IDENTIFY_CLUSTER_ID) incomingMessage -> apsFrame -> clusterId = END_DEVICE_ANNOUNCE;
        printf("sourceEndpoint: %X\n", incomingMessage -> apsFrame -> sourceEndpoint);
        //if (incomingMessage -> apsFrame -> sourceEndpoint == 0x08) incomingMessage -> apsFrame -> sourceEndpoint = 0x0;
        printf("destinationEndpoint: %X\n", incomingMessage -> apsFrame -> destinationEndpoint);
        //if (incomingMessage -> apsFrame -> destinationEndpoint == 0xFF) incomingMessage -> apsFrame -> destinationEndpoint = 0x0;
        printf("groupId: %X\n", incomingMessage -> apsFrame -> groupId);
        printf("sequence: %X\n", incomingMessage -> apsFrame -> sequence);
        char *option = "";
        switch (incomingMessage -> apsFrame -> options) {
            case EMBER_APS_OPTION_NONE:
                option = "EMBER_APS_OPTION_NONE";
                break;
            case EMBER_APS_OPTION_DSA_SIGN :
                option = "EMBER_APS_OPTION_DSA_SIGN ";
                break;
            case EMBER_APS_OPTION_ENCRYPTION :
                option = "EMBER_APS_OPTION_ENCRYPTION ";
                break;
            case EMBER_APS_OPTION_RETRY :
                option = "EMBER_APS_OPTION_RETRY ";
                break;
            case EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY :
                option = "EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY ";
                break;
            case EMBER_APS_OPTION_FORCE_ROUTE_DISCOVERY :
                option = "EMBER_APS_OPTION_FORCE_ROUTE_DISCOVERY ";
                break;
            case EMBER_APS_OPTION_SOURCE_EUI64 :
                option = "EMBER_APS_OPTION_SOURCE_EUI64 ";
                break;
            case EMBER_APS_OPTION_DESTINATION_EUI64 :
                option = "EMBER_APS_OPTION_DESTINATION_EUI64 ";
                break;
            case EMBER_APS_OPTION_ENABLE_ADDRESS_DISCOVERY  :
                option = "EMBER_APS_OPTION_ENABLE_ADDRESS_DISCOVERY  ";
                break;
            case EMBER_APS_OPTION_POLL_RESPONSE  :
                option = "EMBER_APS_OPTION_POLL_RESPONSE  ";
                break;
            case EMBER_APS_OPTION_ZDO_RESPONSE_REQUIRED  :
                option = "EMBER_APS_OPTION_ZDO_RESPONSE_REQUIRED  ";
                break;
            case EMBER_APS_OPTION_FRAGMENT  :
                option = "EMBER_APS_OPTION_FRAGMENT  ";
                break;
        }
        printf("option: %s\n", option);
        return FALSE;
//    }
    return FALSE;
}
/** @brief Trust Center Join
 *
 * This callback is called from within the application framework's
 * implementation of emberTrustCenterJoinHandler or ezspTrustCenterJoinHandler.
 * This callback provides the same arguments passed to the
 * TrustCenterJoinHandler. For more information about the TrustCenterJoinHandler
 * please see documentation included in stack/include/trust-center.h.
 *
 * @param newNodeId   Ver.: always
 * @param newNodeEui64   Ver.: always
 * @param parentOfNewNode   Ver.: always
 * @param status   Ver.: always
 * @param decision   Ver.: always
 */
void emberAfTrustCenterJoinCallback(EmberNodeId newNodeId,
                                    EmberEUI64 newNodeEui64,
                                    EmberNodeId parentOfNewNode,
                                    EmberDeviceUpdate status,
                                    EmberJoinDecision decision)
{
    printf("Trust Callback nodeId: %X\n", newNodeId);
    char* decisionStr = "";
    switch (decision) {
        case EMBER_USE_PRECONFIGURED_KEY:
            decisionStr = "EMBER_USE_PRECONFIGURED_KEY";
            break;
        case EMBER_SEND_KEY_IN_THE_CLEAR:
            decisionStr = "EMBER_SEND_KEY_IN_THE_CLEAR";
            break;
        case EMBER_DENY_JOIN:
            decisionStr = "EMBER_DENY_JOIN";
            break;
        case EMBER_NO_ACTION:
            decisionStr = "EMBER_NO_ACTION";
            break;
    }
    printf("decision: %s\n", decisionStr);
}

/** @brief Remote Set Binding Permission
 *
 * This function is called by the framework to request permission to service
 * the remote set binding request. Return EMBER_SUCCESS to allow request,
 * anything else to disallow request.
 *
 * @param entry Ember Binding Tablet Entry  Ver.: always
 */
EmberStatus emberAfRemoteSetBindingPermissionCallback(const EmberBindingTableEntry * entry)
{
    printf("Binding identifier: %X\n", entry -> identifier);
    printf("Binding clusterId: %X\n", entry -> clusterId);
    printf("Binding networkIndex: %X\n", entry -> networkIndex);

    return EMBER_SUCCESS; // default
}

int16s getFieldId(int16s nodeId, int8u col) {
    int i;
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i][0] == nodeId) {
            return ListNodeField[i][col];
        }
    }
    return -1;
}

void updateFieldId(int16s nodeId, int8u col, int16s fieldId) {
    int i;
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i][0] == nodeId) {
            ListNodeField[i][col] = fieldId;
            return;
        }
    }
    //Didn't find the nodeId - Add it
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i][0] == NULL || ListNodeField[i][0] == -1) {
            ListNodeField[i][0] = nodeId;
            ListNodeField[i][col] = fieldId;
            return;
        }
    }

}

void resetFieldIds(int16s nodeId) {
    int i;
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i][0] == nodeId) {
            ListNodeField[i][1] = -1;
            ListNodeField[i][2] = -1;
            ListNodeField[i][3] = -1;
            ListNodeField[i][4] = -1;
            return;
        }
    }
}

void querrySensors(void) {
    emberEventControlSetInactive(querrySensorEventControl);

}

void printEventFunction(void) {
    emberAfEventControlSetDelay( &printEventControl, 10000);

    printf("checking for commands.... \n");
    EmberStatus status;
    int commandID = -1;
    char sendfile[300];
    char urlBuf[410];

    if (test == 1) {
        //get command
        sprintf(urlBuf, "%s%s.xml?gateway_id=%d&master_key=%s", host, COMMAND_PATH, gatewayId, masterKey);
        //printf("%s\n", urlBuf);
        status = UpdateNodeSensorSettings(urlBuf, &commandID, des);
        if (status == EMBER_SUCCESS) {
            //command successfully ran, get next command
            RestMethod method = PUT;
            sprintf(urlBuf, "%s%s.xml?gateway_id=%d&master_key=%s&command_id=%d", host, COMMAND_PATH, gatewayId, masterKey, commandID);
            if (des < 0) {
                status = sendDataToServer(urlBuf, (char)0, method);
            } else {
                status = sendDataToServer_Cellular(urlBuf, (char)0, method, des);
            }
            //test = 0;
            if (status == EMBER_SUCCESS) {
                checkServerForNodeList(des);
            }
        }

    }

}

/** @brief Main Init
 *
 * This function is called from the application's main function. It gives the
 * application a chance to do any initialization required at system startup.
 * Any code that you would normally put into the top of the application's
 * main() routine should be put into this function.
        Note: No callback
 * in the Application Framework is associated with resource cleanup. If you
 * are implementing your application on a Unix host where resource cleanup is
 * a consideration, we expect that you will use the standard Posix system
 * calls, including the use of atexit() and handlers for signals such as
 * SIGTERM, SIGINT, SIGCHLD, SIGPIPE and so on. If you use the signal()
 * function to register your signal handler, please mind the returned value
 * which may be an Application Framework function. If the return value is
 * non-null, please make sure that you call the returned function from your
 * handler to avoid negating the resource cleanup of the Application Framework
 * itself.
 *
 */
void emberAfMainInitCallback(void) {

    emberAfEventControlSetDelay( &findSensorEventControl, 1000);
    emberAfEventControlSetDelay( &querrySensorEventControl, 5000);
    emberAfEventControlSetDelay( &printEventControl, 5000);
}

/** @brief Get Current Time
 *
 * This callback is called when device attempts to get current time from the
 * hardware. If this device has means to retrieve exact time, then this method
 * should implement it. If the callback can't provide the exact time it should
 * return 0 to indicate failure. Default action is to return 0, which
 * indicates that device does not have access to real time.
 *
 */
int32u emberAfGetCurrentTimeCallback(void) {
    return time(NULL) - ZIGBEE_EPOCH_OFFSET;
}

/** @brief Broadcast Sent
 *
 * This function is called when a new MTORR broadcast has been successfully
 * sent by the concentrator plugin.
 *
 */
void emberAfPluginConcentratorBroadcastSentCallback(void) {}

/** @brief Finished
 *
 * This callback is fired when the network-find plugin is finished with the
 * forming or joining process.  The result of the operation will be returned
 * in the status parameter.
 *
 * @param status   Ver.: always
 */
void emberAfPluginNetworkFindFinishedCallback(EmberStatus status) {}

/** @brief Get Radio Power For Channel
 *
 * This callback is called by the framework when it is setting the radio power
 * during the discovery process. The framework will set the radio power
 * depending on what is returned by this callback.
 *
 * @param channel   Ver.: always
 */
int8s emberAfPluginNetworkFindGetRadioPowerForChannelCallback(int8u channel) {
    return EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER;
}

/** @brief Join
 *
 * This callback is called by the plugin when a joinable network has been
 * found.  If the application returns TRUE, the plugin will attempt to join
 * the network.  Otherwise, the plugin will ignore the network and continue
 * searching.  Applications can use this callback to implement a network
 * blacklist.
 *
 * @param networkFound   Ver.: always
 * @param lqi   Ver.: always
 * @param rssi   Ver.: always
 */
boolean emberAfPluginNetworkFindJoinCallback(EmberZigbeeNetwork *networkFound,
                                             int8u lqi,
                                             int8s rssi) {
    printf("FindJoinCallBack - lqi: %X\n", lqi);

    return TRUE;
}

/** @brief Select File Descriptors
 *
 * This function is called when the Gateway plugin will do a select() call to
 * yield the processor until it has a timed event that needs to execute.  The
 * function implementor may add additional file descriptors that the
 * application will monitor with select() for data ready.  These file
 * descriptors must be read file descriptors.  The number of file descriptors
 * added must be returned by the function (0 for none added).
 *
 * @param list A pointer to a list of File descriptors that the function
 * implementor may append to  Ver.: always
 * @param maxSize The maximum number of elements that the function implementor
 * may add.  Ver.: always
 */
int emberAfPluginGatewaySelectFileDescriptorsCallback(int *list,
                                                      int maxSize) {
    return 0;
}

/** @brief Read Attributes Response
 *
 * This function is called by the application framework when a Read Attributes
 * Response command is received from an external device.  The application
 * should return TRUE if the message was processed or FALSE if it was not.
 *
 * @param clusterId The cluster identifier of this response.  Ver.: always
 * @param buffer Buffer containing the list of read attribute status records.
 * Ver.: always
 * @param bufLen The length in bytes of the list.  Ver.: always
 */
boolean emberAfReadAttributesResponseCallback(EmberAfClusterId clusterId,
                                              int8u *buffer,
                                              int16u bufLen) {
    return FALSE;
}

/** @brief External Attribute Write
 *
 * This function is called whenever the Application Framework needs to write
 * an attribute which is not stored within the data structures of the
 * Application Framework itself. One of the new features in Version 2 is the
 * ability to store attributes outside the Framework. This is particularly
 * useful for attributes that do not need to be stored because they can be
 * read off the hardware when they are needed, or are stored in some central
 * location used by many modules within the system. In this case, you can
 * indicate that the attribute is stored externally. When the framework needs
 * to write an external attribute, it makes a call to this callback.

 * This callback is very useful for host micros which need to store attributes
 * in persistent memory. Because each host micro (used with an Ember NCP) has
 * its own type of persistent memory storage, the Application Framework does
 * not include the ability to mark attributes as stored in flash the way that
 * it does for Ember SoCs like the EM35x. On a host micro, any attributes that
 * need to be stored in persistent memory should be marked   as external and
 * accessed through the external read and write callbacks. Any host code
 * associated with the persistent storage should be implemented within this
 * callback.
        All of the important information about the attribute
 * itself is passed as a pointer to an EmberAfAttributeMetadata struct, which
 * is stored within the application and used to manage the attribute. A
 * complete description of the EmberAfAttributeMetadata struct is provided in
 * app/framework/include/af-types.h.
        This function assumes that the
 * application is able to write the attribute and return immediately. Any
 * attributes that require a state machine for reading and writing are not
 * candidates for externalization at the present time. The Application
 * Framework does not currently include a state machine for reading or writing
 * attributes that must take place across a series of application ticks.
 * Attributes that cannot be written immediately should be stored within the
 * Application Framework and updated occasionally by the application code from
 * within the emberAfMainTickCallback.
        If the application was
 * successfully able to write the attribute, it returns a value of
 * EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the application
 * was not able to write the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeWriteCallback(int8u endpoint,
                                                    EmberAfClusterId clusterId,
                                                    EmberAfAttributeMetadata *attributeMetadata,
                                                    int16u manufacturerCode,
                                                    int8u *buffer) {
    return EMBER_ZCL_STATUS_FAILURE;
}

/** @brief External Attribute Read
 *
 * Like emberAfExternalAttributeWriteCallback above, this function is called
 * when the framework needs to read an attribute that is not stored within the
 * Application Framework's data structures.
        All of the important
 * information about the attribute itself is passed as a pointer to an
 * EmberAfAttributeMetadata struct, which is stored within the application and
 * used to manage the attribute. A complete description of the
 * EmberAfAttributeMetadata struct is provided in
 * app/framework/include/af-types.h
        This function assumes that the
 * application is able to read the attribute, write it into the passed buffer,
 * and return immediately. Any attributes that require a state machine for
 * reading and writing are not really candidates for externalization at the
 * present time. The Application Framework does not currently include a state
 * machine for reading or writing attributes that must take place across a
 * series of application ticks. Attributes that cannot be read in a timely
 * manner should be stored within the Application Framework and updated
 * occasionally by the application code from within the
 * emberAfMainTickCallback.
        If the application was successfully able
 * to read the attribute and write it into the passed buffer, it should return
 * a value of EMBER_ZCL_STATUS_SUCCESS. Any other return value indicates the
 * application was not able to read the attribute.
 *
 * @param endpoint   Ver.: always
 * @param clusterId   Ver.: always
 * @param attributeMetadata   Ver.: always
 * @param manufacturerCode   Ver.: always
 * @param buffer   Ver.: always
 */
EmberAfStatus emberAfExternalAttributeReadCallback(int8u endpoint,
                                                   EmberAfClusterId clusterId,
                                                   EmberAfAttributeMetadata *attributeMetadata,
                                                   int16u manufacturerCode,
                                                   int8u *buffer) {
    return EMBER_ZCL_STATUS_FAILURE;
}

void appSensorAddressFound(const EmberAfServiceDiscoveryResult *result) {
    // We are only concerned with positive matches for our service discovery
    printf ("appSensorAddressFound - address: %X\n", result -> matchAddress);
    if (result -> status != EMBER_AF_UNICAST_SERVICE_DISCOVERY_COMPLETE_WITH_RESPONSE) return;

    // Find the table index for the sensor we just found the long address of
    int i = 0;
    AppSensor *sensor = NULL;
    for (i = 0; i < numSensors; i++) {
        if (result -> matchAddress == Sensors[i].nodeId) {
            sensor = &(Sensors[i]);
            break;
        }
    }
    if (sensor == NULL) return;

    // Now that we have the long address (EUI64) of the sensor, we can add it
    // to the address table.
    int8u NodeIdTable;
    EmberEUI64 address;
    const EmberEUI64 *r = result -> responseData;
    for (i = 0; i < EUI64_SIZE; i++) address[i] = ( *r)[i];

    NodeIdTable = CheckListEuiNode( &address);

    if (NodeIdTable != 0xff) {
        emberAfAppPrintln(" i got right :%d", NodeIdTable);

        sensor -> index = emberAfAddAddressTableEntry(address, sensor -> nodeId);
        sensor -> nodeNumber = NodeIdTable;
    } else {
        emberAfAppPrintln(" i got wrong, address: %X", address);

        //Check who is allowed on the network
        checkServerForNodeList(des);

        //TODO: create list to hold items that have already requested to join network (so we don't keep sending requests for no reason).
        //Send Requestion to join network
        requestToJoinNetwork(address, des);

        sensor -> index = EMBER_NULL_ADDRESS_TABLE_INDEX;
    }
}

void requestToJoinNetwork(EmberEUI64 address, int fdes) {
    char *addressStr = malloc(40);
    strcpy(addressStr, "");
    int8u *p;
    int8u j;
    p = (int8u *) address;
    for (j = 0; j < 8; j++) {
        char addressStrBuf[3];
        sprintf(addressStrBuf, "%X",p[j]);
        strcat(addressStr, addressStrBuf);
        if ( j < 7 ) {
            char comma[2] = ",";
            strcat(addressStr, comma);
        }
    }

    char urlBuf[150];
    sprintf(urlBuf, "%s%s/approvalrequest.xml?master_key=%s&gateway_id=%d&address=%s", host, DEVICES_PATH, masterKey, gatewayId, addressStr);

    EmberStatus status;
    RestMethod method = PUT;
    if (fdes > 0) {
        status = sendDataToServer(urlBuf, (char)0, method);
    } else {
        status = sendDataToServer_Cellular(urlBuf, (char)0, method, fdes);
    }

    emberAfAppPrintln("Node Requested Access (Address): %s", addressStr);

    free(addressStr);
}

void checkServerForNodeList(int fdes) {
    EmberStatus status = !EMBER_SUCCESS;
    char urlBuf[110];
    RestMethod method = GET;
    struct string recievedData;
    init_string(&recievedData);

    sprintf(urlBuf, ".xml%s%s?master_key=%s&gateway_id=%d", host, DEVICES_PATH, masterKey, gatewayId);
    if (fdes > 0) {
        status = getDataFromServer(urlBuf, &recievedData, method);
    } else {
        status = getDataFromServer_Cellular(urlBuf, &recievedData, method, fdes);
    }

    if (status == EMBER_SUCCESS) {
        char *ptr = recievedData.ptr;
        while (ptr != NULL) {
            //Check if there are no more devices
            if (strstr(ptr,"<device>") == NULL) {
                break;
            }

            int nodeId = -1;

            char *pfound = strstr(ptr, "id");

            if (pfound != NULL) {
                pfound += 18;

                nodeId = atoi(pfound);
            }

            char *buffer = malloc(40);
            strcpy(buffer, "");
            pfound = strstr(ptr, "<address>");
            char *pfoundend = strstr(ptr, "</address>");
            if (pfound != NULL) {

                pfound += 9;
                int len = pfoundend-pfound;
                memcpy(buffer, pfound, len);
            }

            if (nodeId >= 0) {
                const char comma[2] = ",";
                char *token;
                token = strtok (buffer,comma);

                EmberEUI64 addressBuf;
                int i = 0;

                while (token != NULL) {
                    int number = (int)strtol(token, NULL, 16);
                    addressBuf[i] = number;
                    i += 1;
                    token = strtok(NULL, comma);
                }
                int8u alreadyExists = CheckListEuiNode(&addressBuf);
                if (alreadyExists == 0xff) {
                    emberAfAppPrintln("Adding Address : %X", addressBuf);
                    int x = 0;
                    for (x; x < 8;x++) {
                        ListEuiNode[nodeId][x] = addressBuf[x];
                    }
                }
            }

            emberAfAppPrintln("Checking Fields");
            resetFieldIds(nodeId);
            while (strstr(ptr, "<field>") != NULL) {
                pfound = strstr(ptr, "<field>");
                pfound = strstr(pfound, "id");
                pfound += 18;

                int16s fieldId = (int16s) atoi(pfound);

                pfound = strstr(pfound, "<field-type>");
                pfoundend = strstr(pfound, "</field-type>");
                pfound += 12;

                int len = pfoundend-pfound;
                char *fieldType = malloc(len+1);
                memset(fieldType, 0, len+1);
                memcpy(fieldType, pfound, len);

                emberAfAppPrintln("Field NodeId: %d", nodeId);
                emberAfAppPrintln("Field FieldId: %d", fieldId);
                emberAfAppPrintln("Field Type: %s", fieldType);
                if (strcmp(fieldType,"temp") == 0) {
                    updateFieldId((int8u) nodeId, 1, fieldId);
                } else if (strcmp(fieldType,"x-accel") == 0) {
                    updateFieldId((int8u) nodeId, 2, fieldId);
                } else if (strcmp(fieldType,"y-accel") == 0) {
                    updateFieldId((int8u) nodeId, 3, fieldId);
                } else if (strcmp(fieldType,"z-accel") == 0) {
                    updateFieldId((int8u) nodeId, 4, fieldId);
                }

                pfound = pfoundend;
                pfound += 13;

                free(fieldType);

                //find if we are done fields for this node
                int nextFieldPosition = strstr(pfound, "<field>") - pfound;
                int endOfFieldsPosition = strstr(pfound, "</fields>") - pfound;
                emberAfAppPrintln("nextFieldPosition: %d", nextFieldPosition);
                emberAfAppPrintln("endOfFieldsPosition: %d", endOfFieldsPosition);
                if (nextFieldPosition > endOfFieldsPosition) {
                    emberAfAppPrintln("No more Fields");
                    break;
                }

                pfound = strstr(ptr, "</field>");
                pfound += 8;

                ptr = pfound;
            }
            int z;
            for (z = 0; z < 4; z++) {
                int y;
                emberAfAppPrint("{");
                for (y = 0; y < 5; y++) {
                    emberAfAppPrint("%d,",ListNodeField[z][y]);
                }
                emberAfAppPrintln("}");
            }

            pfound = strstr(ptr, "</device>");
            pfound += 9;

            ptr = pfound;

            free(buffer);
        }
    }

    free(recievedData.ptr);
}

void appAddTempSensor(const EmberAfServiceDiscoveryResult *result) {
    // We are only concerned with positive matches for our service discovery
    printf ("appAddTempSensor - address: %X\n", result -> matchAddress);
    if (result -> status != EMBER_AF_BROADCAST_SERVICE_DISCOVERY_RESPONSE_RECEIVED) return;
    EmberNodeId node = result -> matchAddress;

    // Nothing to do if we already know about this node
    int i;
    for (i = 0; i < numSensors; i++)
        if (Sensors[i].nodeId == node) return;

    // If it's a new sensor, add it to the list
    AppSensor *s = &(Sensors[numSensors++]);
    s -> nodeId = node;
    s -> nodeNumber = -1;
    s -> sensors = SENSOR_TEMP;
    s -> index = EMBER_NULL_ADDRESS_TABLE_INDEX;
    s -> temp = 0;
    s -> cur = 0;
    s -> xaccel = 0;
    s -> yaccel = 0;
    s -> zaccel = 0;
    s -> nodeParent = 1;
    s -> nodeRole = 0;
}

//This function will return the index of an EUI address fron the table ListEuiNode
// if the address is not on the table, will return FF, this shouldn't happen but just in case.
int8u CheckListEuiNode(EmberEUI64 *EUI) {

    int8u *p;
    int8u *q;
    boolean flag = FALSE;
    EmberEUI64 *temp;
    int8u i;
    int8u j;

    for (i = 0; i < (Node_count); i++) {

        temp = &ListEuiNode[i];

        p = (int8u *) EUI;
        q = (int8u *) temp;

        flag = TRUE;

        for (j = 0; j < 8; j++) {

            if (p[j] != q[j]) {
                flag = FALSE;
            }
        }

        if (flag == TRUE) {
            return i; // this value will indicate the index on the table
        }
    }
    return 0xff; // this value will indicate there is no match on the table
}

EmberStatus prepare_message(int nodeId, int8u *send_buf, int send_len, char *apiKeyBuffer) {
    EmberApsFrame apsFrame;
    EmberStatus status;
    apsFrame.profileId = 0x0104;
    apsFrame.clusterId = ZCL_CONFIG_CLUSTER_ID;
    apsFrame.sourceEndpoint = 1;
    apsFrame.destinationEndpoint = 1;
    apsFrame.options = EMBER_APS_OPTION_NONE;
    apsFrame.groupId = 0;
    apsFrame.sequence = 1;

    int i = 0;
    for (i = 0; i < Node_count; i++) {

        if (Sensors[i].index != EMBER_NULL_ADDRESS_TABLE_INDEX) {

            if ((int8u) Sensors[i].nodeNumber == (int8u) nodeId) {
                break;
            }
        }
    }
    if (i < Node_count) {
        memcpy(Sensors[i].apiKey, apiKeyBuffer, 17);
        memcpy(Sensors[i].config, send_buf, send_len);
        printf("found node: sending uniCast\n");
        status = emberAfSendUnicast(EMBER_OUTGOING_VIA_ADDRESS_TABLE, Sensors[i].index, &apsFrame, send_len, send_buf);

    } else {
        printf("node not found: NOT sending uniCast\n");
        return status = !EMBER_SUCCESS;
    }
    if (status != EMBER_SUCCESS) {
        printf("sending Message failed \n");
    }
    return status;
}

void getNameAndId(char *buf, int *commandID, Key *name) {
    char buffer[30];
    char *pfound = strstr(buf, "id");

    if (pfound != NULL) {
        pfound += 18;

        ( *commandID) = atoi(pfound);
    }

    pfound = strstr(buf, "name>");
    if (pfound != NULL) {

        pfound += 5;
        memcpy(buffer, pfound, 29);

        buffer[29] = 0;

        pfound = strstr(buffer, values[SET_CONFIG]);

        if (pfound != NULL) {
            ( *name) = SET_CONFIG;
            return;
        }

        pfound = strstr(buffer, values[GET_CONFIG]);
        if (pfound != NULL) {
            ( *name) = GET_CONFIG;
            return;
        }

        ( *name) = NONE;
    } else {
        ( *name) = NONE;
    }

}

EmberStatus UpdateNodeSensorSettings(char *data_buff, int *commandID, int fdes) {

    EmberStatus status = !EMBER_SUCCESS;

    struct string s;
    init_string( &s);

    RestMethod method = GET;
    EmberStatus getStatus;
    if (des < 0) {
        getStatus = getDataFromServer(data_buff, &s, method);
    } else {
        getStatus = getDataFromServer_Cellular(data_buff, &s, method, fdes);
    }

    /* check if getdata was successful */
    if (getStatus != EMBER_SUCCESS) {
        status = !EMBER_SUCCESS;
    }else {
        if (commandID != -2) {

            AcclConfiguration accl;
            TempConfiguration temp;
            AdcConfiguration adc;
            int nodeId = 1;
            int8u send_buff[15];
            char apiKeyBuffer[18] = "M1MB4DZDYRJ03NA5";
            int send_len;

            //init the structures
            initDefaultConfigs( &accl, &temp, &adc);
            Key name = 17;
            getNameAndId(s.ptr, commandID, &name);

            //find settings
            switch (name) {
                case SET_CONFIG:
                    nodeId = config_value(s.ptr, NODE_ID, &accl, &temp, &adc, NULL);
                    emberAfAppPrintln("Command Node Id: %d", nodeId);
                    config_value(s.ptr, ADC_MOD, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_BITS, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_CLK, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_PERIOD, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C0_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C0_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C1_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C1_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C2_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C2_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C3_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C3_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, TEMP_PERIOD, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ACCL_MAXG, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ACCL_PERIOD, &accl, &temp, &adc, NULL);
                    //  config_value (s.ptr, API_KEY,&accl,&temp,&adc,apiKeyBuffer);
                    if (nodeId >= 0) {
                        encodeConfig(nodeId, accl, adc, temp, send_buff, &send_len);

                        //send message
                        send_len = 18;
                        status = prepare_message(nodeId, send_buff, send_len, apiKeyBuffer);
                    }
                    break;

                case GET_CONFIG:
                    status = EMBER_SUCCESS;
                    break;

                default: break;
            }
        }
    }

    /* always cleanup */
    free(s.ptr);

    return status;
}

int config_value(char *buf, Key key, AcclConfiguration *accl, TempConfiguration *temp, AdcConfiguration *adc, char *apiKeyBuffer) {
    char *pfound = strstr(buf, values[key]); //pointer to the first character found in the string buf
    int status = 0;
    if (pfound != NULL) {
        pfound += strlen(values[key]) + 21;
        //printf("%16s\n",pfound);
        //pfound points to value
        switch (key) {
            case NODE_ID:
                status = atoi(pfound);
                break;
            case ADC_MOD:
                adc -> mode = (int8u) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_BITS:
                adc -> numberBits = (int16u) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_PERIOD:
                adc -> periodS = (int16u) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_CLK:
                status = atoi(pfound);
                if (status) adc -> is_clock_1MHz = DISABLED;
                else adc -> is_clock_1MHz = ENABLED;
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_C0_P:
                adc -> pIn[0] = (adcInputChEnum) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_C0_N:
                adc -> nIn[0] = (adcInputChEnum) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                adc -> numberChannels++;
                break;
            case ADC_C1_P:
                adc -> pIn[1] = (adcInputChEnum) atoi(pfound);
                break;
            case ADC_C1_N:
                adc -> nIn[1] = (adcInputChEnum) atoi(pfound);
                adc -> numberChannels++;
                break;
            case ADC_C2_P:
                adc -> pIn[2] = (adcInputChEnum) atoi(pfound);
                break;
            case ADC_C2_N:
                adc -> nIn[2] = (adcInputChEnum) atoi(pfound);
                adc -> numberChannels++;
                break;
            case ADC_C3_P:
                adc -> pIn[3] = (adcInputChEnum) atoi(pfound);
                break;
            case ADC_C3_N:
                adc -> nIn[3] = (adcInputChEnum) atoi(pfound);
                adc -> numberChannels++;
                break;
            case TEMP_PERIOD:
                temp -> periodS = (int16u) atoi(pfound);
                temp -> is_temp_enabled = ENABLED;
                break;
            case ACCL_MAXG:
                accl -> maxG = (int16u) atoi(pfound);
                accl -> is_accl_enabled = ENABLED;
                accl -> is_x_enabled = accl -> is_y_enabled = accl -> is_z_enabled = ENABLED;
                break;
            case ACCL_PERIOD:
                accl -> periodS = (int16u) atoi(pfound);
                accl -> is_accl_enabled = ENABLED;
                accl -> is_x_enabled = accl -> is_y_enabled = accl -> is_z_enabled = ENABLED;
                break;
            case API_KEY:
                memcpy(apiKeyBuffer, pfound, 17);
                apiKeyBuffer[16] = 0;
                break;
        }
    } else if (key == NODE_ID) {
        status = -1;
    }
    return status;
}

unsigned long int ElapsedTime_m(struct timeval PreviousTime) {
    struct timeval CurrentTime;
    int sec, usec;
    gettimeofday( &CurrentTime, NULL); // Get current time
    sec = CurrentTime.tv_sec - PreviousTime.tv_sec; // Compute the number of second elapsed since last call

    usec = CurrentTime.tv_usec - PreviousTime.tv_usec; // Compute

    if (usec < 0) { // If the previous usec is higher than the current one
        usec = (1000000 - PreviousTime.tv_usec) + CurrentTime.tv_usec; // Recompute the microseonds
        sec--; // Substract one second

    }
    return ((sec *1000 + usec / 1000) / 2);
}

// EmberStatus sendDataToServer_Cellular(char *data, int fdes, int commandID) {

//     char dat_send[50];
//     char sense_data[1460];
//     char ptr[200];
//     size_t nbytes;
//     ssize_t bytes_read;
//     int data_length;
//     EmberStatus status = !EMBER_SUCCESS;

//     sprintf(sense_data, "GET %s HTTP/1.0\x0D\x0A", data);
//     char *collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";
//     data_length = strlen(sense_data) + strlen(collect) - 1;
//     // printf ("The buffer is is %s \n",sense_data);

//     sprintf(dat_send, "AT+SDATATSEND=1,%d\r", data_length);
//     SendSerialString(fdes, dat_send);
//     printf("Sending data...\n");

//     if (commandtimeout(5, ">", fdes) == 0) {
//         printf("Sending data failed.. \n");
//         fdes = cell_init();
//         status = sendDataToServer_Cellular(data, fdes, commandID);
//         return status;
//     }

//     SendSerialString(fdes, sense_data);
//     SendSerialString(fdes, collect);
//     serialib_FlushReceiver(fdes);
//     if (commandtimeout(40, "STCPD", fdes) == 0) {
//         printf("Sending data failed.. \n");
//         fdes = cell_init();
//         status = sendDataToServer_Cellular(data, fdes, commandID);
//         return status;
//     } else {
//         printf("Data Sent.. \n");

//         SendSerialString(fdes, "AT+SDATATREAD=1");
//         bytes_read = read(fdes, ptr, nbytes);

//         if (commandID != -2) {

//             AcclConfiguration accl;
//             TempConfiguration temp;
//             AdcConfiguration adc;
//             int nodeId;
//             int8u send_buff[15];
//             char apiKeyBuffer[18];
//             int send_len = 0;

//             //init the structures
//             initDefaultConfigs( &accl, &temp, &adc);
//             Key name;
//             getNameAndId(ptr, &commandID, &name);

//             //find settings
//             switch (name) {
//             case SET_CONFIG:
//                 nodeId = config_value(ptr, NODE_ID, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_MOD, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_BITS, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_CLK, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_PERIOD, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C0_P, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C0_N, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C1_P, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C1_N, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C2_P, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C2_N, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C3_P, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ADC_C3_N, &accl, &temp, &adc, NULL);
//                 config_value(ptr, TEMP_PERIOD, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ACCL_MAXG, &accl, &temp, &adc, NULL);
//                 config_value(ptr, ACCL_PERIOD, &accl, &temp, &adc, NULL);
//                 // config_value (ptr, API_KEY,&accl,&temp,&adc,apiKeyBuffer);
//                 if (nodeId >= 0) {
//                     //  encodeConfig(nodeId, accl, adc, temp, send_buff, &send_len);

//                     //send message

//                     status = prepare_message(nodeId, send_buff, send_len, apiKeyBuffer);

//                 }
//                 break;

//             case GET_CONFIG:
//                 status = EMBER_SUCCESS;
//                 break;

//             default : break;
//             }
//         }

//     }

//     SendSerialString(fdes, "AT+SDATASTATUS=0\r\n");
//     int y = commandtimeout(5, "SOCK", fdes);
//     return status;

// }