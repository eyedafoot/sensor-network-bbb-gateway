//Configuration encoding

#include "app/framework/include/af.h"

#include "configurationTypes.h"
#include "configurationEncoding.h"

void initDefaultConfigs(AcclConfiguration *accl, TempConfiguration *temp, AdcConfiguration *adc){
	//force them all to be disabled
	accl->is_accl_enabled = DISABLED;
	accl->is_x_enabled = DISABLED;
	accl->is_y_enabled = DISABLED;
	accl->is_z_enabled = DISABLED;
	accl->numberAxes = 0;
	accl->maxG = 2;
	accl->periodS = 30;
	
	adc->is_adc_enabled = DISABLED;
	adc->is_clock_1MHz = ENABLED;
	adc->numberBits = 14;
	adc->mode = PERIODIC_SINGLE;
	adc->numberChannels = 0;
	adc->periodS = 30;
        
	temp->is_temp_enabled = ENABLED;
	temp->periodS = 3;
}

// byte 0 [node id] 
// byte 1 [X|Y|Z|T|C0|C1|C2|C3]<-master enable 
// byte 2 [M|M|B|B|B|C|N|N]<- mode,bits,clock,none "dont care"
// byte 3 - 6 [P0|N0]...[P3|N3]<-adc configurations
// byte 7 - 8 [temp period]
// byte 9 [accl maxG]
// byte 10 [active data rate]
// byte 11 - 12 [accl period]
/**
 *      7         6         5         4         3         2         1         0
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                                    Node ID                                    | Byte 0.
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * | ADC CH3 | ADC CH2 | ADC CH1 | ADC CH0 |  TEMP   | Accl Z  | Accl Y  | Accl X  | Byte 1. Master Enable 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |    DON'T CARE     | CLK= 1M |          RESOLUTION         |      ADC MODE     | Byte 2. ADC configuration
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |            ADC CH0    NEGATIVE        |            ADC CH0    POSITIVE        | Byte 3. Channel 0 Terminals
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |            ADC CH1    NEGATIVE        |            ADC CH1    POSITIVE        | Byte 4. Channel 1 Terminals
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |            ADC CH2    NEGATIVE        |            ADC CH2    POSITIVE        | Byte 5. Channel 2 Terminals
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |            ADC CH3    NEGATIVE        |            ADC CH3    POSITIVE        | Byte 6. Channel 3 Terminals
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                         ADC SAMPLE PERIOD. (LOW BYTE)                         | Byte 7. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                         ADC SAMPLE PERIOD. (HIGH BYTE)                        | Byte 8. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                     TEMPERATURE SAMPLE PERIOD. (LOW BYTE)                     | Byte 7. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                     TEMPERATURE SAMPLE PERIOD. (HIGH BYTE)                    | Byte 8. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                        Max G (FIXME: 2 bits are enough)                       | Byte 9. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                 Active Data Rate (FIXME: 3 bits are  enough)                  | Byte 10. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                    ACCELEROMETER SAMPLE PERIOD. (LOW BYTE)                    | Byte 11. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * |                    ACCELEROMETER SAMPLE PERIOD. (HIGH BYTE)                   | Byte 12. 
 * +---------+---------+---------+---------+---------+---------+---------+---------+
 * 
*/
void encodeConfig(int8u nodeId, AcclConfiguration accl, AdcConfiguration adc, TempConfiguration temp, int8u *buffer, int *length){
  //define node id
  int i = 0;                                                    // buffer Index
  buffer[ i++ ] = nodeId;                                       // buffer Index = 0
  
  //clear master enable [X|Y|Z|T|C0|C1|C2|C3]
  buffer[ i ] = 0;                                              // buffer Index = 1
  if(accl.is_x_enabled == ENABLED) buffer[ i ] |= 0x1;
  if(accl.is_y_enabled == ENABLED) buffer[ i ] |= 0x2;
  if(accl.is_z_enabled == ENABLED) buffer[ i ] |= 0x4;
  if(temp.is_temp_enabled == ENABLED) buffer[ i ] |= 0x8;
  buffer[ i ] |= ((0x0F << adc.numberChannels)&0xF0);
  i++;
  
  //establish adc config [M|M|B|B|B|C|X|X]
  buffer[ i ] = (adc.mode&0x3);                                 // buffer Index = 2
  buffer[ i ] |= (((adc.numberBits-7)&0x7)<<2);
  if(adc.is_clock_1MHz == ENABLED) buffer[ i ] |= 0x20;
  i++;
  
  buffer[ i++ ] = ((adc.pIn[0]&0xF) | ((adc.nIn[0]&0xF)<<4));   // buffer Index = 3
  buffer[ i++ ] = ((adc.pIn[1]&0xF) | ((adc.nIn[1]&0xF)<<4));   // buffer Index = 4
  buffer[ i++ ] = ((adc.pIn[2]&0xF) | ((adc.nIn[2]&0xF)<<4));   // buffer Index = 5
  buffer[ i++ ] = ((adc.pIn[3]&0xF) | ((adc.nIn[3]&0xF)<<4));   // buffer Index = 6
  
  //load ADC period
  (*(int16u *)&buffer[ i ]) = (adc.periodS);                    // buffer Index = 7
  i += 2;
  
  //load temperature period
  (*(int16u *)&buffer[ i ]) = (temp.periodS);                   // buffer Index = 9
  i += 2;
  
  //acceleration config 
//  buffer[ i++ ] = accl.maxG;                                    // buffer Index = 
//  buffer[ i++ ] = accl.activeDataRate;                          // buffer Index = 
  
 //[G|G|DR|DR|DR|X|X|X]
  buffer[ i ] = (accl.maxG >> 2) & 3;                           // �2g= 00b, �4g= 01b  �8g = 10b
  buffer[ i ] |= (accl.activeDataRate & 7) < 2;                 // (000->111) = 800, 400, 200, 100, 50, 12.5, 6.25, 1.56 Hz
  i++;                                                          // buffer Index = 11
  
  (*(int16u *)&buffer[ i ]) = (accl.periodS);                   // buffer Index = 12
  i += 2;
  
  //update the length
  (*length) = i;
}

void decodeConfig(int8u *nodeId, int8u *buffer, int length, AcclConfiguration *accl, AdcConfiguration *adc, TempConfiguration *temp){
  int i = 0;
  (*nodeId) = buffer[ i++ ];
  
  accl->numberAxes = 0;
  if(buffer[ i ]&0x1) {accl->is_x_enabled = ENABLED; accl->numberAxes++;}
  if(buffer[ i ]&0x2) {accl->is_y_enabled = ENABLED; accl->numberAxes++;}
  if(buffer[ i ]&0x4) {accl->is_z_enabled = ENABLED; accl->numberAxes++;}
  if(accl->is_x_enabled == ENABLED || accl->is_y_enabled == ENABLED || accl->is_z_enabled == ENABLED) accl->is_accl_enabled = ENABLED;
  if(buffer[ i ]&0x8) temp->is_temp_enabled = ENABLED;
  switch(buffer[ i ]&0xF0){
    case 0x10:
      adc->numberChannels = 1;
      break;
    case 0x30:
      adc->numberChannels = 2;
      break;
    case 0x70:
      adc->numberChannels = 3;
      break;
    case 0xF0:
      adc->numberChannels = 4;
      break;
  }
  if(adc->numberChannels >0) adc->is_adc_enabled = ENABLED;
  i++;
  
  adc->mode = (buffer[ i ]&0x3);
  adc->numberBits = ((buffer[ i ]&0x1C)>>2) + 7;
  if(buffer[ i ]&0x20) adc->is_clock_1MHz = ENABLED;
  else adc->is_clock_1MHz = DISABLED;
  i++;
  
  adc->pIn[0] = buffer[ i ]&0xF;
  adc->nIn[0] = ((buffer[ i ]&0xF0)>>4);
  i++;
  
  adc->pIn[1] = buffer[ i ]&0xF;
  adc->nIn[1] = ((buffer[ i ]&0xF0)>>4);
  i++;
  
  adc->pIn[2] = buffer[ i ]&0xF;
  adc->nIn[2] = ((buffer[ i ]&0xF0)>>4);
  i++;
  
  adc->pIn[3] = buffer[ i ]&0xF;
  adc->nIn[3] = ((buffer[ i ]&0xF0)>>4);
  i++;
  
  adc->periodS = ((*(int16u *)&buffer[ i ]));
  i += 2;

  temp->periodS = (int16u)((*(int16u *)&buffer[ i ]));
  i += 2;

  //accl->maxG = buffer[9];
  //accl->activeDataRate = buffer[10];

  accl->maxG  = 2 << ( buffer[ i ] & 3 );         // �2g= 00b, �4g= 01b  �8g = 10b
  accl->activeDataRate = (buffer[ i ] >> 2) & 7; // (000->111) = 800, 400, 200, 100, 50, 12.5, 6.25, 1.56 Hz
  i++;
 
  accl->periodS = ((*(int16u *)&buffer[ i ]));
  i += 2;
  
}

/*
void testConfigurationEncoding(){
	AcclConfiguration accl;
	AdcConfiguration adc;
	TempConfiguration temp;
	int8u nodeID = 0;
	int length = 13;
	//                      0   1     2    3    4    5    6   7    8     9   10   11   12   
	int8u encodedBuf[] = {0x01,0x3D,0x09,0x23,0x4B,0x58,0x81,0x02,0xAF,0x02,0x03,0xAF,0x02};
	int8u decodedBuf[13];
	decodeConfig(&nodeID,encodedBuf,length,&accl,&adc,&temp);
	encodeConfig(nodeID,accl,adc,temp,decodedBuf,&length);
	
	int faults = 0;
	for(int i=0;i<13;i++){
		if(encodedBuf[i] != decodedBuf[i]){ 
			faults++;
			logPrintln("Index: %d Expected: %X Received: %X",i,encodedBuf[i],decodedBuf[i]);
		}
	}
	
	if(faults) logPrintln("FAIL");
	else logPrintln("SUCCESS");
}*/