#ifndef CONFIGURATION_TYPES_H
#define CONFIGURATION_TYPES_H

//adc dependency
#include "app/framework/include/af.h"
#include "hal/ece/haldrvADC.h"

typedef enum {ERROR, SUCCESS, ENABLED, DISABLED, INCOMPATABLE_PARAMS} status_SM; 
typedef enum {ACCL_X, ACCL_Y, ACCL_Z, ADC, TEMP} Sensor_SM;
typedef enum {PERIODIC_SINGLE=0,PERIODIC_MULTI=1,RMS_SINGLE =2,RMS_MULTI} ADC_MODE;

typedef struct {
  status_SM is_accl_enabled;
  int8u maxG;
  int8u activeDataRate; //NOT UPDATED BY CLOUD
  int numberAxes;
  int16u periodS;
  status_SM is_x_enabled;
  status_SM is_y_enabled;
  status_SM is_z_enabled;
} AcclConfiguration;

typedef struct {
  status_SM is_temp_enabled;
  int16u periodS;
} TempConfiguration;

typedef struct {
  status_SM is_adc_enabled;
  status_SM is_clock_1MHz;
  int8u mode; //MISSING 
  int16u periodS;
  int numberChannels;
  int16u numberBits;
  adcInputChEnum pIn[4];
  adcInputChEnum nIn[4];
} AdcConfiguration;

typedef struct {
	int size;
	int16s *buffer;
	int head;
	int tail;
} RingBuffer;

typedef struct {
	int numberOfAdcConfigs;
	RingBuffer adc[4];
	RingBuffer temp;
	int numberOfAcclAxes;
	RingBuffer accl[3];
} Buffers;

#endif //CONFIGURATION_TYPES_H