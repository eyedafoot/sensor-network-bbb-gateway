#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/time.h>
#include <time.h>

#define bool int
#define TRUE 1
#define FALSE 0

//Need to make sure mysql libraries are installed in compiling system.
//Compile: gcc -o DB-2-CSV DB-2-CSV.c -I/usr/include/mysql –lmysqlclient

//Compile without main: gcc -c DB_Temp_2_CSV.c -I/usr/include/mysql -lmysqlclient

//Path to mysql on BBB: /usr/include/mysql

#include <mysql.h>

#include "mysql-connector.h"
void send_data (char* data,int des);
int cell_init(void);
void cell_reset(int des);
/*!
    \brief      Initialise the timer. It writes the current time of the day in the structure PreviousTime.
*/
//Initialize the timer
struct timeval InitTimer()
{  struct timeval PreviousTime;
   gettimeofday(&PreviousTime, NULL);
   return(PreviousTime);
}

/*!
    \brief      Returns the time elapsed since initialization.  It write the current time of the day in the structure CurrentTime.
                Then it returns the difference between CurrentTime and PreviousTime.
    \return     The number of microseconds elapsed since the functions InitTimer was called.
  */
//Return the elapsed time since initialization
unsigned long int ElapsedTime_ms(struct timeval PreviousTime )
{
    struct timeval CurrentTime;
    int sec,usec;
    gettimeofday(&CurrentTime, NULL);                                   // Get current time
    sec=CurrentTime.tv_sec-PreviousTime.tv_sec;                         // Compute the number of second elapsed since last call
    usec=CurrentTime.tv_usec-PreviousTime.tv_usec;                      // Compute
    if (usec<0) {                                                       // If the previous usec is higher than the current one
        usec=1000000-PreviousTime.tv_usec+CurrentTime.tv_usec;          // Recompute the microseonds
        sec--;                                                          // Substract one second
    }
    return (sec*1000+usec/1000);
}


/*
	Attempt to open the serial port
	Return the file descriptor
*/
int OpenSerial(char *port)
{
	  int fdes;

	if ((fdes = open(port,O_RDWR | O_NONBLOCK)) < 0) {
		perror("OpenSerial");
	} 

	int n;
	struct termios options; //,options2;
        memset(&options,0,sizeof(options));     
        options.c_iflag=0;
        options.c_oflag=0;
        options.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
        options.c_lflag=0;
        options.c_cc[VMIN]=1;
        options.c_cc[VTIME]=5;
        cfsetispeed(&options,B115200);
        cfsetospeed(&options,B115200);
        tcsetattr(fdes,TCSANOW,&options);
     return fdes;
}


void CheckSerialPort(int fdes)
{
   int input_speed,output_speed;
   struct termios options;

   /* Get the current options */
   if (tcgetattr(fdes,&options) != 0) {
      perror("CheckSpeed");
   }

   fprintf(stderr,"iflag - 0%o; oflag - 0%o; cflag - 0%o; lflag - 0%o\n",
      (unsigned int)options.c_iflag,
		(unsigned int)options.c_oflag,
		(unsigned int)options.c_cflag,
		(unsigned int)options.c_lflag);
   fprintf(stderr,"c_ispeed - 0%o; c_ospeed - 0%o\n",
		(unsigned int)options.c_ispeed,
		(unsigned int)options.c_ospeed);

   input_speed = cfgetispeed(&options);
   output_speed = cfgetospeed(&options);
   fprintf(stderr,"Input speed: %d\n",input_speed);
   fprintf(stderr,"Output speed: %d\n",output_speed);
}

void CloseSerial(int fdes)
{

    close (fdes);

}



/*
   Write a string to the serial port device
*/
int SendSerialString(int fdes,char *buffer)
{
   int len;
  
   len = strlen(buffer);  
   
  if (write(fdes,buffer,len) != len) 
		return(FALSE);
	return(TRUE);
}

/*
   Write raw bytes to the serial port device
*/
int SendSerialRaw(int fdes,unsigned char *buffer,int len)
{
   if (write(fdes,buffer,len) != len)
      return(FALSE);
   return(TRUE);
}



int ReadPort_To_Screen(int fdes,char *buffer,int maxread)
{
    unsigned char c='D';
    struct termios stdio;
    struct termios old_stdio;
    tcgetattr(STDOUT_FILENO,&old_stdio);
    memset(&stdio,0,sizeof(stdio));
        stdio.c_iflag=0;
        stdio.c_oflag=0;
        stdio.c_cflag=0;
        stdio.c_lflag=0;
        stdio.c_cc[VMIN]=1;
        stdio.c_cc[VTIME]=0;
        tcsetattr(STDOUT_FILENO,TCSANOW,&stdio);
        tcsetattr(STDOUT_FILENO,TCSAFLUSH,&stdio);
        fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);       // make the reads non-blockin
    int i = -1;
    while (i < 0){
    i = read(fdes,&buffer,maxread);  
    if (i > 0) { 
    write(STDOUT_FILENO,&buffer,i);}    
    
     }
    tcsetattr(STDOUT_FILENO,TCSANOW,&old_stdio);    
    return(i);
}


/*          
   Read a single character from the port without timer
*/       
int  ReadSerialChar(int fdes,unsigned int *n)
{  
	char c[8];
   int len;

   len = read(fdes,&c,1);
   
   if (len < 1 && errno != EAGAIN)
      return(FALSE);
	*n = c[0];
   return(TRUE);
}





/*!
     \brief Wait for a byte from the serial device and return the data read
     \param pByte : data read on the serial device
     \param TimeOut_ms : delay of timeout before giving up the reading
            If set to zero, timeout is disable (Optional)
     \return 1 success
     \return 0 Timeout reached
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
  */
int serialib_ReadChar(int fdes,char *pByte,unsigned int TimeOut_ms)
{

   struct timeval         Timer;                                              // Timer used for timeout
    Timer = InitTimer();                                                  // Initialise the timer
    while (ElapsedTime_ms(Timer)<TimeOut_ms || TimeOut_ms==0)          // While Timeout is not reached
    {
        switch (read(fdes,pByte,1)) {                                     // Try to read a byte on the device
        case 1  : return 1;                                             // Read successfull
        case -1 : return -2;                                            // Error while reading
        }
    }
    return 0;

}




/*!
     \brief Read a string from the serial device (without TimeOut)
     \param String : string read on the serial device
     \param FinalChar : final char of the string
     \param MaxNbBytes : maximum allowed number of bytes read
     \return >0 success, return the number of bytes read
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
     \return -3 MaxNbBytes is reached
  */
int serialib_ReadStringNoTimeOut(int fdes,char *String,char FinalChar,unsigned int MaxNbBytes)
{
    unsigned int    NbBytes=0;                                          // Number of bytes read
    char            ret;                                                // Returned value from Read
    while (NbBytes<MaxNbBytes)                                          // While the buffer is not full
    {                                                                   // Read a byte with the restant time
        ret=serialib_ReadChar(fdes,&String[NbBytes],0);
        if (ret==1)                                                     // If a byte has been read
        {
            if (String[NbBytes]==FinalChar)                             // Check if it is the final char
            {
                String  [++NbBytes]=0;                                  // Yes : add the end character 0
                return NbBytes;                                         // Return the number of bytes read
            }
            NbBytes++;                                                  // If not, just increase the number of bytes read
        }
        if (ret<0) return ret;                                          // Error while reading : return the error number
    }
    return -3;                                                          // Buffer is full : return -3
}



/*!
     \brief Read a string from the serial device (with timeout)
     \param String : string read on the serial device
     \param FinalChar : final char of the string
     \param MaxNbBytes : maximum allowed number of bytes read
     \param TimeOut_ms : delay of timeout before giving up the reading (optional)
     \return  >0 success, return the number of bytes read
     \return  0 timeout is reached
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
     \return -3 MaxNbBytes is reached
  */
int serialib_ReadString(int fdes, char *String,char FinalChar,unsigned int MaxNbBytes,unsigned int TimeOut_ms)
{
    if (TimeOut_ms==0)
        return serialib_ReadStringNoTimeOut(fdes,String,FinalChar,MaxNbBytes);

    unsigned int    NbBytes=0;                                          // Number of bytes read
    char            ret;                                                // Returned value from Read
    struct timeval         Timer;                                          // Timer used for timeout
    Timer = InitTimer();                                                  // Initialise the timer
    long int        TimeOutParam;
    
    while (NbBytes<MaxNbBytes)                                          // While the buffer is not full
    {                                                                   // Read a byte with the restant time
        TimeOutParam=TimeOut_ms- ElapsedTime_ms(Timer);                 // Compute the TimeOut for the call of ReadChar
        if (TimeOutParam>0)                                             // If the parameter is higher than zero
        {
            ret= serialib_ReadChar(fdes,&String[NbBytes],TimeOutParam);                // Wait for a byte on the serial link            
            if (ret==1)                                                 // If a byte has been read
            {

                if (String[NbBytes]==FinalChar)                         // Check if it is the final char
                {
                    String  [++NbBytes]=0;                              // Yes : add the end character 0
                    return NbBytes;                                     // Return the number of bytes read
                }
                NbBytes++;                                              // If not, just increase the number of bytes read
            }
            if (ret<0) return ret;                                      // Error while reading : return the error number
        }
        if (ElapsedTime_ms(Timer)>TimeOut_ms) {                        // Timeout is reached
            String[NbBytes]=0;                                          // Add the end caracter
            return NbBytes;                                                   // Return 0
        }
    }
    return -3;                                                          // Buffer is full : return -3
}




/*!
     \brief Read an array of bytes from the serial device (with timeout)
     \param Buffer : array of bytes read from the serial device
     \param MaxNbBytes : maximum allowed number of bytes read
     \param TimeOut_ms : delay of timeout before giving up the reading
     \return 1 success, return the number of bytes read
     \return 0 Timeout reached
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
  */
int serialib_Read (int fdes,void *Buffer,unsigned int MaxNbBytes,unsigned int TimeOut_ms)
{
     struct timeval         Timer;                                          // Timer used for timeout
    Timer = InitTimer();                                                  // Initialise the timer
    unsigned int     NbByteRead=0;
    while (ElapsedTime_ms(Timer)<TimeOut_ms || TimeOut_ms==0)          // While Timeout is not reached
    {
        unsigned char* Ptr=(unsigned char*)Buffer+NbByteRead;           // Compute the position of the current byte
        int Ret=read(fdes,(void*)Ptr,MaxNbBytes-NbByteRead);              // Try to read a byte on the device
        if (Ret==-1) return -2;                                         // Error while reading
        if (Ret>0) {                                                    // One or several byte(s) has been read on the device
            NbByteRead+=Ret;                                            // Increase the number of read bytes
            if (NbByteRead>=MaxNbBytes)                                 // Success : bytes has been read
                return 1;
        }
    }
    return 0;                                                           // Timeout reached, return 0

}


/*!
    \brief Empty receiver buffer 
*/

void serialib_FlushReceiver(int fdes)
{

    tcflush(fdes,TCIFLUSH);

}



/*!
    \brief  Return the number of bytes in the received buffer (UNIX only)
    \return The number of bytes in the received buffer
*/
int serialib_Peek(int fdes)
{
    int Nbytes=0;

    ioctl(fdes, FIONREAD, &Nbytes);

    return Nbytes;
}




/*
   Read raw bytes from the serial port
	Return the number read
*/
int ReadSerialRaw(int fdes,unsigned char *buffer,int maxread,int timeout)
{
   int len,iptr=0;
   unsigned char buf;
   struct timeval tp;
   double startsecs,secs;

   gettimeofday(&tp,NULL);
   startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;

   for (;;) {
      len = read(fdes,&buf,1);
      if (len > 0) {
         buffer[iptr] = buf;
         iptr++;
			if (iptr >= maxread)
				break;
      }
      gettimeofday(&tp,NULL);
      secs = tp.tv_sec + tp.tv_usec / 1000000.0;
      if (secs-startsecs > timeout)
         break;
   }
   return(iptr);
}

/*
	Clear serial, read until nothing is left 
*/
void ClearSerial(int fdes)
{
	char buf;

   while (read(fdes,&buf,1) > 0) 
		;
}

/*
	Wait for a particular character
	The timeout is in seconds
*/
int SkipSerialUntil(int fdes,char c,double timeout)
{
	char buf;
	int len;
	struct timeval tp;
	double startsecs,secs;

	gettimeofday(&tp,NULL);
	startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;

	for (;;) {
   	len = read(fdes,&buf,1);
		if (len > 0 && buf == c)
			break;
		gettimeofday(&tp,NULL);
		secs = tp.tv_sec + tp.tv_usec / 1000000.0;
		if (secs-startsecs > timeout)
			return(FALSE);
	}
   return(TRUE);
}

/*
	Read until a particular character is received
	Return a C string
*/
int ReadSerialUntil(int fdes,char *buffer,char c,int maxread,double timeout)
{
   int len,iptr=0;
	char buf;
   struct timeval tp;
   double startsecs,secs;

   gettimeofday(&tp,NULL);
   startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;
	buffer[iptr] = '\0';

   for (;;) {
      len = read(fdes,&buf,1);
      if (len > 0 && buf == c)
         break;
		if (len > 0 && iptr < maxread-1) {
			buffer[iptr] = buf;
			iptr++;
			buffer[iptr] = '\0';
		}
      gettimeofday(&tp,NULL);
      secs = tp.tv_sec + tp.tv_usec / 1000000.0;
      if (secs-startsecs > timeout)
         return(FALSE);
   }
   return(TRUE);
}

/*
	Display a possibly binary string
*/
void Raw2stderr(unsigned char *buf,int len)
{
	int i;

	for (i=0;i<len;i++) {
      if (buf[i] < ' ' || buf[i] > 126)
         fprintf(stderr,"<%03d>",buf[i]);
      else
         fprintf(stderr,"%c",buf[i]);
	}
}

/*
   Display a string potentially with non printable characters
*/
void Str2stderr(char *buf)
{
   unsigned int i;

   for (i=0;i<strlen(buf);i++) {
      if (buf[i] < ' ' || buf[i] > 126)
         fprintf(stderr,"<%03d>",buf[i]);
      else
         fprintf(stderr,"%c",buf[i]);
   }
}

/*
	Given a 3 byte string from the mouse in "s"
	Return the button and position.
*/
void DecodeSerialMouse(unsigned char *s,int *button,int *x,int *y)
{
   *button = 'n';
   if ((s[0] & 0x20) != 0)
      *button = 'l';
   else if ((s[0] & 0x10) != 0)
      *button = 'r';
   *x = (s[0] & 0x03) * 64 + (s[1] & 0x3F);
   if (*x > 127)
      *x = *x - 256;
   *y = (s[0] & 0x0C) * 16 + (s[2] & 0x3F);
   if (*y > 127)
      *y = *y - 256;
}

/*
	Set up for the serialversion of the Magellan Spacemosue
*/
int SerialSpacemouse(char *s)
{
   int fd;
   struct termios options,options2;

   if ((fd = open(s,O_RDWR | O_NOCTTY | O_NONBLOCK)) < 0) {
      fprintf(stderr,"Failed to open serial port\n");
   } else {
      fcntl(fd,F_SETFL,FNDELAY);
   }

   // Get the current options
   if (tcgetattr(fd,&options) < 0) {
      fprintf(stderr,"First get failed\n");
      exit(-1);
   }

   // Change things
   cfsetispeed(&options,B9600);
   cfsetospeed(&options,B9600);
	options.c_cflag &= ~CSIZE;
   options.c_cflag |= CS8;
   options.c_cflag &= ~PARENB;
   options.c_cflag |= CSTOPB;
   options.c_cc[VMIN] = 0;
   options.c_cc[VTIME] = 0;

   options.c_lflag = 0;  // No local flags
   options.c_lflag &= ~ICANON; // Don't canonicalise
   options.c_lflag &= ~ECHO; // Don't echo
   options.c_lflag &= ~ECHOK; // Don't echo

   options.c_cflag &= ~CRTSCTS; // Disable RTS/CTS
   options.c_cflag |= CLOCAL; // Ignore status lines
   options.c_cflag |= CREAD; // Enable receiver
   options.c_cflag |= HUPCL; // Drop DTR on close

   options.c_oflag &= ~OPOST; // No output processing
   options.c_oflag &= ~ONLCR; // Don't convert linefeeds

   options.c_iflag |= IGNPAR; // Ignore parity
   options.c_iflag &= ~ISTRIP; // Don't strip high order bit
   options.c_iflag |= IGNBRK; // Ignore break conditions
   options.c_iflag &= ~INLCR; // Don't Map NL to CR
   options.c_iflag &= ~ICRNL; // Don't Map CR to NL
   options.c_iflag |= (IXON | IXOFF | IXANY); // xon/xoff flow control

   // Update the options and do it NOW
   if (tcsetattr(fd,TCSANOW,&options) < 0) {
      fprintf(stderr,"Failed to set\n");
      exit(-1);
   }

   // Read the options again and check they have "taken"
   if (tcgetattr(fd,&options2) < 0) {
      fprintf(stderr,"Second get failed\n");
      exit(-1);
   }

   // Compare
   if (memcmp((void *)&options,(void *)&options2,sizeof(options)) != 0) {
      fprintf(stderr,"Did not compare favourably!\n");
      exit(-1);
   }

	return(fd);
}



void delay(double timeout)
{
   struct timeval tp;
   double startsecs,secs;

   gettimeofday(&tp,NULL);
   startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;
	
   for (;;) {
      
      gettimeofday(&tp,NULL);
      secs = tp.tv_sec + tp.tv_usec / 1000000.0;
      if (secs-startsecs > timeout)
      return;
   }
 
}


int waitFor(int fdes,char* waitLetter,int timer)
{
    char Buffer[128];
    serialib_ReadString(fdes,Buffer,'K',128,timer) ;   // Read a maximum of 128 characters with a timeout                                                                         
    
    if (strstr(Buffer, waitLetter) != NULL) 
        { //printf ("%s \n",Buffer);
          
                return 1;
          }
 //printf ("not receive: %s \n",Buffer);

return 0;
}


      
int commandtimeout(double waittime, char* letter, int fdes)
{
     int t =0;    
     struct timeval tp;
     double startsecs,secs; 
   gettimeofday(&tp,NULL);
   startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;
       while (t != 1) {
       t = waitFor(fdes,letter,100);
       gettimeofday(&tp,NULL);
      secs = tp.tv_sec + tp.tv_usec / 1000000.0;
      if (secs-startsecs > waittime) { 
       return 0;}
       }
    return 1;
}



int cell_init(void)
{

     #define         DEVICE_PORT             "/dev/ttyO2"                         // ttyS0 for linux

     
    int des;                                                               // Used for return values
   
    char* apn ="internet.com";          // access-point name for GPRS
    char* ip = "47.55.87.129";   //IP address of the server 
    
    des= OpenSerial(DEVICE_PORT);                                                       // Open serial port
    if (des < 0) {                                                           // If an error occured...
        printf ("Error while opening port. Permission problem ?\n");        // ... display a message ...
        return des;                                                         // ... quit the application
    }
   printf ("Serial port opened successfully: \n");
   serialib_FlushReceiver(des);

SendSerialString(des,"AT+CGATT?\r\n"); 
printf ("Attaching GPRS.. \n");                                           
if (commandtimeout(5,"+CGATT: 1",des)== 0) {
printf ("Attaching GPRS failed.. \n");
printf ("Trying Connection again.. \n");
cell_reset(des);
}

SendSerialString(des,"AT+CGDCONT=1,\"IP\",\"internet.com\"\r\n");                                            
printf ("Setting up PDP Context... \n");                                                                                          
if (commandtimeout(5,"OK",des)== 0) {
printf ("Setting up PDP Context failed.. \n");
 }

SendSerialString(des,"AT+CGPCO=0,\"wapuser1\",\"wap\",1\r\n");                                            
printf ("Setting up PDP Password... \n");                                                                                          
if (commandtimeout(5,"OK",des)== 0) {
printf ("Setting up PDP Password failed.. \n");
}


SendSerialString(des,"AT+CGACT=1,1\r\n");                                            
printf ("Activating PDP Context... \n");                                                                                          
if (commandtimeout(5,"OK",des)== 0) {
printf ("Activating PDP Context failed.. \n");
} 

serialib_FlushReceiver(des);
char ipConnection[80];
sprintf(ipConnection,"AT+SDATACONF=1,\"TCP\",\"%s\",8080\r\n",ip);
SendSerialString(des,ipConnection);                                            
printf ("Configuring TCP connection to TCP Server...\n");                                                                                          
if (commandtimeout(5,"OK",des)== 0) {
printf ("Configuring TCP connection to TCP Server failed.. \n");
 } 

SendSerialString(des,"AT+SDATASTART=1,1\r\n");                                            
printf ("Starting TCP Connection...\n");                                                                                          
if (commandtimeout(5,"OK",des)== 0) {
printf ("Starting TCP Connection failed.. \n");
 } 

delay(1);

// checking the socket status and only breaking when we connect
  printf("Checking socket status:\n");
    SendSerialString(des,"AT+SDATASTATUS=1\r\n");        
   
     if (commandtimeout(5,"102",des) == 0)  {
      printf("Socket fails to connect.\n");
       cell_reset(des); 
    }
    else {
      printf("Socket connected\n");
    }

serialib_FlushReceiver(des);
return des;
    
} 

void cell_reset(int des)
{
     printf("Resetting the cellular device\n");
     SendSerialString(des,"AT+CFUN=1,1\r\n");
      delay(10);
      if (commandtimeout(50,"SIND: 4",des) == 0) { 
       cell_reset(des);}
       
      cell_init();

      
}

void send_data (char* data,int des) {

    char dat_send[50];
     char sense_data[1460];
   
    int data_length; 
        
    sprintf(sense_data,"GET http://47.55.87.129:8080/datapage.php?data=%s HTTP/1.0\x0D\x0A",data);
    char* collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";
    data_length = strlen(sense_data)+strlen(collect)-1;
  
    
    sprintf(dat_send,"AT+SDATATSEND=1,%d\r",data_length);
    SendSerialString(des,dat_send);
    printf ("Sending data...\n");

    if (commandtimeout(5,">",des)== 0) {
     printf ("Sending data failed.. \n");
         des = cell_init();
         send_data (data,des);
         return;
     } 
     SendSerialString(des,sense_data);
     SendSerialString(des,collect);
     serialib_FlushReceiver(des);
     if (commandtimeout(40,"STCPD",des)== 0) {
     printf ("Sending data failed.. \n");
     des = cell_init();
     send_data (data,des);
     return;
     } 
     else {printf ("Data Sent.. \n");
           }
    

    SendSerialString(des,"AT+SDATASTATUS=0\r\n");
    int y = commandtimeout(5,"SOCK",des);
     
}


void file_write_csv(MYSQL_RES *res, int num_cols_in_table_info, char* file)
{
	FILE *f = fopen(file, "a");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return;
	}
	
	fprintf (f, "\n");
	
	MYSQL_ROW row;
	
	while ((row = mysql_fetch_row(res)) != NULL)
	{
		int i;
		for (i = 0; i < num_cols_in_table_info ; i++)
		{
			fprintf (f, "%s,", row[i]);
		}
	}
	
	
	fclose(f);
	return;
}

void read_table(MYSQL_RES *res, char* tablename, int u)
{	
	MYSQL_ROW row;
        char filename[1405];
        char filena[1405];
        int j= 0;
         int m=0, g=0; 
         int h = 0;
        int p = 4;
       char stre[30];
       char* cur = "AA" ;
       char* temp = "BB" ;
       //char* xaxis = "CC" ;
      // char* yaxis = "DD" ;
      // char* zaxis = "EE" ;
     	char* acce = "CC";
       if (strstr(tablename,"accel") != NULL) { p = 6;}

	while ((row = mysql_fetch_row(res)) != NULL)
	{
		int i;
		for (i = 1; i < p ; i++)
		{
			
                      if (j == 0) {
                       
                       if (strstr(tablename,"cur") != NULL) {sprintf(stre,"%s,",cur);}
                       if (strstr(tablename,"temp") != NULL) {sprintf(stre,"%s,",temp);} 
                       //if (strstr(tablename,"xAxis") != NULL) {sprintf(stre,"%s,",xaxis);}
                       //if (strstr(tablename,"yAxis") != NULL) {sprintf(stre,"%s,",yaxis);}
                       //if (strstr(tablename,"zAxis") != NULL) {sprintf(stre,"%s,",zaxis);}
                       if (strstr(tablename,"accel") != NULL) {sprintf(stre,"%s,",acce);}

                       for(h=0;h!= strlen(stre);h++){
                      filename[j++] = stre[h];
                       }
                      i= i-1;
                     
                      }
                   
                     else{ 
                      if ( i == p-2) { sprintf(stre,"%s;",row[i]);} 
                      else { sprintf(stre,"%s,",row[i]);}

                      if (j < 1310) {

                     for(h=0;h!= strlen(stre);h++){
                     filename[j++] = stre[h];
                       }
                     }
                     else { 
                        if ( i > 1) {

                      for (m = i; m < p ; m++)
                      {  if ( m == p-2) { sprintf(stre,"%s;",row[m]);} 
                      else { sprintf(stre,"%s,",row[m]);}

                     for(h=0;h!= strlen(stre);h++){
                     filename[j++] = stre[h];
                       }
                     }
                     }

                     filename[j++]= 'X';

                       for ( j=0; j != strlen(filename); j++){

                     if (filename[j] != ' ') { 
                        
                              filena[g++] = filename[j];}


                    }

                    // printf("data is %s \n",filena);
                     send_data(filena,u);
                    g = 0;
                     j=0;
                  
                     i = p;
                       }
                     }
		
                    }

	}
	
            filename[j++] = 'X';

                for ( j=0; j != strlen(filename); j++){

                     if (filename[j] != ' ') { 
                       
                              filena[g++] = filename[j];}

                    }

           send_data(filena,u);
              
	return;
}

void file_add_headers_csv(MYSQL_ROW row, char* file)
{	
	FILE *f = fopen(file, "a");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return;
	}
	
	fprintf (f, "%s,", row[0]);
	
	fclose(f);
	return;
}

void file_clear_csv(char * file)
{
	FILE *f = fopen(file, "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return;
	}
	fclose(f);
}

void Table_2_CSV(char* fileName, char* tableName)
{	
	MYSQL_RES *res;
	MYSQL_ROW row;
	
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}   
	
	char query[100];
	sprintf(query,"SHOW COLUMNS FROM %s",tableName);
	
	//getting the columns from info
	if (mysql_query(con, &query[0]))
	{
		finish_with_error(con);
	}
	
	file_clear_csv(fileName);
	
	res = mysql_use_result(con);
	while ((row = mysql_fetch_row(res)) != NULL)
		file_add_headers_csv(row, fileName);
		
	//sets static global num_cols_in_table_info to the number of columns in table info.
	
        int num_cols_in_table_info = mysql_num_rows(res);
	mysql_free_result(res);
	
	sprintf(query,"SELECT * FROM %s",tableName);
	
	//getting the values from the table info
	if (mysql_query(con, &query[0])) 
	{
		finish_with_error(con);
	}
   
	res = mysql_use_result(con);
	file_write_csv(res, num_cols_in_table_info, fileName);

	mysql_free_result(res);
	
	mysql_close(con);


	return;
}


void Table_2 (char* tableName, int des)
{	
	MYSQL_RES *res;
	MYSQL_ROW row;
	
	MYSQL *con = mysql_init(NULL);
  
	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		return;
	}  
 
	//make mysql DB connection else return error
	if (mysql_real_connect(con, SQL_HOST_IP_ADRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE_NAME, 0, NULL, 0) == NULL) 
	{
		finish_with_error(con);
	}   
	
	char query[100];
	
	sprintf(query,"SELECT * FROM %s",tableName);
	
	//getting the values from the table info
	if (mysql_query(con, &query[0])) 
	{
		finish_with_error(con);
	}
   
	res = mysql_use_result(con);
         
	
        read_table(res,tableName,des);
	mysql_free_result(res);
	
	mysql_close(con);


	return;
}




void Delete_File(char* fileName)
{
	unlink(fileName);
}













