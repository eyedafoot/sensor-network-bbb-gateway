//Compile: gcc -o DB_Memory_Checker DB_Memory_Checker.c -lpthread


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/statvfs.h>
#include <pthread.h>

#include "mysql-connector.h"

int DBMemoryCheckerThreadContinue = 1;

int wait_dif, min_num_inserts;
int fdes; 

int Memory_Status = 2;

void *check_mem_loop(void* minutes)
{
	
	while (DBMemoryCheckerThreadContinue)
	{
		int total_queries = 0;
		total_queries = Get_Total_Inserts();
		
		 //printf("total_queries: %d\n", total_queries);
		
		if (total_queries >= min_num_inserts)
		{
			//DO_TCP_Transfer("xAxis");
                      // DO_Transfer("xAxis",fdes);
			DO_Transfer("accel",fdes);
			//DO_TCP_Transfer("yAxis");
			//DO_Transfer("yAxis",fdes);

			//DO_Transfer("zAxis");
			//DO_Transfer("zAxis",fdes);

			//DO_TCP_Transfer("temp");
			DO_Transfer("temp",fdes);

			//DO_TCP_Transfer("cur");
			DO_Transfer("cur",fdes);
		}
		
		if(minutes<MAX_THREAD_SLEEP_TIME_MIN)
		{
			wait_dif = (int)minutes;
			printf("in sleep\n");
			sleep((int)minutes*60);
		}
		else
		{
			wait_dif = wait_dif - MAX_THREAD_SLEEP_TIME_MIN;
			sleep((int)MAX_THREAD_SLEEP_TIME_MIN*60);
		}
	}
	pthread_exit(&Memory_Status);
}


pthread_t Memory_Check(int minutes, int num_DB_Inserts,int des)
{	
	fdes = des;
        min_num_inserts = num_DB_Inserts;
	pthread_t thread;
	int thr = pthread_create(&thread, NULL, &check_mem_loop, (void*)minutes);
	if (thr) 
		printf("failed to create thread.\n");
		
	return thread;
}