#include "DataTransferUtil.h"

#include "app/framework/include/af.h"
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>

EmberStatus sendDataToServer_Cellular (char *urlBuffer, char *dataBuffer, RestMethod restMethod, int fdes) {//(char *data, int fdes, int commandID) {

    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == PUT || restMethod == POST) {
        char data_send[50];
        char sense_data[1460];
        char dataRecieved[200];
        size_t nbytes;
        ssize_t bytes_read;
        int data_length;

        if (restMethod == PUT) {
            sprintf(sense_data, "PUT %s HTTP/1.0\x0D\x0A", urlBuffer);
            char *collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";

            strcat(sense_data, collect);
        } else if (restMethod == POST) {
            char* post = "";
            sprintf(post, "POST %s HTTP/1.0\x0D\x0A", urlBuffer);
            char* contentType = "Content-Type: text/xml; charset=utf-8\x0D\x0A";
            char* contentLength = "";
            sprintf(contentLength, "Content-Length: %d\x0D\x0A", strlen(dataBuffer));
            char *collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";
            
            sprintf(sense_data, "%s", post);
            strcat(sense_data, contentType);
            strcat(sense_data, contentLength);
            strcat(sense_data, collect);
            strcat(sense_data, dataBuffer);

            /*Data Transmission Format:
             *POST host:port/path.xml?query HTTP/1.0
             *Content-Type: text/xml; charset=utf-8 //Data can be of a different type//
             *Content-Length: 88
             *Connection: Keep-Alive
             * //Data goes here, format must match content-type//
             *<?xml version="1.0" encoding="utf-8"?>
             *<string xmlns="http://clearforest.com/">string</string>
            */
        }
        data_length = strlen(sense_data) - 1;
        // printf ("The buffer is is %s \n",sense_data);

        if (data_length >= 1460) {
            return status;
        }

        sprintf(data_send, "AT+SDATATSEND=1,%d\r", data_length);
        SendSerialString(fdes, data_send);
        printf("Sending data...\n");

        if (commandtimeout(5, ">", fdes) == 0) {
            printf("Sending data failed.. \n");
            fdes = cell_init();
            status = sendDataToServer_Cellular(urlBuffer, dataBuffer, restMethod, fdes);
            return status;
        }

        SendSerialString(fdes, sense_data);
        serialib_FlushReceiver(fdes);
        if (commandtimeout(40, "STCPD", fdes) == 0) {
            printf("Sending data failed.. \n");
            fdes = cell_init();
            status = sendDataToServer_Cellular(urlBuffer, dataBuffer, restMethod, fdes);
            return status;
        } else {
            printf("Data Sent.. \n");

            SendSerialString(fdes, "AT+SDATATREAD=1");
            bytes_read = read(fdes, dataRecieved, nbytes);

        }

        SendSerialString(fdes, "AT+SDATASTATUS=0\r\n");
        int y = commandtimeout(5, "SOCK", fdes);
    }
    return status;

}

EmberStatus getDataFromServer_Cellular(char *urlBuffer, struct string *dataRecieved, RestMethod restMethod, int fdes) {

    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == GET) {

        char data_send[50];
        char sense_data[1460];
        size_t nbytes;
        ssize_t bytes_read;
        int data_length;
        char *ptr;

        sprintf(sense_data, "GET %s HTTP/1.0\x0D\x0A", urlBuffer);
        char *collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";
        data_length = strlen(sense_data) + strlen(collect) - 1;
        // printf ("The buffer is is %s \n",sense_data);

        sprintf(data_send, "AT+SDATATSEND=1,%d\r", data_length);
        SendSerialString(fdes, data_send);
        printf("Sending data...\n");

        if (commandtimeout(5, ">", fdes) == 0) {
            printf("Sending data failed.. \n");
            fdes = cell_init();
            status = getDataFromServer_Cellular(urlBuffer, dataRecieved, restMethod, fdes);
            return status;
        }

        SendSerialString(fdes, sense_data);
        SendSerialString(fdes, collect);
        serialib_FlushReceiver(fdes);
        if (commandtimeout(40, "STCPD", fdes) == 0) {
            printf("Sending data failed.. \n");
            fdes = cell_init();
            status = getDataFromServer_Cellular(urlBuffer, dataRecieved, restMethod, fdes);
            return status;
        } else {
            printf("Data Sent.. \n");

            SendSerialString(fdes, "AT+SDATATREAD=1");
            bytes_read = read(fdes, ptr, nbytes);
            _writefunc(ptr, bytes_read, nbytes, dataRecieved);

        }

        SendSerialString(fdes, "AT+SDATASTATUS=0\r\n");
        int y = commandtimeout(5, "SOCK", fdes);
    }
    return status;

}