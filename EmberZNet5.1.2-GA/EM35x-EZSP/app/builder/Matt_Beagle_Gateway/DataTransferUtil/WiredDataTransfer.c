#include "DataTransferUtil.h"

#include "app/framework/include/af.h"
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>

EmberStatus sendDataToServer(char *urlBuffer, char* dataBuffer, RestMethod restMethod) {
    
    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == PUT || restMethod == POST) {
        CURL *curl;
        CURLcode res;

        /* get a curl handle */
        curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();
        if (curl) {
            /* we want to use our own read function */
            /* specify target */
            curl_easy_setopt(curl, CURLOPT_URL, urlBuffer);

            if (restMethod == PUT) {
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
            } else if (restMethod == POST) {
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, dataBuffer);
            }

            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");

            /* Now run off and do what you've been told! */
            printf("%s: ",  urlBuffer);
            res = curl_easy_perform(curl);

            /* check for errors */
            if (res != CURLE_OK) {
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
                printf("URL: %s\n", urlBuffer);
            }else {
                status = EMBER_SUCCESS;
            }

            /* always cleanup */
            curl_easy_cleanup(curl);
            /* we're done with libcurl, so clean it up */
            curl_global_cleanup();
        }
    }

    return status;
}

EmberStatus getDataFromServer(char *urlBuffer, struct string *dataRecieved, RestMethod restMethod) { 

    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == GET) {
        CURL *curl;
        CURLcode res;

        /* get a curl handle */
        curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();
        if (curl) {

            /* we want to use our own read function */
            /* specify target */
            curl_easy_setopt(curl, CURLOPT_URL, urlBuffer);

            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _writefunc);

            /* now specify which file to write to */
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, dataRecieved);
            // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);

            /* Now run off and do what you've been told! */
            res = curl_easy_perform(curl);

            /* check for errors */
            if (res != CURLE_OK) {
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
                printf("URL: %s\n", urlBuffer);
            }else {
                status = EMBER_SUCCESS;
            }

            /* always cleanup */
            curl_easy_cleanup(curl);
            /* we're done with libcurl, so clean it up */
            curl_global_cleanup();
        }
    }
    
    return status;
}