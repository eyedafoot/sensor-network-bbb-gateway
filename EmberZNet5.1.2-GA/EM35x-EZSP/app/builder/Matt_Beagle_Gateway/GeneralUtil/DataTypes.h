#ifndef __DATA_TYPES__
#define __DATA_TYPES__

/********************************
 * Dependancies
 ********************************/
#include <stdlib.h>
 #include "app/framework/include/af.h"

/********************************
 * Constants
 ********************************/
 
/********************************
 * Data Types
 ********************************/

struct string {
    char *ptr;
    size_t len;
};

/********************************
 * Public Methods
 ********************************/
/*
 * init_string, used to initialize string struct
 *
 * s = pointer to string struct being initialized
 */
void init_string(struct string *s);

#endif // __DATA_TYPES__