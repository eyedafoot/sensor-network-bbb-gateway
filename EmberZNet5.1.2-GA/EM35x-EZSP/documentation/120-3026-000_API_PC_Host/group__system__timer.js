var group__system__timer =
[
    [ "halIdleForMilliseconds", "group__system__timer.htm#ga1650ceea2a1a53ccba7dbc74116564fd", null ],
    [ "halInternalStartSystemTimer", "group__system__timer.htm#gae0b935fd7ac5ee0070e31fb43bf82fba", null ],
    [ "halCommonGetInt16uMillisecondTick", "group__system__timer.htm#gacbbf0cd5321a6bd054b7e99c7b492f08", null ],
    [ "halCommonGetInt32uMillisecondTick", "group__system__timer.htm#ga7033d63e4ee56452e3e6c67d49f01ef2", null ],
    [ "halCommonGetInt16uQuarterSecondTick", "group__system__timer.htm#ga5aec52f8f5e5b85acacfb41a4a437593", null ],
    [ "halSleepForQuarterSeconds", "group__system__timer.htm#gaba299b09f7ca58322d8993b55aa80331", null ],
    [ "halSleepForMilliseconds", "group__system__timer.htm#ga808d2ad3a55898b4ca37b1ad05ba88f6", null ],
    [ "halCommonIdleForMilliseconds", "group__system__timer.htm#ga9a16dcabe220dad1c36d2c0693f710ce", null ]
];