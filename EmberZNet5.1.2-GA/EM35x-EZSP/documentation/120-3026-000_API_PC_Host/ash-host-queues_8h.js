var ash_host_queues_8h =
[
    [ "TX_POOL_BUFFERS", "ash-host-queues_8h.htm#ga2f763955153f2a9ca3ef45de2112431c", null ],
    [ "RX_FREE_LWM", "ash-host-queues_8h.htm#ga340c2fdf03a1cd7dac6904ed899ebf1a", null ],
    [ "RX_FREE_HWM", "ash-host-queues_8h.htm#ga8186f2f1d89891d40ea19f5a953c9d5c", null ],
    [ "AshBuffer", "ash-host-queues_8h.htm#ga37de1a17e11f25f3fec463392a422f01", null ],
    [ "ashInitQueues", "ash-host-queues_8h.htm#gad91e2cf4e7ef4e66b7b1f70355e4fb6a", null ],
    [ "ashFreeBuffer", "ash-host-queues_8h.htm#ga0b900eb64117f3116aae1362f7118a6c", null ],
    [ "ashAllocBuffer", "ash-host-queues_8h.htm#ga2e0c39aaf9efbfedf779b4816413b4b2", null ],
    [ "ashRemoveQueueHead", "ash-host-queues_8h.htm#gad1e7a61ecf509fbc42dcb2819d88afdc", null ],
    [ "ashQueueHead", "ash-host-queues_8h.htm#ga677ab08656d32185757b684aadc8ff8b", null ],
    [ "ashQueueNthEntry", "ash-host-queues_8h.htm#gabc56ebff8534f6d8a28a3d300543fbc2", null ],
    [ "ashQueuePrecedingEntry", "ash-host-queues_8h.htm#ga2a0567a19fb04444b5bb3f78b6965052", null ],
    [ "ashRemoveQueueEntry", "ash-host-queues_8h.htm#ga4f49042c77fdedca6ad6fe2a3281ff7f", null ],
    [ "ashQueueLength", "ash-host-queues_8h.htm#ga847387661259b00101f750af184c0f8c", null ],
    [ "ashFreeListLength", "ash-host-queues_8h.htm#ga6997fc8fa8f176ac40ba88835200fed6", null ],
    [ "ashAddQueueTail", "ash-host-queues_8h.htm#ga4edcf0c7f04bb19734e1e39e6cba6011", null ],
    [ "ashQueueIsEmpty", "ash-host-queues_8h.htm#gad5d5ab7c983e5888fe885212c6a6addc", null ],
    [ "txQueue", "ash-host-queues_8h.htm#gaab15c4512df121780a8c2bd3e19b1b16", null ],
    [ "reTxQueue", "ash-host-queues_8h.htm#gad6f25b6618b0f3b2cf6f7a3991b5b360", null ],
    [ "rxQueue", "ash-host-queues_8h.htm#ga81be96792b9e47cb0919dd8072a4f45d", null ],
    [ "txFree", "ash-host-queues_8h.htm#gac368463dc24aead1b0d74ce4c63f2db8", null ],
    [ "rxFree", "ash-host-queues_8h.htm#ga1f8e697cdd2d9e465a8f10e45e36e501", null ]
];