var structIasZoneDevice =
[
    [ "ieeeAddress", "structIasZoneDevice.html#abcf3ed12aeabda5e297f9ba30082d45b", null ],
    [ "zoneType", "structIasZoneDevice.html#a08d32b5837612ca1cddd9120739a2b24", null ],
    [ "zoneStatus", "structIasZoneDevice.html#a564da8bac9f606c8f99128e14f23996f", null ],
    [ "zoneState", "structIasZoneDevice.html#aebc4549511264dd550f3e1a83a493689", null ],
    [ "endpoint", "structIasZoneDevice.html#aebfef89a89b2581ec9eeba4aaffe8199", null ],
    [ "zoneId", "structIasZoneDevice.html#af93fe7f0291a9486ed1741812d418f9c", null ]
];