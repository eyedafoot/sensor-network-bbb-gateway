var structrxFragmentedPacket =
[
    [ "status", "structrxFragmentedPacket.html#a546b75bdf42ea97508ac3acec406fc30", null ],
    [ "ackedPacketAge", "structrxFragmentedPacket.html#adb746a125bb1d5a83232fa25b36921fb", null ],
    [ "buffer", "structrxFragmentedPacket.html#ae6ca12e104616d098d8728926f245b73", null ],
    [ "fragmentSource", "structrxFragmentedPacket.html#aa7a52d50e9b00a63c0250b2cefc7bd93", null ],
    [ "fragmentSequenceNumber", "structrxFragmentedPacket.html#a62dab74968035b9c4a55a9eedbcfe2dd", null ],
    [ "fragmentBase", "structrxFragmentedPacket.html#a6329371be5c354698bc3486b6cc07479", null ],
    [ "windowFinger", "structrxFragmentedPacket.html#ada46a96e3161d07738374b1d258f1d36", null ],
    [ "fragmentsExpected", "structrxFragmentedPacket.html#a25440c728b9a5873faaefe673effee8b", null ],
    [ "fragmentsReceived", "structrxFragmentedPacket.html#a2fcc9c0e2220ff9df5f41342a515a45b", null ],
    [ "fragmentMask", "structrxFragmentedPacket.html#a47fa2ef2fec8e9a6759de5ad404b5ee0", null ],
    [ "lastfragmentLen", "structrxFragmentedPacket.html#ad7336b03643d644039bb38e6323e48ef", null ],
    [ "fragmentLen", "structrxFragmentedPacket.html#a5fb7c7f4087426e9222117c42c0f4a7c", null ],
    [ "fragmentEventControl", "structrxFragmentedPacket.html#a193099bf608a79585f78d3144fc173df", null ]
];