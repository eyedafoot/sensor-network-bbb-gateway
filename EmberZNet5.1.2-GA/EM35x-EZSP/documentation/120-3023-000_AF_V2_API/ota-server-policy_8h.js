var ota_server_policy_8h =
[
    [ "emAfOtaServerPolicyPrint", "ota-server-policy_8h.html#a9baba2af2d203e2bb513350073617a8b", null ],
    [ "emAfOtaServerSetQueryPolicy", "ota-server-policy_8h.html#a483c5cc1652c0221200d673b7f10732e", null ],
    [ "emAfOtaServerSetBlockRequestPolicy", "ota-server-policy_8h.html#abaa02085ee4a71abc24fabac7a9130b2", null ],
    [ "emAfOtaServerSetUpgradePolicy", "ota-server-policy_8h.html#a5fcab32bae433659410fc1357c5a81b8", null ],
    [ "emAfServerPageRequestTickCallback", "ota-server-policy_8h.html#a659de203e3c05d1ff27c38f0bc4aa789", null ],
    [ "emAfSetPageRequestMissedBlockModulus", "ota-server-policy_8h.html#a03351dea05d9b2e2eef22c62d47a081c", null ],
    [ "emAfOtaServerSetPageRequestPolicy", "ota-server-policy_8h.html#ae3c76cbe71947bd769ea848a9da37d7b", null ],
    [ "emAfOtaServerPolicySetMinBlockRequestPeriod", "ota-server-policy_8h.html#a9faa0bcb8137dd8787ba4feba704184f", null ],
    [ "emAfOtaServerImageBlockRequestCallback", "ota-server-policy_8h.html#a45bddc500b10dcb7a39b55fbccd6ac3c", null ]
];