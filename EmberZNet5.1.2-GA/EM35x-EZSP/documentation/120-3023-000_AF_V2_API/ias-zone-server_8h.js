var ias_zone_server_8h =
[
    [ "UNDEFINED_ZONE_ID", "ias-zone-server_8h.html#a71979d19fdd499bb5970dc6033eec9a1", null ],
    [ "UNKNOWN_ENDPOINT", "ias-zone-server_8h.html#aded477150c4cc59e9bb55021952d435c", null ],
    [ "emberAfPluginIasZoneServerUpdateZoneStatus", "ias-zone-server_8h.html#a3e8f6706eaa9f1c5656f6a70d1c58bd9", null ],
    [ "emAfGetIasZoneServerEndpoint", "ias-zone-server_8h.html#a24400023e8f107f30544d15a01c554e0", null ],
    [ "emAfGetRemoteIasZoneClientEndpoint", "ias-zone-server_8h.html#ae1facbd292f54963f41cb625d5fba253", null ],
    [ "emberAfPluginIasZoneServerGetZoneId", "ias-zone-server_8h.html#a5b8cb2fb9f9778b1e68425009ad68ff1", null ]
];