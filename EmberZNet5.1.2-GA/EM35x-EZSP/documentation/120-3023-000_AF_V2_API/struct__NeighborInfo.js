var struct__NeighborInfo =
[
    [ "neighbor", "struct__NeighborInfo.html#a879da468896fb6a86a3cdaf596118e20", null ],
    [ "x", "struct__NeighborInfo.html#a615d3ff6109c415739f7cf26c2e4d77c", null ],
    [ "y", "struct__NeighborInfo.html#a97f448209d7c133206e145967d2227b2", null ],
    [ "z", "struct__NeighborInfo.html#af1f61fbd46c6409e804d219edc409ef5", null ],
    [ "rssi", "struct__NeighborInfo.html#a2aad2941d25170a8a6ad88a17e2c2ddf", null ],
    [ "numberRssiMeasurements", "struct__NeighborInfo.html#a20a50dd077c44e50866e3db1f6ec67b7", null ]
];