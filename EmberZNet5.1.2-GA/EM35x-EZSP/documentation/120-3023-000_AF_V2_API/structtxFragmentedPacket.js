var structtxFragmentedPacket =
[
    [ "messageType", "structtxFragmentedPacket.html#aed36bd7357696801e4712f679a04779c", null ],
    [ "indexOrDestination", "structtxFragmentedPacket.html#a697d2f67b7754bf40cbb8251f4be8bec", null ],
    [ "sequence", "structtxFragmentedPacket.html#a084e9f460bc907a15fda234fcf4b62d7", null ],
    [ "apsFrame", "structtxFragmentedPacket.html#aa820196c2ea1b47e3914d0adc7a502e7", null ],
    [ "buffer", "structtxFragmentedPacket.html#aaf9e21f84ad5c1af54f3d10aae122d3a", null ],
    [ "bufLen", "structtxFragmentedPacket.html#a8a0f5233f8c4dc1b30ab79c77abda64d", null ],
    [ "fragmentLen", "structtxFragmentedPacket.html#a39d11b11b92549a407a562fcaacd8ef6", null ],
    [ "fragmentCount", "structtxFragmentedPacket.html#acc54367cc9dd17491eb66c4c167cad3d", null ],
    [ "fragmentBase", "structtxFragmentedPacket.html#a8e1369acd6dfe1397f23cbc5416fa978", null ],
    [ "fragmentsInTransit", "structtxFragmentedPacket.html#aca834031279b52d6e059ba49c0532730", null ]
];