var modules =
[
    [ "Ember Application Framework API Reference", "group__appframework.html", "group__appframework" ],
    [ "Ember Application Framework Command Line Interface (CLI)", "group__cli.html", "group__cli" ],
    [ "Application Framework V2 hal callback interface Reference", "group__halCallback.html", "group__halCallback" ],
    [ "Status_codes", "group__status__codes.html", "group__status__codes" ],
    [ "Ember_types", "group__ember__types.html", "group__ember__types" ]
];