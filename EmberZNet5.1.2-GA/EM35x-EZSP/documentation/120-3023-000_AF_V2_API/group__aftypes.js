var group__aftypes =
[
    [ "EmberAfDefaultAttributeValue", "unionEmberAfDefaultAttributeValue.html", [
      [ "ptrToDefaultValue", "unionEmberAfDefaultAttributeValue.html#aedf8ec4bdc7e648a471e936c1676feda", null ],
      [ "defaultValue", "unionEmberAfDefaultAttributeValue.html#a86e9ebd44842d6b6700f75bd470e83fc", null ]
    ] ],
    [ "EmberAfAttributeMinMaxValue", "structEmberAfAttributeMinMaxValue.html", [
      [ "defaultValue", "structEmberAfAttributeMinMaxValue.html#a9c3924d3a468148afde9cd642dcdf1f2", null ],
      [ "minValue", "structEmberAfAttributeMinMaxValue.html#af8751a949ea8e9fbecd3024b876378dc", null ],
      [ "maxValue", "structEmberAfAttributeMinMaxValue.html#a3d90ca7b710e4f35ce51a36eb758b30d", null ]
    ] ],
    [ "EmberAfDefaultOrMinMaxAttributeValue", "unionEmberAfDefaultOrMinMaxAttributeValue.html", [
      [ "ptrToDefaultValue", "unionEmberAfDefaultOrMinMaxAttributeValue.html#a32359be8a0f0c9f1118a52acd46443b8", null ],
      [ "defaultValue", "unionEmberAfDefaultOrMinMaxAttributeValue.html#a0bf2ce5047f21eacace33a08dae2bde0", null ],
      [ "ptrToMinMaxValue", "unionEmberAfDefaultOrMinMaxAttributeValue.html#a37570707408044421455e9fe31f205ba", null ]
    ] ],
    [ "EmberAfAttributeMetadata", "structEmberAfAttributeMetadata.html", [
      [ "attributeId", "structEmberAfAttributeMetadata.html#a718375e111cb2538da87e28a249b6e17", null ],
      [ "attributeType", "structEmberAfAttributeMetadata.html#abd764b6a60df5eb2a20265d4b065fcfa", null ],
      [ "size", "structEmberAfAttributeMetadata.html#a80c138d5282992c192f2150d5a58c81a", null ],
      [ "mask", "structEmberAfAttributeMetadata.html#ab0d015b79b9a165e4f029e6676523613", null ],
      [ "defaultValue", "structEmberAfAttributeMetadata.html#a6907df64b68244bdc73303dfd07c350c", null ]
    ] ],
    [ "EmberAfCluster", "structEmberAfCluster.html", [
      [ "clusterId", "structEmberAfCluster.html#ab12348d25f86b7368d5b365cd290454a", null ],
      [ "attributes", "structEmberAfCluster.html#aa9aecf7e6f41eb37fdb49df7f8bc738c", null ],
      [ "attributeCount", "structEmberAfCluster.html#a45bf6b1d853a6873e59dcdd8c9f28cdf", null ],
      [ "clusterSize", "structEmberAfCluster.html#a6a312d08cd6ff436ac943f1cdf90d4c2", null ],
      [ "mask", "structEmberAfCluster.html#a27bea3d1cd019aa022d51e1f6cfe773b", null ],
      [ "functions", "structEmberAfCluster.html#abcf6b689018d9631ee614618916f9055", null ]
    ] ],
    [ "EmberAfAttributeSearchRecord", "structEmberAfAttributeSearchRecord.html", [
      [ "endpoint", "structEmberAfAttributeSearchRecord.html#a752319448405bb2cc4e6b020f3a8555e", null ],
      [ "clusterId", "structEmberAfAttributeSearchRecord.html#a52dfb155b11cee780b269284327c57cd", null ],
      [ "clusterMask", "structEmberAfAttributeSearchRecord.html#ad6657b2236b6f0b4abff692cda56b4e7", null ],
      [ "attributeId", "structEmberAfAttributeSearchRecord.html#af35e2771754af43e4bbb5fafd2c10583", null ],
      [ "manufacturerCode", "structEmberAfAttributeSearchRecord.html#ab41bee8f20e5873a39c1d2674cb2569c", null ]
    ] ],
    [ "EmberAfManufacturerCodeEntry", "structEmberAfManufacturerCodeEntry.html", [
      [ "index", "structEmberAfManufacturerCodeEntry.html#a06e58ee4d327369f4bf9aecbba0397a3", null ],
      [ "manufacturerCode", "structEmberAfManufacturerCodeEntry.html#a9d1b0f831901bd56fc42f4df4cc1c7ab", null ]
    ] ],
    [ "EmberAfIncomingMessage", "structEmberAfIncomingMessage.html", [
      [ "type", "structEmberAfIncomingMessage.html#a92d186927b561b821aeab4ff6f080ce5", null ],
      [ "apsFrame", "structEmberAfIncomingMessage.html#a23373b4a1a27c6ca8bb1b4be40585008", null ],
      [ "message", "structEmberAfIncomingMessage.html#a75b5c2ae1a0520e71fcc05357aed2cd6", null ],
      [ "msgLen", "structEmberAfIncomingMessage.html#a93090475c01435453404a6f7a89d6829", null ],
      [ "source", "structEmberAfIncomingMessage.html#a9adefb8feb3ff63c9256ab32c2834e0f", null ],
      [ "lastHopLqi", "structEmberAfIncomingMessage.html#a53c6a9a0f1a1a2dd1a7b4ea5b3b5e031", null ],
      [ "lastHopRssi", "structEmberAfIncomingMessage.html#a92936760ca64aac72c424d4f0bc7e467", null ],
      [ "bindingTableIndex", "structEmberAfIncomingMessage.html#a1382f766801b36035feed3e4afe534df", null ],
      [ "addressTableIndex", "structEmberAfIncomingMessage.html#af9dd178ae4f494b0826599d0305bce5d", null ],
      [ "networkIndex", "structEmberAfIncomingMessage.html#a51874ff931d53759706b8888746a07db", null ]
    ] ],
    [ "EmberAfInterpanHeader", "structEmberAfInterpanHeader.html", [
      [ "messageType", "structEmberAfInterpanHeader.html#ae647e0829008ee6a21b9153172e3f0ec", null ],
      [ "longAddress", "structEmberAfInterpanHeader.html#a3f487397576c5d7accbd229e5ca726e7", null ],
      [ "shortAddress", "structEmberAfInterpanHeader.html#a3c41ab30760c9c6fa9694d4b8138b1c3", null ],
      [ "panId", "structEmberAfInterpanHeader.html#a8355a01e76c4295cfcd7b66fe856dc06", null ],
      [ "profileId", "structEmberAfInterpanHeader.html#a16d9227a5a80de52d08af6290b86d044", null ],
      [ "clusterId", "structEmberAfInterpanHeader.html#afe6dbeaf77cefa427ced749a61064a26", null ],
      [ "groupId", "structEmberAfInterpanHeader.html#a524c15ea4b1e761c26ee1fc3a557da7c", null ],
      [ "options", "structEmberAfInterpanHeader.html#aeb0c6b090032d5f6365f21628dff0ad0", null ]
    ] ],
    [ "EmberAfAllowedInterPanMessage", "structEmberAfAllowedInterPanMessage.html", [
      [ "profileId", "structEmberAfAllowedInterPanMessage.html#a2bee7014c743ffa0ca403f3c18de180a", null ],
      [ "clusterId", "structEmberAfAllowedInterPanMessage.html#a09c93c3c92755f11837d3e0708244f7b", null ],
      [ "commandId", "structEmberAfAllowedInterPanMessage.html#a8850480b92bb85d0d7ef89f6f13e211b", null ],
      [ "options", "structEmberAfAllowedInterPanMessage.html#ad25bc04a8ad13b78c209931f2ffd59df", null ]
    ] ],
    [ "EmberAfClusterCommand", "structEmberAfClusterCommand.html", [
      [ "apsFrame", "structEmberAfClusterCommand.html#aeb6b7d7c321dbfe5d8e6b9db48a3782c", null ],
      [ "type", "structEmberAfClusterCommand.html#a8c47d482eeed608d85dfcb4fbe22a74b", null ],
      [ "source", "structEmberAfClusterCommand.html#a1e532ceb80a2c1b286602b1f4da8faf1", null ],
      [ "buffer", "structEmberAfClusterCommand.html#ae51a05f75839b68fc93c21652815b3c6", null ],
      [ "bufLen", "structEmberAfClusterCommand.html#aecb2d690382c3634ee83f3ba3e4667d6", null ],
      [ "clusterSpecific", "structEmberAfClusterCommand.html#ac8a942ed30dfebb7811e89d1f0307e45", null ],
      [ "mfgSpecific", "structEmberAfClusterCommand.html#a2e298c4607365cd9264724ee8b1290cc", null ],
      [ "mfgCode", "structEmberAfClusterCommand.html#ac1f445fa6b1049fa0bab5e1b50b019f6", null ],
      [ "seqNum", "structEmberAfClusterCommand.html#ab43c58acfb2fae7c9abb10c29e271e1b", null ],
      [ "commandId", "structEmberAfClusterCommand.html#acde1f8b25701fc95429e38aaa53a2c46", null ],
      [ "payloadStartIndex", "structEmberAfClusterCommand.html#afccb28074cdd33d11a3437bf0652d351", null ],
      [ "direction", "structEmberAfClusterCommand.html#ad82340f45d8e83ba932956423135439f", null ],
      [ "interPanHeader", "structEmberAfClusterCommand.html#a5307eb0e5bf9991e5666ab47c3843d54", null ],
      [ "networkIndex", "structEmberAfClusterCommand.html#a0a52163874f84b915629c514699dbef8", null ]
    ] ],
    [ "EmberAfEndpointType", "structEmberAfEndpointType.html", [
      [ "cluster", "structEmberAfEndpointType.html#a8f8d90d59eb1025498f7f018e63ca615", null ],
      [ "clusterCount", "structEmberAfEndpointType.html#a1a726fd97191c1ef8af3dfb22e4b0cbc", null ],
      [ "endpointSize", "structEmberAfEndpointType.html#a680f6374ecd709919077b3581359b32f", null ]
    ] ],
    [ "EmberAfSecurityProfileData", "structEmberAfSecurityProfileData.html", [
      [ "securityProfile", "structEmberAfSecurityProfileData.html#ab3e7221dd90a0e7cf6e2b547aa7ba4ab", null ],
      [ "tcBitmask", "structEmberAfSecurityProfileData.html#ae8b2a411e62d3669cec9554f50ed87b3", null ],
      [ "tcExtendedBitmask", "structEmberAfSecurityProfileData.html#a981d400357ee01b4732548ee280a2675", null ],
      [ "nodeBitmask", "structEmberAfSecurityProfileData.html#a65024340cbd1a501dd7cd4c6676f8357", null ],
      [ "nodeExtendedBitmask", "structEmberAfSecurityProfileData.html#a004f254b80f3eccde44ecf9c18f4f7f9", null ],
      [ "tcLinkKeyRequestPolicy", "structEmberAfSecurityProfileData.html#adc6fe5e7a88bb4efbc0df5967a3e45de", null ],
      [ "appLinkKeyRequestPolicy", "structEmberAfSecurityProfileData.html#a218dbba76dd21ca9c03bbd57dc7a034f", null ],
      [ "preconfiguredKey", "structEmberAfSecurityProfileData.html#af0f3a7badc425d1838dfecae1d7f765f", null ]
    ] ],
    [ "EmberAfNetwork", "structEmberAfNetwork.html", [
      [ "nodeType", "structEmberAfNetwork.html#a46eeb9b00f6347fb47aeb9dc96471555", null ],
      [ "securityProfile", "structEmberAfNetwork.html#adf344a620579570150d17264a920a498", null ]
    ] ],
    [ "EmberAfDefinedEndpoint", "structEmberAfDefinedEndpoint.html", [
      [ "endpoint", "structEmberAfDefinedEndpoint.html#a032888fe5ca74b59ed1776f5ef5ef0c0", null ],
      [ "profileId", "structEmberAfDefinedEndpoint.html#aad843ef773f6477d0b4686a32ac1bb4b", null ],
      [ "deviceId", "structEmberAfDefinedEndpoint.html#ad3176cc2bd084e0f4501f344034746d2", null ],
      [ "deviceVersion", "structEmberAfDefinedEndpoint.html#a2c3684fa8a482179118645bf9d8fbee0", null ],
      [ "endpointType", "structEmberAfDefinedEndpoint.html#af6ea50f152c24e38d5c0dd0592970523", null ],
      [ "networkIndex", "structEmberAfDefinedEndpoint.html#a7c0294e1faecddffdbcbaa0003279a67", null ],
      [ "bitmask", "structEmberAfDefinedEndpoint.html#a1a8cfac088eb36dccb5bbf29ae5732be", null ]
    ] ],
    [ "EmberAfLoadControlEvent", "structEmberAfLoadControlEvent.html", [
      [ "eventId", "structEmberAfLoadControlEvent.html#a9856c861ce6006d320fd0ac1011df9be", null ],
      [ "destinationEndpoint", "structEmberAfLoadControlEvent.html#a43591d7493f0302a51b0fae554072295", null ],
      [ "deviceClass", "structEmberAfLoadControlEvent.html#aa42b4d65bfea9f168568937aea9897ef", null ],
      [ "utilityEnrollmentGroup", "structEmberAfLoadControlEvent.html#af25ad13e922189aed800c572ae3d674f", null ],
      [ "startTime", "structEmberAfLoadControlEvent.html#ab55566222755e28f5d792dd9e0caa9a7", null ],
      [ "duration", "structEmberAfLoadControlEvent.html#a922e537c355cbe75422129fe3756548a", null ],
      [ "criticalityLevel", "structEmberAfLoadControlEvent.html#a76531d494c7916e51637b0e1e6e7fbca", null ],
      [ "coolingTempOffset", "structEmberAfLoadControlEvent.html#a9bb4e7f6337dfd5d00874401c27f1d9f", null ],
      [ "heatingTempOffset", "structEmberAfLoadControlEvent.html#a406b6bcbd5f13760f5e80ccf523df13c", null ],
      [ "coolingTempSetPoint", "structEmberAfLoadControlEvent.html#a336464ea065f0a00b2cc2a7d62f167eb", null ],
      [ "heatingTempSetPoint", "structEmberAfLoadControlEvent.html#a7056cc70db00c4341342082951aab96d", null ],
      [ "avgLoadPercentage", "structEmberAfLoadControlEvent.html#ad91ec1a6a93ea51b1e365dcbdc964d13", null ],
      [ "dutyCycle", "structEmberAfLoadControlEvent.html#af88c5bb1c01aa864f5722834d5f2ad70", null ],
      [ "eventControl", "structEmberAfLoadControlEvent.html#a96edf251af3406303d0c3943efea3b18", null ],
      [ "startRand", "structEmberAfLoadControlEvent.html#a2e43a43632656f5c79767929562dfa3b", null ],
      [ "endRand", "structEmberAfLoadControlEvent.html#a248f09d80c86de4f1d89d884e673e61f", null ],
      [ "optionControl", "structEmberAfLoadControlEvent.html#aa7660da4c24a874bc650a2e2566b90ab", null ]
    ] ],
    [ "EmberAfServiceDiscoveryResult", "structEmberAfServiceDiscoveryResult.html", [
      [ "status", "structEmberAfServiceDiscoveryResult.html#aef80f9ab4fa123dd244336a86ba40360", null ],
      [ "zdoRequestClusterId", "structEmberAfServiceDiscoveryResult.html#a857b89159763db90574762f2766f455f", null ],
      [ "matchAddress", "structEmberAfServiceDiscoveryResult.html#abd37e60ee1d8a2752ed2fa37907cf5f3", null ],
      [ "responseData", "structEmberAfServiceDiscoveryResult.html#a4ef0d8794820d1beb25ee6b5eb05d61d", null ]
    ] ],
    [ "EmberAfEndpointList", "structEmberAfEndpointList.html", [
      [ "count", "structEmberAfEndpointList.html#a9909b4f74eb8c4b29d3c22b9edd33922", null ],
      [ "list", "structEmberAfEndpointList.html#a0a2a8df16dc0af0743c2c6e74c964827", null ]
    ] ],
    [ "EmberAfClusterList", "structEmberAfClusterList.html", [
      [ "inClusterCount", "structEmberAfClusterList.html#a430878553737362c00d3376b8459c7ad", null ],
      [ "inClusterList", "structEmberAfClusterList.html#add60e2b322fe3324066b74399212ccea", null ],
      [ "outClusterCount", "structEmberAfClusterList.html#a112041c38555bdbf6abfab0fc67d7b70", null ],
      [ "outClusterList", "structEmberAfClusterList.html#a9da461ab5d41891b8f8814a0dd7dec0f", null ]
    ] ],
    [ "EmberAfEventContext", "structEmberAfEventContext.html", [
      [ "endpoint", "structEmberAfEventContext.html#adbf496dc8357c062f450fb9cb6ef0d71", null ],
      [ "clusterId", "structEmberAfEventContext.html#a6d81db8534bb8b6ed39e0114a83b6944", null ],
      [ "isClient", "structEmberAfEventContext.html#a507b6cb4d02b7e3434363ec90728cdc7", null ],
      [ "sleepControl", "structEmberAfEventContext.html#ade05744f60cf72402eadb26e347669ca", null ],
      [ "eventControl", "structEmberAfEventContext.html#aab2c652e966066fa6b4c24d98f5b0991", null ]
    ] ],
    [ "EmberAfSceneTableEntry", "structEmberAfSceneTableEntry.html", [
      [ "endpoint", "structEmberAfSceneTableEntry.html#aa5dfc85d8e3b06409efb190534d423b4", null ],
      [ "groupId", "structEmberAfSceneTableEntry.html#a43a0f69e7146f0e1d077dbab845daff0", null ],
      [ "sceneId", "structEmberAfSceneTableEntry.html#a779fa4c632c11fda0ad08aa5f13ef39b", null ],
      [ "transitionTime", "structEmberAfSceneTableEntry.html#afd5a748985acdf2c015cff467ad423d7", null ],
      [ "transitionTime100ms", "structEmberAfSceneTableEntry.html#a1747c921d37507905566db65c79ebcba", null ]
    ] ],
    [ "EmberAfPluginReportingEntry", "structEmberAfPluginReportingEntry.html", [
      [ "direction", "structEmberAfPluginReportingEntry.html#a9cbc49142cc5b7bc891693fcc0d41cc8", null ],
      [ "endpoint", "structEmberAfPluginReportingEntry.html#ab7fe76f41a6b3fd00f81c7717fbd81e4", null ],
      [ "clusterId", "structEmberAfPluginReportingEntry.html#a9cc68fb4d029333ca0d62b8f8e4ed01f", null ],
      [ "attributeId", "structEmberAfPluginReportingEntry.html#a8153f5d2151d06f8dcb28758cf55971f", null ],
      [ "mask", "structEmberAfPluginReportingEntry.html#abb1d0ea7556bccf66c94d87daecd76bd", null ],
      [ "manufacturerCode", "structEmberAfPluginReportingEntry.html#a47706406fea80a6e4fec659ecc951243", null ],
      [ "minInterval", "structEmberAfPluginReportingEntry.html#aad609506985b5ae37a80beb82014e3ac", null ],
      [ "maxInterval", "structEmberAfPluginReportingEntry.html#a3d8b005f92af5e97bba9c3fe3082066c", null ],
      [ "reportableChange", "structEmberAfPluginReportingEntry.html#ab2de2624ac9212f1c28278dab8f52181", null ],
      [ "reported", "structEmberAfPluginReportingEntry.html#a092fcbf97ac7b224f84d582deac1a3e7", null ],
      [ "source", "structEmberAfPluginReportingEntry.html#a64d2155f23b2f8f34fac143f5756501e", null ],
      [ "timeout", "structEmberAfPluginReportingEntry.html#a1d421c320dcc7b5d48b71ee122ccf6ef", null ],
      [ "received", "structEmberAfPluginReportingEntry.html#a60badb5f1873702e9fda89b9a5abb086", null ],
      [ "data", "structEmberAfPluginReportingEntry.html#a1c5c9bacd70f3ac2a05310bfc3caf422", null ]
    ] ],
    [ "EmberAfOtaImageId", "structEmberAfOtaImageId.html", [
      [ "manufacturerId", "structEmberAfOtaImageId.html#a0b589c93d0d0c35f00318b1d9cf994a2", null ],
      [ "imageTypeId", "structEmberAfOtaImageId.html#af65194ebd3d73e47024f43e8cf53fa5f", null ],
      [ "firmwareVersion", "structEmberAfOtaImageId.html#a584d447de2bc630f28541e2cbc592457", null ],
      [ "deviceSpecificFileEui64", "structEmberAfOtaImageId.html#a3b1807c46d423e9edf4f353c4b7a065d", null ]
    ] ],
    [ "EmberAfImageBlockRequestCallbackStruct", "structEmberAfImageBlockRequestCallbackStruct.html", [
      [ "source", "structEmberAfImageBlockRequestCallbackStruct.html#a4ad9897797d09e5013a9d8925a742b56", null ],
      [ "id", "structEmberAfImageBlockRequestCallbackStruct.html#a3e6a5aa94110a0d6be824c45499a70d0", null ],
      [ "offset", "structEmberAfImageBlockRequestCallbackStruct.html#afbd55763850d968bfd800af246110a08", null ],
      [ "waitTimeMinutesResponse", "structEmberAfImageBlockRequestCallbackStruct.html#a44689d1d3522de367f9da1c52ce802c5", null ],
      [ "minBlockRequestPeriod", "structEmberAfImageBlockRequestCallbackStruct.html#ac8699f8c801795990538c9168bce02e7", null ],
      [ "maxDataSize", "structEmberAfImageBlockRequestCallbackStruct.html#a02cbf81fef29eaaf255a9d756fe29995", null ],
      [ "bitmask", "structEmberAfImageBlockRequestCallbackStruct.html#ac6e130dc05e7dec745e688b428e20822", null ]
    ] ],
    [ "EmberAfOtaHeader", "structEmberAfOtaHeader.html", [
      [ "headerVersion", "structEmberAfOtaHeader.html#ae4a21adbec47767ab833e8f635914e2f", null ],
      [ "headerLength", "structEmberAfOtaHeader.html#a7a4693ad9c8dc4daec13461d047dda54", null ],
      [ "fieldControl", "structEmberAfOtaHeader.html#a8457bd3c95b211f99a95a05b757e8989", null ],
      [ "manufacturerId", "structEmberAfOtaHeader.html#a5f0e1be64df780b393706acf87983a05", null ],
      [ "imageTypeId", "structEmberAfOtaHeader.html#af5b0d911db1c18957b0765d713b968cd", null ],
      [ "firmwareVersion", "structEmberAfOtaHeader.html#a3ff5fcbee1b150ece145d40279989001", null ],
      [ "zigbeeStackVersion", "structEmberAfOtaHeader.html#a42013aa32c94ea553286a00b433ac598", null ],
      [ "headerString", "structEmberAfOtaHeader.html#aea03bbe149332288b5cb6c582462b3b4", null ],
      [ "imageSize", "structEmberAfOtaHeader.html#a232c9d0527adb07af2058a77c342a50b", null ],
      [ "securityCredentials", "structEmberAfOtaHeader.html#aac48276076c42d49f5fd2723521d6840", null ],
      [ "upgradeFileDestination", "structEmberAfOtaHeader.html#a0a72bb56bf9f855b1fa147760bdf0890", null ],
      [ "minimumHardwareVersion", "structEmberAfOtaHeader.html#ad22aacdc395a1dbf32330031a943a68b", null ],
      [ "maximumHardwareVersion", "structEmberAfOtaHeader.html#a560760bfc61a2047804ad884da5ccc26", null ]
    ] ],
    [ "EmberAfTagData", "structEmberAfTagData.html", [
      [ "id", "structEmberAfTagData.html#af5f33d15a840e4a1701bb6314991c870", null ],
      [ "length", "structEmberAfTagData.html#aa0c91a4858a1b3b708e2811e395a2388", null ]
    ] ],
    [ "EmberAfLinkKeyBackupData", "structEmberAfLinkKeyBackupData.html", [
      [ "deviceId", "structEmberAfLinkKeyBackupData.html#a15ceb7932ea55a189e403652c37e8f56", null ],
      [ "key", "structEmberAfLinkKeyBackupData.html#a0992d21f85d685ca76269e976d6e3668", null ]
    ] ],
    [ "EmberAfTrustCenterBackupData", "structEmberAfTrustCenterBackupData.html", [
      [ "extendedPanId", "structEmberAfTrustCenterBackupData.html#aa0a568b721436b793ab9551382620b5c", null ],
      [ "keyListLength", "structEmberAfTrustCenterBackupData.html#abdf6f3f4b8e35230525fd2a78976848f", null ],
      [ "maxKeyListLength", "structEmberAfTrustCenterBackupData.html#adada36fb478087252f2ad733ee3e2166", null ],
      [ "keyList", "structEmberAfTrustCenterBackupData.html#a5ad8842f0295b7c6b9e55045928b3f60", null ]
    ] ],
    [ "EmberAfStandaloneBootloaderQueryResponseData", "structEmberAfStandaloneBootloaderQueryResponseData.html", [
      [ "hardwareTag", "structEmberAfStandaloneBootloaderQueryResponseData.html#aca893685dcd24809d5e889de2bfc3836", null ],
      [ "eui64", "structEmberAfStandaloneBootloaderQueryResponseData.html#a436f1d1b35b92710adc5a5d5198fb9f1", null ],
      [ "mfgId", "structEmberAfStandaloneBootloaderQueryResponseData.html#a0afb5b8e489c6a48d148216ec62ecc45", null ],
      [ "bootloaderVersion", "structEmberAfStandaloneBootloaderQueryResponseData.html#a702e7b3994174a15f3e0650cd18091d8", null ],
      [ "capabilities", "structEmberAfStandaloneBootloaderQueryResponseData.html#aec1e34cd04e93acb8069553f035874a7", null ],
      [ "platform", "structEmberAfStandaloneBootloaderQueryResponseData.html#a17537ae244275aad3e84c6823759b1a9", null ],
      [ "micro", "structEmberAfStandaloneBootloaderQueryResponseData.html#ad26d59783787869c452b6fb1a28a4f84", null ],
      [ "phy", "structEmberAfStandaloneBootloaderQueryResponseData.html#a693942dd621f9be72c263da270be346f", null ],
      [ "bootloaderActive", "structEmberAfStandaloneBootloaderQueryResponseData.html#afca1d74a79400136639cc12e2298508e", null ]
    ] ],
    [ "EmberAfCommandMetadata", "structEmberAfCommandMetadata.html", [
      [ "clusterId", "structEmberAfCommandMetadata.html#aca05ff2d50caea54110c2359d40d2a5f", null ],
      [ "commandId", "structEmberAfCommandMetadata.html#a78ccd45da141142cb8d91b50ca93a29f", null ],
      [ "mask", "structEmberAfCommandMetadata.html#adf9583e834455bc026db41da0efc0d42", null ]
    ] ],
    [ "EmberAfTimeStruct", "structEmberAfTimeStruct.html", [
      [ "year", "structEmberAfTimeStruct.html#a1b656e4344c184bb578983f644a97071", null ],
      [ "month", "structEmberAfTimeStruct.html#ad20505a68892ef909487d08d89a21689", null ],
      [ "day", "structEmberAfTimeStruct.html#af9affc2118ca92b0ced8c1aa0ab0f808", null ],
      [ "hours", "structEmberAfTimeStruct.html#a985c8b61df66f4a00b5a6c0cfe6e7db0", null ],
      [ "minutes", "structEmberAfTimeStruct.html#af28bc1920699913674b791af6b53b9b7", null ],
      [ "seconds", "structEmberAfTimeStruct.html#a2a0582cd2656f7974b3c1592f67f081d", null ]
    ] ],
    [ "EMBER_AF_NULL_MANUFACTURER_CODE", "group__aftypes.html#gae7e0aedd82318bddfb88b5a5605424fd", null ],
    [ "EMBER_AF_INTER_PAN_UNICAST", "group__aftypes.html#gad129580313b48aff1c42af5930291d4b", null ],
    [ "EMBER_AF_INTER_PAN_BROADCAST", "group__aftypes.html#ga88c728a7536082a06276766a99dbed7b", null ],
    [ "EMBER_AF_INTER_PAN_MULTICAST", "group__aftypes.html#ga7589fbd30efbea8ea5f6d81c873e2436", null ],
    [ "INTER_PAN_UNICAST", "group__aftypes.html#ga863e7f1949933404c860e8aef7723dc2", null ],
    [ "INTER_PAN_BROADCAST", "group__aftypes.html#gaa80b00a03b8376f5325d712b20a2e27a", null ],
    [ "INTER_PAN_MULTICAST", "group__aftypes.html#gaba09538bc0e840dfb88a209cf1d4ce46", null ],
    [ "EMBER_AF_INTERPAN_OPTION_NONE", "group__aftypes.html#ga3bf1c85f753fb2e23c1f6aece563725a", null ],
    [ "EMBER_AF_INTERPAN_OPTION_APS_ENCRYPT", "group__aftypes.html#ga82e77b19662476ebfb4ee4397e05b681", null ],
    [ "EMBER_AF_INTERPAN_OPTION_MAC_HAS_LONG_ADDRESS", "group__aftypes.html#ga96581c87586cc7400e48d04eb0ffb050", null ],
    [ "InterPanHeader", "group__aftypes.html#ga73d46102e0b7a09a77fa205aabfdf6f3", null ],
    [ "EMBER_AF_INTERPAN_DIRECTION_CLIENT_TO_SERVER", "group__aftypes.html#gabb4fbc571bc094837f94bfd5e7e0da98", null ],
    [ "EMBER_AF_INTERPAN_DIRECTION_SERVER_TO_CLIENT", "group__aftypes.html#gad8737ba17b3254e674803371ac2bd164", null ],
    [ "EMBER_AF_INTERPAN_DIRECTION_BOTH", "group__aftypes.html#gaddaf50c9023b36eb5c67ec4ec649a333", null ],
    [ "EMBER_AF_INTERPAN_GLOBAL_COMMAND", "group__aftypes.html#gafa02d24469db78572cf5d8634394dd69", null ],
    [ "EMBER_AF_INTERPAN_MANUFACTURER_SPECIFIC", "group__aftypes.html#gab2502458ae0d9c65c479ecc0293b68f8", null ],
    [ "EMBER_AF_ALLOW_TC_KEY_REQUESTS", "group__aftypes.html#gad3eeb85d9eb91f4d9d433f294775c5a3", null ],
    [ "EMBER_AF_DENY_TC_KEY_REQUESTS", "group__aftypes.html#gaa456be6bf2f82eab23cd8da07b3ca82c", null ],
    [ "EMBER_AF_ALLOW_APP_KEY_REQUESTS", "group__aftypes.html#ga4bdb9639b471d581d4b6aa86465990e1", null ],
    [ "EMBER_AF_DENY_APP_KEY_REQUESTS", "group__aftypes.html#gac0faccf78774dde68e32e953f2857802", null ],
    [ "EM_AF_DISCOVERY_RESPONSE_MASK", "group__aftypes.html#gae1e2f2e6ab29ba98b20917cb639371ff", null ],
    [ "emberAfHaveDiscoveryResponseStatus", "group__aftypes.html#ga8589ba62445dcc818c48606a3c30bc5a", null ],
    [ "EMBER_AF_SCENE_TABLE_NULL_INDEX", "group__aftypes.html#ga4a46af5a20dba738f36bfbb9307b890f", null ],
    [ "EMBER_AF_SCENE_TABLE_UNUSED_ENDPOINT_ID", "group__aftypes.html#gac0a244d1f87bfe87029ec0f83483e4a9", null ],
    [ "ZCL_SCENES_CLUSTER_MAXIMUM_NAME_LENGTH", "group__aftypes.html#ga2efc9b67c775f7950700a54f1e3e56de", null ],
    [ "ZCL_SCENES_GLOBAL_SCENE_GROUP_ID", "group__aftypes.html#ga8142555d83f9382e9c8deb7bfa34ca11", null ],
    [ "ZCL_SCENES_GLOBAL_SCENE_SCENE_ID", "group__aftypes.html#gaae57c495f4cba1e0a976f6ead8a4b330", null ],
    [ "ZCL_PRICE_CLUSTER_MAXIMUM_RATE_LABEL_LENGTH", "group__aftypes.html#ga2fc851e300b5ace20f9084c7ebda7884", null ],
    [ "EMBER_AF_PLUGIN_REPORTING_UNUSED_ENDPOINT_ID", "group__aftypes.html#gae564ec425c409b42ab5997cfffed07aa", null ],
    [ "EMBER_AF_OTA_MAX_HEADER_STRING_LENGTH", "group__aftypes.html#ga84baa92e64bb735a7006a8aaa50553f3", null ],
    [ "APP_NOTIFY_ERROR_CODE_START", "group__aftypes.html#ga6e18130092834d022dd564afd5975d54", null ],
    [ "APP_NOTIFY_MESSAGE_TEXT", "group__aftypes.html#ga910b66e9c001b53a8ef5c582542da025", null ],
    [ "EMBER_AF_STANDALONE_BOOTLOADER_HARDWARE_TAG_LENGTH", "group__aftypes.html#gae4657429470de9a97c5fe10b6e48d74c", null ],
    [ "EMBER_AF_PLUGIN_SIMPLE_METERING_SERVER_ELECTRIC_METER", "group__aftypes.html#gaf7efaa396468c46bc2b99132d7f77bae", null ],
    [ "EMBER_AF_PLUGIN_SIMPLE_METERING_SERVER_GAS_METER", "group__aftypes.html#gae66d1eaaf71ef384b4af2faeb72d84a7", null ],
    [ "EmberAfProfileId", "group__aftypes.html#ga7a5905af9ace80ae0e28f25f56c3d2b0", null ],
    [ "EmberAfAttributeId", "group__aftypes.html#ga70de7d5081a801871be8698e1b233df3", null ],
    [ "EmberAfClusterId", "group__aftypes.html#ga9261792b876f7cb9b463f66102a029d1", null ],
    [ "EmberAfAttributeType", "group__aftypes.html#ga36762d5abfe0be8985c4d3ca21a6595d", null ],
    [ "EmberAfClusterMask", "group__aftypes.html#ga8af9a6ed0d1835d358a1612deecb7f4d", null ],
    [ "EmberAfAttributeMask", "group__aftypes.html#gab316b300ebf74d33f7718091ec4b0df0", null ],
    [ "EmberAfGenericClusterFunction", "group__aftypes.html#ga57bab44354eeafd5ddaba16e7bd3bb88", null ],
    [ "EmberAfInterpanMessageType", "group__aftypes.html#ga001f4da4540bdb7fe1cb8066205ec169", null ],
    [ "EmberAfInterpanOptions", "group__aftypes.html#ga46027dec868339ce21074ca710b92914", null ],
    [ "EmberAfAllowedInterpanOptions", "group__aftypes.html#ga3f91749fecf19b9433ff841bd7c7db62", null ],
    [ "EmberAfLinkKeyRequestPolicy", "group__aftypes.html#ga151e6ea7a93a97999a2c12cc004733da", null ],
    [ "EmberAfPluginEsiManagementBitmask", "group__aftypes.html#ga88ec9031d45b61bc88cacc3dd4fd25ac", null ],
    [ "EmberAfServiceDiscoveryCallback", "group__aftypes.html#ga0f3b8e4d2649750b46e9e5f8d490f92c", null ],
    [ "EmberAfPartnerLinkKeyExchangeCallback", "group__aftypes.html#gabfbd269176d13643fb526fc7e52919dd", null ],
    [ "EmberAfNetworkEventHandler", "group__aftypes.html#ga06a54b84c1df955d6f0c3c40c1518769", null ],
    [ "EmberAfEndpointEventHandler", "group__aftypes.html#ga3d57ab38806b4a1a65efb7b010f061a4", null ],
    [ "EmberAfImageBlockRequestOptions", "group__aftypes.html#ga7af69b8194676ef1b83b789e449de694", null ],
    [ "EmberAfTickFunction", "group__aftypes.html#gafe6a7c81a35e5deee28c041e1e6e63fe", null ],
    [ "EmberAfInitFunction", "group__aftypes.html#gada88eb26ffd5e90833c5012318f39d10", null ],
    [ "EmberAfClusterAttributeChangedCallback", "group__aftypes.html#ga8565fcf50ecdbb20e2e22f6910a504da", null ],
    [ "EmberAfManufacturerSpecificClusterAttributeChangedCallback", "group__aftypes.html#ga412b4bd61bf90562d1baf2c089212af1", null ],
    [ "EmberAfClusterPreAttributeChangedCallback", "group__aftypes.html#gaa6bd0477754a1da63b81d03ed0b9a7ed", null ],
    [ "EmberAfDefaultResponseFunction", "group__aftypes.html#ga47a9020a80b0afec4c357db484ddfa4c", null ],
    [ "EmberAfMessageSentFunction", "group__aftypes.html#ga9c117adec4937177551110bfcd1d7fbc", null ],
    [ "EmberAfSecurityProfile", "group__aftypes.html#ga6d10af177853bcafe2967c46061f6fce", [
      [ "EMBER_AF_SECURITY_PROFILE_NONE", "group__aftypes.html#gga6d10af177853bcafe2967c46061f6fcea531d9bfc74ae397c8ba6e7190ec6511b", null ],
      [ "EMBER_AF_SECURITY_PROFILE_HA", "group__aftypes.html#gga6d10af177853bcafe2967c46061f6fceafa7ce49887d6480fe5cd3fcde676b2aa", null ],
      [ "EMBER_AF_SECURITY_PROFILE_HA12", "group__aftypes.html#gga6d10af177853bcafe2967c46061f6fceae574a84519f022e23802552a858b82d1", null ],
      [ "EMBER_AF_SECURITY_PROFILE_SE_TEST", "group__aftypes.html#gga6d10af177853bcafe2967c46061f6fcea482105838f24b5035843de99acb0d63c", null ],
      [ "EMBER_AF_SECURITY_PROFILE_SE_FULL", "group__aftypes.html#gga6d10af177853bcafe2967c46061f6fcea8e02ffd90671e9b7243d15f01c9f463c", null ],
      [ "EMBER_AF_SECURITY_PROFILE_CUSTOM", "group__aftypes.html#gga6d10af177853bcafe2967c46061f6fceadcdae4f561246ffd2a3521efb8073a87", null ]
    ] ],
    [ "EmberAfServiceDiscoveryStatus", "group__aftypes.html#gae1cf02d11a8370f774344e8ab20f5373", [
      [ "EMBER_AF_BROADCAST_SERVICE_DISCOVERY_COMPLETE", "group__aftypes.html#ggae1cf02d11a8370f774344e8ab20f5373a85a31d5cc5ff1e75c79a39a5c190cc89", null ],
      [ "EMBER_AF_BROADCAST_SERVICE_DISCOVERY_RESPONSE_RECEIVED", "group__aftypes.html#ggae1cf02d11a8370f774344e8ab20f5373a859edd91f23001ef4c3edaaf1145ca38", null ],
      [ "EMBER_AF_UNICAST_SERVICE_DISCOVERY_TIMEOUT", "group__aftypes.html#ggae1cf02d11a8370f774344e8ab20f5373a2abe5f6bbe891ece1430632de3eca874", null ],
      [ "EMBER_AF_UNICAST_SERVICE_DISCOVERY_COMPLETE_WITH_RESPONSE", "group__aftypes.html#ggae1cf02d11a8370f774344e8ab20f5373a9520ce36bb93370e81e1a9b86d7bab12", null ]
    ] ],
    [ "EmberAfEventSleepControl", "group__aftypes.html#ga4a612352f4b32cb9713cc3150a04c776", [
      [ "EMBER_AF_OK_TO_HIBERNATE", "group__aftypes.html#gga4a612352f4b32cb9713cc3150a04c776a5465a34aabe179b50aa485bcc684154d", null ],
      [ "EMBER_AF_OK_TO_NAP", "group__aftypes.html#gga4a612352f4b32cb9713cc3150a04c776ad6b9c5ecd21100855464d3c1c6292964", null ],
      [ "EMBER_AF_STAY_AWAKE", "group__aftypes.html#gga4a612352f4b32cb9713cc3150a04c776ac73f7edf40ae73902c3cf28937e4abfe", null ]
    ] ],
    [ "EmberAfApplicationTask", "group__aftypes.html#ga5fe6fcbee8bfbc13bd0b484bd36c2a08", [
      [ "EMBER_AF_WAITING_FOR_DATA_ACK", "group__aftypes.html#gga5fe6fcbee8bfbc13bd0b484bd36c2a08a35b0e8e921783eb13ef2a0b822678c21", null ],
      [ "EMBER_AF_LAST_POLL_GOT_DATA", "group__aftypes.html#gga5fe6fcbee8bfbc13bd0b484bd36c2a08a6b4d120935bc520811361f1b323bf44b", null ],
      [ "EMBER_AF_WAITING_FOR_SERVICE_DISCOVERY", "group__aftypes.html#gga5fe6fcbee8bfbc13bd0b484bd36c2a08af423244c23037d63c19ae5258e12e1b8", null ],
      [ "EMBER_AF_WAITING_FOR_ZDO_RESPONSE", "group__aftypes.html#gga5fe6fcbee8bfbc13bd0b484bd36c2a08abaf0ca7dbc32027e09942af0360b788b", null ],
      [ "EMBER_AF_WAITING_FOR_ZCL_RESPONSE", "group__aftypes.html#gga5fe6fcbee8bfbc13bd0b484bd36c2a08aab86c8ac338ff9452831c245566a646d", null ],
      [ "EMBER_AF_WAITING_FOR_REGISTRATION", "group__aftypes.html#gga5fe6fcbee8bfbc13bd0b484bd36c2a08a2a2856639b83e0344346dd8992bdd2eb", null ],
      [ "EMBER_AF_WAITING_FOR_PARTNER_LINK_KEY_EXCHANGE", "group__aftypes.html#gga5fe6fcbee8bfbc13bd0b484bd36c2a08af2c9f06e367b017a17011113da3f3029", null ],
      [ "EMBER_AF_IMAGE_BLOCK_REQUEST_OPTIONS_NONE", "group__aftypes.html#gga06fc87d81c62e9abb8790b6e5713c55ba2fc450daa5a5739692829cb4e82c6dda", null ],
      [ "EMBER_AF_IMAGE_BLOCK_REQUEST_MIN_BLOCK_REQUEST_SUPPORTED_BY_CLIENT", "group__aftypes.html#gga06fc87d81c62e9abb8790b6e5713c55ba974f9d921fa00b169795d15d0b80829d", null ],
      [ "EMBER_AF_IMAGE_BLOCK_REQUEST_MIN_BLOCK_REQUEST_SUPPORTED_BY_SERVER", "group__aftypes.html#gga06fc87d81c62e9abb8790b6e5713c55ba46c920a3ab96a83fd47200dbe2190387", null ]
    ] ],
    [ "EmberAfOtaStorageStatus", "group__aftypes.html#ga223fed51289c09452ae48913fa68d2af", [
      [ "EMBER_AF_OTA_STORAGE_SUCCESS", "group__aftypes.html#gga223fed51289c09452ae48913fa68d2afaf7cc6b8ad33e52a95cf36fb28e05ee62", null ],
      [ "EMBER_AF_OTA_STORAGE_ERROR", "group__aftypes.html#gga223fed51289c09452ae48913fa68d2afa6f66c2c00ef600be6f0363a7c6addd3e", null ],
      [ "EMBER_AF_OTA_STORAGE_RETURN_DATA_TOO_LONG", "group__aftypes.html#gga223fed51289c09452ae48913fa68d2afab91997e2914649f504ef70071f0b7961", null ],
      [ "EMBER_AF_OTA_STORAGE_PARTIAL_FILE_FOUND", "group__aftypes.html#gga223fed51289c09452ae48913fa68d2afa9130c577c7897880074daff8a1fbd447", null ],
      [ "EMBER_AF_OTA_STORAGE_OPERATION_IN_PROGRESS", "group__aftypes.html#gga223fed51289c09452ae48913fa68d2afadcac9fb0076be142c9fbd3adfdf76c46", null ]
    ] ],
    [ "EmberAfKeyEstablishmentNotifyMessage", "group__aftypes.html#ga908101ba54b5ff8c36bc1129995e8d0d", [
      [ "NO_APP_MESSAGE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da2da0430b20c66b4abd4c19ebebe07cb2", null ],
      [ "RECEIVED_PARTNER_CERTIFICATE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0dab5d0f0a3a4fd4c8d034537eba9b7e903", null ],
      [ "GENERATING_EPHEMERAL_KEYS", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0daecb61943b14a90e64a6d9ffd25868ab2", null ],
      [ "GENERATING_SHARED_SECRET", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da9df6303edfb427bd0c84f2d423543c44", null ],
      [ "KEY_GENERATION_DONE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0daff4f3e020a7d5dfa5492cff250328f5a", null ],
      [ "GENERATE_SHARED_SECRET_DONE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da5fa2050a0d3f5efb6892c4959aaa7f15", null ],
      [ "LINK_KEY_ESTABLISHED", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0dabf5ca4b46527a9a129a119abd2e481f0", null ],
      [ "NO_LOCAL_RESOURCES", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0dabb89b555d6c28d45beccfe66c5176f43", null ],
      [ "PARTNER_NO_RESOURCES", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0dac31f0ae93cb7a8c733680883d380941b", null ],
      [ "TIMEOUT_OCCURRED", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0daff7fc85a553fcf379a7069bcda4cdd6c", null ],
      [ "INVALID_APP_COMMAND", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0dafea3e71fedff8d523880701d1317770a", null ],
      [ "MESSAGE_SEND_FAILURE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da5fc974490551fce3ec16b6eed700d714", null ],
      [ "PARTNER_SENT_TERMINATE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0daac2f45cb61702789c493d777bfd0f82d", null ],
      [ "INVALID_PARTNER_MESSAGE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da7255e43497032484440c64076d96fcb6", null ],
      [ "PARTNER_SENT_DEFAULT_RESPONSE_ERROR", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0dafe3636ac29cbae732a0a2cc80f5d023b", null ],
      [ "BAD_CERTIFICATE_ISSUER", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0daca8e1afa447ae9fa0b20c1b9a7bce55b", null ],
      [ "KEY_CONFIRM_FAILURE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da906d128af4e92ef5783f346e835160c6", null ],
      [ "BAD_KEY_ESTABLISHMENT_SUITE", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0daa0cbf115f0c5e4d0b03e7a31db71b215", null ],
      [ "KEY_TABLE_FULL", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da1b08b54c1e737016168a104b46b3ded3", null ],
      [ "NO_ESTABLISHMENT_ALLOWED", "group__aftypes.html#gga908101ba54b5ff8c36bc1129995e8d0da3bb2dbc4ce03555cea440a37b41b90c7", null ]
    ] ],
    [ "EmberAfImageVerifyStatus", "group__aftypes.html#ga1507d91e2449d9042f5b94e044914fc2", [
      [ "EMBER_AF_IMAGE_GOOD", "group__aftypes.html#gga1507d91e2449d9042f5b94e044914fc2a9214bd7f198a6d4da1d025cff3c8352a", null ],
      [ "EMBER_AF_IMAGE_BAD", "group__aftypes.html#gga1507d91e2449d9042f5b94e044914fc2abd4881935b8d17b8ee8eab4738bf3f98", null ],
      [ "EMBER_AF_IMAGE_VERIFY_IN_PROGRESS", "group__aftypes.html#gga1507d91e2449d9042f5b94e044914fc2ae879a9264aa3a04ef42fb2dfd2589a91", null ]
    ] ]
];