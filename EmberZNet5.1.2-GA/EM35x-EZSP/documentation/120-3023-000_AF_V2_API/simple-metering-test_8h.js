var simple_metering_test_8h =
[
    [ "PROFILE_INTERVAL_PERIOD_TIMEFRAME", "simple-metering-test_8h.html#a005930e8c20c150106d5e8b3260e84a8", null ],
    [ "PROFILE_INTERVAL_PERIOD_IN_MINUTES", "simple-metering-test_8h.html#a4218aef9c11c8bae13a6a5daa5d51caa", null ],
    [ "PROFILE_INTERVAL_PERIOD_IN_SECONDS", "simple-metering-test_8h.html#abf108ec20a1fdbd95dbd565b3f1c6f4a", null ],
    [ "PROFILE_INTERVAL_PERIOD_IN_MILLISECONDS", "simple-metering-test_8h.html#afe496eac3345880ecd003fed0e439b1e", null ],
    [ "MAX_PROFILE_INDEX", "simple-metering-test_8h.html#a090a7948a6bd15982d20ad3f137ac86d", null ],
    [ "TOTAL_PROFILE_TIME_SPAN_IN_SECONDS", "simple-metering-test_8h.html#a9efd7978be35e4ac425f5e732d7d123b", null ],
    [ "emAfTestMeterTick", "simple-metering-test_8h.html#ab220eebca5bc425f807e5beadd7fcf5b", null ],
    [ "emAfTestMeterInit", "simple-metering-test_8h.html#a2fc2a05e448bafe9d258b52ade3612d3", null ],
    [ "afTestMeterPrint", "simple-metering-test_8h.html#a715cd70ce343a6d9ba966ee014041d15", null ],
    [ "afTestMeterSetConsumptionRate", "simple-metering-test_8h.html#a6e55366d55d38643c307524b428ebdba", null ],
    [ "afTestMeterSetConsumptionVariance", "simple-metering-test_8h.html#a5cf91a564caef86c9664e71fc534d994", null ],
    [ "afTestMeterAdjust", "simple-metering-test_8h.html#a569b91d711ab5d75727a77ad2381a876", null ],
    [ "afTestMeterMode", "simple-metering-test_8h.html#a5a4c2d03e1fd603a6ece8651988fc1d9", null ],
    [ "afTestMeterSetError", "simple-metering-test_8h.html#af90ac72257a214def960b9cb104f8572", null ],
    [ "afTestMeterRandomError", "simple-metering-test_8h.html#a7193ba9c9815936f870ce9897ec9a62b", null ],
    [ "afTestMeterEnableProfiles", "simple-metering-test_8h.html#a3a216e86da936f8f37eb8b0bbee27204", null ],
    [ "emAfTestMeterGetProfiles", "simple-metering-test_8h.html#a82e4558dabe17d4ee5318655518b29b0", null ]
];