var structEmAfOtaStorageLinuxConfig =
[
    [ "memoryDebug", "structEmAfOtaStorageLinuxConfig.html#a92b99c565da87cee3a30fafdc37f274d", null ],
    [ "fileDebug", "structEmAfOtaStorageLinuxConfig.html#a00010429bdd135d37cb55adb1a41dec9", null ],
    [ "fieldDebug", "structEmAfOtaStorageLinuxConfig.html#a9dea8fca9a0f5f32d1b73ab08a2e5cd7", null ],
    [ "ignoreFilesWithUnderscorePrefix", "structEmAfOtaStorageLinuxConfig.html#a51f55cf3acdc317b22d4ba7ea5bd50a2", null ],
    [ "printFileDiscoveryOrRemoval", "structEmAfOtaStorageLinuxConfig.html#a408dc7b57868900804485bf0b1240336", null ],
    [ "fileAddedHandler", "structEmAfOtaStorageLinuxConfig.html#ad4c9826d4217a9f836d567a7a8b3c45a", null ]
];