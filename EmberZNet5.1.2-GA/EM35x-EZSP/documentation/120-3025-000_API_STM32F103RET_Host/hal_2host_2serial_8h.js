var hal_2host_2serial_8h =
[
    [ "SerialBaudRate", "hal_2host_2serial_8h.htm#ga79e9d2305515318a1ae0ab5aaffd6fcb", [
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "hal_2host_2serial_8h.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ]
    ] ],
    [ "NameOfType", "hal_2host_2serial_8h.htm#ga2ef5202380ba5c66c597f205203ca233", [
      [ "DEFINE_PARITY", "hal_2host_2serial_8h.htm#gga2ef5202380ba5c66c597f205203ca233a9569a17323e2dd2a5bb1db6a96621815", null ],
      [ "DEFINE_PARITY", "hal_2host_2serial_8h.htm#gga2ef5202380ba5c66c597f205203ca233a9569a17323e2dd2a5bb1db6a96621815", null ],
      [ "DEFINE_PARITY", "hal_2host_2serial_8h.htm#gga2ef5202380ba5c66c597f205203ca233a9569a17323e2dd2a5bb1db6a96621815", null ]
    ] ],
    [ "halInternalUartInit", "hal_2host_2serial_8h.htm#ga34c407551884ddac42d8db59b39f4ba8", null ],
    [ "halInternalPrintfWriteAvailable", "hal_2host_2serial_8h.htm#ga58eb9d5e19caf7941d487aa8d9198f71", null ],
    [ "halInternalPrintfReadAvailable", "hal_2host_2serial_8h.htm#gab633dfe51e997336491816336822dbe7", null ],
    [ "halInternalForcePrintf", "hal_2host_2serial_8h.htm#ga5544ff3159abd290d0f534074c09cc79", null ]
];