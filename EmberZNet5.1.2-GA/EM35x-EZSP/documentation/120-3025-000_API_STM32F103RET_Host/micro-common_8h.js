var micro_common_8h =
[
    [ "MICRO_DISABLE_WATCH_DOG_KEY", "micro-common_8h.htm#gabb42f7171eba6959a54c6e6d9aeca64e", null ],
    [ "SleepModes", "micro-common_8h.htm#gace58749df14c14b64252eb55f40d2c32", [
      [ "SLEEPMODE_RUNNING", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a4b1e70cf7dd0396d75a5ef3bc357694a", null ],
      [ "SLEEPMODE_IDLE", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a8dec81d54908044ef56016aee3b1b506", null ],
      [ "SLEEPMODE_WAKETIMER", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a93af1c45a33be62df00d0ab82ef04128", null ],
      [ "SLEEPMODE_MAINTAINTIMER", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a4c95cce8a2fe32d302ce3a42a74c58d1", null ],
      [ "SLEEPMODE_NOTIMER", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a559adb5abaebc7504743f8684ab16f28", null ],
      [ "SLEEPMODE_RESERVED", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a8d06b6cc298a4a34eaca616b86f800d5", null ],
      [ "SLEEPMODE_POWERDOWN", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a0f74b29aa0a12fbc2c31db42392101d7", null ],
      [ "SLEEPMODE_POWERSAVE", "micro-common_8h.htm#ggace58749df14c14b64252eb55f40d2c32a34e3924778494e4fd21702fb4fbed0b2", null ]
    ] ],
    [ "halInit", "micro-common_8h.htm#gafd89c1650df524d95aef39b8bc38170d", null ],
    [ "halReboot", "micro-common_8h.htm#ga3550a689dc90ddd9d7d973bb154dd909", null ],
    [ "halPowerUp", "micro-common_8h.htm#ga467bf8ac5d5964ca282f332f4e394654", null ],
    [ "halPowerDown", "micro-common_8h.htm#gae13140ae48ea28772b67717f0d28f5e9", null ],
    [ "halInternalEnableWatchDog", "micro-common_8h.htm#ga7ea499662dd11955f9f3cc340e2455b8", null ],
    [ "halInternalDisableWatchDog", "micro-common_8h.htm#ga81c4a9744062969d68ab3a3ce56286c5", null ],
    [ "halCommonDelayMicroseconds", "micro-common_8h.htm#ga81df7d5e74c518f1cee0c40ce8c2a199", null ],
    [ "halCommonDelayMilliseconds", "micro-common_8h.htm#ga3b180f12872a4b39217327000947fe8d", null ],
    [ "halGetResetInfo", "micro-common_8h.htm#ga3d08021495e9082c02fa113e624c8cb2", null ],
    [ "halGetResetString", "micro-common_8h.htm#ga7e5131c65da9fcf7ab185503f4137799", null ],
    [ "halStackSeedRandom", "micro-common_8h.htm#gaad3b617d26789fb6c4789847a007eaad", null ],
    [ "halCommonGetRandom", "micro-common_8h.htm#ga92d978a2b55bcb6d8ffe996e9a768f57", null ],
    [ "halSleep", "micro-common_8h.htm#ga6d05736655f1a012dc969d1d912e835b", null ]
];