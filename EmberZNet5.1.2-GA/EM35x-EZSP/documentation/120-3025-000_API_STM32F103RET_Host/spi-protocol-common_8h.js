var spi_protocol_common_8h =
[
    [ "halNcpSerialInit", "spi-protocol-common_8h.htm#gaa808460cb21ae92e4690c62c9eade081", null ],
    [ "halNcpSerialPowerup", "spi-protocol-common_8h.htm#gaf43c537a3b902940d5a853ce6fe81823", null ],
    [ "halNcpSerialPowerdown", "spi-protocol-common_8h.htm#ga40c5d27b0cef2f179d8c769e7cffb003", null ],
    [ "halNcpHardReset", "spi-protocol-common_8h.htm#gac8d937e569fe0d66cd4295b077275f31", null ],
    [ "halNcpHardResetReqBootload", "spi-protocol-common_8h.htm#ga2a2c6ff66e3225b81e0f343e3d75b2bf", null ],
    [ "halNcpWakeUp", "spi-protocol-common_8h.htm#gaee2f8e01d6923b36c88f3becca472416", null ],
    [ "halNcpSendCommand", "spi-protocol-common_8h.htm#gaf0eef394d0b779b83ac363ad6c39afde", null ],
    [ "halNcpSendRawCommand", "spi-protocol-common_8h.htm#ga2baabc4b033caca5f2b98a8a7d97a988", null ],
    [ "halNcpPollForResponse", "spi-protocol-common_8h.htm#ga3ed78ade1b7abbd5edfc0764eaad54f5", null ],
    [ "halNcpIsAwakeIsr", "spi-protocol-common_8h.htm#ga31fdb1f9c0a811e3347f19f087a44ced", null ],
    [ "halNcpHasData", "spi-protocol-common_8h.htm#ga1e97b66871b268621b5cdebb9162120f", null ],
    [ "halNcpVerifySpiProtocolVersion", "spi-protocol-common_8h.htm#ga2fe6d531fb543858e198dd264f359b9b", null ],
    [ "halNcpVerifySpiProtocolActive", "spi-protocol-common_8h.htm#gae145e6dded848cc1b5155e4c2ad2a4ad", null ],
    [ "halNcpFrame", "spi-protocol-common_8h.htm#ga5cc00e5731b37abb536206ed0fae28ba", null ],
    [ "halNcpSpipErrorByte", "spi-protocol-common_8h.htm#gab1f5cc26aa0d263b86e4b4d84ba03df9", null ]
];