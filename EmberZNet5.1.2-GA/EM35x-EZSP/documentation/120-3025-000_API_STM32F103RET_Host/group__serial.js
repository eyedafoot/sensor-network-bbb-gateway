var group__serial =
[
    [ "STM32F103RET Specific UART", "group__stm32f103ret__uart.htm", "group__stm32f103ret__uart" ],
    [ "SerialBaudRate", "group__serial.htm#ga79e9d2305515318a1ae0ab5aaffd6fcb", [
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ],
      [ "DEFINE_BAUD", "group__serial.htm#gga79e9d2305515318a1ae0ab5aaffd6fcbaddd7d1b8d46d2e29d36ba073b1ea8dcd", null ]
    ] ],
    [ "NameOfType", "group__serial.htm#ga2ef5202380ba5c66c597f205203ca233", [
      [ "DEFINE_PARITY", "group__serial.htm#gga2ef5202380ba5c66c597f205203ca233a9569a17323e2dd2a5bb1db6a96621815", null ],
      [ "DEFINE_PARITY", "group__serial.htm#gga2ef5202380ba5c66c597f205203ca233a9569a17323e2dd2a5bb1db6a96621815", null ],
      [ "DEFINE_PARITY", "group__serial.htm#gga2ef5202380ba5c66c597f205203ca233a9569a17323e2dd2a5bb1db6a96621815", null ]
    ] ],
    [ "halInternalUartInit", "group__serial.htm#ga34c407551884ddac42d8db59b39f4ba8", null ],
    [ "halInternalPrintfWriteAvailable", "group__serial.htm#ga58eb9d5e19caf7941d487aa8d9198f71", null ],
    [ "halInternalPrintfReadAvailable", "group__serial.htm#gab633dfe51e997336491816336822dbe7", null ],
    [ "halInternalForcePrintf", "group__serial.htm#ga5544ff3159abd290d0f534074c09cc79", null ]
];