var adc_8h =
[
    [ "TEMP_SENSOR_PIN", "adc_8h.htm#gac99e9d8b94740fda8d12772cd73749ff", null ],
    [ "TEMP_SENSOR_PORT", "adc_8h.htm#ga92383553cebf5ecc49aeea69715f7e57", null ],
    [ "TEMP_SENSOR_ADC", "adc_8h.htm#ga05b69c2a1617c1e5c43ec360efdf89e9", null ],
    [ "TEMP_SENSOR_ADC_CHAN", "adc_8h.htm#gaaeb6c714e918414761bca3d453fc5341", null ],
    [ "TEMP_ENABLE_PIN", "adc_8h.htm#ga87db42463b5136434a9f852c996c62a1", null ],
    [ "TEMP_ENABLE_PORT", "adc_8h.htm#ga5c193563c1853cb860482f40f00e43a9", null ],
    [ "halInternalInitAdc", "adc_8h.htm#ga992c8aa018221a70ab1a525910509035", null ],
    [ "halSampleAdc", "adc_8h.htm#ga5bafc57a631c06a9f6dbf37c06345c98", null ],
    [ "halConvertValueToVolts", "adc_8h.htm#ga4ae24ef736a337ff1586cfdefadaffd7", null ]
];