/*
 * File: haldrvRelay.c
 * Description: HAL functions for BV4627-X relay 
 * Author: Erik Hatfield
*/
 
#include "app/framework/include/af.h"
#include <hal/hal.h>
#include "hal/ece/haldrvI2C.h"
#include "hal/ece/haldrvRelay.h"

// Change the specified relay to the given state after a delay value
void setRelay(multRelay relay, boolean turnOn, int16u delayValue)
{
  // If delay is zero, set it to one (minimum)
  delayValue = (delayValue==0)? 1 : delayValue;
  int8u bytes[] = {turnOn, HIGH_BYTE(delayValue), LOW_BYTE(delayValue)};
  halI2CWrite(0x64, relay+9, bytes, 3);
} // End of setRelays


void disableAllRelays(void)
{
  halI2CWrite(0x64, 18, NULL, 0);
} // End of disableAllRelays


void enableAllRelays(void)
{
  halI2CWrite(0x64, 19, NULL, 0);
} // End of enableAllRelays


// Reset to default state. If relay is non-responsive, see note in the header file.  
void resetRelay(void)
{
  halI2CWrite(0x64, 85, NULL, 0);
} // End of resetRelay


// Turns on the given set of relays
void turnOnRelays(multRelay *relays, int8u numRelays)
{
  int8u bits = 0;
  for (int8u i = 0; i < numRelays; i++)
  {
    bits |= 1 << (relays[i]-1);
  }
  halI2CWrite(0x64, 20, &bits, 1);
} // End of turnOnRelays