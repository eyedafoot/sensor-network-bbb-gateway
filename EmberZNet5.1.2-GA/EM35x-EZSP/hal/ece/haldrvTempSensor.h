/** @file hal/ece/tempSensor.h
 * @brief Header for TMP100 temperature sensor.
 *
 * See @ref temperature for documentation.
 *
 * <!-- University of New Brunswick. 2014 -->
 */
#ifndef __TEMPERATURE_SENSOR_H__
#define __TEMPERATURE_SENSOR_H__

   
/* TMP100 allows up to 8 devices in a single I2C bus
  -- First  Device I2C Address = 0x90
  -- Second Device I2C Address = 0x92
  -- ...
  -- Last   Device I2C Address = 0x9E
*/
// I2C address of the temperature sensor
#define TMP100_I2C_ADDR_0  0x90 //default
#define TMP100_I2C_ADDR_1  0x92
#define TMP100_I2C_ADDR_2  0x94
#define TMP100_I2C_ADDR_3  0x96
#define TMP100_I2C_ADDR_4  0x98
#define TMP100_I2C_ADDR_5  0x9A
#define TMP100_I2C_ADDR_6  0x9C
#define TMP100_I2C_ADDR_7  0x9E

#define TMP100_DEFAULT_ADDRESS   TMP100_I2C_ADDR_0
#define TMP100_I2C_ADDR(devNum)  (((devNum & 0x0F) << 1) | 0x90)
#define TMP100_INVALID_ADDR(devAddr) (((devAddr&0xFE) < TMP100_I2C_ADDR_0) || ((devAddr&0xFE) > TMP100_I2C_ADDR_7))

   

typedef int8u TMP100_address;

/* I2C General Call
The TMP100 and TMP101 respond to the I2C General Call address (0000000)
if the eighth bit is 0. The device acknowledges the general call address and 
responds to commands in the second byte. 
-- If the second byte is 00000100, the TMP100 and TMP101 latch the status of 
their address pins, but will not reset. 
-- If the second byte is 00000110, the TMP100 and TMP101 latch the status of 
their address pins and reset their internal registers.

The general call can be used for: 
1. Checking the presence of TMP100 devices on the bus
2. Reseting the device
*/
#define TMP100_I2C_GENERAL_CALL 0x00

/** TMP100/TMP101 Internal Register addresses
Internal registers:
Address | Register      | Access     | Description
0x00    | Data          | READ ONLY  | Temperature Register (two bytes)
0x01    | Configuration | READ WRITE | Configuration Register (one Byte)
0x02    | TLow          | READ WRITE | Low Temperature Threshold (two bytes)
0x03    | THigh         | READ WRITE | High Temperature Threshold (two bytes) 

TMP100/101  Registers Addresses                                               */
#define TMP100_RA_DATA   0x00
#define TMP100_RA_CFG    0x01
#define TMP100_RA_TLOW   0x02
#define TMP100_RA_THIGH  0x03

/* Macros for Reading/writing internal registers*/
#define TMP100_RD_DATA(devAddr)       I2CREAD16 (devAddr, TMP100_RA_DATA)

#define TMP100_RD_CONFIG(devAddr)     I2CREAD08 (devAddr, TMP100_RA_CFG)
#define TMP100_WR_CONFIG(devAddr,val) I2CWRITE08(devAddr, TMP100_RA_CFG, val)

#define TMP100_RD_TLOW(devAddr)       I2CREAD16 (devAddr, TMP100_RA_TLOW)
#define TMP100_WR_TLOW(devAddr,val)   I2CWRITE16(devAddr, TMP100_RA_TLOW, val)

#define TMP100_RD_THIGH(devAddr)      I2CREAD16 (devAddr, TMP100_RA_THIGH)
#define TMP100_WR_THIGH(devAddr,val)  I2CWRITE16(devAddr, TMP100_RA_THIGH, val)



 /** 
* @brief Register configuration bits.
+--------+--------+--------+--------+--------+--------+--------+--------+
|OS/ALERT|   R1   |   R0   |   F1   |   F0   |   POL  |   TM   |   SD   |
+--------+--------+--------+--------+--------+--------+--------+--------+
  100/101  100/101  100/101  100/101  100/101    101      101    100/101
 
  OS/ALERT -- One Shot (W) or Alert bit (R in TMP101)
  R0/R1    -- Resolution bits (00-11 0.5-0.0625, 9-12 bits)
              BBBB BBBB.FFFF 0000 (B - integer part, F Fractional part)
           --                     | Conversion Times  | Conversion Rates
              R0/R1  |  Precision | Typical | Maximum | Rate
               00    |   9 bits   |   40 ms |   75 ms | 25 s/s
               01    |  10 bits   |   80 ms |  150 ms | 12 s/s
               10    |  11 bits   |  160 ms |  300 ms |  6 s/s
               11    |  12 bits   |  320 ms |  600 ms |  3 s/s
  SD       -- 1- Shutdown Mode (< 1uA consumption, Longer convertion times)
              0- Normal (continuous mode)
  TM       -- Thermostat Mode (TMP101 only). 0-Comparator Mode; 1-Interrupt Mode
  POL      -- Polarity bit (TMP101 only). ALERT pin polarity. 0-Active Low
  FAULT QUEUE (F1/F0) - To avoid noise to trigger an alert condition. 

Note: The Power-Up/Reset value of the configuration Register is: 0x80
*/

#define TMP100_CFG_BIT_SD    0    
#define TMP100_CFG_BIT_TM    1    
#define TMP100_CFG_BIT_POL   2
#define TMP100_CFG_BIT_F0    3
#define TMP100_CFG_BIT_F1    4
#define TMP100_CFG_BIT_R0    5
#define TMP100_CFG_BIT_R1    6
#define TMP100_CFG_BIT_OS    7
#define TMP100_CFG_BIT_ALERT 7

#define TMP100_CFG_SET_SD    0x01
#define TMP100_CFG_SET_TM    0x02
#define TMP100_CFG_SET_POL   0x04
#define TMP100_CFG_SET_F0    0x08
#define TMP100_CFG_SET_F1    0x10
#define TMP100_CFG_SET_R0    0x20
#define TMP100_CFG_SET_R1    0x40
#define TMP100_CFG_SET_OS    0x80

#define TMP100_MODE_CONTINUOUS 0x00 // default 
#define TMP100_MODE_ONESHOT    0x01 
#define TMP100_MODE_SHUTDOWN   TMP100_MODE_ONESHOT 
#define TMP100_CFG_MODE_MASK   0x01
#define TMP100_DEFAULT_MODE    TMP100_MODE_CONTINUOUS

#define TMP100_TMODE_COMPARATOR 0x00 // default 
#define TMP100_TMODE_INTERRUPT  0x01 
#define TMP100_CFG_TMODE_MASK   0x02
#define TMP100_DEFAULT_TMODE    TMP100_TMODE_COMPARATOR

#define TMP100_COMP_POL_ACTIVE_LOW  0x00  // default
#define TMP100_COMP_POL_ACTIVE_HIGH 0x01    
#define TMP100_COMP_POL_MASK        0x04    
#define TMP100_DEFAULT_COMP_POL     TMP100_COMP_POL_ACTIVE_LOW

/**
 * @brief Mask for changing or reading resolution info.
 */
#define RESOL_MASK       (TMP100_CFG_SET_R0|TMP100_CFG_SET_R1)

/**
 * @brief Mask for changing or reading fault queue info.
 */
#define FAULT_MASK       (TMP100_CFG_SET_F0|TMP100_CFG_SET_F1)

/**
 * @brief Macros to get resolution R0R1-code or value.
 */
#define TMP100_MIN_RESOLUTION 9  //default
#define TMP100_MAX_RESOLUTION 12
#define TMP100_DEFAULT_RESOLUTION TMP100_MIN_RESOLUTION

#define RESOLUTION_BITS(val) ((val-TMP100_MIN_RESOLUTION)<<TMP100_CFG_BIT_R0)
#define RESOLUTION(cfg)      (((cfg&RESOL_MASK)>>TMP100_CFG_BIT_R0)+TMP100_MIN_RESOLUTION)
#define DFLT_RESOL_BITS      RESOLUTION_BITS(TMP100_DEFAULT_RESOLUTION)

/**
 * @brief Macros to get fault tolerance F0/F1-code or value.
 */
#define TMP100_DEFAULT_TOL   1
#define FAULT_TOL_BITS(val)  (((val)>>1)<<TMP100_CFG_BIT_F0)
#define FAULT_TOL(cfg)       ((cfg&FAULT_MASK)>>TMP100_CFG_BIT_F0)
#define DFLT_TOL              FAULT_TOL(0)

/**
 * @brief Macro to extract the Polarity of ALERT Bit.
 */
#define ALERT_ACTIVE_LOW(cfg)  ((cfg&TMP100_CFG_SET_POL)==TMP100_COMP_POL_ACTIVE_LOW)

/**
 * @brief Typical conversion times for One Shot measurements.
 */
#define OS_TYP_CONV_TIME_9_BITS  40 // 40  milliseconds
#define OS_TYP_CONV_TIME_10_BITS (OS_TYP_CONV_TIME_9_BITS*2)  // 80 msec  
#define OS_TYP_CONV_TIME_11_BITS (OS_TYP_CONV_TIME_10_BITS*2) // 160 msec
#define OS_TYP_CONV_TIME_12_BITS (OS_TYP_CONV_TIME_11_BITS*2) // 320 msec

/**
 * @brief Macro to get typical conversion time for One Shot measurements.
 */
#define OS_TYP_CONV_TIME(resol) ((1<<(resol-9))*OS_TYP_CONV_TIME_9_BITS)

/**
 * @brief Maximum conversion times for One Shot measurements.
 */
#define OS_MAX_CONV_TIME_9_BITS  75 // 75  milliseconds
#define OS_MAX_CONV_TIME_10_BITS (OS_MAX_CONV_TIME_9_BITS*2)  // 150 msec  
#define OS_MAX_CONV_TIME_11_BITS (OS_MAX_CONV_TIME_10_BITS*2) // 300 msec
#define OS_MAX_CONV_TIME_12_BITS (OS_MAX_CONV_TIME_11_BITS*2) // 600 msec

/**
 * @brief Macro to get Maximum conversion time for One Shot measurements.
 */
#define OS_MAX_CONV_TIME(resol) ((1<<(resol-9))*OS_MAX_CONV_TIME_9_BITS)

/**
  * @brief Default configuration at reset
  */
#define TMP100_DEFAULT_CONFIG ((TMP100_MODE_CONTINUOUS     << TMP100_CFG_BIT_SD) | \
                               (TMP100_TMODE_COMPARATOR    << TMP100_CFG_BIT_TM) | \
                               (TMP100_COMP_POL_ACTIVE_LOW << TMP100_CFG_BIT_POL)| \
                               (FAULT_TOL_BITS(TMP100_DEFAULT_TOL))              | \
                               (RESOLUTION_BITS(TMP100_DEFAULT_RESOLUTION)) )


#define TMP100_DEFAULT_TLOW     0x4B00  // 75.0 �C
#define TMP100_DEFAULT_THIGH    0x5000  // 80.0 �C

/******************************************************************************/
/* Reading Temperature Functions                                              */
/******************************************************************************/
/** @brief Reads Temperature in float regardless TMP100 Operating Mode.  
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as a float number
 */
float getTemperature( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Reads Temperature in units of 1/100 oC regardless TMP100 Operating Mode
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an integer number containig the tempearture in 1/100 oC number. 
 * This format facilitates printing out temperature values and it is easier to interpret. 
 *   E.g.
 *   T = getTemperatureIn100sC(0);
 *   emberAfCorePrintln(" %d.%d oC", T/100, T%100);
 */
int16s getTemperatureIn100sC( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Reads Temperature in fixed point format regardless TMP100 Operating Mode.
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as a fixed point number. The High Byte holds the integer part
 * The Low Byte holds the fractional part. This format facilitates printing out 
 * the temperature values. E.g.
 *   fpT = getTemperatureFpoint(0);
 *   emberAfCorePrintln(" %d.%d �C", HIGH_BYTE(fpT), LOW_BYTE(fpT));
 */
int16s getTemperatureFpoint( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Reads Temperature in TMP100 format regardless TMP100 Operating Mode.
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s getTemperatureRaw( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Issues a OneShot temperature measurement and waits in sleep mode. 
 * 
 * The processor commands a measurement to the TMP100 device and goes to sleep 
 * mode until the measurement is ready. It uses the function 
 * halCommonIdleForMilliseconds(&ms) from Ember to go to sleep the waiting time
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s readTemperatureRawSleeping( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Issues a OneShot temperature measurement and waits in a busy waiting loop. 
 * 
 * The processor commands a measurement to the TMP100 device and waits awake, in  
 * busy waiting loop until the measurement is ready. It uses the function 
 * halCommonDelayMilliseconds(ms) from Ember to wait the required amount of time
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s readTemperatureRawBlocking( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Read temperature in the continuous mode of operation.
 * 
 * This is a Non-blocking function as the TMP100 device is in countinuous mode
 * rather than the oneshot mode. It only reads the TMP100 data register.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return The temperature as an int16s number. This is actually a Q8 fixed point format. 
 * Use the function rawT2fpoint to convert to an easier to print value or the function
 * rawT2float, to convert to a floating point number.
 */
int16s readTemperatureRawNonBlocking( int8u devNum );
/*----------------------------------------------------------------------------*/


/******************************************************************************/
/*   Old (deprecated) Functions                                               */
/******************************************************************************/
//#ifndef _TEMPERATURE_SENSOR_H

/** @brief Initialize Temprature Sensor
 *
 * Configure the onboard temprature sensor via the I2C bus.
 * This function should be called when the Temprature Sensor cluster is being
 * initialized.
 */
#define initTemprature() TMP100initDevice (UNBNODE_TEMP_DEV_NUM)
/*----------------------------------------------------------------------------*/

/** @brief Read Temprature
 *
 * This function returns the node's temprature in units of 1/100 C
 * This function is left here for compatibility with previous version of the driver
 */
#define readTemprature() getTemperatureIn100sC(UNBNODE_TEMP_DEV_NUM)
/*----------------------------------------------------------------------------*/

//#endif


/******************************************************************************/
/*  Device Configuration Functions                                            */
/******************************************************************************/
/* All the functions listed below has the format TMP100cccc. cccc is a command 
 * issued to the device
*/

/** @brief Resets TMP100 device
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * Following reset:
 * -Temperature Register will read 0�C until the first conversion is complete.
 * -Configuration Register is all bits equal to 0.
 * -OS/ALERT bit will read as 1
 * -The ALERT pin is cleared
 * -THIGH = 80�C and TLOW = 75�C.
 */
void TMP100Reset( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Shutdown/Awake TMP100 device
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * @param enable TRUE|FALSE. TRUE puts teh device in Shutdown
 *                           FALSE awakes the device
 */
void TMP100ShutDown( boolean enable, int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Initialize TMP100 device
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 */
void TMP100initDevice( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief reads TMP100 configuration register
 *
 * The Configuration Register is an 8-bit read/write register used to store bits 
 * that control the operational modes of the temperature sensor.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return Configuration register
 * 
 */
int8u TMP100getConfig( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets TMP100 configuration and Thermostat registers
 *
 * The Configuration Register is an 8-bit read/write register used to store bits 
 * that control the operational modes of the temperature sensor.
 *
 * @param devConf  Device configuration.
 * 
 * @param TLow  Thermostat low Threshold.
 * 
 * @param THigh Thermostat high Threshold.
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 */
void TMP100setConfig(int8u devConf, int16s TLow, int16s THigh, int8u devNum  );
/*----------------------------------------------------------------------------*/

/** @brief Sets new thermostat configuration 
 *
 * @param tmode: "1" Interrupt Mode. "0" Comparator Mode (default)
 * 
 * @param AlertPol: Alert pin polarity
 * 
 * @param tol: Thermostat's fault queue length in number of samples (1, 2, 4 or 6).
 * 
 * @param TLow: Thermostat's Low Threshold
 * 
 * @param THigh: Thermostat's High Threshold
 * 
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 */
void TMP100setTConfig(int8u tmode,  int8u AlertPol, int8u tol, int16s TLow, int16s THigh, int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Sets new thermostat mode 
 *
 * sets Comparator/Interrupt Mode of Operation
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @param mode "1" Interrupt Mode. "0" Comparator Mode (default)
 * 
 */
void TMP100setThermostatMode (int8u mode , int8u devNum);
/*----------------------------------------------------------------------------*/

/** @brief reads TMP100 operational mode
 *
 * Returns ShutDown/Continuous Mode of Operation
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return Operational Mode: "1" ShutDown Mode. "0" Continuous Mode
 * 
 */
int8u TMP100getMode( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets TMP100 operational mode
 *
 * sets ShutDown(One Shot)/Continuous Mode of Operation
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @param Operational Mode: "1" ShutDown Mode. "0" Continuous Mode (default)
 * 
 */
void TMP100setMode( int8u mode, int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets TMP100 Continuous mode
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 */
void TMP100setContinuousMode( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets TMP100 One Shot mode
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 */
void TMP100setOneShotMode( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief reads TMP100 resolution (in number of bits)
 *
 * The Converter Resolution Bits control the resolution of the internal 
 * Analog-to-Digital (A/D) converter. This allows the user to maximize efficiency 
 * by programming for higher resolution or faster conversion time. the following Table
 * identifies the Resolution Bits and relationship between resolution and
 * conversion time.
 * R1 R0 | RESOLUTION        | CONVERSION TIME(typical)
 * 0  0  | 9 Bits (0.5�C)    | 40ms
 * 0  1  | 10 Bits (0.25�C)  | 80ms
 * 1  0  | 11 Bits (0.125�C) | 160ms
 * 1  1  | 12 Bits (0.0625�C)| 320ms
 * The temperature register has 16 bits. The 8 MSB represent the integral part 
 * of the temperature. The fractional part is from 1 to 4 bits depending on 
 * device resolution. For example:
 * 0x4BF0 in the temperature register means 
 *  TEMPERATURE | RESOLUTION
 *  75.9375 �C  | 12 bits
 *  75.875 �C   | 10 bits
 *  75.75 �C    | 10 bits
 *  75.5 �C     | 9  bits
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return temperature resolution (9, 10, 11 or 12 bits)
 * 
 */
int8u TMP100getResolution( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets TMP100 resolution (in number of bits)
 *
 * The Converter Resolution Bits control the resolution of the internal 
 * Analog-to-Digital (A/D) converter. This allows the user to maximize efficiency 
 * by programming for higher resolution or faster conversion time. the following Table
 * identifies the Resolution Bits and relationship between resolution and
 * conversion time.
 * R1 R0 | RESOLUTION        | CONVERSION TIME(typical)
 * 0  0  | 9 Bits (0.5�C)    | 40ms
 * 0  1  | 10 Bits (0.25�C)  | 80ms
 * 1  0  | 11 Bits (0.125�C) | 160ms
 * 1  1  | 12 Bits (0.0625�C)| 320ms
 * The temperature register has 16 bits. The 8 MSB represent the integral part 
 * of the temperature. The fractional part is from 1 to 4 bits depending on 
 * device resolution. For example:
 * 0x4BF0 in the temperature register means 
 *  TEMPERATURE | RESOLUTION
 *  75.9375 �C  | 12 bits
 *  75.875 �C   | 10 bits
 *  75.75 �C    | 10 bits
 *  75.5 �C     | 9  bits
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @param Resolution temperature resolution (9, 10, 11 or 12 bits)
 * 
 */
void TMP100setResolution ( int8u Resolution, int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief returns Thermostat's fault queue length in number of samples (1, 2, 4 or 6).
 *
 * A fault condition occurs when the measured temperature exceeds the 
 * user-defined limits set in the THIGH and TLOW Registers. Additionally, the 
 * number of fault conditions required to generate an alert may be programmed 
 * using the Fault Queue. The Fault Queue is provided to prevent a false alert 
 * due to environmental noise. The Fault Queue requires consecutive fault 
 * measurements in order to trigger the alert function. If the temperature falls 
 * below TLOW, prior to reaching the number of programmed consecutive faults 
 * limit, the count is reset to 0. Table 7 defines the number of measured faults 
 * that may be programmed to trigger an alert condition in the device.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return Thermostat's fault queue length in number of samples (1, 2, 4 or 6).
 * 
 */
int8u TMP100getTTolerance( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets Thermostat's fault queue length in number of samples (1, 2, 4 or 6).
 *
 * A fault condition occurs when the measured temperature exceeds the 
 * user-defined limits set in the THIGH and TLOW Registers. Additionally, the 
 * number of fault conditions required to generate an alert may be programmed 
 * using the Fault Queue. The Fault Queue is provided to prevent a false alert 
 * due to environmental noise. The Fault Queue requires consecutive fault 
 * measurements in order to trigger the alert function. If the temperature falls 
 * below TLOW, prior to reaching the number of programmed consecutive faults 
 * limit, the count is reset to 0. Table 7 defines the number of measured faults 
 * that may be programmed to trigger an alert condition in the device.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @param Tolerance fault queue length in number of samples (1, 2, 4 or 6).
 * 
 */
void TMP100setTTolerance( int8u Tolerance, int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief returns Thermostat's LOW Limit Register.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return Thermostat's LOW Limit Register.
 * 
 */
int16s TMP100getTLowThresholdRaw( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief returns Thermostat's LOW Limit Register round an integer value.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return rounded Thermostat's LOW Limit Register.
 * 
 */
int8s TMP100getTLowThresholdRoundC( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets Thermostat's LOW Limit Register.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @param threshold Thermostat's LOW Limit Register.
 * 
 */
void TMP100setTLowThresholdRaw( int16s threshold, int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief returns Thermostat's HIGH Limit Register.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return Thermostat's HIGH Limit Register.
 * 
 */
int16s TMP100getTHighThresholdRaw( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief returns Thermostat's HIGH Limit Register round an integer value.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return rounded Thermostat's HIGH Limit Register.
 * 
 */
int8s TMP100getTHighThresholdRoundC( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief sets Thermostat's HIGH Limit Register.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @param threshold Thermostat's HIGH Limit Register.
 * 
 */
void TMP100setTHighThresholdRaw(int16s threshold, int8u devNum );
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* Low level functions for trigering a reading an for directly read the data register*/
/******************************************************************************/
/** @brief starts a single temperature conversion. 
 *
 * The device will return to the shutdown state at the completion of the single
 * conversion. This is useful to reduce power consumption in the TMP100 and 
 * TMP101 when continuous monitoring of temperature is not required.
 *
 * Note**: this function however, does not check whether the device is in 
 * ShutDown mode or not.
 *
 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 *
*/
   void TMP100triggerOneShotMeasurement( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief returns contents of the device's temperature register.
 *
 * No check is made whether this value is valid or not. It is just a reading of 
 * an internal register.

 * @param devNum  Device number on I2C Bus (0 if using with UNB board).
 * 
 * @return Device's temperature register.
 * 
 */
int16s TMP100getData(int8u devNum );
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/*  Debug Functions                                                           */
/******************************************************************************/
/** @brief Shows the current TMP100 Configuration. 
 *  
 * This function displays the contents of the configuration register and the 
 * threshold registers using the function emberAfAppPrintln
 */
void TMP100showConfig ( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Shows the resolution (in bits) set in the configuration register. 
 *  
 * This function displays the number of bits (9, 10,11, or 12) of resolution  
 * used by the device. It displays the results with the help of the function 
 * emberAfAppPrintln
 */
void TMP100showMeasResol ( int8u devNum );
/*----------------------------------------------------------------------------*/

/** @brief Shows the delay  (in bits) set in the configuration register. 
 *  
 * This function displays the resolution in number of bits(9, 10,11, or 12) used 
 * by the device. It displays the results with the help of the function 
 * emberAfAppPrintln
 */
void TMP100showMeasDelay ( int8u devNum );
/*----------------------------------------------------------------------------*/


#endif
