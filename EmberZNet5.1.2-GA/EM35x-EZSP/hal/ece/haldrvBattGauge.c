#include "app/framework/include/af.h"
#include <haldrvI2C.h>
#include <haldrvUtils.h>
#include <haldrvBattGauge.h>

/* 
 * All variables and functions that are visible outside of the module have the 
 * device name prepended to them. This makes it easier to know the device we are 
 * working with and where to look for function and variable definitions.
 *
 * Functions are separated by a single-line comment consisting only of dashes. 
 */

static int8u batteryGaugeAccessError;
/******************************************************************************/
/* LC709203F I2C related functions                                            */
/******************************************************************************/
static int16u registerRead(int8u rA){ // I2C read register
  int16u rV;
  int8u *buffer;
  
  buffer = (int8u*) &rV;
  batteryGaugeAccessError = halI2CReadWithCRC(LC709203F_I2C_ADDRESS, rA, buffer, 2);
  return rV;
} // End of readRegister
/*----------------------------------------------------------------------------*/

static void registerWrite(int8u rA, int16u rV){ // I2C write command
  int8u *buffer;
  buffer = (int8u*) &rV;
  halI2CWriteWithCRC(LC709203F_I2C_ADDRESS, rA, buffer, 2);
} // End of writeRegister
/*----------------------------------------------------------------------------*/


/******************************************************************************/
/* LC709203F Before RSOC (Relative State of Charge)Register                  */
/******************************************************************************/
/* (Register Address: 0x04. Access:Write Only. Default value: -)

  Executes RSOCinitialization with sampled maximum voltage when 0xAA55 is set
Possible values: 0xAA55

Related functions:
void LC709203F_beforeRSOC(void);

*/

void LC709203F_beforeRSOC(void){
  registerWrite(LC709203F_RA_BEFORE_RSOC, LC709203F_INIT_RSOC);
}//End of beforeRSOC
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Thermistor � Register                                            */
/******************************************************************************/
/* (Register Address: 0x06. Access:Read/Write. Default value: 0x0D34)

 Sets selected thermistor constant value �.
Possible values: 0x0000 to 0xFFFF

Related functions:
int16u LC709203F_getThermistorB(void);
void LC709203F_setThermistorB(int16u B);

*/

int16u LC709203F_getThermistorB(void){
  return registerRead(LC709203F_RA_THERMISTOR_B);
}//End of getThermistorB
/*----------------------------------------------------------------------------*/

void LC709203F_setThermistorB(int16u B){
  registerWrite(LC709203F_RA_THERMISTOR_B, B);
}//End of setThermistorB
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Initial RSOC (Relative State of Charge)Register                  */
/******************************************************************************/
/* (Register Address: 0x07. Access:Write Only. Default value: -)

Sets RSOC (Relative State of Charge) to its initial value 0xAA55.
Possible values: 0xAA55

Related functions:
void LC709203F_initRSOC(void);

*/

void LC709203F_initRSOC(void){
  registerWrite(LC709203F_RA_INITIAL_RSOC, LC709203F_INIT_RSOC);
}//End of initRSOC
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Cell temperature Register                                        */
/******************************************************************************/
/* (Register Address: 0x08. Access:Read/Write. Default value: 0x0BA6 = 25�C)

Displays the value of the Cell Temperature in 0.1�K steps 
(0.0�C = 0x0AAC = 273.2 �K) 

Possible values: 0x09E4 to 0x0D04 = -20�C to 60�C

In thermistor Mode (reading from the device), values can range from 0x0000 to 0xFFFF

Convertion  Kelvin <-> Celsius
Tc = Tk - 273.2  // Convert Kelvin to celsius
Tk = Tc + 273.2  // Convert Celsius to Kevin

Related functions:
int16u LC709203F_getBattTemperature(void);
void   LC709203F_setBattTemperature(int16u value);

*/

int16u LC709203F_getBattTemperature(void){
  int16u kelvin;
  kelvin = registerRead(LC709203F_RA_TEMPERATURE);
  return kelvin;
}//End of getBattTemperature
/*----------------------------------------------------------------------------*/

void   LC709203F_setBattTemperature(int16u kelvin){
  if (kelvin < 0x09E4) kelvin = 0x09E4;
  if (kelvin > 0x0D04) kelvin = 0x0D04;
  registerWrite(LC709203F_RA_TEMPERATURE, kelvin);
}//End of setBattTemperature
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Cell Voltage Register                                            */
/******************************************************************************/
/* (Register Address: 0x09. Access:Read Only. Default value: -)

Displays the value of the Cell Voltage in mVolts 

Possible values: 0x0000 to 0xFFFF 

Related functions:
int16u LC709203F_getBattVoltage(void);
*/

int16u LC709203F_getBattVoltage(void){
  return registerRead(LC709203F_RA_VOLTAGE);
}//End of getBattVoltage
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Current Direction Register                                       */
/******************************************************************************/
/* (Register Address: 0x0A. Access:Read/Write. Default value: 0x0000 = Auto)

Register that allows Fuel Gauge to report RSOC in various conditions 
0x0000 ( 0): Auto
0x0001 (+1): Charge
0xFFFF (-1): Discharge

Possible values: 0x0000, 0x0001, 0xFFFF

Related functions:
int16s LC709203F_getCurrentDirection(void);
void LC709203F_setCurrentDirection(int16s value);
*/
#define CURRENT_DIRECTION_AUTO       0
#define CURRENT_DIRECTION_CHARGE     1
#define CURRENT_DIRECTION_DISCHARGE -1

int16s LC709203F_getCurrentDirection(void){
  return (int16s) registerRead(LC709203F_RA_CURRENT_DIRECTION);
}//End of getCurrentDirection
/*----------------------------------------------------------------------------*/

void LC709203F_setCurrentDirection(int16s value){
  
  switch (value) {
  case CURRENT_DIRECTION_AUTO:
  case CURRENT_DIRECTION_CHARGE:
  case CURRENT_DIRECTION_DISCHARGE:
    registerWrite(LC709203F_RA_CURRENT_DIRECTION, (int16u)value);
    break;
  default:
    registerWrite(LC709203F_RA_CURRENT_DIRECTION, CURRENT_DIRECTION_AUTO);
  }
}//End of setCurrentDirection
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F APA (Adjustment Pack Application) Register                       */
/******************************************************************************/
/* (Register Address: 0x0B. Access:Read/Write. Default value: -)

Register that allows to compensate for total impedance between the Battery and
Fuel Gauge  

Possible values: 0x0000 to 0xFFFF

Related functions:
int16u LC709203F_getAPA(void);
void  LC709203F_setAPA(int16u value);
*/

int16u LC709203F_getAPA(void){
  return registerRead(LC709203F_RA_ADJUSTMENT_PACK_APPLI);
}//End of getAPA
/*----------------------------------------------------------------------------*/

void  LC709203F_setAPA(int16u value){
  registerWrite(LC709203F_RA_ADJUSTMENT_PACK_APPLI, value);
}//End of setAPA
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F APT (Adjustment Pack Thermistor) Register                        */
/******************************************************************************/
/* (Register Address: 0x0C. Access:Read/Write. Default value: 0x001E)

Delay time to compensate thermistor value due to parallel capacitor & Pull-Up Resistor 

Possible values: 0x0000 to 0xFFFF

Related functions:
int16u LC709203F_getAPT(void);
void  LC709203F_setAPT(int16u value);
*/

int16u LC709203F_getAPT(void){
  return registerRead(LC709203F_RA_ADJUSTMENT_PACK_THERM);
}//End of getAPT
/*----------------------------------------------------------------------------*/

void  LC709203F_setAPT(int16u value){
  registerWrite(LC709203F_RA_ADJUSTMENT_PACK_THERM, value);
}//End of setAPT
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F RSOC (Relative State of Charge) Register                         */
/******************************************************************************/
/* (Register Address: 0x0D. Access:Read Only. Default value: -)

Displays RSOC value based on a 0-100 scale 

Possible values: 0x0000 to 0x0064 (0%-100%) 

Related functions:
int16u LC709203F_getRSOC(void);
*/


int16u LC709203F_getRSOC(void){
  return registerRead(LC709203F_RA_RSOC);
}//End of getRSOC
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Indicator to Empty (FG unit) Register                            */
/******************************************************************************/
/* (Register Address: 0x0F. Access:Read Only. Default value: -)

Displays remaining capacity of battery based on a 0-1000 scale 

Possible values: 0x0000 to 0x03E8 (0-1000) 

Related functions:
int16u LC709203F_getIndicatorEmpty(void);
*/

int16u LC709203F_getIndicatorEmpty(void){
  return registerRead(LC709203F_RA_INDICATOR_TO_EMPTY);
}//End of getIndicatorEmpty
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F IC Version Register                                              */
/******************************************************************************/
/* (Register Address: 0x11. Access:Read Only. Default value: from 0x2717)

Displays the IC version 

Possible values: 0x2717 to 0xFFFF  

Related functions:
int16u LC709203F_getICVersion(void);
*/

int16u LC709203F_getICVersion(void){
  return registerRead(LC709203F_RA_IC_VERSION);
}//End of getICVersion
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Default or Alternative Profile Select Register                   */
/******************************************************************************/
/* (Register Address: 0x12. Access:Read/Write. Default value: 0x0001)

Possible values: 0x0000 or 0x0001
Allows to choose between stored battery profiles  

 The LC709203F currently has the options of four battery chemistries. 
 Batteries come in several variations with different characteristics. It is 
 critical to assign the correct Battery Type in order to observe accurate results. 

 The LC709203F has the option of having either battery profile 301 or 504 
 pre-loaded (see register 0x1A -  Profile Number -). Each battery profile has
 two optional battery types to select from. 

 The following table contains the four possible battery profiles.Each profile is 
 unique and correlates to a specific battery chemistry

*-----------+--------------+-----------+-----------+---------+---------------+
|IC-Type     | Nominal/    | Charging  | Battery   | Battery | Change of the |
|            |Rated Voltage| Voltage   | Type      | Profile |  parameter    |
+------------+-------------+-----------+-----------+---------+---------------+
|            | 3.7 V       |   4.2V    | Type 01   |         |     0x0000    |
|            | 3.8 V       |   4.35V   | Type 03   |         |     0x0001    |  
|LC709203F-01|-------------+-----------+-----------|   301   +---------------+
|            |          Any Type       | Re-Scaling|         |               |
|            |                         | Required  |         |               |
+------------+-------------------------+-----------+---------+---------------+
|            | Use only for UR-18560ZY | Type 04   |         |     0x0000    | 
|            |        (Panasonic)      |           |         |               |
|            |Use only for ICR18650-26H| Type 05   |         |     0x0001    |
|            |         (Samsung)       |           |   504   |               |
|LC709203F-04|-------------+-----------+-----------|         +---------------+
|            |          Any Type       | Re-Scaling|         |               |
|            |                         | Required  |         |               |
+------------+-------------------------+-----------+---------+---------------+

 The LC709203F can be Re-Scaled to alter either a different high end voltage or 
 low end voltage. In most cases Re-Scaling is not required due to LC709203F�s 
 four commonly used lithium-ion battery types. Additional information on 
 Re-Scaling can be made available by contacting ON Semiconductor. 

Related functions:
int16u LC709203F_getProfileSelect(void);
void  LC709203F_setProfileSelect(int16u value);
*/

int16u LC709203F_getProfileSelect(void){
  return registerRead(LC709203F_RA_CHANGE_OF_THE_PARAM);
}//End of getProfileSelect
/*----------------------------------------------------------------------------*/

void  LC709203F_setProfileSelect(int16u value){
  registerWrite(LC709203F_RA_CHANGE_OF_THE_PARAM, value&1);
}//End of setProfileSelect
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Alarm Low RSOC Register                                          */
/******************************************************************************/
/* (Register Address: 0x13. Access:Read/Write. Default value: 0x0008)

Allows the assignment of an alarm with a given RSOC value  

Possible values: 0x0000 to 0x0064

The LC709203F has two possible alarm points that can be set by user.
1) Low RSOC value
2) Low Voltage value
 Alarm on Low RSOC value allows you to select a given RSOC value as an alarm 
 point. RSOC is based on a 0-100 scale which can also be expressed as percentage 
(0%-100%). Default alarm setting is 8%. 

 If the Battery�s remaining charge drops lower than the set �Low RSOC Value�, 
 the output at AlarmB will be pulled low by the open drain internal FET. 

Related functions:
int16u LC709203F_getAlarmLowRSOC(void);
void   LC709203F_setAlarmLowRSOC(int16u value);
*/

int16u LC709203F_getAlarmLowRSOC(void){
  return registerRead(LC709203F_RA_ALARM_LOW_CELL_RSOC);
}//End of getAlarmLowRSOC
/*----------------------------------------------------------------------------*/

void   LC709203F_setAlarmLowRSOC(int16u value){
  registerWrite(LC709203F_RA_ALARM_LOW_CELL_RSOC, (value>100)?value%100:value);
}//End of setAlarmLowRSOC

/******************************************************************************/
/* LC709203F Alarm Low Voltage Register                                       */
/******************************************************************************/
/* (Register Address: 0x14. Access:Read/Write. Default value: 0x0000)

Allows the assignment of an alarm with a given voltage value

Possible values: 0x0000 to 0xFFFF  (mVolts)

 The LC709203F has two possible alarm points that can be set by user.
 1) Low RSOC value
 2) Low Voltage value
 Alarm onLow Voltage value allows you to select a low voltage level as an alarm 
 point. Alarm point units are mV. Default alarm setting is 0mV.

 If the Battery�s remaining charge drops lower than the set �Low Voltage Value�, 
 the output at AlarmB will be pulled low by the open drain internal FET. 

Related functions:
int16u LC709203F_getAlarmLowVoltage(void);
void   LC709203F_setAlarmLowVoltage(int16u value);
*/

int16u LC709203F_getAlarmLowVoltage(void){
  return registerRead(LC709203F_RA_ALARM_LOW_CELL_VOLT);
}//End of getAlarmLowVoltage
/*----------------------------------------------------------------------------*/

void   LC709203F_setAlarmLowVoltage(int16u value){
  registerWrite(LC709203F_RA_ALARM_LOW_CELL_VOLT,value);
}//End of setAlarmLowVoltage


/******************************************************************************/
/* LC709203F IC Power Mode Register                                           */
/******************************************************************************/
/* (Register Address: 0x15. Access:Read/Write. Default value: 0x0001)

Allows the assignment of an alarm with a given voltage value

Possible values: 0x0000 to 0x0002

The LC709203F has three possible power modes in which it can operate in.
0x0000 : Testing Mode
0x0001 : Operational Mode
0x0002 : Sleep Mode
*Test Mode and Sleep Mode is used for testing and should not be used for 
 ordinary operation. Operational Mode is recommended. 

 Upon powering on LC709203F, IC enters �Sleep Mode� and initializing is required 
 to return LC709203F back in to �Operational Mode�. 

 Power consumption is different depending on which mode is selected. Fuel Gauge 
 is recommended to be assigned to Operational Mode. In this mode LC709203F goes 
 through a series of decisions to optimize power consumption. 
 

Related functions:
int16u LC709203F_getPowerMode(void);
void   LC709203F_setPowerMode(int16u mode);
*/

tICGaugeMode LC709203F_getPowerMode(void){
  int16u rV;
  rV = registerRead(LC709203F_RA_IC_POWER_MODE);
  return (tICGaugeMode)LOW_BYTE(rV);
}//End of getPowerMode
/*----------------------------------------------------------------------------*/

void   LC709203F_setPowerMode(tICGaugeMode mode){
  int16u rV;
  rV = (int8u) mode;
  registerWrite(LC709203F_RA_IC_POWER_MODE, rV);
}//End of setPowerMode
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Thermistor Status Bit Register                                   */
/******************************************************************************/
/* (Register Address: 0x16. Access:Read/Write. Default value: 0x0000)

 Displays whether thermistor is enabled or disabled  

Possible values: 0x0000 or 0x0001
bit 0 : Thermistor Mode
bit 1 to 15 : Reserved (fix 0)

Thermistor Mode
0 : disable
1 : enable


Related functions:
boolean LC709203F_getThermistorMode(void);
void   LC709203F_setThermistorMode(boolean enable);
*/


boolean LC709203F_getThermistorMode(void){
  int16u rV;  		       
  rV = registerRead(LC709203F_RA_THERMISTOR_STATUS); 
  return (rV & BIT(LC709203F_THERMISTOR_STATUS_BIT));
}//End of getThermistorMode
/*----------------------------------------------------------------------------*/

void   LC709203F_setThermistorMode(boolean enable){
  registerWrite(LC709203F_RA_THERMISTOR_STATUS, enable);
}//End of setThermistorMode
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Profile Number Register                                          */
/******************************************************************************/
/* (Register Address: 0x1A. Access:Read Only. Default value: -)

Displays battery profiles uploaded 301 or 504 (Please refer to list on register 
0x12 description) 

Possible values: 0x0301 or 0x0504  

Related functions:
int16u LC709203F_getProfileNumber(void);
*/

int16u LC709203F_getProfileNumber(void){
  return registerRead(LC709203F_RA_NUM_OF_THE_PARAM);
}//End of getProfileNumber
/*----------------------------------------------------------------------------*/


/******************************************************************************/
/* LC709203F Initialization routines                                          */
/******************************************************************************/

/* It is crucial to flow chart in Figure 4 to insure LC709203F has been properly 
 * initialized to ensure accurate measurements. Upon powering on LC709203F, IC 
 * enters �Sleep Mode� and initializing is required to return LC709203F back in 
 * to �Operational Mode�.
 */
void   LC709203F_initBasic(int16u battImpedance, int16u profile, boolean noLoadnoCharge, boolean useThermistor, int16u thermistorB){
  
  /* Send IC Power mode operational mode (twice) */
  LC709203F_setPowerMode(eOperational);
  LC709203F_setPowerMode(eOperational);
  
  /* Account for impedance path of battery to Fuel Gauge */
  LC709203F_setAPA(battImpedance);
  
  /* Assign correct battery profile */
  LC709203F_setProfileSelect(profile);
  
  /*Initialize RSOC (only with  No-Load and No-Charge condition */
  if (noLoadnoCharge){
    LC709203F_initRSOC();
  }
  
  /* Enable/Disable use of thermistor (if necessary) */
  LC709203F_setThermistorMode( useThermistor );
  
  if ( useThermistor ){
    // temperature will be read from cell. Store the termistor B value
    LC709203F_setThermistorB( thermistorB );
  }else{
    // temperature will be given by the host processor
  }
  
}//End of initBasic
/*----------------------------------------------------------------------------*/

/**
  * The LC709203F can be Re-Scaled to alter either a different high end voltage 
  * or low end voltage. In most cases Re-Scaling is not required due to 
  * LC709203F�s four commonly used lithium-ion battery types. Additional 
  * information on Re-Scaling can be made available by contacting ON Semiconductor. 
  */
void   LC709203F_initWithRescaling(int16u battImpedance, int16u profile, boolean noLoadnoCharge, boolean useThermistor, int16u thermistorB, int16u ITELow, int16u ITEHigh){
  int16u ITE;
  int16u ReScaledRSOC;
  
  /* Send IC Power mode operational mode (twice) */
  LC709203F_setPowerMode(eOperational);
  LC709203F_setPowerMode(eOperational);
  
  /* Account for impedance path of battery  to Fuel Gauge */
  LC709203F_setAPA(battImpedance);
  
  /* Assign correct battery profile */
  LC709203F_setProfileSelect(profile);
  
  /*Initialize RSOC (only with  No-Load and No-Charge condition */
  if (noLoadnoCharge){
    LC709203F_initRSOC();
  }else{
    LC709203F_beforeRSOC();
  }
  
  /* Enable/Disable use of thermistor (if necessary) */
  LC709203F_setThermistorMode( useThermistor );
  
  if ( useThermistor ){
    LC709203F_setThermistorB( thermistorB );
  }
  
  
  /* Identifies Capacity of batter 1000-0 scale */
  ITE = LC709203F_getIndicatorEmpty();
  
  /* RSOC Re-scaling. Can alter RSOC values for different high and low voltage levels*/
  ReScaledRSOC = (ITE-ITELow)*(1000/(1000-ITELow-(1000-ITEHigh)))/10;
  
  if (ITE >= ITEHigh) ReScaledRSOC = 100;
  if (ITE <= ITEHigh) ReScaledRSOC = 0;
  
    
  /* Report Re-Scaled RSOC*/
  
}//End of initBasic
/*----------------------------------------------------------------------------*/



