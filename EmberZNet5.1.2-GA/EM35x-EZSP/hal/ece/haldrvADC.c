/*
 * File: haldrvADC.c
 * Description: ADC HAL functions specific to unb PCB
 * Author: Erik Hatfield
 */
 
#include "app/framework/include/af.h"
#include <hal/hal.h>
#include "haldrvADC.h"

#ifndef logPrintln
	#define logPrintln emberAfCorePrintln
#endif

static int16s* dmaBufferPtr;
static int16s* processBufferPtr;
static int16s* tmpbufferPtr;
static adcMode currentMode;
static adcConfigStruct cfgBack;
void (*dmaCallback)(int16s*,int16u);
void (*multiSampleCallback)(int16s*,adcMultiCfgStruct);

// -- Variables for multi-channel reading ----
adcMultiCfgStruct multiChStruct;
static int16u setInd, configInd;
// -- End of multi-channel variables ---------


// Ember event for delayed processing of ACD data
//   Control: adcEventControl
//   Function: adcEvent
EmberEventControl adcEventControl;

void enableAdc(void)  { SETBIT(ADC_CFG, ADC_ENABLE_BIT);   } // (public)
void disableAdc(void) { CLEARBIT(ADC_CFG, ADC_ENABLE_BIT); } // (public)

// Converts an adc measurement into a value in mV
// When calibrated just scale by nominal vref 1200mV and subtract 0.5*vref (public)
int16s convertToSignedMillivolts(int16s adcdata) { return ((adcdata * 1200L) / 16384L); } 

// Converts an adc measurement into a current value in mA
// Used only with the ECS1030 Split Core Current Transformer
int16s convertToMilliAmps(int16s adcdata) 
{
	return (convertToSignedMillivolts(adcdata)*200);
}

// Configures the Adc to have the desired behavior (public)
// Can read single ended by setting the n input to REF/2
void setAdcConfig(boolean is1MHzClock, int16s sigBits, adcInputChEnum inputP, adcInputChEnum inputN)
{	
	ADC_CFG = 0; // Reset Config, stopping the adc
	
	// Set ADC_1MHZCLK to 1 if is1MHzClock true.
	if (is1MHzClock) {
		ADC_CFG |= ADC_1MHZCLK_MASK;
	} else {
		ADC_CFG &= ~ADC_1MHZCLK_MASK;
	}
	
	ADC_CFG |= (sigBits-7 << ADC_PERIOD_BIT) // period field value = sigBits-7 
		| ((int16u)inputP << ADC_MUXP_BIT)
		| ((int16u)inputN << ADC_MUXN_BIT);
	enableAdc();
} //End of setAdcConfig


// Get the current adc configuration (public)
adcConfigStruct getAdcConfig(void)
{ 
	return (adcConfigStruct) {
		(ADC_CFG & ADC_1MHZCLK_MASK)? TRUE : FALSE,
		(int16s)((ADC_CFG & ADC_PERIOD_MASK) >> ADC_PERIOD_BIT) + 7,
		(adcInputChEnum)((ADC_CFG & ADC_MUXP_MASK) >> ADC_MUXP_BIT),
		(adcInputChEnum)((ADC_CFG & ADC_MUXN_MASK) >> ADC_MUXN_BIT)
	};
} // End of getAdcConfig


// Initialize the Adc (public)
// Default behaviour is single ended reading on ADC0 with fast clock and 14 bits precision
void halInternalInitAdc(void)
{
	// Reset calibration and set default config
	ADC_OFFSET = ADC_OFFSET_RESET;  
	ADC_GAIN = ADC_GAIN_RESET;
	setAdcConfig(FALSE,14,ADC0,VREF2);
	
	// Configure interrupts for polling mode
	INT_CFGCLR = INT_ADC;
	INT_ADCCFG = INT_ADCDATA;
	enableAdc();
} // End of halInternalInitAdc


// Read a value from the current configurations (public)
int16s readAdcOneShot(void)
{
	enableAdc();
	INT_ADCFLAG = 0xFFFF; // clear interrupt flags
	
	while (!(INT_ADCFLAG & INT_ADCDATA)); // wait for INT_ADCDATA
	return (int16s)ADC_DATA;
} // End of readAdcOneShot


// Set the adc to measure absolute voltage (public)
void calibrateAdc(void) 
{
	int16s ngnd, nvref;
	int16u temp;
	enableAdc();
	ADC_OFFSET = ADC_OFFSET_RESET;
	ADC_GAIN = ADC_GAIN_RESET;
	cfgBack = getAdcConfig(); 

	emberCalibrateVref();

	// Determine the ADC offset by sampling GND.
	setAdcConfig(cfgBack.is1MHzClock,cfgBack.sigBits,GND,VREF2);
	ngnd = readAdcOneShot();

	// Apply offset correction.
	logPrintln("ngnd: %d (0x%2X)", ngnd, ngnd);
	ADC_OFFSET = (int16u)(2*(57344 - ngnd)); //0xFD5C

	// Determine the ADC gain by sampling independently VREF and GND.
	// Gain is calculated from the slope of these two measurements.
	setAdcConfig(cfgBack.is1MHzClock,cfgBack.sigBits,VREF,VREF2);
	nvref = readAdcOneShot();
	setAdcConfig(cfgBack.is1MHzClock,cfgBack.sigBits,GND,VREF2);
	ngnd = readAdcOneShot();

	// Apply gain correction.
	temp = (int16u)(32768L * 16384L / (nvref - ngnd));
	logPrintln("gain: %d (0x%2X)", temp, temp);
	ADC_GAIN = temp;

	setAdcConfig(cfgBack.is1MHzClock, cfgBack.sigBits, cfgBack.inputP, cfgBack.inputN);
} // End of calibrateAdc


// Determine if the adc is calibrated to measure absolute voltage (public)
boolean isCalibrated(void)
{
	enableAdc();
	boolean isCal; 
	cfgBack = getAdcConfig(); // Backup config 
	
	// Read VREF to see if it's the max value
	setAdcConfig(FALSE,14,VREF,VREF2);
	isCal = (readAdcOneShot()==16384)? TRUE : FALSE;
	
	setAdcConfig(cfgBack.is1MHzClock,cfgBack.sigBits,cfgBack.inputP,cfgBack.inputN); // Restore config
	return isCal;
} // End of isCalibrated


// Common code for buffer reading (private)
static void startAdcDMA(void) 
{
	enableAdc();
	
	// Reset dma & define buffer 
	ADC_DMACFG = ADC_DMARST;
	INT_ADCCFG = INT_ADCULDFULL; // Enable DMA full interrupts only
	INT_ADCFLAG = 0xFFFF; // Clear the ADC buffer full flag
	
	// Set DMA to linear mode
	ADC_DMACFG = ADC_DMALOAD;
        INT_CFGSET |= INT_ADC;
	// Wait until the INT_ADCULDFULL bit is set in INT_ADCFLAG, then read results from dmaBuffer
}


// Reads values into a single buffer (public) 
void readAdcSingleBuffer(int16s* buffPtr, int16u numSamples, void (*callback)(int16s*,int16u))
{	
	dmaBufferPtr = buffPtr;
	processBufferPtr = 0;
	currentMode = SingleBuff;
	dmaCallback = callback;  // Specify user callback
	ADC_DMASIZE = numSamples;
	ADC_DMABEG = (int32u)dmaBufferPtr;
	startAdcDMA();
} // End of readAdcSingleBuffer


// Reads values with a double buffer configuration (public)
void readAdcDoubleBufferStream(int16s* buffPtr1, int16s* buffPtr2, int16u numSamples, void (*callback)(int16s*,int16u))
{
	dmaBufferPtr = buffPtr1; 
	processBufferPtr = buffPtr2;
	currentMode = DoubleBuff;
	dmaCallback = callback;  // Specify user callback
	ADC_DMASIZE = numSamples;
	ADC_DMABEG = (int32u)dmaBufferPtr;
	startAdcDMA();
        INT_CFGSET |= INT_ADC;
} // End of readAdcDoubleBuffer


// Reads values from multiple channels passed as an array, by writing interleaved values into an array (public)
// Can read single ended by setting the n input to REF/2
void readAdcMultiConfig(adcInputChEnum* pSetPtr, adcInputChEnum* nSetPtr, int16u configPerSet, 
								int16s* arrayBufferPtr, int16u setsPerBuffer, void (*callback)(int16s*,adcMultiCfgStruct))
{
	cfgBack = getAdcConfig(); // Backup config 
	multiChStruct = (adcMultiCfgStruct){pSetPtr,nSetPtr,configPerSet,setsPerBuffer}; 
	dmaBufferPtr = arrayBufferPtr;
	processBufferPtr = 0;
	currentMode = MultiCh;
	setInd = 0; configInd = 0; // Start on first channel of first set
	multiSampleCallback = callback;  // Specify user callback
	ADC_DMASIZE = 1; // Only take 1 sample before running ISR
	ADC_DMABEG = (int32u)dmaBufferPtr;
	startAdcDMA(); 
} // End of readAdcMultChannels

// Reads values from multiple channels passed as an array, by writing interleaved values into an array (public)
// Can read single ended by setting the n input to REF/2
void readAdcMultiConfigDoubleBufferStream(adcInputChEnum* pSetPtr, adcInputChEnum* nSetPtr, int16u configPerSet, 
								int16s* arrayBufferPtr, int16s* processBuffPtr, int16u setsPerBuffer, void (*callback)(int16s*,adcMultiCfgStruct))
{
	multiChStruct = (adcMultiCfgStruct){pSetPtr,nSetPtr,configPerSet,setsPerBuffer}; 
	dmaBufferPtr = arrayBufferPtr;
	processBufferPtr = processBuffPtr;
	currentMode = MultiCh;
	setInd = 0; configInd = 0; // Start on first channel of first set
	multiSampleCallback = callback;  // Specify user callback
	ADC_DMASIZE = 1; // Only take 1 sample before running ISR
	ADC_DMABEG = (int32u)dmaBufferPtr;
	startAdcDMA(); 
} // End of readAdcMultChannels


// Function called by the stack's event processing mechanism after a successful DMA operation. (protected)
// It calls the callback function provided by the user, passing the dma buffer pointer.
void adcEvent(void)
{
	switch (currentMode) {
		case MultiCh:
			if (processBufferPtr != 0) { // Double buffer
				multiSampleCallback(processBufferPtr,multiChStruct);
			} else { // Single buffer
				multiSampleCallback(dmaBufferPtr,multiChStruct);
			}
			break;
		case DoubleBuff:
			dmaCallback(processBufferPtr,(ADC_DMASIZE & ADC_DMASIZE_FIELD_MASK));
			break;
		case SingleBuff:
			dmaCallback(dmaBufferPtr,(ADC_DMASIZE & ADC_DMASIZE_FIELD_MASK));
			break;
	}
	emberEventControlSetInactive(adcEventControl);
}

// ADC top-level ISR defined by the HAL (protected)
// If the active interrupt is "DMA buffer full" then this function will schedule
// the ADC event to run the next time events are run.
void halAdcIsr(void)
{
        INT_CFGCLR = INT_ADC; // Disable Adc interrupts
	
	switch (currentMode) {

		case MultiCh:
			// Done if reached the last sample in the last set 
			if ((setInd >= (multiChStruct.setsPerBuffer-1)) && (configInd >= (multiChStruct.configPerSet-1))) {
				setInd = 0;
				configInd = 0;
				if (processBufferPtr != 0) { // Double buffer
					// Interchange dma and process buffer pointers
					tmpbufferPtr = processBufferPtr;
					processBufferPtr = dmaBufferPtr;
					dmaBufferPtr = tmpbufferPtr;
					ADC_DMABEG = (int32u)dmaBufferPtr;
					ADC_CFG &= ~(ADC_MUXP_MASK|ADC_MUXN_MASK); // Reset channel mux
					ADC_CFG |= ((int16u)multiChStruct.pSetPtr[configInd] << ADC_MUXP_BIT) // Set the new channels
							|  ((int16u)multiChStruct.nSetPtr[configInd] << ADC_MUXN_BIT);
					emberEventControlSetActive(adcEventControl); // Schedule the event
					startAdcDMA(); // Start reading into new dma buffer
				} else {
					INT_ADCCFG = INT_ADCDATA; // Only use data interrupts
					emberEventControlSetActive(adcEventControl); // Call event	
				}
			} else {
				// Go to next sample set if reached last configuration in set, otherwise go to next configuration
				if (configInd >= (multiChStruct.configPerSet-1))  {
					configInd = 0; 
					setInd++; 
				} else {
					configInd++;
				}
				ADC_DMABEG = (int32u)(&dmaBufferPtr[setInd*multiChStruct.configPerSet+configInd]); // Move buffer begin address
				ADC_CFG &= ~(ADC_MUXP_MASK|ADC_MUXN_MASK); // Reset channel mux
				ADC_CFG |= ((int16u)multiChStruct.pSetPtr[configInd] << ADC_MUXP_BIT) // Set the new channels
						|  ((int16u)multiChStruct.nSetPtr[configInd] << ADC_MUXN_BIT);
				startAdcDMA();
			}
			// Done if reached the last sample in the last set 
			break;

		case DoubleBuff: ; // Empty statement
			// Interchange dma and process buffer pointers
			tmpbufferPtr = processBufferPtr;
			processBufferPtr = dmaBufferPtr;
			dmaBufferPtr = tmpbufferPtr;
			emberEventControlSetActive(adcEventControl); // Schedule the event
			ADC_DMABEG = (int32u)dmaBufferPtr;
			startAdcDMA(); // Start reading into new dma buffer
			break;

		case SingleBuff:
			// Otherwise disable DMA full interrupts so that it only runs once
			INT_ADCCFG = INT_ADCDATA; // Only use data interrupts
			emberEventControlSetActive(adcEventControl); // Schedule the event
			break;
	}
} //End of halAdcIsr