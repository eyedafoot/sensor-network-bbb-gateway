#include "app/framework/include/af.h"
#include "analog.h"

#define ADC_INPUT_PB5 0x0   /* ADC0 */
#define ADC_INPUT_PB6 0x1   /* ADC1 */
#define ADC_INPUT_PB7 0x2   /* ADC2 */
#define ADC_INPUT_PC1 0x3   /* ADC3 */
#define ADC_INPUT_PA4 0x4   /* ADC4 */
#define ADC_INPUT_PA5 0x5   /* ADC5 */
#define ADC_INPUT_GND 0x8   /* GND, 0V */
#define ADC_INPUT_VREF2 0x9 /* VREF/2, 0.6V */
#define ADC_INPUT_VREF  0xA /* VREF, 1.2V */
#define ADC_INPUT_VREG2 0xB /* VDD_PADSA/2, 0.9 */

// Forward declerations to private functions
void internalCalibrateADC(void);
void internalReadDiffSeries(void);
int16s internalReadADC(void);

// Globals
int16s dmaBuffer[DMA_BUFFER_SIZE];
void (*dmaCallback)(int16u);

// Ember event for delayed processing of ACD data, requires a custom event to
// be defined in the AppBuilder
//   Control: adcEventControl
//   Function: adcEvent
EmberEventControl adcEventControl;

// Function called by the stack's event processing mechenism after a successful
// DMA operation. It processes the ADC measurements to calculate the Irms,
// and then returnes the value to the callback function provided by the user
// to readACCurrent(). Note: All of the calculations are done using integer
// arithmatic, so there will be rounding errors.
void adcEvent(void)
{
  int i;
  int32s avg = 0;
  int32u irms = 0;
  
  // First convert the measurements from arbitrary ADC units to mA
  for (i = 0; i < DMA_BUFFER_SIZE; i++) {
    dmaBuffer[i] = ((dmaBuffer[i] * 1200L) / 16384L); // Convert to mV
    dmaBuffer[i] = (dmaBuffer[i] * 2000L) / BURDEN_RESISTOR_VALUE; // V to I
    avg += dmaBuffer[i];
  }
  avg /= DMA_BUFFER_SIZE;
  
  // Calculate the RMS value from the AC measurements
  for (i = 0; i < DMA_BUFFER_SIZE; i++) {
    // Offeset corretion to help prevent overflow errors
    dmaBuffer[i] -= avg;
    irms += dmaBuffer[i] * dmaBuffer[i];
  }
  irms = squareRoot(irms / DMA_BUFFER_SIZE);
  
  // Finished, clean up and call the user-provided callback function
  emberEventControlSetInactive(adcEventControl);
  if (dmaCallback != NULL) dmaCallback((int16u)irms);
}

void halInternalInitAdc(void)
{
  // Configure adc registers default to slow clock for higher input
  // impedance.
  // Default to best resolution (0x7 = 4096 clock cycles, 14 bit)
  // measure PB5 vs vref/2
  ADC_CFG = ADC_1MHZCLK
            | (0x7 << ADC_PERIOD_BIT)
            | (ADC_INPUT_PB7 << ADC_MUXP_BIT)
            | (ADC_INPUT_VREF2 << ADC_MUXN_BIT);
  
  // Configure interrupts for polling
  INT_CFGCLR = INT_ADC;
  INT_ADCCFG = INT_ADCDATA;
  
  // Start the ADC
  ADC_CFG |= ADC_ENABLE;
  
  // Calibrate the ADC for GND - 1.2V
  internalCalibrateADC();
}

int16s readBattVoltage(void)
{
  // Immidiatly fail if the DMA is in use
  if (ADC_DMASTAT & ADC_DMAACT) return 0;
  
  // Read from a battery-feedback voltage divider on ADC2
  SET_REG_FIELD(ADC_CFG, ADC_MUXP, ADC_INPUT_PB7);
  SET_REG_FIELD(ADC_CFG, ADC_MUXN, ADC_INPUT_VREF2);
  int16s value = internalReadADC();
  
  // Convert to mV
  value = (value * 1200L) / 16384L;
  
  // Adjust for the voltage divider circuit
  // R1 = 9.83 kOHM
  // R2 = 5.14 kOHM
  value = (value * 14970L) / 5140L;
  return value;
}

void readACCurrent(void (*callback)(int16u))
{
  // Read from the ADC inputs connected to a current transformer (diffrential)
  // and reduce the sample time to improve the measurement resolution
  ADC_CFG = ADC_1MHZCLK
            | ADC_ENABLE
            | (0x4 << ADC_PERIOD_BIT)
            | (ADC_INPUT_PA4 << ADC_MUXP_BIT)
            | (ADC_INPUT_PA5 << ADC_MUXN_BIT);
  
  // Depending on the number of samples being taken, it can easilly take longer
  // for the DMA buffer to fill then the watchdog timer to run down. So instead
  // of returning the result immidiatly we will give it to the user-specified
  // callback function when it's ready.
  dmaCallback = callback;
  internalReadDiffSeries();
}

// This function is only intended to be used internally by this file. It polls
// the ADC for a new sample and returns it. This function is independent of
// ADC configuration and input selection.
int16s internalReadADC(void)
{
  // Clear the interrupt flags
  INT_ADCFLAG = 0xFFFF;
  
  // Wait for a new sample
  while (!(INT_ADCFLAG & INT_ADCDATA));
  
  return (int16s)ADC_DATA;
}

// This function is only intended to be used internally by this file. It
// configures the ADC to use it's DMA functionality to take a series of
// readings without tieing up the main program thred. The main application
// will be notified when the DMA operation is finished
void internalReadDiffSeries(void)
{
  // Reset the DMA and provide a pointer to the buffer
  ADC_DMACFG = ADC_DMARST;
  ADC_DMABEG = (int32u)dmaBuffer;
  ADC_DMASIZE = DMA_BUFFER_SIZE;
  
  // Activate the DMA finished interrupt
  INT_ADCCFG = INT_ADCULDFULL;
  
  // Load DMA
  ADC_DMACFG = ADC_DMALOAD;
  
  // Clear the interrupt flags, and enable the ADC top-level interrupt
  INT_ADCFLAG = 0xFFFF;
  INT_CFGSET = INT_ADC;
}

// This function is only intended to be used internally by this file. It
// calibrates the ADC by providing providing appropriate offset and gain values.
//   Calibrated operating range: 0 V - 1.2 V
void internalCalibrateADC(void)
{
  int16s ngnd;
  int16s vdd;
  int16s nvreg;
  
  // Ensure any existing corrections are reset
  ADC_OFFSET = ADC_OFFSET_RESET;
  ADC_GAIN = ADC_GAIN_RESET;
  
  // Read factory trimmed vdd value from token
  halCommonGetToken(&vdd, TOKEN_MFG_1V8_REG_VOLTAGE);
  
  // Take ground reading (ngnd)
  SET_REG_FIELD(ADC_CFG, ADC_MUXP, ADC_INPUT_GND);
  SET_REG_FIELD(ADC_CFG, ADC_MUXN, ADC_INPUT_VREF2);
  ngnd = internalReadADC();
  
  // Multiply ngnd by -2 and write to offset register
  ADC_OFFSET = -2 * ngnd;
  
  // Read 1.8V regulator/2 (nvreg)
  SET_REG_FIELD(ADC_CFG, ADC_MUXP, ADC_INPUT_VREG2);
  nvreg = internalReadADC();
  
  // Adjust gain so 1200mV produces 16384 at adc raw output
  // (32768 corresponds to a gain value of 1.0)
  ADC_GAIN = (int16u)(32768L * (16384L * vdd / 24000L) / nvreg);
}

// ADC top-level ISR defined by the HAL
// If the active interrupt is "DMA buffer full" then this function will schedule
// the ADC event to run the next time events are run. Afterwords it returns the
// ADC configuration to polling mode.
void halAdcIsr(void)
{
  // WARNING: ISR Context!
  
  // We only want to respond to the DMA full interrupt
  if (!(INT_ADCFLAG & INT_ADCULDFULL)) {
    INT_ADCFLAG = 0xFFFF;
    return;
  }
  
  // Run the event that processes the DMA data
  emberEventControlSetActive(adcEventControl);
  
  // Disable the ADC interrupt
  INT_CFGCLR = INT_ADC;
  
  // Reset to polling configuration
  INT_ADCCFG = INT_ADCDATA;
  ADC_CFG = ADC_1MHZCLK
            | ADC_ENABLE
            | (0x7 << ADC_PERIOD_BIT)
            | (ADC_INPUT_PB7 << ADC_MUXP_BIT)
            | (ADC_INPUT_VREF2 << ADC_MUXN_BIT);
}

// Integer square root algorithem for calculating RMS value
int32u squareRoot(int32u value)
{
    int32u op  = value;
    int32u res = 0;
    int32u one = 1uL << 30; // The second-to-top bit is set: use 1u << 14 for int16u type; use 1uL<<30 for int32u type


    // "one" starts at the highest power of four <= than the argument
    while (one > op) one >>= 2;

    while (one != 0) {
        if (op >= res + one) {
            op = op - (res + one);
            res = res +  2 * one;
        }
        res >>= 1;
        one >>= 2;
    }

    // Do arithmetic rounding to nearest integer
    if (op > res) res++;
    
    return res;
}
