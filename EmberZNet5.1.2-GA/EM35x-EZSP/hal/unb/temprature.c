#include "app/framework/include/af.h"
#include "i2c.h"
#include "temprature.h"

// I2C address of the temprature sensor
#define TEMP_ADDRESS  0x90

// Regester addresses
#define TEMP_DATA_REG 0x00
#define TEMP_CFG_REG  0x01

// Regester configuration bits
#define TEMP_SD       0x01
#define TEMP_TM       0x02
#define TEMP_POL      0x04
#define TEMP_FLT_F0   0x08
#define TEMP_FLT_F1   0x10
#define TEMP_RES_R0   0x20
#define TEMP_RES_R1   0x40
#define TEMP_OS       0x80

// Default configuration is full resolution and low-power mode
#define TEMP_CONFIG (TEMP_RES_R0 | TEMP_RES_R1 | TEMP_SD)

void initTemprature(void)
{
  // Initialize the temprature sensor to it's operating configuration
  halI2CWriteint8u(TEMP_ADDRESS, TEMP_CFG_REG, TEMP_CONFIG);
}

int16s readTemprature(void)
{
  // Tell the sensor to do a one-shot measurement
  halI2CWriteint8u(TEMP_ADDRESS, TEMP_CFG_REG, TEMP_CONFIG | TEMP_OS);
  
  // Wait for the measurement to compleate. 320ms for full resolution
  halCommonDelayMilliseconds(320);
  
  // Retrive temprature from on on-board sensor
  // Data comes out of the sensor as a signed 12-bit left-aligned value
  int16s value = (int16s)halI2CReadint16u(TEMP_ADDRESS, TEMP_DATA_REG) >> 4;
  
  // Convert to units of 1/100 deg C
  value = (value * 100L) / 16L;
  return value;
}
