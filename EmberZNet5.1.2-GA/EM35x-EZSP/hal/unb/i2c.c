#include "app/framework/include/af.h"
#include "i2c.h"

// Macros for common I2C operations to help improve readability of this file
#define halInternalI2CWait(mask)  while( !(SC1_TWISTAT & mask) )
#define halInternalI2CStart()     SC1_TWICTRL1 = SC_TWISTART;       \
                                  halInternalI2CWait(SC_TWICMDFIN)
                                  
#define halInternalI2CStop()      SC1_TWICTRL1 = SC_TWISTOP;        \
                                  halInternalI2CWait(SC_TWICMDFIN)
                                  
#define halInternalI2CWrite(data) SC1_DATA = data;                  \
                                  SC1_TWICTRL1 = SC_TWISEND;        \
                                  halInternalI2CWait(SC_TWITXFIN)
int8u halInternalI2CRead(void)
{
  SC1_TWICTRL1 = SC_TWIRECV;
  halInternalI2CWait(SC_TWIRXFIN);
  return SC1_DATA;
}

void halI2CInit(void) {
  // Set the mode of SC1 to Two Wire Serial (I2C)
  SC1_MODE = SC1_MODE_I2C;
  
  // Send ACK after recieved frames
  SC1_TWICTRL2 = SC_TWIACK;
  
  // Configure baud rate to 375 kbps
  SC1_RATEEXP = 1;
  SC1_RATELIN = 15;
}

void halI2CWriteint8u(int8u address, int8u regester, int8u data)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  halInternalI2CWrite(data);
  halInternalI2CStop();
}

void halI2CWriteint16u(int8u address, int8u regester, int16u data)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  halInternalI2CWrite((int8u)(0xFF & (data >> 8)));
  halInternalI2CWrite((int8u)(0xFF & data));
  halInternalI2CStop();
}

int8u halI2CReadint8u(int8u address, int8u regester)
{
  int8u data;
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();
  halInternalI2CWrite(address | 1);
  data = halInternalI2CRead();
  halInternalI2CStop();
  return data;
}

int16u halI2CReadint16u(int8u address, int8u regester)
{
  int16u data;
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();
  halInternalI2CWrite(address | 1);
  data = halInternalI2CRead() << 8;
  data |= halInternalI2CRead();
  halInternalI2CStop();
  return data;
}

void halI2CRead(int8u address, int8u regester, int8u * buffer, int8u count)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();
  halInternalI2CWrite(address | 1);
  
  int8u i;
  for (i = 0; i < count; i++) buffer[i] = halInternalI2CRead();
  halInternalI2CStop();
}

void halI2CWrite(int8u address, int8u regester, const int8u * buffer, int8u count)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  int8u i;
  for (i = 0; i < count; i++) halInternalI2CWrite(buffer[i]);
  halInternalI2CStop();
}
