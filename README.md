# UNB Sensor Network #

### What is this repository for? ###

* Contains the ember stacks for developing the UNB Sensor Network, and the corresponding code.
* Version: 1.0 (since repo)

### How do I get set up? ###

* Create an account with Bitbucket.
* Accept the invitation to the repository.
* Install tortoise hg.
* Target the URL of this repository.
* Pull the latest version of the repository.
* Extract the build files in the stacks into their build folders.
* Build the required projects in IAR.

### Contribution guidelines ###

* Make changes where required.
* Open Tortoise hg, and refresh the file list.
* Create a new commit with a good description.
* Push the code to Bitbucket.

### Who do I talk to? ###

* Erik Hatfield (erik.hatfield7@gmail.com)

### End Goals for Product ###

* Wireless centralized network.
* Controlled from java application.
* Data stored in database.
* Configurable for a variety of applications.
* Marketed towards power industry.

### The following is an illustration of one possible end product ###

![Alt text](http://i.imgur.com/2Qf5rbm.png "Projection of End Product")

### Thinkspeak keywords ###

* Channel - The name for where data can be inserted or retrieved within the ThingSpeak API, identified by a numerical Channel ID
* Channel ID - Every channel has a unique Channel ID. The Channel ID number is used to identify the channel when your application reads data from the channel
* Field - One of eight specific locations for data inside of a channel, identified by a number between 1 to 8 – A field can store numeric data from sensors or alphanumeric strings from serial devices or RFID readers
* Status - A short status message to augment the data stored in a channel
* Location - The latitude, longitude, and elevation of where data is being sent from
* Feed - The collective name for the data stored inside a channel, which may be any combination of field data, status updates, and location info
* Write API Key – A 16 digit code that allows an application to write data to a channel
* Read API Key – A 16 digit code that allows an application to read the data stored in a channel