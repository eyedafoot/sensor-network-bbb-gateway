/**
 * \defgroup testing-cli UNB ember Node Test
 *
 * This is how a Doxygen module is documented - start with a \defgroup
 * Doxygen keyword at the beginning of the file to define a module,
 * and use the \addtogroup Doxygen keyword in all other files that
 * belong to the same module. Typically, the \defgroup is placed in
 * the .h file and \addtogroup in the .c file.
 *
 * @{
 */

/**
 * \file
 *         A brief description of what this file is.
 * \author
 *         Julian Cardenas <jcardena@unb.ca>
 * 
 *         Every file that is part of a documented module has to have
 *         a \file block, else it will not show up in the Doxygen
 *         "Modules" * section.
 */
#include "app/framework/include/af.h"
#include "app/framework/util/af-main.h"
#include "app/util/serial/command-interpreter2.h"


#define majorVer 1
#define minorVer 0
#define cliPrintf(...)   emberSerialPrintf(EMBER_AF_PRINT_OUTPUT,  __VA_ARGS__)
#define cliPrintfLn(...) emberSerialPrintfLine(EMBER_AF_PRINT_OUTPUT,  __VA_ARGS__)

/* Forward declarations*/
void printChildTable(void);
void printNeighborTable(void);
void printRouteTable(void);
void printInfo(void);

   
#if !defined(ZA_CLI_MINIMAL) && !defined(ZA_CLI_FULL) && !defined(EMBER_AF_GENERATE_CLI)
  /* Define this to guarrantee that our command line will be available to user*/
  EmberCommandEntry *emberCommandTable;
#endif
 
 void unb_testing_cli_init(void){
   emberCommandTable = UNBTestingCLiCommandTable;
 }//End of testing_cle_init
/*---------------------------------------------------------------------------*/
void enUNBVersionCB(void)
{
  cliPrintfLn("UNB Ember Node Test Application v%d.%d", majorVer, minorVer);
  cliPrintfLn("Oct 16 2014, 22:12:00");
} //End of enUNBVersionCB

/*---------------------------------------------------------------------------*/
void printInfo(void){
  int8u index;

  EmberNodeId id = emberAfGetNodeId();
  EmberNodeType type = EMBER_UNKNOWN_DEVICE;
  EmberNetworkParameters parameters;
  emberAfGetNetworkParameters(&type, &parameters);

  emberAfAppPrintln("Stack Profile: %d", emberAfGetStackProfile());
  emberAfAppPrintln("Configured Node Type (%d): %p",
                    emAfCurrentNetwork->nodeType,
                    nodeTypeStrings[emAfCurrentNetwork->nodeType]);
  emberAfAppPrintln("Running Node Type    (%d): %p\n",
                    type,
                    nodeTypeStrings[type]);
  emberAfAppPrintln("Channel:       %d", parameters.radioChannel);
  emberAfAppPrintln("Node ID:       0x%2x", id);
  emberAfAppPrintln("PAN ID:        0x%2X", parameters.panId);
  emberAfAppPrint(  "Extended PAN:  ");
  emberAfPrintBigEndianEui64(parameters.extendedPanId);
  emberAfAppPrintln("\nNWK Update ID: %d\n", parameters.nwkUpdateId);

  emberAfAppPrintln("NWK Manager ID: 0x%2X", parameters.nwkManagerId);
  emberAfAppPrint(  "NWK Manager Channels: ");
  emberAfPrintChannelListFromMask(parameters.channels);
  emberAfAppPrintln("\n");

  emberAfAppPrintln("Send Multicasts to sleepies: %p\n",
                    (emberAfGetSleepyMulticastConfig()
                     ? "yes"
                     : "no"));
}//End of printInfo

/* The following stuff ends the \defgroup block at the beginning of
   the file: */

/** @} */