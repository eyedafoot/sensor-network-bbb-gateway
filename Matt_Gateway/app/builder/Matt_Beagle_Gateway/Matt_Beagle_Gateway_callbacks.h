//
// Created by Matthew on 9/17/2017.
//

#ifndef MATT_GATEWAY_MATT_BEAGLE_GATEWAY_CALLBACKS_H
#define MATT_GATEWAY_MATT_BEAGLE_GATEWAY_CALLBACKS_H

#include "app/framework/include/af.h"
#include "app/util/common/form-and-join.h"
#include "app/framework/plugin/ezmode-commissioning/ez-mode.h"

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "NodeConfiguration/configurationTypes.h"
#include "GeneralUtil/DataTypes.h"
#include "GeneralUtil/NodeList.h"
#include "GeneralUtil/TestOutPut.h"

#include "ServerConnectionProcess/ServerConnectionMain.h"

/********************************
 * Constants
 ********************************/
// This callback file is created for your convenience. You may add application code
// to this file. If you regenerate this file over a previous version, the previous
// version will be overwritten and any code you have added will be lost.

#define DATA1_PATH "data1.csv"
#define DATA2_PATH "data2.csv"

#define SEM_DATA1_PATH "/sem_data1_v2"
#define SEM_DATA2_PATH "/sem_data2_v2"

#define DATA_FILE_SIZE 6144//8192

//key value pair for xml parsing
typedef enum keys {
    NODE_ID = 0,
    ADC_MOD = 1,
    ADC_BITS = 2,
    ADC_CLK = 3,
    ADC_PERIOD = 4,
    ADC_C0_P = 5,
    ADC_C0_N = 6,
    ADC_C1_P = 7,
    ADC_C1_N = 8,
    ADC_C2_P = 9,
    ADC_C2_N = 10,
    ADC_C3_P = 11,
    ADC_C3_N = 12,
    TEMP_PERIOD = 13,
    ACCL_MAXG = 14,
    ACCL_PERIOD = 15,
    GET_CONFIG = 16,
    SET_CONFIG = 17,
    API_KEY = 18,
    TI_SENSORTAG = 19,
    UPDATE_FIELDS = 20,
    REMOVE_NODE = 21,
    NONE = -1
} Key;

static const char *values[] = {
        "node",
        "adc_mode",
        "adc_bits",
        "adc_clock",
        "adc_seconds",
        "adc_cfg0_positive",
        "adc_cfg0_negative",
        "adc_cfg1_positive",
        "adc_cfg1_negative",
        "adc_cfg2_positive",
        "adc_cfg2_negative",
        "adc_cfg3_positive",
        "adc_cfg3_negative",
        "temp_seconds",
        "acc_maxG",
        "acc_seconds",
        "Get_Node_Configuration",
        "Set_Node_Configuration",
        "api_key",
        "ti-sensortag",
        "update-fields",
        "remove-node"
};

/********************************
 * Variables
 ********************************/

//Gateway MasterKey
//static const char *masterKey = "0306ed6be336c532";
//static const char *masterKey = "3855abaacd40ab61"; //UNB test user
extern char masterKey[17];

//UNB Sensor Network URL constants
//static const char *host = "http://192.168.0.15:3000";
//static const char *host = "http://Ecepe2950.ece.unb.ca:3000";
extern char host[50];

//Gateway Id
//static const uint8_t gatewayId = 4;
extern uint8_t gatewayId;

/********************************
 * Data Types
 ********************************/


/********************************
 * Public Methods
 ********************************/
/*
 * appReadSensor, TODO
 *
 * cluster = TODO
 * attributes = TODO
 * len = TODO
 * index = TODO
 */
void appReadSensor(int16u cluster, int8u *attributes, int8u len, int8u index);
/*
 * appSensorAddressFound, TODO
 *
 * result = TODO
 */
void appSensorAddressFound(const EmberAfServiceDiscoveryResult *result);
/*
 * appAddTempSensor, TODO
 *
 * result = TODO
 */
void appAddTempSensor(const EmberAfServiceDiscoveryResult *result);
/*
 * config_value, TODO
 *
 * data_buff = TODO
 * commandID = TODO
 */
int config_value(char *buf, Key key, AcclConfiguration *accl, TempConfiguration *temp, AdcConfiguration *adc, char *apiKeyBuffer);
/*
 * UpdateNodeSensorSettings, TODO
 *
 * data_buff = TODO
 * commandID = TODO
 * fdes = TODO
 */
EmberStatus UpdateNodeSensorSettings(char *data_buff, int *commandID, int fdes);
/*
 * prepare_message, used to send messages to nodes (IE. commands for turning on/off sensors and polling rates)
 *
 *
 * nodeId = TODO
 * send_buf = TODO
 * send_len = TODO
 * apiKeyBuffer = TODO
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus prepare_message(int nodeId, int8u *send_buf, int send_len, char *apiKeyBuffer);
/*
 * prepare_message_TiSensor, used to send messages to nodes (IE. commands for turning on/off sensors and polling rates)
 *
 *
 * nodeId = TODO
 * send_buf = TODO
 * command = TODO
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus prepare_message_TiSensor(int nodeId, int8u send_buf, int8u command);
/*
 * getNameAndId, parses command XML for commandId and command name
 *
 * buf = char pointer containing the XML to be parsed
 * commandID = pointer to be assigned the command Id
 * name = pointer to be assigned the command name
 */
void getNameAndId(char *buf, int *commandID, Key *name);
/*
 * prepare_nodeinfo, TODO (Is this even used??)
 *
 * filename = TDOD
 */
//void prepare_nodeinfo(char *filename);
/*
 * ElapsedTime_m, TODO
 *
 * PreviousTime = TODO
 */
unsigned long int ElapsedTime_m(struct timeval PreviousTime);

#endif //MATT_GATEWAY_MATT_BEAGLE_GATEWAY_CALLBACKS_H
