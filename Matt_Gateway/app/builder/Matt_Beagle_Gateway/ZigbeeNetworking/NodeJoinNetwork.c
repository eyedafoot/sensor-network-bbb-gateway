//
// Created by Matthew on 9/16/2017.
//

#include "NodeJoinNetwork.h"
#include "../DataTransferUtil/DataTransferUtil.h"
#include "../GeneralUtil/NodeList.h"

#include "../../../../stack/include/ember-types.h"

boolean attemptJoin(AppSensor* Sensors, int16u *numSensors, EmberNodeId childId, EmberEUI64 childEui64, EmberEUI64 EmberEUI64[], int16s ListNodeField[][MAX_NUM_NODE_FIELDS]) {
    int i = findSensorListIndex(childId, childEui64);

    if (i >= 0) {
        GeneralDebugPrint("Sensor %d's nodeId: %X\n", i, Sensors[i].nodeId);

        if (appDeviceFound(&Sensors[i], childId, childEui64, ListEuiNode, ListNodeField, (*numSensors))) {
            EmberStatus status;
            status = emberAfEzmodeServerCommissionWithTimeout(emberAfPrimaryEndpoint(), 10);
            if (status == EMBER_BAD_ARGUMENT) {
                GeneralDebugPrint("EMBER_BAD_ARGUMENT");
                return FALSE;
            }
            return TRUE;
        }
    }
    return FALSE;
}

boolean appDeviceFound(AppSensor* sensor, EmberNodeId emberNodeId, EmberEUI64 address, EmberEUI64 ListEuiNode[], int16s ListNodeField[][MAX_NUM_NODE_FIELDS], int16u numSensors) {
    // Find the table index for the sensor we just found the long address of
    int des = -1;

    if (sensor == NULL) {
        GeneralDebugPrint("null sensor");
        requestToJoinNetwork(address, des);
        return FALSE;
    }

    int8u NodeIdTable;
    NodeIdTable = CheckListEuiNode(address);

    if (NodeIdTable != 0xff) {
        emberAfAppPrintln(" i got right :%d", NodeIdTable);

        if (sensor->index == EMBER_NULL_ADDRESS_TABLE_INDEX || emberAfPluginAddressTableLookupByEui64(sensor->address) == EMBER_NULL_ADDRESS_TABLE_INDEX) {
            sensor->index = emberAfPluginAddressTableAddEntry(address, (EmberNodeId) sensor->nodeId);
        }
        emberAfAppPrintln(" i got address table index :%d -- Node Number: %d", sensor->index, sensor->nodeNumber );
        GeneralDebugPrint("Node Number: %d\n", NodeIdTable);
        return TRUE;
    } else {
        emberAfAppPrintln(" i got wrong, address: %x%x%x%x%x%x%x%x", address[0], address[1], address[2], address[3], address[4], address[5], address[6], address[7]);

        //Check who is allowed on the network
        checkServerForNodeList(ListEuiNode, ListNodeField, numSensors, des);

        //check list again to confirm it wasn't added online
        NodeIdTable = CheckListEuiNode(address);

        if (NodeIdTable != 0xff) {
            emberAfAppPrintln(" i got right :%d", NodeIdTable);

            if (sensor->index == EMBER_NULL_ADDRESS_TABLE_INDEX || emberAfPluginAddressTableLookupByEui64(sensor->address) == EMBER_NULL_ADDRESS_TABLE_INDEX) {
                sensor->index = emberAfPluginAddressTableAddEntry(address, (EmberNodeId) sensor->nodeId);
            }
            emberAfAppPrintln(" i got address table index :%d", sensor->index );
            GeneralDebugPrint("Node Number: %d\n", NodeIdTable);
            return TRUE;
        } else {
            //TODO: create list to hold items that have already requested to join network (so we don't keep sending requests for no reason).
            //Send Requestion to join network
            requestToJoinNetwork(address, des);

            emberLeaveRequest(sensor->nodeId, emptyEmberEUI64, LEAVE_REQUEST_REMOVE_CHILDREN_FLAG, EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY|EMBER_APS_OPTION_DESTINATION_EUI64);
            sensor -> index = EMBER_NULL_ADDRESS_TABLE_INDEX;
            return FALSE;
        }
    }
}

void requestToJoinNetwork(EmberEUI64 address, int fdes) {
    char *addressStr = malloc(40);
    strcpy(addressStr, "");
    int8u *p;
    int8u j;
    p = (int8u *) address;
    for (j = 0; j < EUI64_SIZE; j++) {
        char addressStrBuf[3];
        sprintf(addressStrBuf, "%X",p[j]);
        strcat(addressStr, addressStrBuf);
        if ( j < 7 ) {
            char comma[2] = ",";
            strcat(addressStr, comma);
        }
    }

    char urlBuf[255];
    sprintf(urlBuf, "%s%s/approvalrequest.xml?master_key=%s&gateway_id=%d&address=%s", host, DEVICES_PATH, masterKey, gatewayId, addressStr);

    EmberStatus status;
    RestMethod method = PUT;
    if (fdes < 0) {
        status = sendDataToServer(urlBuf, (char)0, method);
    } else {
        status = sendDataToServer_Cellular(urlBuf, (char)0, method, fdes);
    }

    emberAfAppPrintln("Node Requested Access (Address): %s", addressStr);

    free(addressStr);
}

void checkServerForNodeList(EmberEUI64 ListEuiNode[], int16s ListNodeField[][MAX_NUM_NODE_FIELDS], int16u numSensors, int fdes) {
    EmberStatus status = !EMBER_SUCCESS;
    char urlBuf[255];
    RestMethod method = GET;
    struct string recievedData;
    init_string(&recievedData);

    sprintf(urlBuf, "%s%s.xml?master_key=%s&gateway_id=%d", host, DEVICES_PATH, masterKey, gatewayId);
    GeneralDebugPrint("%s\n", urlBuf);
    if (fdes < 0) {
        status = getDataFromServer(urlBuf, &recievedData, method);
    } else {
        status = getDataFromServer_Cellular(urlBuf, &recievedData, method, fdes);
    }

    if (status == EMBER_SUCCESS) {
        char *ptr = recievedData.ptr;
        while (ptr != NULL) {
            //Check if there are no more devices
            if (strstr(ptr,"<device>") == NULL) {
                break;
            }

            int nodeId = -1;

            char *pfound = strstr(ptr, "id");

            if (pfound != NULL) {
                pfound += 18;

                nodeId = atoi(pfound);
            }

            GeneralDebugPrint("Node Id: %d\n", nodeId);
            char *buffer = malloc(40);
            strcpy(buffer, "");
            pfound = strstr(ptr, "<address>");
            char *pfoundend = strstr(ptr, "</address>");
            if (pfound != NULL) {

                pfound += 9;
                int len = pfoundend-pfound;
                memcpy(buffer, pfound, len);
            }

            int8u nodeNumber = 0xff;
            GeneralDebugPrint("Address: %s\n", buffer);
            if (nodeId >= 0) {
                const char comma[2] = ",";
                char *token;
                token = strtok (buffer,comma);

                EmberEUI64 addressBuf;
                int i = 0;

                while (token != NULL) {
                    int number = (int)strtol(token, NULL, 16);
                    addressBuf[i] = number;
                    i += 1;
                    token = strtok(NULL, comma);
                }
                nodeNumber = CheckListEuiNode(addressBuf);
                if (nodeNumber == 0xff) {
                    int16s index = NextListEuiNode();
                    GeneralDebugPrint("New Node Eui Index %d\n", index);
                    if (index != -1) {
                        emberAfAppPrintln("Adding Address : %x%x%x%x%x%x%x%x\n", addressBuf[0], addressBuf[1],
                                          addressBuf[2], addressBuf[3], addressBuf[4], addressBuf[5], addressBuf[6],
                                          addressBuf[7]);
                        int x = 0;
                        for (x; x < EUI64_SIZE; x++) {
                            ListEuiNode[index][x] = addressBuf[x];
                        }
                        nodeNumber = index;
                    }
                }
            }

            emberAfAppPrintln("Checking Fields");
            setNodeNumberByAddress(ListEuiNode[nodeNumber], nodeId);
            emberAfAppPrintln("ResetFieldIds");
            resetFieldIds(nodeId);
            emberAfAppPrintln("Parse xml for fields");
            if (strstr(ptr, "</device>") != NULL && strstr(ptr, "<field>") != NULL && strstr(ptr, "</device>") - strstr(ptr, "<field>") > 0) {
                while (strstr(ptr, "<field>") != NULL) {
                    pfound = strstr(ptr, "<field>");
                    pfound = strstr(pfound, "id");
                    pfound += 18;

                    int16s fieldId = (int16s) atoi(pfound);
                    if (strstr(pfound,  "<field-type>") != NULL) {
                        pfound = strstr(pfound, "<field-type>");
                        if (strstr(ptr, "</field>") - pfound > 0) {
                            pfoundend = strstr(pfound, "</field-type>");
                            pfound += 12;

                            int len = pfoundend - pfound;
                            char *fieldType = malloc(len + 1);
                            memset(fieldType, 0, len + 1);
                            memcpy(fieldType, pfound, len);

                            emberAfAppPrintln("Field NodeId: %d", nodeId);
                            emberAfAppPrintln("Field FieldId: %d", fieldId);
                            emberAfAppPrintln("Field Type: %s", fieldType);
                            if (strcmp(fieldType, "temp") == 0) {
                                updateFieldId((int8u) nodeId, NODE_FIELD_TEMP, fieldId);
                            } else if (strcmp(fieldType, "x-accel") == 0) {
                                updateFieldId((int8u) nodeId, NODE_FIELD_X_ACCEL, fieldId);
                            } else if (strcmp(fieldType, "y-accel") == 0) {
                                updateFieldId((int8u) nodeId, NODE_FIELD_Y_ACCEL, fieldId);
                            } else if (strcmp(fieldType, "z-accel") == 0) {
                                updateFieldId((int8u) nodeId, NODE_FIELD_Z_ACCEL, fieldId);
                            }

                            pfound = pfoundend;
                            pfound += 13;

                            free(fieldType);
                        }
                    }

                    //find if we are done fields for this node
                    int nextFieldPosition = strstr(pfound, "<field>") - pfound;
                    int endOfFieldsPosition = strstr(pfound, "</fields>") - pfound;
                    emberAfAppPrintln("nextFieldPosition: %d", nextFieldPosition);
                    emberAfAppPrintln("endOfFieldsPosition: %d", endOfFieldsPosition);
                    if (nextFieldPosition > endOfFieldsPosition) {
                        emberAfAppPrintln("No more Fields");
                        break;
                    }

                    pfound = strstr(ptr, "</field>");
                    pfound += 8;

                    ptr = pfound;
                }
            }
            int z;
            for (z = 0; z < 8; z++) {
                int y;
                emberAfAppPrint("{");
                for (y = 0; y < 5; y++) {
                    emberAfAppPrint("%d,",ListNodeField[z][y]);
                }
                emberAfAppPrintln("}");
            }

            pfound = strstr(ptr, "</device>");
            pfound += 9;

            ptr = pfound;

            free(buffer);
        }
    }

    free(recievedData.ptr);
}