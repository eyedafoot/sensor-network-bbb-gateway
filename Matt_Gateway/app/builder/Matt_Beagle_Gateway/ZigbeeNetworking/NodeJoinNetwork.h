//
// Created by Matthew on 9/16/2017.
//

#ifndef MATT_GATEWAY_JOIN_NETWORK_H
#define MATT_GATEWAY_JOIN_NETWORK_H

/********************************
 * Dependancies
 ********************************/
#include "../Matt_Beagle_Gateway.h"
#include "../Matt_Beagle_Gateway_callbacks.h"

#include "app/framework/include/af.h"
#include <stdio.h>
#include <stdlib.h>

/********************************
 * Constants
 ********************************/

/********************************
 * Data Types
 ********************************/

/********************************
 * Public Methods
 ********************************/

/*
* SendDataToServer, used to send data to server (either POST or PUT)
*
* urlBuffer = URL plus data appended using ?data=value&data2=value2
* dataBuffer = data to be send in POST (NOTE: put (char)0 if using PUT)
* restMethod = enum for type of REST operation being preformed (POST or PUT)
*
* return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
*/
boolean attemptJoin(AppSensor* Sensors, int16u *numSensors, EmberNodeId childId, EmberEUI64 childEui64, EmberEUI64 ListEuiNode[], int16s ListNodeField[][MAX_NUM_NODE_FIELDS]);
/*
 * appDeviceFound, TODO
 *
 * emberNodeId = TODO
 * extendPanId = TODO
 */
boolean appDeviceFound(AppSensor* sensor, EmberNodeId emberNodeId, EmberEUI64 address, EmberEUI64 ListEuiNode[], int16s ListNodeField[][MAX_NUM_NODE_FIELDS], int16u numSensors);
/*
 * sendDataToServer_Cellular, used to send data to server using cellular network
 *
 * data = TODO
 * fdes = TODO
 * commandID = TODO
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
void requestToJoinNetwork(EmberEUI64 address, int fdes);
/*
 * checkServerForNodeList, used to request device list from server and to update the list of allowed devices accordingly.
 *                         This function also updates the list of fieldIds for nodes
 *
 * fdes = TODO
 */
void checkServerForNodeList(EmberEUI64 ListEuiNode[], int16s ListNodeField[][MAX_NUM_NODE_FIELDS], int16u numSensors, int fdes);

#endif //MATT_GATEWAY_JOIN_NETWORK_H
