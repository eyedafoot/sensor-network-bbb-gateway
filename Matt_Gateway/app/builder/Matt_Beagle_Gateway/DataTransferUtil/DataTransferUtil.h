#ifndef __DATA_TRANSFER_UTIL__
#define __DATA_TRANSFER_UTIL__

/********************************
 * Dependancies
 ********************************/
#include "app/framework/include/af.h"
#include <stdio.h>
#include <stdlib.h>

#include "app/builder/Matt_Beagle_Gateway/GeneralUtil/DataTypes.h"

/********************************
 * Constants
 ********************************/
 //UNB Sensor Network URL constants
#define DATA_PATH "/mobileapi/data"
#define DATAFILE_PATH "/mobileapi/datafile"
#define COMMAND_PATH "/mobileapi/command"
#define DEVICES_PATH "/mobileapi/devices"

/********************************
 * Data Types
 ********************************/
//Rest method types
typedef enum RestMethods {
    GET = 0,
    POST = 1,
    PUT = 2
} RestMethod;

/********************************
 * Public Methods
 ********************************/

/*
 * SendDataToServer, used to send data to server (either POST or PUT)
 * 
 * urlBuffer = URL plus data appended using ?data=value&data2=value2
 * dataBuffer = data to be send in POST (NOTE: put (char)0 if using PUT)
 * restMethod = enum for type of REST operation being preformed (POST or PUT)
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus sendDataToServer(char *urlBuffer, struct string *dataBuffer, RestMethod restMethod);
/*
 * sendDataFileToServer, used to send data files to server (POST only)
 *
 * urlBuffer = URL plus data appended using ?data=value&data2=value2
 * dataBuffer = data to be send in POST
 * fileName = name of file to be uploaded.
 * restMethod = enum for type of REST operation being preformed (POST only)
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus sendDataFileToServer(char *urlBuffer, struct string *dataBuffer, char *fileName, RestMethod restMethod);
/*
 * GetDataFromServer, used to get data from server
 * 
 * urlBuffer = URL plus data appended using ?data=value&data2=value2
 * dataRecieved = pointer to string struct to store the recieved data
 * restMethod = enum for type of REST operation being preformed (GET)
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus getDataFromServer(char *urlBuffer, struct string *dataRecieved, RestMethod restMethod);
/*
 * sendDataToServer_Cellular, used to send data to server using cellular network
 *
 * urlBuffer = TODO
 * dataBuffer = TODO
 * commandID = TODO
 * restMethod = TODO
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus sendDataToServer_Cellular (char *urlBuffer, struct string *dataBuffer, RestMethod restMethod, int fdes);
/*
 * getDataFromServer_Cellular, used to get data from server using cellular network
 *
 * urlBuffer = TODO
 * dataRecieved = TODO
 * RestMethod = TODO
 * fdes = TODO
 *
 * return = EmberStatus (Either EMBER_SUCCESS or !EMBER_SUCCESS)
 */
EmberStatus getDataFromServer_Cellular(char *urlBuffer, struct string *dataRecieved, RestMethod restMethod, int fdes);
/*
 * cell_init, used to initialize using cellular network
 *
 * return = 1 if successful, 0 if failed
 */
int cell_init(void);

/********************************
 * Shared Private Methods
 ********************************/
/*
 * _writefunc, TODO
 *
 * ptr = TODO
 * size = TODO
 * nmemb = TODO
 * s = TODO
 */
size_t _writefunc(void *ptr, size_t size, size_t nmemb, struct string *s);

#endif // __DATA_TRANSFER_UTIL__