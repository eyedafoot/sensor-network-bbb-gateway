#include "DataTransferUtil.h"

#include "app/framework/include/af.h"
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include "../GeneralUtil/DataTypes.h"

EmberStatus sendDataToServer(char *urlBuffer, struct string *dataBuffer, RestMethod restMethod) {
    
    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == PUT || restMethod == POST) {
        CURL *curl;
        CURLcode res;

        /* get a curl handle */
        curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();
        if (curl) {
            struct curl_slist *headers = NULL;
            /* we want to use our own read function */
            /* specify target */
            curl_easy_setopt(curl, CURLOPT_URL, urlBuffer);

            if (restMethod == PUT) {
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
            } else if (restMethod == POST) {
                headers = curl_slist_append(headers, "Content-Type: application/json");
                curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
                curl_easy_setopt(curl, CURLOPT_POST, 1L);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, dataBuffer->ptr);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (curl_off_t)dataBuffer->len);
            }




            /* Now run off and do what you've been told! */
            GeneralDebugPrint("%s: ",  urlBuffer);
            res = curl_easy_perform(curl);

            /* check for errors */
            if (res != CURLE_OK) {
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
                GeneralDebugPrint("URL: %s\n", urlBuffer);
            }else {
                status = EMBER_SUCCESS;
            }

            /* always cleanup */
            curl_slist_free_all(headers);
            curl_easy_cleanup(curl);
            /* we're done with libcurl, so clean it up */
            curl_global_cleanup();
        }
    }

    return status;
}

EmberStatus sendDataFileToServer(char *urlBuffer, struct string *dataBuffer, char *fileName, RestMethod restMethod) {
    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod != POST) {
        return status;
    }

    char readBuffer[200];

    CURL *curl;
    CURLcode res;

    struct curl_httppost *formpost = NULL;
    struct curl_httppost *lastptr = NULL;
    struct curl_slist *headerlist = NULL;
    static const char buf[] = "Expect:";

    curl_global_init(CURL_GLOBAL_ALL);

/* Fill in the file upload field */
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "sendfile",
                 CURLFORM_FILE, fileName,
                 CURLFORM_END);

/* Fill in the filename field */
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "filename",
                 CURLFORM_COPYCONTENTS, fileName,
                 CURLFORM_END);
/* Fill in the submit field too, even if this is rarely needed */
    curl_formadd(&formpost,
                 &lastptr,
                 CURLFORM_COPYNAME, "submit",
                 CURLFORM_COPYCONTENTS, "send",
                 CURLFORM_END);

    curl = curl_easy_init();
/* initialize custom header list (stating that Expect: 100-continue is     not
   wanted */
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
/* what URL that receives this POST */
        curl_easy_setopt(curl, CURLOPT_URL, urlBuffer);
        if (res != CURLE_OK) {
            printf("could not set url : %s\n", curl_easy_strerror(res));
        } else {
            printf("able to set\n");
        }
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
//        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, dataBuffer->ptr);
//        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (curl_off_t)dataBuffer->len);

/* Perform the request, res will get the return code */
        printf("calling curl_easy_perform\n");
        res = curl_easy_perform(curl);
/* Check for errors */
        if (res != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
        } else {
            //TODO: should check server reply for success...
            status = EMBER_SUCCESS;
        }
        printf("after calling curl_easy_perform\n");
/* always cleanup */
        curl_easy_cleanup(curl);

/* then cleanup the formpost chain */
        curl_formfree(formpost);
    }
    curl_global_cleanup();
    return status;
}

EmberStatus getDataFromServer(char *urlBuffer, struct string *dataRecieved, RestMethod restMethod) { 

    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == GET) {
        CURL *curl;
        CURLcode res;

        /* get a curl handle */
        curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();
        if (curl) {

            /* specify target */
            curl_easy_setopt(curl, CURLOPT_URL, urlBuffer);

            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _writefunc);

            /* now specify which file to write to */
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, dataRecieved);
            // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);

            /* Now run off and do what you've been told! */
            res = curl_easy_perform(curl);

            /* check for errors */
            if (res != CURLE_OK) {
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));
                GeneralDebugPrint("URL: %s\n", urlBuffer);
            } else {
                status = EMBER_SUCCESS;
            }

            /* always cleanup */
            curl_easy_cleanup(curl);
            /* we're done with libcurl, so clean it up */
            curl_global_cleanup();
        }
    }
    
    return status;
}
