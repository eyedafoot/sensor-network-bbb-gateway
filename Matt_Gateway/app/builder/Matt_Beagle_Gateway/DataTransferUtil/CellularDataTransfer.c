#include "DataTransferUtil.h"
#include "../GeneralUtil/DataTypes.h"
#include <curl/curl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/time.h>

#define DEVICE_PORT "/dev/ttyO2" // ttyS0 for linux

/*!
    \brief      Initialise the timer. It writes the current time of the day in the structure PreviousTime.
*/
//Initialize the timer
struct timeval InitTimer()
{  struct timeval PreviousTime;
    gettimeofday(&PreviousTime, NULL);
    return(PreviousTime);
}

/********************************
 * Private Methods Declaration
 ********************************/
void cell_reset(int des);
int commandtimeout(double waittime, char* letter, int fdes);
int SendSerialString(int fdes,char *buffer);
int OpenSerial(char *port);
int serialib_ReadChar(int fdes,char *pByte,unsigned int TimeOut_ms);
int serialib_ReadStringNoTimeOut(int fdes,char *String,char FinalChar,unsigned int MaxNbBytes);
int serialib_ReadString(int fdes, char *String,char FinalChar,unsigned int MaxNbBytes,unsigned int TimeOut_ms);
int serialib_Read (int fdes,void *Buffer,unsigned int MaxNbBytes,unsigned int TimeOut_ms);
void serialib_FlushReceiver(int fdes);
int serialib_Peek(int fdes);
unsigned long int ElapsedTime_ms(struct timeval PreviousTime );
int waitFor(int fdes,char* waitLetter,int timer);
void delay(double timeout);

/********************************
 * Public Methods
 ********************************/
EmberStatus sendDataToServer_Cellular (char *urlBuffer, struct string *dataBuffer, RestMethod restMethod, int fdes) {

    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == PUT || restMethod == POST) {
        char data_send[50];
        char sense_data[1460];
        char dataRecieved[200];
        size_t nbytes;
        ssize_t bytes_read;
        int data_length;

        if (restMethod == PUT) {
            sprintf(sense_data, "PUT %s HTTP/1.0\x0D\x0A", urlBuffer);
            char *collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";

            strcat(sense_data, collect);
        } else if (restMethod == POST) {
            char* post = "";
            sprintf(post, "POST %s HTTP/1.0\x0D\x0A", urlBuffer);
            char* contentType = "Content-Type: text/xml; charset=utf-8\x0D\x0A";
            char* contentLength = "";
            sprintf(contentLength, "Content-Length: %d\x0D\x0A", dataBuffer->len);
            char *collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";
            
            sprintf(sense_data, "%s", post);
            strcat(sense_data, contentType);
            strcat(sense_data, contentLength);
            strcat(sense_data, collect);
            strcat(sense_data, dataBuffer->ptr);

            /*Data Transmission Format:
             *POST host:port/path.xml?query HTTP/1.0
             *Content-Type: text/xml; charset=utf-8 //Data can be of a different type//
             *Content-Length: 88
             *Connection: Keep-Alive
             * //Data goes here, format must match content-type//
             *<?xml version="1.0" encoding="utf-8"?>
             *<string xmlns="http://clearforest.com/">string</string>
            */
        }
        data_length = strlen(sense_data) - 1;
        // GeneralDebugPrint ("The buffer is is %s \n",sense_data);

        if (data_length >= 1460) {
            return status;
        }

        sprintf(data_send, "AT+SDATATSEND=1,%d\r", data_length);
        SendSerialString(fdes, data_send);
        GeneralDebugPrint("Sending data...\n");

        if (commandtimeout(5, ">", fdes) == 0) {
            GeneralDebugPrint("Sending data failed.. \n");
            fdes = cell_init();
            status = sendDataToServer_Cellular(urlBuffer, dataBuffer, restMethod, fdes);
            return status;
        }

        SendSerialString(fdes, sense_data);
        serialib_FlushReceiver(fdes);
        if (commandtimeout(40, "STCPD", fdes) == 0) {
            GeneralDebugPrint("Sending data failed.. \n");
            fdes = cell_init();
            status = sendDataToServer_Cellular(urlBuffer, dataBuffer, restMethod, fdes);
            return status;
        } else {
            GeneralDebugPrint("Data Sent.. \n");

            SendSerialString(fdes, "AT+SDATATREAD=1");
            bytes_read = read(fdes, dataRecieved, nbytes);

        }

        SendSerialString(fdes, "AT+SDATASTATUS=0\r\n");
        int y = commandtimeout(5, "SOCK", fdes);
    }
    return status;

}

EmberStatus getDataFromServer_Cellular(char *urlBuffer, struct string *dataRecieved, RestMethod restMethod, int fdes) {

    EmberStatus status = !EMBER_SUCCESS;

    if (restMethod == GET) {

        char data_send[50];
        char sense_data[1460];
        size_t nbytes;
        ssize_t bytes_read;
        int data_length;
        char *ptr;

        sprintf(sense_data, "GET %s HTTP/1.0\x0D\x0A", urlBuffer);
        char *collect = "Connection: Keep-Alive\x0D\x0A\x0D\x0A\x1A";
        data_length = strlen(sense_data) + strlen(collect) - 1;
        // GeneralDebugPrint ("The buffer is is %s \n",sense_data);

        sprintf(data_send, "AT+SDATATSEND=1,%d\r", data_length);
        SendSerialString(fdes, data_send);
        GeneralDebugPrint("Sending data...\n");

        if (commandtimeout(5, ">", fdes) == 0) {
            GeneralDebugPrint("Sending data failed.. \n");
            fdes = cell_init();
            status = getDataFromServer_Cellular(urlBuffer, dataRecieved, restMethod, fdes);
            return status;
        }

        SendSerialString(fdes, sense_data);
        SendSerialString(fdes, collect);
        serialib_FlushReceiver(fdes);
        if (commandtimeout(40, "STCPD", fdes) == 0) {
            GeneralDebugPrint("Sending data failed.. \n");
            fdes = cell_init();
            status = getDataFromServer_Cellular(urlBuffer, dataRecieved, restMethod, fdes);
            return status;
        } else {
            GeneralDebugPrint("Data Sent.. \n");

            SendSerialString(fdes, "AT+SDATATREAD=1");
            bytes_read = read(fdes, ptr, nbytes);
            _writefunc(ptr, bytes_read, nbytes, dataRecieved);

        }

        SendSerialString(fdes, "AT+SDATASTATUS=0\r\n");
        int y = commandtimeout(5, "SOCK", fdes);
    }
    return status;

}

int cell_init(void)
{
    int des; // Used for return values

    char* apn ="internet.com";   // access-point name for GPRS
    char* ip = "47.55.87.129";   //IP address of the server

    des= OpenSerial(DEVICE_PORT);                                     // Open serial port
    if (des < 0) {                                                    // If an error occured...
        GeneralDebugPrint ("Error while opening port. Permission problem ?\n");  // ... display a message ...
        return des;                                                   // ... quit the application
    }
    GeneralDebugPrint ("Serial port opened successfully: \n");
    serialib_FlushReceiver(des);

    SendSerialString(des,"AT+CGATT?\r\n");
    GeneralDebugPrint ("Attaching GPRS.. \n");
    if (commandtimeout(5,"+CGATT: 1",des)== 0) {
        GeneralDebugPrint ("Attaching GPRS failed.. \n");
        GeneralDebugPrint ("Trying Connection again.. \n");
        cell_reset(des);
    }

    SendSerialString(des,"AT+CGDCONT=1,\"IP\",\"internet.com\"\r\n");
    GeneralDebugPrint ("Setting up PDP Context... \n");
    if (commandtimeout(5,"OK",des)== 0) {
        GeneralDebugPrint ("Setting up PDP Context failed.. \n");
    }

    SendSerialString(des,"AT+CGPCO=0,\"wapuser1\",\"wap\",1\r\n");
    GeneralDebugPrint ("Setting up PDP Password... \n");
    if (commandtimeout(5,"OK",des)== 0) {
        GeneralDebugPrint ("Setting up PDP Password failed.. \n");
    }


    SendSerialString(des,"AT+CGACT=1,1\r\n");
    GeneralDebugPrint ("Activating PDP Context... \n");
    if (commandtimeout(5,"OK",des)== 0) {
        GeneralDebugPrint ("Activating PDP Context failed.. \n");
    }

    serialib_FlushReceiver(des);
    char ipConnection[80];
    sprintf(ipConnection,"AT+SDATACONF=1,\"TCP\",\"%s\",8080\r\n",ip);
    SendSerialString(des,ipConnection);
    GeneralDebugPrint ("Configuring TCP connection to TCP Server...\n");
    if (commandtimeout(5,"OK",des)== 0) {
        GeneralDebugPrint ("Configuring TCP connection to TCP Server failed.. \n");
    }

    SendSerialString(des,"AT+SDATASTART=1,1\r\n");
    GeneralDebugPrint ("Starting TCP Connection...\n");
    if (commandtimeout(5,"OK",des)== 0) {
        GeneralDebugPrint ("Starting TCP Connection failed.. \n");
    }

    delay(1);

// checking the socket status and only breaking when we connect
    GeneralDebugPrint("Checking socket status:\n");
    SendSerialString(des,"AT+SDATASTATUS=1\r\n");

    if (commandtimeout(5,"102",des) == 0)  {
        GeneralDebugPrint("Socket fails to connect.\n");
        cell_reset(des);
    }
    else {
        GeneralDebugPrint("Socket connected\n");
    }

    serialib_FlushReceiver(des);
    return des;

}


/********************************
 * Private Methods
 ********************************/
void cell_reset(int des)
{
    GeneralDebugPrint("Resetting the cellular device\n");
    SendSerialString(des,"AT+CFUN=1,1\r\n");
    delay(10);
    if (commandtimeout(50,"SIND: 4",des) == 0) {
        cell_reset(des);}

    cell_init();
}

int commandtimeout(double waittime, char* letter, int fdes)
{
    int t =0;
    struct timeval tp;
    double startsecs,secs;
    gettimeofday(&tp,NULL);
    startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;
    while (t != 1) {
        t = waitFor(fdes,letter,100);
        gettimeofday(&tp,NULL);
        secs = tp.tv_sec + tp.tv_usec / 1000000.0;
        if (secs-startsecs > waittime) {
            return 0;}
    }
    return 1;
}

/*
   Write a string to the serial port device
*/
int SendSerialString(int fdes,char *buffer)
{
    int len;

    len = strlen(buffer);

    if (write(fdes,buffer,len) != len)
        return(FALSE);
    return(TRUE);
}

/*
	Attempt to open the serial port
	Return the file descriptor
*/
int OpenSerial(char *port)
{
    int fdes;

    if ((fdes = open(port,O_RDWR | O_NONBLOCK)) < 0) {
        perror("OpenSerial");
    }

    int n;
    struct termios options; //,options2;
    memset(&options,0,sizeof(options));
    options.c_iflag=0;
    options.c_oflag=0;
    options.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
    options.c_lflag=0;
    options.c_cc[VMIN]=1;
    options.c_cc[VTIME]=5;
    cfsetispeed(&options,B115200);
    cfsetospeed(&options,B115200);
    tcsetattr(fdes,TCSANOW,&options);
    return fdes;
}

/*!
     \brief Wait for a byte from the serial device and return the data read
     \param pByte : data read on the serial device
     \param TimeOut_ms : delay of timeout before giving up the reading
            If set to zero, timeout is disable (Optional)
     \return 1 success
     \return 0 Timeout reached
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
  */
int serialib_ReadChar(int fdes,char *pByte,unsigned int TimeOut_ms)
{

    struct timeval         Timer;                                              // Timer used for timeout
    Timer = InitTimer();                                                  // Initialise the timer
    while (ElapsedTime_ms(Timer)<TimeOut_ms || TimeOut_ms==0)          // While Timeout is not reached
    {
        switch (read(fdes,pByte,1)) {                                     // Try to read a byte on the device
            case 1  : return 1;                                             // Read successfull
            case -1 : return -2;                                            // Error while reading
        }
    }
    return 0;

}

/*!
     \brief Read a string from the serial device (without TimeOut)
     \param String : string read on the serial device
     \param FinalChar : final char of the string
     \param MaxNbBytes : maximum allowed number of bytes read
     \return >0 success, return the number of bytes read
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
     \return -3 MaxNbBytes is reached
  */
int serialib_ReadStringNoTimeOut(int fdes,char *String,char FinalChar,unsigned int MaxNbBytes)
{
    unsigned int    NbBytes=0;                                          // Number of bytes read
    char            ret;                                                // Returned value from Read
    while (NbBytes<MaxNbBytes)                                          // While the buffer is not full
    {                                                                   // Read a byte with the restant time
        ret=serialib_ReadChar(fdes,&String[NbBytes],0);
        if (ret==1)                                                     // If a byte has been read
        {
            if (String[NbBytes]==FinalChar)                             // Check if it is the final char
            {
                String  [++NbBytes]=0;                                  // Yes : add the end character 0
                return NbBytes;                                         // Return the number of bytes read
            }
            NbBytes++;                                                  // If not, just increase the number of bytes read
        }
        if (ret<0) return ret;                                          // Error while reading : return the error number
    }
    return -3;                                                          // Buffer is full : return -3
}

/*!
     \brief Read a string from the serial device (with timeout)
     \param String : string read on the serial device
     \param FinalChar : final char of the string
     \param MaxNbBytes : maximum allowed number of bytes read
     \param TimeOut_ms : delay of timeout before giving up the reading (optional)
     \return  >0 success, return the number of bytes read
     \return  0 timeout is reached
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
     \return -3 MaxNbBytes is reached
  */
int serialib_ReadString(int fdes, char *String,char FinalChar,unsigned int MaxNbBytes,unsigned int TimeOut_ms)
{
    if (TimeOut_ms==0)
        return serialib_ReadStringNoTimeOut(fdes,String,FinalChar,MaxNbBytes);

    unsigned int    NbBytes=0;                                          // Number of bytes read
    char            ret;                                                // Returned value from Read
    struct timeval         Timer;                                          // Timer used for timeout
    Timer = InitTimer();                                                  // Initialise the timer
    long int        TimeOutParam;

    while (NbBytes<MaxNbBytes)                                          // While the buffer is not full
    {                                                                   // Read a byte with the restant time
        TimeOutParam=TimeOut_ms- ElapsedTime_ms(Timer);                 // Compute the TimeOut for the call of ReadChar
        if (TimeOutParam>0)                                             // If the parameter is higher than zero
        {
            ret= serialib_ReadChar(fdes,&String[NbBytes],TimeOutParam);                // Wait for a byte on the serial link
            if (ret==1)                                                 // If a byte has been read
            {

                if (String[NbBytes]==FinalChar)                         // Check if it is the final char
                {
                    String  [++NbBytes]=0;                              // Yes : add the end character 0
                    return NbBytes;                                     // Return the number of bytes read
                }
                NbBytes++;                                              // If not, just increase the number of bytes read
            }
            if (ret<0) return ret;                                      // Error while reading : return the error number
        }
        if (ElapsedTime_ms(Timer)>TimeOut_ms) {                        // Timeout is reached
            String[NbBytes]=0;                                          // Add the end caracter
            return NbBytes;                                                   // Return 0
        }
    }
    return -3;                                                          // Buffer is full : return -3
}

/*!
     \brief Read an array of bytes from the serial device (with timeout)
     \param Buffer : array of bytes read from the serial device
     \param MaxNbBytes : maximum allowed number of bytes read
     \param TimeOut_ms : delay of timeout before giving up the reading
     \return 1 success, return the number of bytes read
     \return 0 Timeout reached
     \return -1 error while setting the Timeout
     \return -2 error while reading the byte
  */
int serialib_Read (int fdes,void *Buffer,unsigned int MaxNbBytes,unsigned int TimeOut_ms)
{
    struct timeval         Timer;                                          // Timer used for timeout
    Timer = InitTimer();                                                  // Initialise the timer
    unsigned int     NbByteRead=0;
    while (ElapsedTime_ms(Timer)<TimeOut_ms || TimeOut_ms==0)          // While Timeout is not reached
    {
        unsigned char* Ptr=(unsigned char*)Buffer+NbByteRead;           // Compute the position of the current byte
        int Ret=read(fdes,(void*)Ptr,MaxNbBytes-NbByteRead);              // Try to read a byte on the device
        if (Ret==-1) return -2;                                         // Error while reading
        if (Ret>0) {                                                    // One or several byte(s) has been read on the device
            NbByteRead+=Ret;                                            // Increase the number of read bytes
            if (NbByteRead>=MaxNbBytes)                                 // Success : bytes has been read
                return 1;
        }
    }
    return 0;                                                           // Timeout reached, return 0

}

/*!
    \brief Empty receiver buffer
*/

void serialib_FlushReceiver(int fdes)
{

    tcflush(fdes,TCIFLUSH);

}

/*!
    \brief  Return the number of bytes in the received buffer (UNIX only)
    \return The number of bytes in the received buffer
*/
int serialib_Peek(int fdes)
{
    int Nbytes=0;

    ioctl(fdes, FIONREAD, &Nbytes);

    return Nbytes;
}

/*!
    \brief      Returns the time elapsed since initialization.  It write the current time of the day in the structure CurrentTime.
                Then it returns the difference between CurrentTime and PreviousTime.
    \return     The number of microseconds elapsed since the functions InitTimer was called.
  */
//Return the elapsed time since initialization
unsigned long int ElapsedTime_ms(struct timeval PreviousTime )
{
    struct timeval CurrentTime;
    int sec,usec;
    gettimeofday(&CurrentTime, NULL);                                   // Get current time
    sec=CurrentTime.tv_sec-PreviousTime.tv_sec;                         // Compute the number of second elapsed since last call
    usec=CurrentTime.tv_usec-PreviousTime.tv_usec;                      // Compute
    if (usec<0) {                                                       // If the previous usec is higher than the current one
        usec=1000000-PreviousTime.tv_usec+CurrentTime.tv_usec;          // Recompute the microseonds
        sec--;                                                          // Substract one second
    }
    return (sec*1000+usec/1000);
}

int waitFor(int fdes,char* waitLetter,int timer)
{
    char Buffer[128];
    serialib_ReadString(fdes,Buffer,'K',128,timer) ;   // Read a maximum of 128 characters with a timeout

    if (strstr(Buffer, waitLetter) != NULL)
    { //GeneralDebugPrint ("%s \n",Buffer);

        return 1;
    }
    //GeneralDebugPrint ("not receive: %s \n",Buffer);

    return 0;
}

void delay(double timeout)
{
    struct timeval tp;
    double startsecs,secs;

    gettimeofday(&tp,NULL);
    startsecs = tp.tv_sec + tp.tv_usec / 1000000.0;

    for (;;) {

        gettimeofday(&tp,NULL);
        secs = tp.tv_sec + tp.tv_usec / 1000000.0;
        if (secs-startsecs > timeout)
            return;
    }

}