// This callback file is created for your convenience. You may add application code
// to this file. If you regenerate this file over a previous version, the previous
// version will be overwritten and any code you have added will be lost.


#include "Matt_Beagle_Gateway.h"
#include "Matt_Beagle_Gateway_callbacks.h"
#include <curl/curl.h>
#include <time.h>
#include <semaphore.h>
#include <fcntl.h>

#include "NodeConfiguration/configurationEncoding.h"
#include "DataTransferUtil/DataTransferUtil.h"
#include "ZigbeeNetworking/NodeJoinNetwork.h"
#include "../../framework/include/af-types.h"
#include "../../framework/util/print.h"
#include "GeneralUtil/FileReader.h"
#include "GeneralUtil/FileWriter.h"
//#include "../../framework/util/util.h"
//#include "GeneralUtil/NodeList.h"
//#include "../../../stack/include/ember.h"
//#include "../../../stack/include/network-formation.h"
//#include "../../framework/util/common.h"

// Event control struct declaration
EmberEventControl findSensorEventControl;
EmberEventControl querrySensorEventControl;
EmberEventControl printEventControl;

/********************************
 * Variables
 ********************************/

char masterKey[17];
char host[50];
uint8_t gatewayId = -1;

// Globals
#define PJOIN_DURATION_S 20
int des = -1;
int cellular = 0;
int test = 1;

// Offset between the ZigBee epoch and Unix epoch
#define ZIGBEE_EPOCH_OFFSET 946684800
int valuem = 0;
// How frequently (in seconds) we should initiate a sensor service discovery
#define SENSOR_FIND_FREQUENCY 30

#define SENSOR_TEMP 1
#define SENSOR_ACCEL 2
struct timeval wait_time;
int waiting = 1;

time_t testCountTimeStart;
int testCount = -1;

#define MAX_NUM_STORED_DATA 2000
int curNumStoredData = 0;


sem_t * sem_data1_id;
sem_t * sem_data2_id;
char *curDataPath = DATA1_PATH;

#define M_PI 3.14159265358979323846264338327950288

int8_t processedAccelXDataIndex = 0;
int16s processedAccelXDataBuffer[100];
int8_t processedAccelYDataIndex = 0;
int16s processedAccelYDataBuffer[100];
int8_t processedAccelZDataIndex = 0;
int16s processedAccelZDataBuffer[100];


/********************************
 * Functions
 ********************************/

void idct(int N, const float X[], float x[]);

/** @brief Main Init
 *
 * This function is called from the application's main function. It gives the
 * application a chance to do any initialization required at system startup.
 * Any code that you would normally put into the top of the application's
 * main() routine should be put into this function.
        Note: No callback
 * in the Application Framework is associated with resource cleanup. If you
 * are implementing your application on a Unix host where resource cleanup is
 * a consideration, we expect that you will use the standard Posix system
 * calls, including the use of atexit() and handlers for signals such as
 * SIGTERM, SIGINT, SIGCHLD, SIGPIPE and so on. If you use the signal()
 * function to register your signal handler, please mind the returned value
 * which may be an Application Framework function. If the return value is
 * non-null, please make sure that you call the returned function from your
 * handler to avoid negating the resource cleanup of the Application Framework
 * itself.
 *
 */
void emberAfMainInitCallback(void) {

    EmberStatus status = EMBER_SUCCESS;
    status = emberNetworkInit();

    if (status == EMBER_NOT_JOINED) {
        //emberScanForUnusedPanId((int32u) EMBER_ALL_802_15_4_CHANNELS_MASK, (int8u)60);
    }
    status = emberClearKeyTable();
    if (status == EMBER_SUCCESS) {
        GeneralDebugPrint("cleared key table\n");
    }

    if (cellular == 1) {
        des = cell_init();
        cellular = 0;
    }

    int success = getUserVariables();

    if (success) {
        pid_t child_pid;



        child_pid = fork();
        if (child_pid != 0) {
            sem_data1_id = sem_open(SEM_DATA1_PATH, O_CREAT, S_IRUSR | S_IWUSR, 0);
            sem_data2_id = sem_open(SEM_DATA2_PATH, O_CREAT, S_IRUSR | S_IWUSR, 0);

            if (sem_data1_id == SEM_FAILED || sem_data2_id == SEM_FAILED) {
                printf("\n\n\n\ninitialization of sems failed!!\n\n\n\n");
                return 0;
            }

            checkServerForNodeList(ListEuiNode, ListNodeField, numSensors, des);

            emberAfEventControlSetDelay(&findSensorEventControl, 1000);
            emberAfEventControlSetDelay(&querrySensorEventControl, 5000);
            emberAfEventControlSetDelay(&printEventControl, 5000);
        } else {
            sleep(1);
            ServerConnectionMain(des);
        }
    } else {
        GeneralDebugPrint("Failed to read User Variables. Please check the UserVariables.txt.\n");
        exit(0);
    }
}

// Callback
// Indicates that a child has joined or left.
void ezspChildJoinHandler(
        // The index of the child of interest.
        int8u index,
        // True if the child is joining. False the child is leaving.
        boolean joining,
        // The node ID of the child.
        EmberNodeId childId,
        // The EUI64 of the child.
        EmberEUI64 childEui64,
        // The node type of the child.
        EmberNodeType childType) {

    GeneralDebugPrint("Joining:%s - Child Id: 0x%X - chiledEui64: 0x%x%x%x%x%x%x%x%x\n", joining?"true":"false", childId, childEui64[0], childEui64[1], childEui64[2], childEui64[3], childEui64[4], childEui64[5], childEui64[6], childEui64[7]);

    if (joining) {
        attemptJoin(Sensors, &numSensors, childId, childEui64, ListEuiNode, ListNodeField);
    } else {
        int i;
        for (i = 0; i < numSensors; i++) {
            if (Sensors[i].nodeId == childId) {
                if (Sensors[i].index != EMBER_NULL_ADDRESS_TABLE_INDEX) {
                    emberAfPluginAddressTableRemoveEntry(Sensors[i].address);
                    Sensors[i].index = EMBER_NULL_ADDRESS_TABLE_INDEX;
                }
                break;
            }
        }
    }
}

/** @brief Pre ZDO Message Received
 *
 * This function passes the application an incoming ZDO message and gives the
 * appictation the opportunity to handle it. By default, this callback returns
 * FALSE indicating that the incoming ZDO message has not been handled and
 * should be handled by the Application Framework.
 *
 * @param emberNodeId   Ver.: always
 * @param apsFrame   Ver.: always
 * @param message   Ver.: always
 * @param length   Ver.: always
 */
boolean emberAfPreZDOMessageReceivedCallback(EmberNodeId emberNodeId,
                                             EmberApsFrame* apsFrame,
                                             int8u* message,
                                             int16u length)
{
    if (((apsFrame->clusterId) == END_DEVICE_ANNOUNCE)) {
        GeneralDebugPrint("RX END_DEVICE_ANNOUNCE from profile 0x%4x culster 0x%4x sourceEndpoint 0x%2x destinationEndpoint 0x%2x of length 0x%x\r\n",
               apsFrame->profileId,
               apsFrame->clusterId,
               apsFrame->sourceEndpoint,
               apsFrame->destinationEndpoint,
               length);

        GeneralDebugPrint("EmberNodeId: %X\n", emberNodeId);

        EmberEUI64 childEui64;
        EmberStatus lookupStatus = emberLookupEui64ByNodeId(emberNodeId, childEui64);

        int8s sensorListIndex = -1;
        int i;
        for (i = 0; i < numSensors; i++) {
            GeneralDebugPrint("Finding Sensor for nodeId: %x \n Current Node number = %d\n", emberNodeId, Sensors[i].nodeNumber);
            if ((Sensors[i].nodeId == emberNodeId || compareEUI64(Sensors[i].address, childEui64)) && Sensors[i].nodeNumber != DEFAULT_NODE_NUMBER) {
                sensorListIndex = i;
                break;
            }
        }

        if (sensorListIndex >= 0) {
            GeneralDebugPrint("In Sensors list\n");
            return FALSE; //allow it to join
        }
//        attemptJoin(Sensors, numSensors, childId, childEui64, ListEuiNode, ListNodeField);
        //TODO: Allow rejoin incase when controller reboots and node is still connected to network;
        GeneralDebugPrint("Not in Sensors list\n");
        return TRUE;
    }
    else if ((apsFrame->clusterId == ACTIVE_ENDPOINTS_RESPONSE)){
        GeneralDebugPrint("RX ACTIVE_ENDPOINTS_RESPONSE from profile 0x%4x culster 0x%4x of length 0x%x\r\n",
               apsFrame->profileId,
               apsFrame->clusterId,
               length);
    }
    else if ((apsFrame->clusterId == SIMPLE_DESCRIPTOR_RESPONSE)){
        GeneralDebugPrint("RX SIMPLE_DESCRIPTOR_RESPONSE from profile 0x%4x culster 0x%4x of length 0x%x \r\n",
               apsFrame->profileId,
               apsFrame->clusterId,
               length);
    }
    else {
        GeneralDebugPrint("RX from profile 0x%4x culster 0x%4x of length 0x%x\r\n",
               apsFrame->profileId,
               apsFrame->clusterId,
               length);
    }
    return FALSE;
}

/** @brief Pre Command Received
 *
 * This callback is the second in the Application Framework’s message
 * processing chain. At this point in the processing of incoming over-the-air
 * messages, the application has determined that the incoming message is a ZCL
 * command. It parses enough of the message to populate an
 * EmberAfClusterCommand struct. The Application Framework defines this struct
 * value in a local scope to the command processing but also makes it
 * available through a global pointer called emberAfCurrentCommand, in
 * app/framework/util/util.c. When command processing is complete, this
 * pointer is cleared.
 *
 * @param cmd   Ver.: always
 */
boolean emberAfPreCommandReceivedCallback(EmberAfClusterCommand* cmd){
    if (cmd->commandId == ZCL_REPORT_ATTRIBUTES_COMMAND_ID ){

    }
    if ((cmd->commandId==ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) &&
        (cmd->apsFrame->clusterId== ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) &&
        (cmd->bufLen==13)) {

    }
    else if ((cmd->commandId==ZCL_REPORT_ATTRIBUTES_COMMAND_ID ) &&
        (cmd->apsFrame->clusterId== ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) &&
        (cmd->bufLen==18)) {

    }
    else if ((cmd->apsFrame->clusterId==ZCL_IDENTIFY_CLUSTER_ID) &&
             (cmd->commandId==ZCL_IDENTIFY_QUERY_COMMAND_ID)){
        GeneralDebugPrint("HELLO RX OK from EP 0x%2x to EP 0x%2x profile 0x%2x culster 0x%2x of source 0x%2x Command ID 0x%2x\r\n",
               cmd->apsFrame->sourceEndpoint,
               cmd->apsFrame->destinationEndpoint,
               cmd->apsFrame->profileId,
               cmd->apsFrame->clusterId,
               cmd->source,
               cmd->commandId);

        int i = 0;
        EmberNodeId emberNodeId = cmd->source;
        EmberEUI64 childEui64;
        emberLookupEui64ByNodeId(emberNodeId, childEui64);

        GeneralDebugPrint("Node Id: %X  Address: 0x%x%x%x%x%x%x%x%x\n", emberNodeId, childEui64[0], childEui64[1], childEui64[2], childEui64[3], childEui64[4], childEui64[5], childEui64[6], childEui64[7]);

        int8s sensorListIndex = -1;
        for (i = 0; i < numSensors; i++) {
            if ((Sensors[i].nodeId == emberNodeId || compareEUI64(Sensors[i].address, childEui64)) && Sensors[i].nodeNumber != DEFAULT_NODE_NUMBER) {
                sensorListIndex = i;
                break;
            }
        }

        if (sensorListIndex >= 0) {
            GeneralDebugPrint("In Sensors list\n");
            EmberStatus status;
            status = emberAfEzmodeServerCommissionWithTimeout(emberAfPrimaryEndpoint(), 5);
            if (status == EMBER_BAD_ARGUMENT) {
                GeneralDebugPrint("EMBER_BAD_ARGUMENT");
            }
            printf("allow to join");
            return FALSE; //allow it to join
        }
        GeneralDebugPrint("Not in Sensors list\n");

        emberLeaveRequest(emberNodeId, emptyEmberEUI64, LEAVE_REQUEST_REMOVE_CHILDREN_FLAG, EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY|EMBER_APS_OPTION_DESTINATION_EUI64);
//        attemptJoin(Sensors,&numSensors, emberNodeId, childEui64, ListEuiNode, ListNodeField);
        return TRUE;

    }
    else {
        GeneralDebugPrint("Unknown command: RX OK from EP 0x%2x to EP 0x%2x profile 0x%2x culster 0x%2x of source 0x%2x Command ID 0x%2x\r\n",
               cmd->apsFrame->sourceEndpoint,
               cmd->apsFrame->destinationEndpoint,
               cmd->apsFrame->profileId,
               cmd->apsFrame->clusterId,
               cmd->source,
               cmd->commandId);
    }

    return FALSE;
}

// *******************************************************************
// Event functions

void findSensorEvent(void) {
    emberAfEventControlSetDelay( &findSensorEventControl, 1000);

    // Start a service discovery for the sensors we want to monitor
    static int8u sensorDiscoveryCountdown;
    if (sensorDiscoveryCountdown-- <= 0) {
        sensorDiscoveryCountdown = SENSOR_FIND_FREQUENCY;

        if (emberNetworkState() != EMBER_NO_NETWORK) {
            emberAfAppPrintln("Running Discovery");
            emberPermitJoining(PJOIN_DURATION_S);
        }
    } else {

    }
}
/** @brief Pre Message Received
 *
 * This callback is the first in the Application Framework's message
 * processing chain. The Application Framework calls it when a message has
 * been received over the air but has not yet been parsed by the ZCL
 * command-handling code. If you wish to parse some messages that are
 * completely outside the ZCL specification or are not handled by the
 * Application Framework's command handling code, you should intercept them
 * for parsing in this callback.
        This callback returns a Boolean
 * value indicating whether or not the message has been handled. If the
 * callback returns a value of TRUE, then the Application Framework assumes
 * that the message has been handled and it does nothing else with it. If the
 * callback returns a value of FALSE, then the application framework continues
 * to process the message as it would with any incoming message.
        Note:
 *  This callback receives a pointer to an incoming message struct. This
 * struct allows the application framework to provide a unified interface
 * between both Host devices, which receive their message through the
 * ezspIncomingMessageHandler, and SoC devices, which receive their message
 * through emberIncomingMessageHandler.
 *
 * @param incomingMessage   Ver.: always
 */
int8u lastSequence = 0x00;
boolean emberAfPreMessageReceivedCallback(EmberAfIncomingMessage *incomingMessage) {
    int index = -1;
    int i;
    for (i = 0; i < numSensors; i++) {
        if (Sensors[i].nodeId == incomingMessage -> source) {
            if (Sensors[i].nodeNumber != DEFAULT_NODE_NUMBER) {
                index = i;
            }
            break;
        }
    }

    if (index != -1) {
        if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_TEMP_MEASUREMENT_CLUSTER_ID) {
            int16u msgLen = (incomingMessage -> msgLen);
            GeneralDebugPrint("Node Id %X\n", incomingMessage -> source);
            int8u msb = (incomingMessage -> message)[msgLen - 1];
            int8u lsb = (incomingMessage -> message)[msgLen - 2];
            int16_t temp = (msb << 8) | lsb;
            GeneralDebugPrint("msb is %X\n", msb);
            GeneralDebugPrint("lsb is %X\n", lsb);
            int16s dataValue = (int16s) temp;//(incomingMessage -> message));
            int16s fixPart = dataValue / 100;
            int16s fractPart = dataValue - fixPart *100;
            GeneralDebugPrint("Temperature is %d or %d.%d\n", temp, fixPart, fractPart);

            GeneralDebugPrint("Sending temperature data to server................. \n");
            GeneralDebugPrint("Node Id %d\n", Sensors[index].nodeNumber);
            GeneralDebugPrint("Temperature is %d.%d\n", fixPart, fractPart);

            int16s fieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_TEMP);
            if (fieldId > 0) {
                Sensors[index].nodeNumber = CheckListEuiNode(Sensors[index].address);
                fieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_TEMP);
            }
            GeneralDebugPrint("Field Id: %d", fieldId);

            if (fieldId > 0) {
                char urlBuf[110];

                sprintf(urlBuf, "%s%s.xml?master_key=%s&field_id=%u&value=%d.%d", host, DATA_PATH, masterKey, fieldId, fixPart, fractPart);
                EmberStatus status;
                printf("%s\n",urlBuf);
                int commandID = -2;
                //GeneralDebugPrint("%s\n",urlBuf);

                RestMethod method = PUT;
                if (des < 0) {
                    //status = sendDataToServer(urlBuf, NULL, method);
                } else {
                    status = sendDataToServer_Cellular(urlBuf, NULL, method, des);
                }
            }
            return TRUE;
        }

        else if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID) {
            int16s *dataValue = ((int16s *)(incomingMessage -> message));
            int16s Xvalue = dataValue[0];
            int16s Yvalue = dataValue[1];
            int16s Zvalue = dataValue[2];

            GeneralDebugPrint("Sending accelerometer data to server................. \n");
            GeneralDebugPrint("Node Id %d\n", Sensors[index].nodeNumber);

            GeneralDebugPrint("xvalue is %d \n", Xvalue);
            GeneralDebugPrint("Yvalue is %d \n", Yvalue);
            GeneralDebugPrint("Zvalue is %d \n", Zvalue);

            EmberStatus status;
            int commandID = -2;
            char urlBuf[110];
            RestMethod method = PUT;

            int16s fieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_X_ACCEL);
            if (fieldId > 0) {
                sprintf(urlBuf, "%s%s.xml?master_key=%s&field_id=%u&value=%d", host, DATA_PATH, masterKey, fieldId, Xvalue);
                if (des < 0) {
                    status = sendDataToServer(urlBuf, NULL, method);
                } else {
                    status = sendDataToServer_Cellular(urlBuf, NULL, method, des);
                }
            }

            fieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_Y_ACCEL);
            if (fieldId > 0) {
                sprintf(urlBuf, "%s%s.xml?master_key=%s&field_id=%u&value=%d", host, DATA_PATH, masterKey, fieldId, Yvalue);
                if (des < 0) {
                    status = sendDataToServer(urlBuf, NULL, method);
                } else {
                    status = sendDataToServer_Cellular(urlBuf, NULL, method, des);
                }
            }

            fieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_Z_ACCEL);
            if (fieldId > 0) {
                sprintf(urlBuf, "%s%s.xml?master_key=%s&field_id=%u&value=%d", host, DATA_PATH, masterKey, fieldId, Zvalue);
                if (des < 0) {
                    status = sendDataToServer(urlBuf, NULL, method);
                } else {
                    status = sendDataToServer_Cellular(urlBuf, NULL, method, des);
                }
            }

            return TRUE;
        }

        else if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_MOVEMENT_MEASUREMENT_CLUSTER_ID) {
            if (testCount == -1) {
                time(&testCountTimeStart);
            }
            if (difftime(time(NULL),testCountTimeStart) >= 10) {
                printf("\n\n---NUMBER OF SAMPLES PER SECOND: %f---\n---AVERAGED OVER 10 SECONDS---\n\n", ((float)testCount/difftime(time(NULL),testCountTimeStart)));//GeneralDebugPrint
                time(&testCountTimeStart);
                testCount = 0;
            }
//            GeneralDebugPrint ( "Current date and time are: %s", ctime(&testCountTimeStart) );
            testCount += 8;

            int8u sequence = incomingMessage -> apsFrame -> sequence;
            if (sequence - lastSequence > 1) {
                printf("\n\n!!!MISSED PACKET!!!\n\n");
            }
            lastSequence = sequence;

            EmberStatus status;
            int commandID = -2;

            GeneralDebugPrint("Node Id %X\n", incomingMessage -> source);

            int i = 3;
            while (i < (incomingMessage -> msgLen)) {
                int8s data[9];
                memcpy(data, &(incomingMessage -> message)[i], sizeof(data));

                int16s *X;
                float Xvalue, Yvalue, Zvalue;

                if (data[0] == ZCL_ACCELERATION_ATTRIBUTE_ID) {
                    X = (int16s*)(data + 3);

                    GeneralDebugPrint("%02X %02X %02X \n", X[0], X[1], X[2]);

                    Xvalue = X[0] / 32768.0 * 8;
                    Yvalue = X[1] / 32768.0 * 8;
                    Zvalue = X[2] / 32768.0 * 8;

                    int16s xFieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_X_ACCEL);
                    if (xFieldId > 0) {
                        writeDataToFile(curDataPath, xFieldId, Xvalue);
                    }

                    int16s yFieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_Y_ACCEL);
                    if (yFieldId > 0) {
                        writeDataToFile(curDataPath, yFieldId, Yvalue);
                    }

                    int16s zFieldId = getFieldId(Sensors[index].nodeNumber, NODE_FIELD_Z_ACCEL);
                    if (zFieldId > 0) {
                        writeDataToFile(curDataPath, zFieldId, Zvalue);
                    }
                    curNumStoredData ++;
                }
                else if (data[0] == ZCL_GYROSCOPE_ATTRIBUTE_ID) {
                    //TODO:
                }
                else if (data[0] == ZCL_MAGNETOMETER_ATTRIBUTE_ID) {
                    //TODO:
                }
                else if (data[0] == ZCL_ACCELERATION_Z_PROCESSED_SEQUENCE_ATTRIBUTE_ID) {
                    int8_t sequence = (int8_t)(data + 3);
                }
                else if (data[0] == ZCL_ACCELERATION_X_PROCESSED_VALUES_ATTRIBUTE_ID) {
                    //TODO: FIGURE OUT HOW TO READ AND COMBINE DATA PROPERLY. USE idct(...) TO CONVERT BACK TO DATA
                    int16s* x = (int16s*)(data + 3);

                    Xvalue = x[0] / 32768.0 * 8;
                }
                else if (data[0] == ZCL_ACCELERATION_X_PROCESSED_REPETITIONS_ATTRIBUTE_ID) {
                    int8_t repetition = (int8_t)(data + 3);
                }
                else if (data[0] == ZCL_ACCELERATION_Y_PROCESSED_VALUES_ATTRIBUTE_ID) {
                    //TODO: FIGURE OUT HOW TO READ AND COMBINE DATA PROPERLY. USE idct(...) TO CONVERT BACK TO DATA
                    int16s* y = (int16s*)(data + 3);

                    Yvalue = y[0] / 32768.0 * 8;
                }
                else if (data[0] == ZCL_ACCELERATION_Y_PROCESSED_REPETITIONS_ATTRIBUTE_ID) {
                    int8_t repetition = (int8_t)(data + 3);
                }
                else if (data[0] == ZCL_ACCELERATION_Z_PROCESSED_VALUES_ATTRIBUTE_ID) {
                    //TODO: FIGURE OUT HOW TO READ AND COMBINE DATA PROPERLY. USE idct(...) TO CONVERT BACK TO DATA
                    int16s* z = (int16s*)(data + 3);

                    Zvalue = z[0] / 32768.0 * 8;
                }
                else if (data[0] == ZCL_ACCELERATION_Z_PROCESSED_REPETITIONS_ATTRIBUTE_ID) {
                    int8_t repetition = (int8_t)(data + 3);
                }

                GeneralDebugPrint("xvalue is %f \n", Xvalue);
                GeneralDebugPrint("Yvalue is %f \n", Yvalue);
                GeneralDebugPrint("Zvalue is %f \n\n", Zvalue);

                i += 9;
            }

            if (curNumStoredData >= MAX_NUM_STORED_DATA) {

                if (!strcmp(curDataPath,DATA1_PATH)) {
                    printf("\n\nPost for sem_data1_id\n\n");
                    curDataPath = DATA2_PATH;
                    sem_post(sem_data1_id);
                } else if (!strcmp(curDataPath,DATA2_PATH)) {
                    printf("\n\nPost for sem_data2_id\n\n");
                    curDataPath = DATA1_PATH;
                    sem_post(sem_data2_id);
                }

                curNumStoredData = 0;
            }

            //return TRUE;
        }

        else if (incomingMessage -> apsFrame -> profileId == 0x0104 &&incomingMessage -> apsFrame -> clusterId == ZCL_ILLUM_MEASUREMENT_CLUSTER_ID) {
            int16s dataValue = *((int16s *)(incomingMessage -> message));
            //int16s fixPart = dataValue / 1000;
            // int16s fractPart = dataValue - fixPart*1000 ;

            GeneralDebugPrint("Sending electric current data to server................. \n");
            GeneralDebugPrint("Node Id %d\n", Sensors[index].nodeNumber);
            GeneralDebugPrint("RMS current value is %d \n", dataValue);

            //TODO: Implement this field into the ListNodeField table
            char urlBuf[110];
            sprintf(urlBuf, "%s%s.xml?master_key=%s&field_id=9&value=%d", host, DATA_PATH, masterKey, dataValue);
            EmberStatus status;
            int commandID = -2;
            RestMethod method = PUT;

            if (des < 0) {
                status = sendDataToServer(urlBuf, NULL, method);
            } else {
                status = sendDataToServer_Cellular(urlBuf, NULL, method, des);
            }

            return TRUE;
        }

        else {
            GeneralDebugPrint("childId: %X\n", incomingMessage -> source);
            GeneralDebugPrint("profileId: %X\n", incomingMessage -> apsFrame -> profileId);
            //if (incomingMessage -> apsFrame -> profileId == 0x0104) incomingMessage -> apsFrame -> profileId = 0x0;
            GeneralDebugPrint("clusterId: %X\n", incomingMessage -> apsFrame -> clusterId);
            //if (incomingMessage -> apsFrame -> clusterId == ZCL_IDENTIFY_CLUSTER_ID) incomingMessage -> apsFrame -> clusterId = END_DEVICE_ANNOUNCE;
            GeneralDebugPrint("sourceEndpoint: %X\n", incomingMessage -> apsFrame -> sourceEndpoint);
            //if (incomingMessage -> apsFrame -> sourceEndpoint == 0x08) incomingMessage -> apsFrame -> sourceEndpoint = 0x0;
            GeneralDebugPrint("destinationEndpoint: %X\n", incomingMessage -> apsFrame -> destinationEndpoint);
            //if (incomingMessage -> apsFrame -> destinationEndpoint == 0xFF) incomingMessage -> apsFrame -> destinationEndpoint = 0x0;
            GeneralDebugPrint("groupId: %X\n", incomingMessage -> apsFrame -> groupId);
            GeneralDebugPrint("sequence: %X\n", incomingMessage -> apsFrame -> sequence);
            char *option = "";
            switch (incomingMessage -> apsFrame -> options) {
                case EMBER_APS_OPTION_NONE:
                    option = "EMBER_APS_OPTION_NONE";
                    break;
                case EMBER_APS_OPTION_DSA_SIGN :
                    option = "EMBER_APS_OPTION_DSA_SIGN ";
                    break;
                case EMBER_APS_OPTION_ENCRYPTION :
                    option = "EMBER_APS_OPTION_ENCRYPTION ";
                    break;
                case EMBER_APS_OPTION_RETRY :
                    option = "EMBER_APS_OPTION_RETRY ";
                    break;
                case EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY :
                    option = "EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY ";
                    break;
                case EMBER_APS_OPTION_FORCE_ROUTE_DISCOVERY :
                    option = "EMBER_APS_OPTION_FORCE_ROUTE_DISCOVERY ";
                    break;
                case EMBER_APS_OPTION_SOURCE_EUI64 :
                    option = "EMBER_APS_OPTION_SOURCE_EUI64 ";
                    break;
                case EMBER_APS_OPTION_DESTINATION_EUI64 :
                    option = "EMBER_APS_OPTION_DESTINATION_EUI64 ";
                    break;
                case EMBER_APS_OPTION_ENABLE_ADDRESS_DISCOVERY  :
                    option = "EMBER_APS_OPTION_ENABLE_ADDRESS_DISCOVERY  ";
                    break;
                case EMBER_APS_OPTION_POLL_RESPONSE  :
                    option = "EMBER_APS_OPTION_POLL_RESPONSE  ";
                    break;
                case EMBER_APS_OPTION_ZDO_RESPONSE_REQUIRED  :
                    option = "EMBER_APS_OPTION_ZDO_RESPONSE_REQUIRED  ";
                    break;
                case EMBER_APS_OPTION_FRAGMENT  :
                    option = "EMBER_APS_OPTION_FRAGMENT  ";
                    break;
            }
            GeneralDebugPrint("option: %s\n", option);
            return FALSE;
        }
    } else {
        printf("Data but no in list?");
        if (incomingMessage -> source == 0x00 ||
            (incomingMessage -> apsFrame -> clusterId != ZCL_TEMP_MEASUREMENT_CLUSTER_ID && incomingMessage -> apsFrame -> clusterId != ZCL_ACCELERATION_MEASUREMENT_CLUSTER_ID
            && incomingMessage -> apsFrame -> clusterId != ZCL_MOVEMENT_MEASUREMENT_CLUSTER_ID && incomingMessage -> apsFrame -> clusterId != ZCL_ILLUM_MEASUREMENT_CLUSTER_ID)) {
            return FALSE;
        }
        GeneralDebugPrint("Device sending data and not confirmed part of network - Remove from network\n");
//        boolean joinStatus = FALSE;
//        EmberEUI64 childEui64;
//        EmberStatus lookupStatus = emberLookupEui64ByNodeId (incomingMessage -> source, childEui64);
//        emberAfAppPrintln("Device sending data and not confirmed part of network - Address : %x%x%x%x%x%x%x%x --- Node Id %X\n", childEui64[0], childEui64[1],
//                          childEui64[2], childEui64[3], childEui64[4], childEui64[5], childEui64[6],
//                          childEui64[7], incomingMessage -> source);
//        if (lookupStatus == EMBER_SUCCESS) {
//            joinStatus = attemptJoin(Sensors, &numSensors, incomingMessage -> source, childEui64, ListEuiNode, ListNodeField);
//            if (joinStatus == FALSE) {
                emberLeaveRequest(incomingMessage->source, emptyEmberEUI64, LEAVE_REQUEST_REMOVE_CHILDREN_FLAG, EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY|EMBER_APS_OPTION_DESTINATION_EUI64);
//            }
//        }
    }
    return FALSE;
}

void querrySensors(void) {
    emberEventControlSetInactive(querrySensorEventControl);

}

void printEvent(void) {
    emberAfEventControlSetDelay( &printEventControl, 60000);

    GeneralDebugPrint("checking for commands.... \n");
    EmberStatus status;
    int commandID = -1;
    char sendfile[300];
    char urlBuf[410];

    if (test == 1) {
        //get command
        sprintf(urlBuf, "%s%s.xml?gateway_id=%d&master_key=%s", host, COMMAND_PATH, gatewayId, masterKey);
        //GeneralDebugPrint("%s\n", urlBuf);
        status = UpdateNodeSensorSettings(urlBuf, &commandID, des);
        if (status == EMBER_SUCCESS) {
            //command successfully ran, get next command
            RestMethod method = PUT;
            sprintf(urlBuf, "%s%s.xml?gateway_id=%d&master_key=%s&command_id=%d", host, COMMAND_PATH, gatewayId, masterKey, commandID);
            if (des < 0) {
                status = sendDataToServer(urlBuf, NULL, method);
            } else {
                status = sendDataToServer_Cellular(urlBuf, NULL, method, des);
            }
        }
    }
}

EmberStatus prepare_message(int nodeId, int8u *send_buf, int send_len, char *apiKeyBuffer) {
    EmberApsFrame apsFrame;
    EmberStatus status;
    apsFrame.profileId = 0x0104;
    apsFrame.clusterId = ZCL_CONFIG_CLUSTER_ID;
    apsFrame.sourceEndpoint = 1;
    apsFrame.destinationEndpoint = 1;
    apsFrame.options = EMBER_APS_OPTION_NONE;
    apsFrame.groupId = 0;
    apsFrame.sequence = 1;

    int i = 0;
    for (i = 0; i < Node_count; i++) {

        if (Sensors[i].index != EMBER_NULL_ADDRESS_TABLE_INDEX) {

            if ((int8u) Sensors[i].nodeNumber == (int8u) nodeId) {
                break;
            }
        }
    }
    if (i < Node_count) {
        memcpy(Sensors[i].apiKey, apiKeyBuffer, 17);
        memcpy(Sensors[i].config, send_buf, send_len);
        GeneralDebugPrint("found node: sending uniCast\n");
        status = emberAfSendUnicast(EMBER_OUTGOING_VIA_ADDRESS_TABLE, Sensors[i].index, &apsFrame, send_len, send_buf);

    } else {
        GeneralDebugPrint("node not found: NOT sending uniCast\n");
        return status = !EMBER_SUCCESS;
    }
    if (status != EMBER_SUCCESS) {
        GeneralDebugPrint("sending Message failed \n");
    }
    return status;
}

EmberStatus prepare_message_TiSensor(int nodeIndex, int8u send_buf, int8u command) {
    EmberStatus status;

    if (command < ZCL_ACCELERATION_ON_OFF_COMMAND_ID || command > ZCL_MAGNETOMETER_POLL_RATE_COMMAND_ID) {
        GeneralDebugPrint("Not a valid command\n");
        return (status = !EMBER_SUCCESS);
    }

    switch(command) {
        case ZCL_ACCELERATION_ON_OFF_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClusterAccelerationOnOff(send_buf);
            break;
        case ZCL_GYROSCOPE_ON_OFF_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClustergyroscopeOnOff(send_buf);
            break;
        case ZCL_MAGNETOMETER_ON_OFF_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClustermagnetometerOnOff(send_buf);
            break;
        case ZCL_ACCELERATION_SLEEP_DURATION_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClusteraccelerationSleepDuration(send_buf);
            break;
        case ZCL_GYROSCOPE_SLEEP_DURATION_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClustergyroscopeSleepDuration(send_buf);
            break;
        case ZCL_MAGNETOMETER_SLEEP_DURATION_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClustermagnetometerSleepDuration(send_buf);
            break;
        case ZCL_ACCELERATION_POLL_RATE_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClusteraccelerationPollRate(send_buf);
            break;
        case ZCL_GYROSCOPE_POLL_RATE_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClustergyroscopePollRate(send_buf);
            break;
        case ZCL_MAGNETOMETER_POLL_RATE_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClustermagnetometerPollRate(send_buf);
            break;
        case ZCL_MOV_PREPROCESSINGTYPE_COMMAND_ID:
            emberAfFillCommandMovementMeasurementClustermagnetometerPreprocessingType(send_buf);
            break;
    }

    emberAfGetCommandApsFrame()->profileId = emberAfPrimaryProfileId();
    emberAfSetCommandEndpoints(emberAfPrimaryEndpoint(), 0xA);
    emberAfGetCommandApsFrame()->options = EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY | EMBER_APS_OPTION_RETRY;
    status = emberAfSendCommandUnicast(EMBER_OUTGOING_VIA_ADDRESS_TABLE , nodeIndex);

    if (status == EMBER_ERR_FATAL) {
        GeneralDebugPrint("sending Message: EMBER_ERR_FATAL \n");
    }
    else if (status == EMBER_TABLE_FULL) {
        GeneralDebugPrint("sending Message: EMBER_TABLE_FULL \n");
    }
    else if (status == EMBER_INVALID_ENDPOINT) {
        GeneralDebugPrint("sending Message: EMBER_INVALID_ENDPOINT \n");
    }
    else if (status == EMBER_MESSAGE_TOO_LONG) {
        GeneralDebugPrint("sending Message: EMBER_MESSAGE_TOO_LONG \n");
    }
    else if (status == EMBER_OPERATION_IN_PROGRESS) {
        GeneralDebugPrint("sending Message: EMBER_OPERATION_IN_PROGRESS \n");
    }
    else if (status == EMBER_SUCCESS) {
        GeneralDebugPrint("sending Message: EMBER_SUCCESS \n");
    }
    else if (status != EMBER_SUCCESS) {
        GeneralDebugPrint("sending Message failed \n");
    }

    return status;
}

void getNameAndId(char *buf, int *commandID, Key *name) {
    char buffer[30];
    strcpy(buffer, "");
    char *pfound = strstr(buf, "id");

    if (pfound != NULL) {
        pfound += 18;

        ( *commandID) = atoi(pfound);
    }

    GeneralDebugPrint("Command: %s\n", buf);
    pfound = strstr(buf, "<name>");
    char* pfoundend = strstr(buf, "</name>");
    int length = pfoundend - pfound;
    if (pfound != NULL || length < 1) {

        pfound += 6;
        memcpy(buffer, pfound, length);
        GeneralDebugPrint("Command name: %s\n", buffer);

        pfound = strstr(buffer, values[SET_CONFIG]);
        if (pfound != NULL) {
            ( *name) = SET_CONFIG;
            return;
        }

        pfound = strstr(buffer, values[GET_CONFIG]);
        if (pfound != NULL) {
            ( *name) = GET_CONFIG;
            return;
        }

        pfound = strstr(buffer, values[TI_SENSORTAG]);
        if (pfound != NULL) {
            ( *name) = TI_SENSORTAG;
            return;
        }

        pfound = strstr(buffer, values[UPDATE_FIELDS]);
        if (pfound != NULL) {
            ( *name) = UPDATE_FIELDS;
            return;
        }

        pfound = strstr(buffer, values[REMOVE_NODE]);
        if (pfound != NULL) {
            ( *name) = REMOVE_NODE;
            return;
        }


        ( *name) = NONE;
    } else {
        ( *name) = NONE;
    }

}

EmberStatus UpdateNodeSensorSettings(char *data_buff, int *commandID, int fdes) {

    EmberStatus status = !EMBER_SUCCESS;

    struct string s;
    init_string( &s);

    char *ptr;
    char *pfound;
    char *pfoundend;

    RestMethod method = GET;
    EmberStatus getStatus;
    if (des < 0) {
        getStatus = getDataFromServer(data_buff, &s, method);
    } else {
        getStatus = getDataFromServer_Cellular(data_buff, &s, method, fdes);
    }

    /* check if getdata was successful */
    if (getStatus != EMBER_SUCCESS) {
        GeneralDebugPrint("NO command data received\n");
        status = !EMBER_SUCCESS;
    }else {
        GeneralDebugPrint("command data received\n");
        if (commandID != -2) {

            AcclConfiguration accl;
            TempConfiguration temp;
            AdcConfiguration adc;
            int nodeId = -1;
            int8u send_buff[15];
            char apiKeyBuffer[18] = "M1MB4DZDYRJ03NA5";
            int send_len;

            //init the structures
            initDefaultConfigs( &accl, &temp, &adc);
            Key name = 17;
            getNameAndId(s.ptr, commandID, &name);

            GeneralDebugPrint("Command for Key: %d\n", name);
            //find settings
            switch (name) {
                case SET_CONFIG:
                    nodeId = config_value(s.ptr, NODE_ID, &accl, &temp, &adc, NULL);
                    emberAfAppPrintln("Command Node Id: %d", nodeId);
                    config_value(s.ptr, ADC_MOD, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_BITS, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_CLK, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_PERIOD, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C0_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C0_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C1_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C1_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C2_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C2_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C3_P, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ADC_C3_N, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, TEMP_PERIOD, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ACCL_MAXG, &accl, &temp, &adc, NULL);
                    config_value(s.ptr, ACCL_PERIOD, &accl, &temp, &adc, NULL);
                    //  config_value (s.ptr, API_KEY,&accl,&temp,&adc,apiKeyBuffer);
                    if (nodeId >= 0) {
                        encodeConfig(nodeId, accl, adc, temp, send_buff, &send_len);

                        //send message
                        send_len = 18;
                        status = prepare_message(nodeId, send_buff, send_len, apiKeyBuffer);
                    }
                    break;

                case GET_CONFIG:
                    status = EMBER_SUCCESS;
                    break;

                case TI_SENSORTAG:
                    GeneralDebugPrint("TI SENSORTAG Command\n");
                    ptr = s.ptr;

                    pfound = strstr(ptr, "<device-id type=\"integer\">");
                    pfoundend = strstr(ptr, "</device-id>");

                    if (pfound != NULL) {
                        pfound += 26;
                        int len = pfoundend-pfound;
                        nodeId = atoi(pfound);
                    }

                    GeneralDebugPrint("Node Number = %d\n", nodeId);

                    int8u comId = 0xFF;
                    int8u value = 0x00;

                    while (ptr != NULL && nodeId != -1) {
                        if (strstr(ptr, "<parameter>") == NULL) {
                            GeneralDebugPrint("done getting parameters\n");
                            break;
                        }
                        ptr = strstr(ptr, "<parameter>");

                        pfound = strstr(ptr, "<name>");
                        pfoundend = strstr(ptr, "</name>");
                        char *buffer = malloc(31);
                        memset(buffer, 0, 31);

                        if (pfound != NULL) {
                            pfound += 6;
                            int len = pfoundend-pfound;
                            memcpy(buffer, pfound, len);
                        }

                        if (strstr(buffer, "accel-pollrate") != NULL) {
                            comId = ZCL_ACCELERATION_POLL_RATE_COMMAND_ID;
                        } else if (strstr(buffer, "gyro-pollrate") != NULL) {
                            comId = ZCL_GYROSCOPE_POLL_RATE_COMMAND_ID;
                        } else if (strstr(buffer, "mov-preprocessingType") != NULL) {
                            comId = ZCL_MOV_PREPROCESSINGTYPE_COMMAND_ID;
                        }

                        ptr = pfoundend;
                        ptr += 7;

                        pfound = strstr(ptr, "<value>");
                        pfoundend = strstr(ptr, "</value>");
                        memset(buffer, 0, 31);

                        if (pfound != NULL) {
                            pfound += 7;
                            int len = pfoundend-pfound;
                            memcpy(buffer, pfound, len);
                            value = atoi(pfound);
                        }

                        ptr = pfoundend;
                        ptr += 8;

                        GeneralDebugPrint("Command Id = %x -- Value = %d\n", comId, value);
                        if (comId != 0xFF) {
                            int i;
                            for (i = 0; i < numSensors; i++) {
                                if (Sensors[i].nodeNumber == nodeId) {
                                    GeneralDebugPrint("\nSensor.nodeId = %x -- Sensor.nodeNumber = %d\n", Sensors[i].nodeId, Sensors[i].nodeNumber);
                                    if (Sensors[i].index != EMBER_NULL_ADDRESS_TABLE_INDEX) {
                                        GeneralDebugPrint("Address table Index: %d -- NULL index = %d\n", Sensors[i].index, EMBER_NULL_ADDRESS_TABLE_INDEX);
                                        status = prepare_message_TiSensor(Sensors[i].index, value, comId);
                                    }
                                    break;
                                }
                            }
                        }

                        free(buffer);
                    }
                    break;
                case UPDATE_FIELDS:
                    GeneralDebugPrint("UPDATE FIELDS Command\n");
                    checkServerForNodeList(ListEuiNode, ListNodeField, numSensors, des);
                    status = EMBER_SUCCESS;
                    break;
                case REMOVE_NODE:
                    GeneralDebugPrint("REMOVE NODE Command\n");
                    ptr = s.ptr;

                    pfound = strstr(ptr, "<name>");
                    pfoundend = strstr(ptr, "</name>");
                    char *buffer = malloc(31);
                    memset(buffer, 0, 31);

                    if (pfound != NULL) {
                        pfound += 6;
                        int len = pfoundend-pfound;
                        memcpy(buffer, pfound, len);
                    }

                    int8u nodeIdValue = 0;
                    if (strstr(buffer, "node") != NULL) {
                        nodeIdValue = 1;
                    }

                    if (nodeIdValue) {
                        ptr = pfoundend;
                        ptr += 7;

                        pfound = strstr(ptr, "<value>");
                        pfoundend = strstr(ptr, "</value>");
                        memset(buffer, 0, 31);

                        if (pfound != NULL) {
                            pfound += 7;
                            int len = pfoundend-pfound;
                            memcpy(buffer, pfound, len);
                            nodeId = atoi(pfound);
                        }

                        if (nodeId != -1) {
                            int i;
                            for (i = 0; i < numSensors; i++) {
                                if (Sensors[i].nodeNumber == nodeId) {
                                    int8u NodeIdTable;

                                    NodeIdTable = CheckListEuiNode(Sensors[i].address);
                                    ListEuiNode[NodeIdTable][0] = 0x00;
                                    ListEuiNode[NodeIdTable][1] = 0x00;
                                    ListEuiNode[NodeIdTable][2] = 0x00;
                                    ListEuiNode[NodeIdTable][3] = 0x00;
                                    ListEuiNode[NodeIdTable][4] = 0x00;
                                    ListEuiNode[NodeIdTable][5] = 0x00;
                                    ListEuiNode[NodeIdTable][6] = 0x00;
                                    ListEuiNode[NodeIdTable][7] = 0x00;

                                    Sensors[i].nodeNumber = DEFAULT_NODE_NUMBER;

                                    emberLeaveRequest(Sensors[i].nodeId, emptyEmberEUI64,
                                                      LEAVE_REQUEST_REMOVE_CHILDREN_FLAG,
                                                      EMBER_APS_OPTION_ENABLE_ROUTE_DISCOVERY |
                                                      EMBER_APS_OPTION_DESTINATION_EUI64);
                                    break;
                                }
                            }
                        }
                    }
                    free(buffer);
                    status = EMBER_SUCCESS;
                    break;
                default: break;
            }
        }
    }

    /* always cleanup */
    free(s.ptr);

    return status;
}

int config_value(char *buf, Key key, AcclConfiguration *accl, TempConfiguration *temp, AdcConfiguration *adc, char *apiKeyBuffer) {
    char *pfound = strstr(buf, values[key]); //pointer to the first character found in the string buf
    int status = 0;
    if (pfound != NULL) {
        pfound += strlen(values[key]) + 21;
        //GeneralDebugPrint("%16s\n",pfound);
        //pfound points to value
        switch (key) {
            case NODE_ID:
                status = atoi(pfound);
                break;
            case ADC_MOD:
                adc -> mode = (int8u) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_BITS:
                adc -> numberBits = (int16u) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_PERIOD:
                adc -> periodS = (int16u) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_CLK:
                status = atoi(pfound);
                if (status) adc -> is_clock_1MHz = DISABLED;
                else adc -> is_clock_1MHz = ENABLED;
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_C0_P:
                adc -> pIn[0] = (adcInputChEnum) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                break;
            case ADC_C0_N:
                adc -> nIn[0] = (adcInputChEnum) atoi(pfound);
                adc -> is_adc_enabled = ENABLED;
                adc -> numberChannels++;
                break;
            case ADC_C1_P:
                adc -> pIn[1] = (adcInputChEnum) atoi(pfound);
                break;
            case ADC_C1_N:
                adc -> nIn[1] = (adcInputChEnum) atoi(pfound);
                adc -> numberChannels++;
                break;
            case ADC_C2_P:
                adc -> pIn[2] = (adcInputChEnum) atoi(pfound);
                break;
            case ADC_C2_N:
                adc -> nIn[2] = (adcInputChEnum) atoi(pfound);
                adc -> numberChannels++;
                break;
            case ADC_C3_P:
                adc -> pIn[3] = (adcInputChEnum) atoi(pfound);
                break;
            case ADC_C3_N:
                adc -> nIn[3] = (adcInputChEnum) atoi(pfound);
                adc -> numberChannels++;
                break;
            case TEMP_PERIOD:
                temp -> periodS = (int16u) atoi(pfound);
                temp -> is_temp_enabled = ENABLED;
                break;
            case ACCL_MAXG:
                accl -> maxG = (int16u) atoi(pfound);
                accl -> is_accl_enabled = ENABLED;
                accl -> is_x_enabled = accl -> is_y_enabled = accl -> is_z_enabled = ENABLED;
                break;
            case ACCL_PERIOD:
                accl -> periodS = (int16u) atoi(pfound);
                accl -> is_accl_enabled = ENABLED;
                accl -> is_x_enabled = accl -> is_y_enabled = accl -> is_z_enabled = ENABLED;
                break;
            case API_KEY:
                memcpy(apiKeyBuffer, pfound, 17);
                apiKeyBuffer[16] = 0;
                break;
        }
    } else if (key == NODE_ID) {
        status = -1;
    }
    return status;
}

/* Direct implementation of the inverse DCT */
void idct(int N, const float X[], float x[]) {
    int n;
    for (n = 0; n < N; ++n) {
        float sum = 0.;
        int k;
        for (k = 0; k < N; ++k) {
            float s = (k == 0) ? sqrt(.5) : 1.;
            sum += s * X[k] * cos(M_PI * (n + .5) * k / N);
        }
        x[n] = sum * sqrt(2. / N);
    }
}

/**
 * Unused functions that are required for Zigbee network
 */


/** @brief Get Current Time
 *
 * This callback is called when device attempts to get current time from the
 * hardware. If this device has means to retrieve exact time, then this method
 * should implement it. If the callback can't provide the exact time it should
 * return 0 to indicate failure. Default action is to return 0, which
 * indicates that device does not have access to real time.
 *
 */
int32u emberAfGetCurrentTimeCallback(void) {
    return time(NULL) - ZIGBEE_EPOCH_OFFSET;
}

/** @brief Read Attributes Response
 *
 * This function is called by the application framework when a Read Attributes
 * Response command is received from an external device.  The application
 * should return TRUE if the message was processed or FALSE if it was not.
 *
 * @param clusterId The cluster identifier of this response.  Ver.: always
 * @param buffer Buffer containing the list of read attribute status records.
 * Ver.: always
 * @param bufLen The length in bytes of the list.  Ver.: always
 */
boolean emberAfReadAttributesResponseCallback(EmberAfClusterId clusterId,
                                              int8u *buffer,
                                              int16u bufLen) {
    return FALSE;
}

/** @brief E Z Mode Invoke
 *
 *
 * @param action   Ver.: always
 */
boolean emberAfIdentifyClusterEZModeInvokeCallback(int8u action)
{
    return FALSE;
}

/** @brief Broadcast Sent
 *
 * This function is called when a new MTORR broadcast has been successfully
 * sent by the concentrator plugin.
 *
 */
void emberAfPluginConcentratorBroadcastSentCallback(void) {}

/** @brief Finished
 *
 * This callback is fired when the network-find plugin is finished with the
 * forming or joining process.  The result of the operation will be returned
 * in the status parameter.
 *
 * @param status   Ver.: always
 */
void emberAfPluginNetworkFindFinishedCallback(EmberStatus status) {}


/** @brief Get Radio Power For Channel
 *
 * This callback is called by the framework when it is setting the radio power
 * during the discovery process. The framework will set the radio power
 * depending on what is returned by this callback.
 *
 * @param channel   Ver.: always
 */
int8s emberAfPluginNetworkFindGetRadioPowerForChannelCallback(int8u channel) {
    return EMBER_AF_PLUGIN_NETWORK_FIND_RADIO_TX_POWER;
}

/** @brief Join
 *
 * This callback is called by the plugin when a joinable network has been
 * found.  If the application returns TRUE, the plugin will attempt to join
 * the network.  Otherwise, the plugin will ignore the network and continue
 * searching.  Applications can use this callback to implement a network
 * blacklist.
 *
 * @param networkFound   Ver.: always
 * @param lqi   Ver.: always
 * @param rssi   Ver.: always
 */
boolean emberAfPluginNetworkFindJoinCallback(EmberZigbeeNetwork *networkFound,
                                             int8u lqi,
                                             int8s rssi) {
    GeneralDebugPrint("FindJoinCallBack - lqi: %X\n", lqi);

    return TRUE;
}

/** @brief Select File Descriptors
 *
 * This function is called when the Gateway plugin will do a select() call to
 * yield the processor until it has a timed event that needs to execute.  The
 * function implementor may add additional file descriptors that the
 * application will monitor with select() for data ready.  These file
 * descriptors must be read file descriptors.  The number of file descriptors
 * added must be returned by the function (0 for none added).
 *
 * @param list A pointer to a list of File descriptors that the function
 * implementor may append to  Ver.: always
 * @param maxSize The maximum number of elements that the function implementor
 * may add.  Ver.: always
 */
int emberAfPluginGatewaySelectFileDescriptorsCallback(int *list,
                                                      int maxSize) {
    return 0;
}

/** @brief Get Group Name
 *
 * This function returns the name of a group with the provided group ID,
 * should it exist.
 *
 * @param endpoint Endpoint  Ver.: always
 * @param groupId Group ID  Ver.: always
 * @param groupName Group Name  Ver.: always
 */
void emberAfPluginGroupsServerGetGroupNameCallback(int8u endpoint,
                                                   int16u groupId,
                                                   int8u * groupName)
{
}

/** @brief Set Group Name
 *
 * This function sets the name of a group with the provided group ID.
 *
 * @param endpoint Endpoint  Ver.: always
 * @param groupId Group ID  Ver.: always
 * @param groupName Group Name  Ver.: always
 */
void emberAfPluginGroupsServerSetGroupNameCallback(int8u endpoint,
                                                   int16u groupId,
                                                   int8u * groupName)
{
}

/** @brief Group Names Supported
 *
 * This function returns whether or not group names are supported.
 *
 * @param endpoint Endpoint  Ver.: always
 */
boolean emberAfPluginGroupsServerGroupNamesSupportedCallback(int8u endpoint)
{
    return FALSE;
}
