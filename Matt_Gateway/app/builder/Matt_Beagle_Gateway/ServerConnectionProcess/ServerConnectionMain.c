//
// Created by mharrin3 on 2/5/2019.
//

#include "ServerConnectionMain.h"
#include <semaphore.h>
#include <fcntl.h>

#include "DataTransferUtil/DataTransferUtil.h"

void ServerConnectionMain(int des) {
    sem_t * sem_data1_id = sem_open(SEM_DATA1_PATH, O_CREAT, S_IRUSR | S_IWUSR, 0);
    sem_t * sem_data2_id = sem_open(SEM_DATA2_PATH, O_CREAT, S_IRUSR | S_IWUSR, 0);

    if (sem_data1_id == SEM_FAILED || sem_data2_id == SEM_FAILED) {
        printf("\n\n\n\ninitialization of sems failed!!\n\n\n\n");
        return 0;
    }

    while(1) {
        printf("\n\nWaiting for sem_data1_id\n\n");
        sem_wait(sem_data1_id);
        SendMovementData(DATA1_PATH, des);

        printf("\n\nWaiting for sem_data2_id\n\n");
        sem_wait(sem_data2_id);
        SendMovementData(DATA2_PATH, des);
    }
}

void SendMovementData(char *dataPath, int des) {
    char urlBuf[110];
    char dataBuf[DATA_FILE_SIZE];
    dataBuf[0] = '\0';
    RestMethod method = POST;

//    strcat(dataBuf, "{\"data\":[");
//    printf("\nGET DATA - %s \n dataBuf - %s\n", dataPath, dataBuf);
//    readDataFromFile(dataPath, dataBuf);
//    printf("\nDATA RETRIEVED - %s\n", dataPath);
//    strcat(dataBuf, "],");

//    if (strlen(dataBuf) > 6) { //12
        char str[80];
        sprintf(str, "{\"master_key\":\"%s\"}", masterKey);
        strcat(dataBuf, str);

        struct string dataString;
        init_string(&dataString);
        set_string(&dataString, dataBuf);

        printf("Sending Accel Data Buffer: %s\n", dataBuf);
        GeneralDebugPrint("Sending movement data to server................. \n");
        sprintf(urlBuf, "%s%s.json?master_key=%s", host, DATAFILE_PATH, masterKey);
        if (des < 0) {
            if (sendDataFileToServer(urlBuf, &dataString, dataPath, method) == EMBER_SUCCESS) {
                printf("clear file - %s\n", dataPath);
                clearFile(dataPath);
            }
            //sendDataToServer(urlBuf, &dataString, method); //TODO: make sure to uncomment when done testing
        } else {
            //sendDataToServer_Cellular(urlBuf, &dataString, method, des);
        }
//    } else {
//        GeneralDebugPrint("Did NOT send movement data to server\n");
//    }
}

void retrieveNodeList() {

}