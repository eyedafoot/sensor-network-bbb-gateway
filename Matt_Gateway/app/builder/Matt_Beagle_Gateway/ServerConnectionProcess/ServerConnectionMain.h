//
// Created by mharrin3 on 2/5/2019.
//

#ifndef UNB_SENSOR_NETWORK_SERVERCONNECTIONMAIN_H
#define UNB_SENSOR_NETWORK_SERVERCONNECTIONMAIN_H

#include "../Matt_Beagle_Gateway_callbacks.h"

void ServerConnectionMain(int des);

#endif //UNB_SENSOR_NETWORK_SERVERCONNECTIONMAIN_H
