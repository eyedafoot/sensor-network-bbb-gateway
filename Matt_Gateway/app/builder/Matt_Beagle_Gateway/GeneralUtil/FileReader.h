//
// Created by Matthew on 7/13/2018.
//

#ifndef MATT_GATEWAY_FILEREADER_H
#define MATT_GATEWAY_FILEREADER_H

#include "../Matt_Beagle_Gateway_callbacks.h"

int getUserVariables(void);
void readDataFromFile(char *dataPath, char *urlData);
void clearFile(char *dataPath);

#endif //MATT_GATEWAY_FILEREADER_H
