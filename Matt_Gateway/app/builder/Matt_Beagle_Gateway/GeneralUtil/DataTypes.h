#ifndef __DATA_TYPES__
#define __DATA_TYPES__

/********************************
 * Dependancies
 ********************************/
#include <stdlib.h>
#include "app/framework/include/af.h"

/********************************
 * Constants
 ********************************/
 
/********************************
 * Data Types
 ********************************/

struct string {
    char *ptr;
    size_t len;
};

/********************************
 * Public Methods
 ********************************/
/*
 * init_string, used to initialize string struct
 *
 * s = pointer to string struct being initialized
 */
void init_string(struct string *s);

/*
 * init_string, used to initialize string struct
 *
 * s = pointer to string struct being set
 * str = pointer to char array setting the  string (Must be null terminated '\0')
 */
void set_string(struct string *s, char* str);

#endif // __DATA_TYPES__