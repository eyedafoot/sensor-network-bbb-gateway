//
// Created by mharrin3 on 1/10/2019.
//

#include <stdio.h>
#include "FileWriter.h"

void writeDataToFile(char *dataPath, int fieldId, float data) {
    FILE *fPointer;
    fPointer = fopen(dataPath, "a+");

    fseek(fPointer, 0, SEEK_END);
    unsigned long len = (unsigned long)ftell(fPointer);

    if (len > 0) {
        fprintf(fPointer, ",");
    }

    fprintf(fPointer, "%d,%f", fieldId, data);

    fclose(fPointer);
}
