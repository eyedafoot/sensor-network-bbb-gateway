//
// Created by Matthew on 7/13/2018.
//

#include <stdio.h>
#include <stdlib.h>
#include "FileReader.h"

int getUserVariables(void) {
    int checker = 0;

    FILE *fPointer;
    fPointer = fopen("UserVariables.txt", "r");

    char line[150];

    if (fPointer != NULL) {
        while (!feof(fPointer)) {
            memset(line, 0, 150);
            fgets(line, 150, fPointer);
            if (line[0] != '#') {
                if (strstr(line, "masterKey")) {
                    char *value = strchr(line, '=') + 1;

                    char *newLine = strchr(value, '\n');
                    *newLine = '\0';

                    strncpy(masterKey, value, strlen(value) - 1);
                    printf("Master Key: %s\n", masterKey);

                    checker += 1;
                } else if (strstr(line, "host")) {
                    char *value = strchr(line, '=') + 1;

                    char *newLine = strchr(value, '\n');
                    *newLine = '\0';

                    strncpy(host, value, strlen(value) - 1);
                    printf("Host: %s\n", host);

                    checker += 2;
                } else if (strstr(line, "gatewayId")) {
                    char *value = strchr(line, '=') + 1;

                    gatewayId = (uint8_t) atoi(value);
                    printf("Gateway Id: %d\n", gatewayId);

                    checker += 3;
                }
            }
        }

        fclose(fPointer);

        return (checker == 6 ? 1 : 0);
    } else {
        return 0;
    }
}

//Might accidentally truncate if data is to long.
void readDataFromFile(char *dataPath, char *urlData) {
    printf('BEFORE Read file - %s\n', dataPath);
    FILE *fPointer;
    fPointer = fopen(dataPath, "r");
    printf('AFTER Read file - %s\n', dataPath);
    int8u first = 1;
    char buffer[(DATA_FILE_SIZE - 2048)];

    if (fPointer != NULL) {
        printf('GOT Read file - %s\n', dataPath);
        while (fgets(buffer, (DATA_FILE_SIZE - 2048), fPointer)) {
            char *tok = strtok(buffer, ",");
            while (tok != NULL) {
                int16s fieldId = atoi(tok);
                tok = strtok(NULL, ",");
                float data = atof(tok);
                tok = strtok(NULL, ",");

                char *strbuf[80];

                if (!first) {
                    strcat(urlData, ",");
                }
                sprintf(strbuf, "{\"field_id\":%d,\"value\":%f}", fieldId, data);
                printf('data - %s\n', strbuf);
                strcat(urlData, strbuf);

                first = 0;
            }
        }
        freopen(dataPath, "w", fPointer); //clear file

        fclose(fPointer);
    }
}

void clearFile(char *dataPath) {
    //printf("clear file - %s\n", dataPath);
    FILE *fPointer;
    fPointer = fopen(dataPath, "w"); //clear file

    fclose(fPointer);
    //printf("END clear file - %s\n", dataPath);
}