//
// Created by Matthew on 6/13/2018.
//

#ifndef MATT_GATEWAY_NODELIST_H
#define MATT_GATEWAY_NODELIST_H
/********************************
 * Dependancies
 ********************************/
#include <stdlib.h>
#include "app/framework/include/af.h"

/********************************
 * Constants
 ********************************/
#define Node_count 29
#define MAX_NUM_NODE_FIELDS 5

#define NODE_FIELD_NODE_NUMBER 0
#define NODE_FIELD_TEMP 1
#define NODE_FIELD_X_ACCEL 2
#define NODE_FIELD_Y_ACCEL 3
#define NODE_FIELD_Z_ACCEL 4

#define DEFAULT_NODE_NUMBER -1

static const EmberEUI64 emptyEmberEUI64 = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

/********************************
 * Data Types
 ********************************/
typedef struct {
    EmberNodeId nodeId; //Id of node given by zigbee network
    int8s nodeNumber; //Id given by server
    int8u index; //Index of same address in ListEuiNode
    int8u sensors;
    int16s temp;
    int16s cur;
    int16s xaccel;
    int16s yaccel;
    int16s zaccel;
    int8u config[15];
    char apiKey[18];
    int8s nodeRole;
    int8s nodeParent;
    EmberEUI64 address;
} AppSensor;

/********************************
 * Variables
 ********************************/

// *******************************************************************
// Node EUI64 address list available in the network, eui is backwards on memory, MSB first

extern EmberEUI64 ListEuiNode[Node_count];

extern AppSensor Sensors[EMBER_AF_PLUGIN_ADDRESS_TABLE_SIZE];

//list to hold 0:nodeNumber, 1:tempFieldId, 2:x-accelFieldId, 3:y-accelFieldId, 4:z-accelFieldId
// -1 for an id will mean field not setup
extern int16s ListNodeField[Node_count][MAX_NUM_NODE_FIELDS];

extern int16u numSensors;

/********************************
 * Public Methods
 ********************************/

/** Node List **/
/*
 * CheckListEuiNode, used to check is an address is in the list of allowed addresses
 *
 * EUI = address of the node being check if access is allowed
 *
 * return = If found: Index node was found at (nodeId)
 *      If not found: 0xFF
 */
int8u CheckListEuiNode (int8u EUI);
/*
 * NextListEuiNode, returns the next available index for the ListEuiNode list.
 *
 * return = If a slot is still avaible return slot index
 *      If no slots left return -1;
 */
int16s NextListEuiNode(void);

/*
 * setNodeNumberByAddress, Uses long address of device to set node number (service Id for node)
 *
 * address = address of the node being check if access is allowed
 * nodeNumber = id for node from server
 *
 * return void
 */
void setNodeNumberByAddress(EmberEUI64 address, int8s nodeNumber);

int8s findSensorListIndex(EmberNodeId nodeId, EmberEUI64 nodeEui64);

/** Node Fields **/
/*
 * getFieldId, used to get fieldId for a node
 *
 * nodeNumber = the nodeNumber of the node you want the fieldId for
 * col = the col of the sensor type (1 = temp, 2 = x-accel, 3 = y-accel, 4 = z-accel)
 *
 * return = fieldId
 */
int16s getFieldId(int16s nodeNumber, int8u col);
/*
 * updateFieldId, used to update a fieldId for a node
 *
 * nodeNumber = the nodeNumber of the node you want to update the fieldId of
 * col = the col of the sensor type (1 = temp, 2 = x-accel, 3 = y-accel, 4 = z-accel)
 * fieldId = new fieldId value
 */
void updateFieldId(int16s nodeNumber, int8u col, int16s fieldId);
/*
 * resetFieldIds, used to reset the fieldIds to -1 for a node
 *
 * nodeNumber = the nodeNumber of the node you want to fieldIds for
 */
void resetFieldIds(int16s nodeNumber);

#endif //MATT_GATEWAY_NODELIST_H