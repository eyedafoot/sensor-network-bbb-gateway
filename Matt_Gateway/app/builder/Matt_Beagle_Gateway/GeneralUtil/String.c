#include "DataTypes.h"

#include <stdio.h>
#include <string.h>

void init_string(struct string *s) {
    s -> len = 0;
    s -> ptr = malloc(s -> len + 1);
    if (s -> ptr == NULL) {
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    s -> ptr[0] = '\0';
}

void set_string(struct string *s, char* str) {
    s -> len = strlen(str);
    s -> ptr = realloc(s -> ptr, s -> len);
    if (s -> ptr == NULL) {
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    s -> ptr = str;
}