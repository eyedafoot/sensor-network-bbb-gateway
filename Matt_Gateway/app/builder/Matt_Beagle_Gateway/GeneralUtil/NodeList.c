//
// Created by Matthew on 6/13/2018.
//
/********************************
 * Dependancies
 ********************************/
#include "NodeList.h"

/********************************
 * Variables
 ********************************/
AppSensor Sensors[EMBER_AF_PLUGIN_ADDRESS_TABLE_SIZE] = {};

EmberEUI64 ListEuiNode[Node_count] = {};

int16s ListNodeField[Node_count][MAX_NUM_NODE_FIELDS] = {};

int16u numSensors = 0;

/********************************
 * Node List
 ********************************/
int8u compareEUI64(EmberEUI64 left, EmberEUI64 right) {
    int8u i;
    for (i = 0; i < EUI64_SIZE; i++) {
        if (left[i] != right[i]) {
            return (int8u)-1;
        }
    }
    return (int8u)1;
}

int8s findSensorListIndex(EmberNodeId nodeId, EmberEUI64 nodeEui64) {
    int8s sensorListIndex = -1;

    int i;
    for (i = 0; i < numSensors; i++) {
        if (Sensors[i].nodeId == nodeId || compareEUI64(Sensors[i].address,nodeEui64) == 1) {
            if (Sensors[i].nodeId == nodeId && compareEUI64(Sensors[i].address,nodeEui64) == -1) {
//                Sensors[i].address = nodeEui64;
                int x = 0;
                for (x; x < EUI64_SIZE; x++) {
                    Sensors[i].address[x] = nodeEui64[x];
                }
//                memcpy(nodeEui64, Sensors[i].address, EUI64_SIZE);
            }
            else if (Sensors[i].nodeId != nodeId && compareEUI64(Sensors[i].address,nodeEui64) == 1) {
                Sensors[i].nodeId = nodeId;
            }
            sensorListIndex = i;
            break;
        }
    }

    if (sensorListIndex >= 0) {
        printf("already in Sensors list\n");
    } else {
        printf("Adding to Sensors list\n");
        // If it's a new sensor, add it to the list
        i = numSensors;
        numSensors++;

        AppSensor s = {};
        s.nodeId = nodeId;
        s.nodeNumber = DEFAULT_NODE_NUMBER;
        s.sensors = 0;
        s.index = EMBER_NULL_ADDRESS_TABLE_INDEX;
        s.temp = 0;
        s.cur = 0;
        s.xaccel = 0;
        s.yaccel = 0;
        s.zaccel = 0;
        s.nodeParent = 1;
        s.nodeRole = 0;
//        memcpy(s.address, nodeEui64, EUI64_SIZE);
//        s.address = nodeEui64;
        int x = 0;
        for (x; x < EUI64_SIZE; x++) {
            s.address[x] = nodeEui64[x];
        }


        Sensors[i] = s;
    }

    return sensorListIndex;
}

//This function will return the index of an EUI address fron the table ListEuiNode
// if the address is not on the table, will return FF, this shouldn't happen but just in case.
int8u CheckListEuiNode(EmberEUI64 EUI) {

    boolean flag = FALSE;
    EmberEUI64 temp;
    int8u i;
    int8u j;

    for (i = 0; i < (Node_count); i++) {

//        temp = &ListEuiNode[i];
        memcpy(temp, ListEuiNode[i], EUI64_SIZE);

        flag = TRUE;

        for (j = 0; j < EUI64_SIZE; j++) {

            if (EUI[j] && EUI[j] != temp[j]) {
                flag = FALSE;
                break;
            }
        }

        if (flag == TRUE) {
            return i; // this value will indicate the index on the table
        }
    }
    return (int8u)0xff; // this value will indicate there is no match on the table
}

int16s NextListEuiNode(void) {

    int8u *q;
    boolean flag = FALSE;
    EmberEUI64 temp;
    int16s i;
    int16s j;

    for (i = 0; i < (Node_count); i++) {
//        temp = &ListEuiNode[i];
        memcpy(temp, ListEuiNode[i], EUI64_SIZE);

        q = (int8u *) temp;
        flag = TRUE;
        for (j = 0; j < EUI64_SIZE; j++) {
            if (q[j] && q[j] != 0x00) {
                flag = FALSE;
                break;
            }
        }

        if (flag == TRUE) {
            return i; // this value will indicate the index on the table
        }
    }
    return (int16s)-1; // this value will indicate there is no match on the table
}

void setNodeNumberByAddress(EmberEUI64 address, int8s nodeNumber) {
    int8u *p;
    int8u *q;
    boolean flag = FALSE;
    EmberEUI64 temp;
    int i;
    int j;

    emberAfAppPrintln("Setting Sensor NodeNumber for address : %x%x%x%x%x%x%x%x\n", address[0], address[1],
                      address[2], address[3], address[4], address[5], address[6], address[7]);

    for (i = 0; i < numSensors; i++){
//        memcpy(temp, Sensors[i].address, EUI64_SIZE);
//        temp[i] = Sensors[i].address;
        int x = 0;
        for (x; x < EUI64_SIZE; x++) {
            temp[x] = Sensors[i].address[x];
        }

        emberAfAppPrintln("temp address : %x%x%x%x%x%x%x%x", temp[0], temp[1],
                          temp[2], temp[3], temp[4], temp[5], temp[6], temp[7]);


        flag = TRUE;

        for (j = 0; j < EUI64_SIZE; j++) {

            if (address[j] != temp[j]) {
                printf("Node Address not a Match\n\n");
                flag = FALSE;
                break;
            }
        }

        if (flag == TRUE) {
            printf("Setting Node Number: %d\n", nodeNumber);
            Sensors[i].nodeNumber = nodeNumber;
            break;
        }
    }

    if (flag == FALSE) {
        printf("Adding to Sensors list\n");
        i = numSensors;
        numSensors++;

        AppSensor s = {};
        s.nodeId = 0xFFFF;
        s.nodeNumber = nodeNumber;
        s.sensors = 0;
        s.index = EMBER_NULL_ADDRESS_TABLE_INDEX;
        s.temp = 0;
        s.cur = 0;
        s.xaccel = 0;
        s.yaccel = 0;
        s.zaccel = 0;
        s.nodeParent = 1;
        s.nodeRole = 0;

        int x = 0;
        for (x; x < EUI64_SIZE; x++) {
            s.address[x] = address[x];
        }

        Sensors[i] = s;
    }
}

/********************************
 * Node Fields
 ********************************/
int16s getFieldId(int16s nodeNumber, int8u col) {
    int i;
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i][NODE_FIELD_NODE_NUMBER] == nodeNumber) {
            return ListNodeField[i][col];
        }
    }
    return (int16s)-1;
}

void updateFieldId(int16s nodeNumber, int8u col, int16s fieldId) {
    int i;
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i][NODE_FIELD_NODE_NUMBER] == nodeNumber) {
            ListNodeField[i][col] = fieldId;
            return;
        }
    }
    //Didn't find the nodeId - Add it
    for (i = 0; i < Node_count; i++) {
        if (!ListNodeField[i][NODE_FIELD_NODE_NUMBER] || ListNodeField[i][NODE_FIELD_NODE_NUMBER] == -1) {
            ListNodeField[i][NODE_FIELD_NODE_NUMBER] = nodeNumber;
            ListNodeField[i][col] = fieldId;
            return;
        }
    }

}

void resetFieldIds(int16s nodeNumber) {
    int i;
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i] && ListNodeField[i][NODE_FIELD_NODE_NUMBER] && ListNodeField[i][NODE_FIELD_NODE_NUMBER] == nodeNumber) {
            ListNodeField[i][NODE_FIELD_TEMP] = -1;
            ListNodeField[i][NODE_FIELD_X_ACCEL] = -1;
            ListNodeField[i][NODE_FIELD_Y_ACCEL] = -1;
            ListNodeField[i][NODE_FIELD_Z_ACCEL] = -1;
            return;
        }
    }
    //Didn't find the nodeId - Add it
    for (i = 0; i < Node_count; i++) {
        if (ListNodeField[i] && (!ListNodeField[i][NODE_FIELD_NODE_NUMBER] || ListNodeField[i][NODE_FIELD_NODE_NUMBER] == -1)) {
            ListNodeField[i][NODE_FIELD_NODE_NUMBER] = nodeNumber;
            ListNodeField[i][NODE_FIELD_TEMP] = -1;
            ListNodeField[i][NODE_FIELD_X_ACCEL] = -1;
            ListNodeField[i][NODE_FIELD_Y_ACCEL] = -1;
            ListNodeField[i][NODE_FIELD_Z_ACCEL] = -1;
            return;
        }
    }
}