//
// Created by Matthew on 7/18/2018.
//

#ifndef MATT_GATEWAY_TESTOUTPUT_H
#define MATT_GATEWAY_TESTOUTPUT_H

#include "app/framework/include/af.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

extern boolean allowGeneralDebugPrint;

void GeneralDebugPrint(const char *fmt, ...);

#endif //MATT_GATEWAY_TESTOUTPUT_H
