//
// Created by mharrin3 on 1/10/2019.
//

#ifndef UNB_SENSOR_NETWORK_FILEWRITER_H
#define UNB_SENSOR_NETWORK_FILEWRITER_H

void writeDataToFile(char *dataPath, int fieldId, float data);

#endif //UNB_SENSOR_NETWORK_FILEWRITER_H
