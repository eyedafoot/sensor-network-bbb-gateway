//
// Created by Matthew on 7/18/2018.
//

#include "TestOutPut.h"

#include "app/framework/include/af.h"

#include <stdio.h>
#include <stdarg.h>


//Output for network connection
//Output for data received from nodes
//Output for data from server
//Output for data to server
//Output for general use
boolean allowGeneralDebugPrint = FALSE;

void GeneralDebugPrint(const char *fmt, ...) {
    if (allowGeneralDebugPrint) {
        va_list args;
        va_start(args, fmt);
        vprintf(fmt, args);
        va_end(args);
    }
}