#ifndef CONFIGURATION_TYPES_H
#define CONFIGURATION_TYPES_H

#include "configurationTypes.h"

void initDefaultConfigs(AcclConfiguration *accl, TempConfiguration *temp, AdcConfiguration *adc);

void encodeConfig(AcclConfiguration accl, AdcConfiguration adc, TempConfiguration temp, int8u *buffer, int *length);

void decodeConfig(int8u *nodeId, int8u *buffer, int length, AcclConfiguration *accl, AdcConfiguration *adc, TempConfiguration *temp);

#endif //CONFIGURATION_TYPES_H