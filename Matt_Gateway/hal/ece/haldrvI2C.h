#ifndef _HAL_I2C_H
#define _HAL_I2C_H

/** @brief Initialize I2C
 *
 * Configure SC1 to operate as a I2C (Two Wire Serial) bus master. This
 * function should be called automatically by the stack on power-up
 * (see eznode.h)
 */
void halI2CInit(void);

/** @brief I2C Write 8 Bits
 *
 * Write "data" to the I2C device at "address".
 *
 * @param address
 * @param regester
 * @param data
 */
void halI2CWriteint8u(int8u address, int8u regester, int8u data);

/** @brief I2C Write 16 Bits
 *
 * Write "data" to the I2C device at "address".
 *
 * @param address
 * @param regester
 * @param data
 */
void halI2CWriteint16u(int8u address, int8u regester, int16u data);

/** @brief I2C Read 8 Bits
 *
 * Return the value in "regester" from the I2C device at "address".
 *
 * @param address
 * @param regester
 */
int8u halI2CReadint8u(int8u address, int8u regester);

/** @brief I2C Read 16 Bits
 *
 * Return the value in "regester" from the I2C device at "address".
 *
 * @param address
 * @param regester
 */
int16u halI2CReadint16u(int8u address, int8u regester);

/** @brief I2C Read
 *
 * Read data will be placed into the provided buffer.
 *
 * @param address
 * @param regester
 * @param buffer
 * @param count
 */
void halI2CRead(int8u address, int8u regester, int8u * buffer, int8u count);

/** @brief I2C Write
 *
 * Write the provided data buffer to the specified address.
 *
 * @param address
 * @param regester
 * @param buffer
 * @param count
 */
void halI2CWrite(int8u address, int8u regester, const int8u * buffer, int8u count);

/** @brief I2C Read with 8-bit cyclic redundancy code (CCITT CRC 8) checking
 *
 * Read data will be placed into the provided buffer. A returnd value of 0 means
 * CRC checking was OK (NO ERROR). Otherwise, an error was detected
 *
 * @param address
 * @param regester
 * @param buffer
 * @param count
 */
int8u halI2CReadWithCRC(int8u address, int8u regester, int8u * buffer, int8u count);

/** @brief I2C Write with 8-bit cyclic redundancy code (CCITT CRC 8) appended 
 *
 * Write the provided data buffer to the specified address.
 *
 * @param address
 * @param regester
 * @param buffer
 * @param count
 */
void halI2CWriteWithCRC(int8u address, int8u regester, const int8u * buffer, int8u count);
   
/** @brief Calculates 8-bit cyclic redundancy code (CCITT CRC 8).
 *
 * This function expands the CRC functions given in crc.c from Freescale.
 * CRC calculations are done using a lookup table.
 *
 * Applies the standard CITT CRC 8 polynomial (X^8 + X^2 + X + 1)
 * to a single byte. It should support being called first with an initial
 * value, then repeatedly until all data is processed. 
 * 
 * CRC algorithms differ from each other in two other regards:
 *  - The initial value of the register.
 *  - The value to be XORed with the final register value.
 * The most common initial value and XORed value is zero.
 * If a different XORed value is to be used, then the last CRC result should be 
 * XORed with that value. 
 *
 * @param newByte     The new byte to be run through CRC.
 *
 * @param prevResult  The previous CRC result.
 *
 * @return The new CRC result.
 */
int8u halCommonCRC8 ( int8u newByte, int8u prevResult  );
#endif
