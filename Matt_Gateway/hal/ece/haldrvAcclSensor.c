#include "app/framework/include/af.h"
#include <haldrvI2C.h>
#include <haldrvUtils.h>
#include <haldrvAcclSensor.h>

/* 
 * All variables and functions that are visible outside of the module have the 
 * device name prepended to them. This makes it easier to know the device we are 
 * working with and where to look for function and variable definitions.
 *
 * Functions are separated by a single-line comment consisting only of dashes. 
 */


/******************************************************************************/
/* MMA8452Q STATUS Register                                              */
/******************************************************************************/
/* (Register Address: 0x00. Access:Read Only. Default value: 00)

 The MMA8452Q has 12-bit XYZ data. The event flag can be monitored by reading the 
 STATUS register (0x00). This can be done by using either a polling or interrupt 
 technique. It is not absolutely necessary to read the STATUS register to clear 
 it. Reading the data clears the STATUS register. 
*/
#define ST_ZYX_OW_BIT 7  /* X, Y, Z-axis Data Overwrite.        */
#define ST_Z_OW_BIT   6  /* Z-axis Data Overwrite.              */
#define ST_Y_OW_BIT   5  /* Y-axis Data Overwrite.              */
#define ST_X_OW_BIT   4  /* X-axis Data Overwrite.              */
#define ST_ZYX_DR_BIT 3  /* X, Y, Z-axis new Data Ready.        */
#define ST_Z_DR_BIT   2  /* Z-axis new Data Available.          */
#define ST_Y_DR_BIT   1  /* Y-axis new Data Available.          */
#define ST_X_DR_BIT   0  /* X-axis new Data Available.          */

#define ST_ZYX_OW BIT(7) /* X, Y, Z-axis Data Overwrite. Default value: 0. 
                            0: No data overwrite has occurred 
                            1: Previous X, Y, or Z data was overwritten by new X, Y, or Z data before it was read*/
#define ST_Z_OW   BIT(6) /* Z-axis Data Overwrite. Default value: 0
                            0: No data overwrite has occurred
                            1: Previous Z-axis data was overwritten by new Z-axis data before it was read*/
#define ST_Y_OW   BIT(5) /* Y-axis Data Overwrite. Default value: 0
                            0: No data overwrite has occurred
                            1: Previous Y-axis data was overwritten by new Y-axis data before it was read */
#define ST_X_OW   BIT(4) /* X-axis Data Overwrite. Default value: 0
                            0: No data overwrite has occurred
                            1: Previous X-axis data was overwritten by new X-axis data before it was read */
#define ST_ZYX_DR BIT(3) /* X, Y, Z-axis new Data Ready. Default value: 0
                            0: No new set of data ready
                            1: A new set of data is ready */
#define ST_Z_DR   BIT(2) /* Z-axis new Data Available. Default value: 0
                            0: No new Z-axis data is ready
                            1: A new Z-axis data is ready */
#define ST_Y_DR   BIT(1) /* Y-axis new Data Available. Default value: 0
                            0: No new Y-axis data ready
                            1: A new Y-axis data is ready */
#define ST_X_DR   BIT(0) /* X-axis new Data Available. Default value: 0
                            0: No new X-axis data ready
                            1: A new X-axis data is ready */

/******************************************************************************/
/* MMA8452Q System Mode (SYSMOD) Register                                     */
/******************************************************************************/
/*   (Register Address: 0x0B. Access:Read Only. Default value: 00)

 The system mode register indicates the current device operating mode. 
 Applications using the Auto-SLEEP/WAKE mechanism should use this register to 
 synchronize the application with the device operating mode transitions.
*/

#define STANDBY_MODE 0x00 // In Standby Mode the device responds to I2C communication but doesnt allow for updated data
#define WAKE_MODE    0x01
#define SLEEP_MODE   0x02

/******************************************************************************/
/* MMA8452Q ystem Interrupt Status Register  (INT_SOURCE)                     */
/******************************************************************************/
/* (Register Address: 0x0C. Access:Read only. Default value: 00)

 In the interrupt source register the status of the various embedded features 
 can be determined. The bits that are set (logic 1) indicate which function has 
 asserted an interrupt and conversely the bits that are cleared (logic 0) 
 indicate which function has not asserted or has deasserted an interrupt. The 
 bits are set by a low to high transition and are cleared by reading the
 appropriate interrupt source register. The SRC_DRDY bit is cleared by reading 
 the X, Y and Z data. It is not cleared by simply reading the Status Register (0x00). 
*/
#define INT_SOURCE_SRC_ASLP_BIT   7 
#define INT_SOURCE_SRC_TRANS_BIT  6 
#define INT_SOURCE_SRC_LNDPRT_BIT 4 
#define INT_SOURCE_SRC_PULSE_BIT  3 
#define INT_SOURCE_SRC_FF_MT_BIT  2 
#define INT_SOURCE_SRC_DRDY_BIT   0 
                              
#define INT_SOURCE_SRC_ASLP   BIT(7) /* interrupt event that can cause a WAKE to 
                                     SLEEP or SLEEP to WAKE system mode 
                                     transition has occurred.*/
#define INT_SOURCE_SRC_TRANS  BIT(6) /* an acceleration transient value greater 
                                     than user specified threshold has occurred. */
#define INT_SOURCE_SRC_LNDPRT BIT(4) /* an interrupt was generated due to a change 
                                     in the device orientation status*/
#define INT_SOURCE_SRC_PULSE  BIT(3) /* an interrupt was generated due to single 
                                     and/or double pulse event*/
#define INT_SOURCE_SRC_FF_MT  BIT(2) /* indicates that the Freefall/Motion 
                                     function interrupt is active*/
#define INT_SOURCE_SRC_DRDY   BIT(0) /* the X, Y, Z data ready interrupt is active 
                                     indicating the presence of new data and/or 
                                     data overrun*/                             
                              
/******************************************************************************/
/* MMA8452Q XYZ_DATA_CFG Register                                             */
/******************************************************************************/
/* (Register Address: 0x0E. Access:Read Write. Default value: 00)

 The XYZ_DATA_CFG register sets the dynamic range and sets the high pass filter 
 for the output data. When the HPF_OUT bit is set. The data registers 
 (RA 0x01 - 0x06) will contain high pass filtered data when this bit is set. 
*/
#define XYZ_HPF_OUT_BIT 4 /* Enable High pass output data 1 = output data high pass filtered. Default value: 0 */
#define XYZ_FS1_BIT     1 /* Output buffer data format full scale. Default value: 00 (2g).*/
#define XYZ_FS0_BIT     0

#define XYZ_HPF_OUT  BIT(4) /* Enable High pass output data. */
#define XYZ_FSMASK  (BIT(0) | BIT(1)) /* There are 3 different dynamic ranges that can be set(2g, 4g, 8g). 
                                         The dynamic range is changeable only in the Standby Mode.
                                         FS1   FS0   g Range
                                          0      0     ±2g
                                          0      1     ±4g
                                          1      0     ±8g
                                          1      1                          */
/******************************************************************************/
/* MMA8452Q HP_FILTER_CUTOFF Register                                             */
/******************************************************************************/
/* (Register Address: 0x0F. Access:Read Write. Default value: 00)
                              
  This register sets the high-pass filter cutoff frequency for removal of the 
  offset and slower changing acceleration data. The output of this filter is 
  indicated by the data registers (0x01-0x06) when bit 4 (HPF_OUT) of Register 
  0x0E is set. The filter cutoff options change based on the data rate selected.
  For details of implementation on the high-pass filter, refer to Freescale 
  application note, AN4071.
 */
#define HP_FILTER_CUTOFF_PULSE_HPF_BYP_BIT 5  
#define HP_FILTER_CUTOFF_PULSE_LPF_EN_BIT 4   
#define HP_FILTER_CUTOFF_SEL0_BIT 0 
#define HP_FILTER_CUTOFF_SEL1_BIT 1 

#define HP_FILTER_CUTOFF_PULSE_HPF_BYP   BIT(5) /* Bypass High-Pass Filter for Pulse Processing Function.
                                                0: HPF enabled for Pulse Processing, 
                                                1: HPF Bypassed for Pulse Processing
                                                Default value: 0.*/
#define HP_FILTER_CUTOFF_PULSE_LPF_EN    BIT(4) /* Enable Low-Pass Filter for Pulse Processing Function.
                                                0: LPF disabled for Pulse Processing, 
                                                1: LPF Enabled for Pulse Processing
                                                Default value: 0.*/
#define HP_FILTER_CUTOFF_SEL_MASK BIT(1)|BIT(0) /* HPF Cutoff frequency selection.
                                                Default value: 00 (see Table on datasheet).*/
#define HP_FILTER_CUTOFF_DEFAULT 16             /* Cut-off frequency is set to 16 Hz @ 800 Hz*/

/******************************************************************************/
/* MMA8452Q Portrait/Landscape Status Register  (PL_STATUS)                   */
/******************************************************************************/
/* (Register Address: 0x10. Access:Read Only. Default value: 00)

 This status register can be read to get updated information on any change in 
 orientation by reading Bit 7, or on the specifics of the orientation by reading 
 the other bits. For further understanding of Portrait Up, Portrait Down, 
 Landscape Left, Landscape Right, Back and Front orientations please refer to 
 AN4068 application note. The interrupt is cleared when reading the PL_STATUS 
 register.
*/
                                            
/*Back or Front orientation. Default value: 0
0: Front: Equipment is in the front facing orientation.
1: Back:  Equipment is in the back facing orientation.*/                                            
#define PL_ST_BAFRO_BIT  0

 /*Landscape/Portrait orientation. Default value: 00
00: Portrait Up:     Equipment standing vertically in the normal orientation
01: Portrait Down:   Equipment standing vertically in the inverted orientation
10: Landscape Right: Equipment is in landscape mode to the right
11: Landscape Left:  Equipment is in landscape mode to the left.*/
#define PL_ST_LAPO0_BIT  1   
#define PL_ST_LAPO1_BIT  2 

/*Z-Tilt Angle Lockout. Default value: 0.
0: Lockout condition has not been detected.
1: Z-Tilt lockout trip angle has been exceeded. Lockout has been detected.*/                                            
#define PL_ST_LO_BIT     6   
                                            
/*Landscape/Portrait status change flag. Default value: 0.
  0: No change, 
  1: BAFRO and/or LAPO and/or Z-Tilt lockout value has changed*/
#define PL_ST_NEWLP_BIT  7  

/* PL_ST MASKS */
#define PL_ST_BAFR_MASK     BIT(PL_ST_BAFRO_BIT)
#define PL_ST_LAPO_MASK     BIT(PL_ST_LAPO0_BIT) | BIT(PL_ST_LAPO1_BIT)
#define PL_ST_LOCKOUT_MASK  BIT(PL_ST_LO_BIT)
                                            
/* PL_ST STATUSES */
#define PL_ST_PO_UP    0 // 00:Portrait Up: Equipment standing vertically in the normal orientation
#define PL_ST_PO_DOWN  2 // 01:Portrait Down: Equipment standing vertically in the inverted orientation
#define PL_ST_LA_RIGHT 4 // 10:Landscape Right: Equipment is in landscape mode to the right
#define PL_ST_LA_LEFT  6 // 11:Landscape Left: Equipment is in landscape mode to the left.

/******************************************************************************/
/* MMA8452Q Portrait/Landscape Configuration Register (PL_CFG)                */
/******************************************************************************/
/* (Register Address: 0x11. Access:Read Write. Default value: 00)
  This register enables the Portrait/Landscape function and sets the behavior of 
  the debounce counter*/
#define PL_CFG_PL_EN_BIT   6 
#define PL_CFG_DBCNTM_BIT  7
                              
#define PL_CFG_PL_EN  BIT(6) /* Portrait/Landscape Detection Enable. Default value: 0
                             0: Portrait/Landscape Detection is Disabled.
                             1: Portrait/Landscape Detection is Enabled.*/
#define PL_CFG_DBCNTM BIT(7)/* Debounce counter mode selection. Default value: 1
                             0: Decrements debounce whenever condition of interest is no longer valid.
                             1: Clears counter whenever condition of interest is no longer valid.*/

/******************************************************************************/
/* MMA8452Q Back/Front and Z Compensation Register  (PL_BF_ZCOMP)             */
/******************************************************************************/
/* (Register Address: 0x13. Access:Read Only. Default value: 0x44)
  The Z-Lock angle compensation is set to 29°. The Back to Front trip angle is 
  set to ±75°.*/
#define PL_BF_ZCOMP_ZLOCK0_BIT 0 /* Z-Lock Angle Fixed Threshold = 100 which is 29°.*/
#define PL_BF_ZCOMP_ZLOCK1_BIT 1
#define PL_BF_ZCOMP_ZLOCK2_BIT 2
                              
#define PL_BF_ZCOMP_BKFR0_BIT  6  /* Back Front Trip Angle Fixed Threshold = 01 which is = ±75°. */                          
#define PL_BF_ZCOMP_BKFR1_BIT  7
                              
#define PL_BF_ZCOMP_BKFR_MASK   BIT(PL_BF_ZCOMP_BKFR1_BIT)  \
                              | BIT(PL_BF_ZCOMP_BKFR0_BIT)   
#define PL_BF_ZCOMP_ZLOCK_MASK  BIT(PL_BF_ZCOMP_ZLOCK2_BIT) \
                              | BIT(PL_BF_ZCOMP_ZLOCK1_BIT) \
                              | BIT(PL_BF_ZCOMP_ZLOCK0_BIT)

/******************************************************************************/
/* MMA8452Q Portrait/Landscape Threshold and Hysteresis Register (P_L_THS_REG)*/
/******************************************************************************/
/* (Register Address: 0x14. Access:Read Only. Default value: 0x84  
  This register represents the Portrait to Landscape trip threshold */

#define P_L_THS_REG_THS_BIT  3 
#define P_L_THS_REG_HYS_BIT  0

#define P_L_THS_REG_THS_MASK 0xF8 /* Portrait/Landscape Fixed Threshold 
                                  angle = 1_0000 (45°).*/
#define P_L_THS_REG_HYS_MASK 0x07 /* This is a fixed angle added to the threshold 
                                  angle for a smoother transition from Portrait 
                                  to Landscape and Landscape to Portrait. This 
                                  angle is fixed at ±14°, which is 100.*/
 
/******************************************************************************/
/* MMA8452Q Freefall/Motion Configuration Register (FF_MT_CFG)                */
/******************************************************************************/
/* (Register Address: 0x15. Access:Read Write. Default value: 0x84  
  This is the Freefall/Motion configuration register for setting up the 
  conditions of the freefall or motion function.*/

#define FF_MT_CFG_ELE_BIT  7 
#define FF_MT_CFG_OAE_BIT  6  
#define FF_MT_CFG_ZEFE_BIT 5  
#define FF_MT_CFG_YEFE_BIT 4  
#define FF_MT_CFG_XEFE_BIT 3  

#define FF_MT_CFG_ELE  BIT(7) /* Event Latch Enable: Event flags are latched into 
                              FF_MT_SRC register. Reading of the FF_MT_SRC 
                              register clears the event flag EA and all 
                              FF_MT_SRC bits. Default value: 0.*/
#define FF_MT_CFG_OAE  BIT(6) /* Motion detect / Freefall detect flag selection. Default value: 0. (Freefall Flag)
                              0: Freefall Flag (Logical AND combination)
                              1: Motion Flag (Logical OR combination)*/
#define FF_MT_CFG_ZEFE BIT(5) /* Event flag enable on Z Default value: 0.
                              0: event detection disabled; 
                              1: raise event flag on measured acceleration value beyond preset threshold*/
#define FF_MT_CFG_YEFE BIT(4) /* Idem Y*/
#define FF_MT_CFG_XEFE BIT(3) /* Idem X*/
                                
#define FF_MT_CFG_XEFE_MASK  BIT(FF_MT_CFG_ZEFE_BIT)\
                           | BIT(FF_MT_CFG_YEFE_BIT)\
                           | BIT(FF_MT_CFG_XEFE_BIT)

/******************************************************************************/
/* MMA8452Q Freefall and Motion Source Register (FF_MT_SRC)                   */
/******************************************************************************/
/* (Register Address: 0x16. Access:Read Only. Default value: 0x00  
  This register keeps track of the acceleration event which is triggering (or 
  has triggered, in case of ELE bit in FF_MT_CFG register being set to 1) the 
  event flag. In particular EA is set to a logic 1 when the logical combination 
  of acceleration events flags specified in FF_MT_CFG register is true. This bit 
  is used in combination with the values in INT_EN_FF_MT and INT_CFG_FF_MT 
  register bits to generate the freefall/motion interrupts. An X,Y, or Z motion 
  is true when the acceleration value of the X or Y or Z channel is higher than 
  the preset threshold value defined in the FF_MT_THS register.
  Conversely an X, Y, and Z low event is true when the acceleration value of the 
  X and Y and Z channel is lower than or equal to the preset threshold value 
  defined in the FF_MT_THS register.*/
#define FF_MT_SRC_EA_BIT  7 
#define FF_MT_SRC_ZHE_BIT 5 
#define FF_MT_SRC_ZHP_BIT 4 
#define FF_MT_SRC_YHE_BIT 3 
#define FF_MT_SRC_YHP_BIT 2 
#define FF_MT_SRC_XHE_BIT 1 
#define FF_MT_SRC_XHP_BIT 0 

#define FF_MT_SRC_EA BIT(7) /* Event Active Flag. Default value: 0.
                             0: No event flag has been asserted; 
                             1: one or more event flag has been asserted.
                             See the description of the OAE bit to determine the effect of the 3-axis event flags on the EA bit.*/
#define FF_MT_SRC_ZHE BIT(5) /* Z Motion Flag. Default value: 0.
                             0: No Z Motion event detected, 
                             1: Z Motion has been detected
                             This bit reads always zero if the ZEFE control bit is set to zero*/
#define FF_MT_SRC_ZHP BIT(4) /* Z Motion Polarity Flag. Default value: 0.
                             0: Z event was Positive g, 
                             1: Z event was Negative g
                             This bit read always zero if the ZEFE control bit is set to zero*/
#define FF_MT_SRC_YHE BIT(3) /* Idem ZHE*/
#define FF_MT_SRC_YHP BIT(2) /* Idem ZHP*/
#define FF_MT_SRC_XHE BIT(1) /* Idem ZHE*/
#define FF_MT_SRC_XHP BIT(0) /* Idem ZHP*/              
                             
/******************************************************************************/
/* MMA8452Q Freefall and Motion Threshold Register (FF_MT_THS)                */
/******************************************************************************/
/* (Register Address: 0x17. Access:Read Write. Default value: 0x00  
  The threshold resolution is 0.063g/LSB and the threshold register has a range 
of 0 to 127 counts. The maximum range is to 8g. Note that even when the full 
scale value is set to 2g or 4g the motion detects up to 8g. If the Low-Noise 
bit is set in Register 0x2A then the maximum threshold will be limited to 4g 
regardless of the full scale range. DBCNTM bit configures the way in which 
the debounce counter is reset when the inertial event of interest is momentarily 
not true..*/

#define FF_MT_THS_DBCNTM_BIT 7
#define FF_MT_THS_THS0_BIT 0

#define FF_MT_THS_DBCNTM BIT(7) /* Debounce counter mode selection. Default value: 0.
                                0: increments or decrements debounce, 
                                1: increments or clears counter.*/
#define FF_MT_THS_MASK     0x7F /* Freefall/Motion Threshold: Default value: 000_0000.*/

/******************************************************************************/
/* MMA8452Q Transient Configuration Register (TRANSIENT_CFG)                  */
/******************************************************************************/
/* (Register Address: 0x1D. Access:Read Write. Default value: 0x00  
  The transient detection mechanism can be configured to raise an interrupt when 
the magnitude of the high-pass filtered acceleration threshold is exceeded. 
The TRANSIENT_CFG register is used to enable the transient interrupt generation
mechanism for the 3 axes (X, Y, Z) of acceleration. There is also an option to 
bypass the high-pass filter. When the high-pass filter is bypassed, the function 
behaves similar to the motion detection.*/

#define TRANSIENT_CFG_ELE_BIT     4
#define TRANSIENT_CFG_ZTEFE_BIT   3 
#define TRANSIENT_CFG_YTEFE_BIT   2 
#define TRANSIENT_CFG_XTEFE_BIT   1 
#define TRANSIENT_CFG_HPF_BYP_BIT 0

#define TRANSIENT_CFG_ELE     BIT(4) /* Transient event flags are latched into 
                                     the TRANSIENT_SRC register. Reading of the 
                                     TRANSIENT_SRC register clears the event flag. 
                                     Default value: 0. 
                                     0: Event flag latch disabled; 
                                     1: Event flag latch enabled*/
#define TRANSIENT_CFG_ZTEFE   BIT(3) /* Event flag enable on Z transient 
                                     acceleration greater than transient 
                                     threshold event. Default value: 0.
                                     0: Event detection disabled; 
                                     1: Raise event flag on measured acceleration 
                                     delta value greater than transient threshold.*/
#define TRANSIENT_CFG_YTEFE   BIT(2) /* Idem for Y*/
#define TRANSIENT_CFG_XTEFE   BIT(1) /* Idem for X*/
#define TRANSIENT_CFG_HPF_BYP BIT(0) /* Bypass High-Pass filter Default value: 0.
                                     0: Data to transient acceleration detection 
                                        block is through HPF 
                                     1: Data to transient acceleration detection 
                                        block is NOT through HPF (similar to 
                                        motion detection function)*/

/******************************************************************************/
/* MMA8452Q Transient Source Register (TRANSIENT_SRC)                  */
/******************************************************************************/
/* (Register Address: 0x1E. Access:Read Only. Default value: 0x00  
  The Transient Source register provides the status of the enabled axes and the polarity (directional) information. When this
register is read it clears the interrupt for the transient detection. When new events arrive while EA = 1, additional *TRANSE bits
may get set, and the corresponding *_Trans_Pol flag become updated. However, no *TRANSE bit may get cleared before the
TRANSIENT_SRC register is read.*/

#define TRANSIENT_SRC_EA_BIT          6
#define TRANSIENT_SRC_ZTRANSE_BIT     5 
#define TRANSIENT_SRC_Z_Trans_Pol_BIT 4 
#define TRANSIENT_SRC_YTRANSE_BIT     3
#define TRANSIENT_SRC_Y_Trans_Pol_BIT 2 
#define TRANSIENT_SRC_XTRANSE_BIT     1 
#define TRANSIENT_SRC_X_Trans_Pol_BIT 0 

#define TRANSIENT_SRC_EA          BIT(6) /* Event Active Flag. Default value: 0.
                                         0: no event flag has been asserted; 
                                         1: one or more event flag has been asserted.*/
#define TRANSIENT_SRC_ZTRANSE     BIT(5) /* Z transient event. Default value: 0.
                                         0: no interrupt, 
                                         1: Z Transient acceleration greater 
                                            than the value of TRANSIENT_THS event has occurred*/
#define TRANSIENT_SRC_Z_Trans_Pol BIT(4) /* Polarity of Z Transient Event that 
                                         triggered interrupt. Default value: 0.
                                         0: Z event was Positive g, 
                                         1: Z event was Negative g*/
#define TRANSIENT_SRC_YTRANSE     BIT(3) /* Idem Y*/
#define TRANSIENT_SRC_Y_Trans_Pol BIT(2) 
#define TRANSIENT_SRC_XTRANSE     BIT(1) /* Idem X*/
#define TRANSIENT_SRC_X_Trans_Pol BIT(0) 

/******************************************************************************/
/* MMA8452Q Transient Threshold Register (TRANSIENT_THS)                */
/******************************************************************************/
/* (Register Address: 0x17. Access:Read Write. Default value: 0x00  
  The Transient Threshold register sets the threshold limit for the detection 
of the transient acceleration. The value in the TRANSIENT_THS register 
corresponds to a g value which is compared against the values of High-Pass 
Filtered Data. If the High-Pass Filtered acceleration value exceeds the 
threshold limit, an event flag is raised and the interrupt is generated if enabled. 
                             
The threshold THS[6:0] is a 7-bit unsigned number, 0.063g/LSB. The maximum 
threshold is 8g. Even if the part is set to full scale at 2g or 4g this function 
will still operate up to 8g. If the Low-Noise bit is set in Register 0x2A, the 
maximum threshold to be reached is 4g.
Note: If configuring the transient detection threshold for less than 1g, the 
high-pass filter will need some settling time. The settling time will vary 
depending on selected ODR, high-pass frequency cutoff and threshold. */

#define TRANSIENT_THS_DBCNTM_BIT 7
#define TRANSIENT_THS_THS0_BIT 0

#define TRANSIENT_THS_DBCNTM BIT(7) /* Debounce counter mode selection. Default value: 0.
                                0: increments or decrements debounce, 
                                1: increments or clears counter.*/
#define TRANSIENT_THS_MASK    0x7F /* Transient Threshold: Default value: 000_0000.*/

/* --- Single, Double and Pulse detection Registers--- */
/******************************************************************************/
/* MMA8452Q Pulse Configuration Register (PULSE_CFG)                  */
/******************************************************************************/
/* (Register Address: 0x21. Access:Read Write. Default value: 0x00  
  For more details of how to configure the pulse detection and sample code, 
  please refer to Freescale application note, AN4072.*/

#define PULSE_CFG_DPE_BIT    7 
#define PULSE_CFG_ELE_BIT    6 
#define PULSE_CFG_ZDPEFE_BIT 5  
#define PULSE_CFG_ZSPEFE_BIT 4  
#define PULSE_CFG_YDPEFE_BIT 3  
#define PULSE_CFG_YSPEFE_BIT 2  
#define PULSE_CFG_XDPEFE_BIT 1  
#define PULSE_CFG_XSPEFE_BIT 0  

#define PULSE_CFG_DPE    BIT(7) /* Double Pulse Abort. Default value: 0.
                             0: Double Pulse detection is not aborted if the 
                             start of a pulse is detected during the time period 
                             specified by the PULSE_LTCY register.
                             1: Setting the DPA bit momentarily suspends the 
                             double pulse detection if the start of a pulse is 
                             detected during the time period specified by the 
                             PULSE_LTCY register and the pulse ends before the 
                             end of the time period specified by the PULSE_LTCY 
                             register.*/
#define PULSE_CFG_ELE    BIT(6) /* Pulse event flags are latched into the 
                             PULSE_SRC register. Reading of the PULSE_SRC 
                             register clears the event flag. Default value: 0.
                             0: Event flag latch disabled; 
                             1: Event flag latch enabled*/
#define PULSE_CFG_ZDPEFE BIT(5) /* Event flag enable on double pulse event on 
                             Z-axis. Default value: 0.
                             0: Event detection disabled; 
                             1: Event detection enabled*/ 
#define PULSE_CFG_ZSPEFE BIT(4) /* Event flag enable on single pulse event on 
                             Z-axis. Default value: 0.
                             0: Event detection disabled; 
                             1: Event detection enabled*/
#define PULSE_CFG_YDPEFE BIT(3) /* Idedm YDP*/
#define PULSE_CFG_YSPEFE BIT(2) /* Idem YSP*/
#define PULSE_CFG_XDPEFE BIT(1) /* Idem XDP*/
#define PULSE_CFG_XSPEFE BIT(0) /* Idem XSP*/

/******************************************************************************/
/* MMA8452Q Pulse Source Register (PULSE_SRC)                                 */
/******************************************************************************/
/* (Register Address: 0x22. Access:Read Only. Default value: 0x00  
  This register indicates a double or single pulse event has occurred and also 
which direction. The corresponding axis and event must be enabled in Register 
0x21 for the event to be seen in the source register.
When the EA bit gets set while ELE = 1, all status bits (AxZ, AxY, AxZ, DPE, and 
PolX, PolY, PolZ) are frozen. Reading the PULSE_SRC register clears all bits. 
Reading the source register will clear the interrupt.*/

#define PULSE_SRC_EA_BIT   7 
#define PULSE_SRC_AXZ_BIT  6 
#define PULSE_SRC_AXY_BIT  5 
#define PULSE_SRC_AXX_BIT  4
#define PULSE_SRC_DPE_BIT  3
#define PULSE_SRC_POLZ_BIT 2 
#define PULSE_SRC_POLY_BIT 1 
#define PULSE_SRC_POLX_BIT 0 

#define PULSE_SRC_EA   BIT(7) /* Event Active Flag. Default value: 0.
(0: No interrupt has been generated; 1: One or more interrupt events have been generated)*/
#define PULSE_SRC_AXZ  BIT(6) /* Z-axis event. Default value: 0.
                             0: No interrupt; 
                             1: Z-axis event has occurred*/
#define PULSE_SRC_AXY  BIT(5) /* Idem Y*/
#define PULSE_SRC_AXX  BIT(4) /* Idem X*/
#define PULSE_SRC_DPE  BIT(3) /* Double pulse on first event. Default value: 0.
                             0: Single Pulse Event triggered interrupt; 
                             1: Double Pulse event triggered interrupt*/
#define PULSE_SRC_POLZ BIT(2) /* Pulse polarity of Z-axis Event. Default value: 0.
                             0: Pulse Event that triggered interrupt was Positive; 
                             1: Pulse Event that triggered interrupt was negative*/
#define PULSE_SRC_POLY BIT(1) /* Idem Y*/
#define PULSE_SRC_POLX BIT(0) /* Idem X*/

/* --- Control Registers--- */
/* Note: Except for STANDBY mode selection, the device must be in STANDBY mode 
   to change any of the fields within CTRL_REG1 (0X2A).*/                             
/******************************************************************************/
/* MMA8452Q Control Register 1 (CTRL_REG_1)                                   */
/******************************************************************************/
#define CR1_ASLP_RATE1  7
#define CR1_ASLP_RATE0  6 
#define CR1_DR2_BIT     5
#define CR1_DR1_BIT     4
#define CR1_DR0_BIT     3
#define CR1_LNOISE_BIT  2
#define CR1_F_READ_BIT  1
#define CR1_ACTIVE_BIT  0

#define ACTIVE_MASK      (BIT(0))
#define CR1_DR_MASK      (BIT(5)|BIT(4)|BIT(3))
#define CR1_ASLP_DR_MASK (BIT(7)|BIT(6))
/******************************************************************************/
/* MMA8452Q Control Register 2 (CTRL_REG_2)                                   */
/******************************************************************************/
#define CR2_ST_BIT      7
#define CR2_RST_BIT     6
#define CR2_SMODS1_BIT  4
#define CR2_SMODS0_BIT  3
#define CR2_SLPE_BIT    2
#define CR2_MODS1_BIT   1
#define CR2_MODS0_BIT   0

#define RESET_MASK      (BIT(6))
#define CR2_MODS_MASK   (BIT(1)|BIT(0))
#define CR2_SMODS_MASK  (BIT(4)|BIT(3))
/******************************************************************************/
/* MMA8452Q Control Register 3 (CTRL_REG_3)                                   */
/******************************************************************************/
#define CR3_WAKE_TRANS_BIT   6
#define CR3_WAKE_LNDPRT_BIT  5
#define CR3_WAKE_PULSE_BIT   4
#define CR3_WAKE_FF_MT_BIT   3
#define CR3_IPOL_BIT         1
#define CR3_PP_OD_BIT        0

/******************************************************************************/
/* MMA8452Q Control Register 4 (CTRL_REG_4)                                   */
/******************************************************************************/
#define CR4_INT_EN_ASLP_BIT    7
#define CR4_INT_EN_TRANS_BIT   5
#define CR4_INT_EN_LNDPRT_BIT  4
#define CR4_INT_EN_PULSE_BIT   3
#define CR4_INT_EN_FF_MT_BIT   2
#define CR4_INT_EN_DRDY_BIT    0

/******************************************************************************/
/* MMA8452Q Control Register 5 (CTRL_REG_5)                                   */
/******************************************************************************/
#define CR5_INT_CFG_ASLP_BIT    7
#define CR5_INT_CFG_TRANS_BIT   5
#define CR5_INT_CFG_LNDPRT_BIT  4
#define CR5_INT_CFG_PULSE_BIT   3
#define CR5_INT_CFG_FF_MT_BIT   2
#define CR5_INT_CFG_DRDY_BIT    0

                             
/******************************************************************************/
/* Some useful variables                                                      */
/******************************************************************************/
/* non-global variables are all declared with the static keyword. This keeps the 
 * size of the symbol table down.
 */
static PGM_P PGM strDataRates[] = {
  "1.56", "6.25", "12.5", "50", "100", "200", "400", "800","INVALID" 
};

static PGM_P PGM strSysModes[] = {
  "STANDBY", "WAKE", "SLEEP"
};

static PGM_P PGM strOSModes[] = {
  "NORMAL", "LOW NOISE LOW POWER", "HI RESOLUTION", "LOW POWER"
};


/******************************************************************************/
/* I2C Related Functions to access the MMA8452Q device                        */
/******************************************************************************/
static int8u registerRead(int8u rA) {
  return I2CREAD08 (MMA8452Q_I2C_ADDRESS, rA); 
}

static void registersRead(int8u rA, int8u* rVs, int8u count) {
  I2CREADBUFF(MMA8452Q_I2C_ADDRESS, rA, rVs,  count);
}

static void registerWrite(int8u rA, int8u rV) {
  I2CWRITE08(MMA8452Q_I2C_ADDRESS, rA, rV);
}

static void registersWrite(int8u rA, int8u* rVs, int8u count) {
  I2CWRITEBUFF(MMA8452Q_I2C_ADDRESS, rA, rVs, count);
}

static int8u registerGetBit(int8u rA, int8u bitPos) {
  return  READBIT(registerRead(rA), bitPos);
}

static void registerSetBit(int8u rA, int8u bitPos, int8u value) {
  int8u rV; 
  
  // Read current value of Register.
  rV = registerRead(rA);
  
  if (value == TRUE)
    SETBIT(rV, bitPos); 
  else
    CLEARBIT(rV, bitPos); 
  
  // Register write back.
  registerWrite(rA, rV);
}

/******************************************************************************/
/* MMA8452Q Register related functions                                          */
/******************************************************************************/
int8u MMA8452Q_getRegister(int8u rA){
  return registerRead(rA);
}//End of getRegister
/*----------------------------------------------------------------------------*/

void MMA8452Q_setRegister(int8u rA, int8u rV){
   registerWrite(rA, rV);
}//End of getRegister
/*----------------------------------------------------------------------------*/

int8u MMA8452Q_getStatus (void){
  return registerRead(MMA8452Q_RA_STATUS);
}//End of getStatus
/*----------------------------------------------------------------------------*/

int8u MMA8452Q_getSysMod(void) {
  return registerRead(MMA8452Q_RA_SYSMOD);
}
/*----------------------------------------------------------------------------*/
 
int8u MMA8452Q_getIntSrc(void) {
  return registerRead(MMA8452Q_RA_INT_SOURCE);
}// End of getIntSrc 
/*----------------------------------------------------------------------------*/

int8u MMA8452Q_getWhoAmI(void){
  return registerRead(MMA8452Q_RA_WHO_AM_I);
}// End of getWhoIam
/*----------------------------------------------------------------------------*/

boolean MMA8452Q_isPresent(void) {
  
int8u whoami = registerRead(MMA8452Q_RA_WHO_AM_I);

if (whoami != WHO_AM_I_SIGNATURE) {
  return FALSE;
} else
  return TRUE;

}// End of isPresent
/*----------------------------------------------------------------------------*/
 
void MMA8452Q_getControlRegisters(int8u regs[]){
  for (int8u i = MMA8452Q_RA_CTRL_REG1; i <= MMA8452Q_RA_CTRL_REG5; i++)
    regs[i] = registerRead(i);//I2CREAD08(MMA8452Q_I2C_ADDRESS, i);//
}//End of getControlRegisters
/*----------------------------------------------------------------------------*/

boolean MMA8452Q_isActive (void){
  int8u devMode;
  boolean active;
  
  /* Get current device mode*/
  devMode = registerRead(MMA8452Q_RA_SYSMOD);
  
  active = FALSE;
  if (devMode != STANDBY_MODE){
    // Both wake and sleep modes are active modes
    active = TRUE; 
  }
  return active;
}//End of isActive
/*----------------------------------------------------------------------------*/

/* Most of the functions have been customized from examples in the Application Note AN4076*/
/***
 Most, although not quite all changes to the registers must be done while the 
 accelerometer is in Standby Mode. Current consumption in Standby Mode is 
 typically 1 - 2 ΅A. To be in Standby Mode the last bit of CTRL_REG1 must be 
 cleared (Active = 0). When Active = 1 the device is in the active mode.
*/


int32u MMA8452Q_getDataRateInUS (int8u odr){
  int32u timeInUS; // in microseconds
  switch (odr){
  case ACTIVE_DR_800HZ:  //0 0 0 800 Hz 1.25 ms
    timeInUS  = 1250; break;
  case ACTIVE_DR_400HZ:  //0 0 1 400 Hz 2.5 ms
    timeInUS  = 2500; break;
  case ACTIVE_DR_200HZ:  //0 1 0 200 Hz 5 ms
    timeInUS  = 5000; break;
  case ACTIVE_DR_100HZ:  //0 1 1 100 Hz 10 ms
    timeInUS  = 10000; break;
  case ACTIVE_DR_50HZ:   //1 0 0 50 Hz 20 ms
    timeInUS  = 20000; break;
  case ACTIVE_DR_12P5HZ: //1 0 1 12.5 Hz 80 ms
    timeInUS  = 80000; break;
  case ACTIVE_DR_6P25HZ: //1 1 0 6.25 Hz 160 ms
    timeInUS  = 160000; break;
  case ACTIVE_DR_1P56HZ: //1 1 1 1.56 Hz 640 ms
    timeInUS  = 640000;
  }
  return timeInUS;
}//End of getDataRateInUS
/*----------------------------------------------------------------------------*/

int32u MMA8452Q_getTurnOnTimeUS(int8u odr){
  // TurOnTime = 2/ODR + 2ms
  return ( (MMA8452Q_getDataRateInUS(odr) << 1) + 2000L );
}//End of getTurnOnTime
/*----------------------------------------------------------------------------*/

int8u MMA8452Q_XYZDataReady(void){
  
  /* Reading DATA STATUS register*/
  int8u rV = registerRead(MMA8452Q_RA_STATUS);
  
  /* ZYX_DR signals that a new sample for any of the enabled channels is 
   * available. ZYXDR is cleared when the high-bytes of the acceleration data 
   * (OUT_X_MSB, OUT_Y_MSB, OUT_Z_MSB) of all the enabled channels are read.
  */
  return (READBIT(rV, ST_ZYX_DR_BIT) >> ST_ZYX_DR_BIT);
}
/*----------------------------------------------------------------------------*/

void MMA8452Q_setActive(boolean enable){
  // Put sensor into Active/StandBy Mode by setting/clearing the Active bit
  registerSetBit(MMA8452Q_RA_CTRL_REG1, CR1_ACTIVE_BIT, enable);
}// End of Active
/*----------------------------------------------------------------------------*/

void MMA8452Q_activate(void){
  int8u dr; 
  int32u turnOnTimems;
  
  dr = MMA8452Q_getActiveDataRate ();

  /* get wake-on time depending on active data rate */
  turnOnTimems = MMA8452Q_getTurnOnTimeUS(dr) >> 10;
  
  
  /* activate accelerometer */
  MMA8452Q_setActive( TRUE );
  
  /* wait for the accelerometer to wake up */
  waitBusy(turnOnTimems);

}
/*----------------------------------------------------------------------------*/

int8u MMA8452Q_getFSx(void){
  //get Full Scale  from XYZ_DATA_CFG Register
  int8u rV;
  
  // Read the XYZ_DATA_REG
  rV = registerRead(MMA8452Q_RA_XYZ_DATA_CFG);
  
  // Read the Scale bits in XYZ_DATA_CFG register 
  return  (READBITS(rV, XYZ_FSMASK)); 

}//End getFSx
/*----------------------------------------------------------------------------*/

int8u MMA8452Q_getScale(void){
  int8u Scale = MMA8452Q_getFSx() << 2;
  if (Scale == 0) Scale = 2;
  return Scale;
}//End of getScale
/*----------------------------------------------------------------------------*/

void MMA8452Q_setScale(int8u scale) {
 // int8u devModeOnEntry;
  int8u rV;
  
//  // Read the current device operating mode. 
//  devModeOnEntry = registerRead(MMA8452Q_RA_SYSMOD);
//  
//  /* If previously defined the StandBy mode, means that the device is under 
//     configuration and it will be activated outside this function. If the device 
//     is in another mode, then it needs to be deactivated before programming and 
//     activating it afterwards in order to apply the changes. */
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Read the XYZ_DATA_REG
  rV = registerRead(MMA8452Q_RA_XYZ_DATA_CFG);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_XYZ_DATA_CFG);//
  
  // Clear the Scale bits in XYZ_DATA_CFG register (setting ±2g scale by default)
  CLEARBITS(rV, XYZ_FSMASK); // Set ±2g as the default mode
  
  switch (scale) {
  case 4: SETBIT(rV, 0); break; // FSX = 01 ==> ±4g
  case 8: SETBIT(rV, 1); break; // FSX = 10 ==> ±8g
  }
  
  registerWrite(MMA8452Q_RA_XYZ_DATA_CFG, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_XYZ_DATA_CFG, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device back to Active Mode
//    MMA8452Q_setActive(TRUE); 
//  }
}
/*----------------------------------------------------------------------------*/

/** Setting the Oversampling Mode
 * There are four different oversampling modes:
 * There is a normal mode, a low noise + power mode, a high-resolution mode and
 * a low-power mode. 
 * The difference between these are the amount of averaging of the sampled data, 
 * which is done internal to the device. The following chart shows the amount of 
 * averaging at each data rate, which is the OSRatio (oversampling ratio). There
 * is a trade-off between the oversampling and the current consumption at each 
 * ODR value.
 */
   
int8u MMA8452Q_getOSMode (void){
  int8u rV;
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG2 ); 
  return (rV & CR2_MODS_MASK) >> CR2_MODS0_BIT;
}// End of getOSMode
/*----------------------------------------------------------------------------*/

void MMA8452Q_setOSMode (tOSMode osMode){
//  int8u devModeOnEntry;
  int8u rV;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG2 );//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2);// 
  
  // Clear the Mode bits in CTRL_REG2 (Normal Mode)
  CLEARBITS(rV, CR2_MODS_MASK);                 // MODS = 00
  
  // rV |= osMode;
  switch (osMode) {
  case HiRes: 
    SETBIT(rV, CR2_MODS1_BIT); break;           // MODS = 10
  case LoPower: 
    SETBIT(rV, CR2_MODS1_BIT);                  // MODS = 11
  case LoNoiseLoPower: 
    SETBIT(rV, CR2_MODS0_BIT); break;           // MODS = 01
  }

  // Return with previous value of System Control 2 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG2, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}// End of setOSMode
/*----------------------------------------------------------------------------*/

int8u MMA8452Q_getSOSMode (void){
  int8u rV;
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG2 ); 
  return (rV & CR2_SMODS_MASK) >> CR2_SMODS0_BIT;
}// End of getSOSMode
/*----------------------------------------------------------------------------*/

void MMA8452Q_setSOSMode (tOSMode osMode){
  int8u rV;
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG2 ); //I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2);//
  
  // Clear the Mode bits in CTRL_REG2 (Normal Mode)
  CLEARBITS(rV, CR2_SMODS_MASK);                 // SMODS = 00
  
  //rV |= osMode;
  switch (osMode) {
  case HiRes: 
    SETBIT(rV, CR2_SMODS1_BIT); break;           // SMODS = 10
  case LoPower: 
    SETBIT(rV, CR2_SMODS1_BIT);                  // SMODS = 11
  case LoNoiseLoPower: 
    SETBIT(rV, CR2_SMODS0_BIT); break;           // SMODS = 01
  }

  // Return with previous value of System Control 2 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG2, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}// End of setSOSMode
/*----------------------------------------------------------------------------*/

int8u  MMA8452Q_getActiveDataRate (void){
  
  int8u rV = registerRead( MMA8452Q_RA_CTRL_REG1 );
  
  return (rV >> 3) & 7;

}//End of getActiveDataRate
/*----------------------------------------------------------------------------*/

void MMA8452Q_setActiveDataRate (int8u DataRateValue){
  int8u rV;
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG1 );//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1);// 
  
  // Clear the Mode bits in CTRL_REG1 
  CLEARBITS(rV, CR1_DR_MASK);                   

  DataRateValue &= (CR1_DR_MASK >> CR1_DR0_BIT); // Ensure user does not affect other bits in CR1 
  
  rV |= (DataRateValue << CR1_DR0_BIT);

  // Return with previous value of System Control 1 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG1, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1, rV);//

//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}//End of setActiveDataRate
/*----------------------------------------------------------------------------*/

int8u  MMA8452Q_getSleepDataRate (void){
  
  int8u rV = registerRead( MMA8452Q_RA_CTRL_REG1 );//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1 );//
  
  return ( rV & CR1_ASLP_DR_MASK ) >> CR1_ASLP_RATE0;

}//End of getSleepDataRate
/*----------------------------------------------------------------------------*/

void MMA8452Q_setSleepDataRate (int8u DataRateValue){
  int8u rV;
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  rV = registerRead( MMA8452Q_RA_CTRL_REG1 ); //I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1);//
  
  // Clear the Mode bits in CTRL_REG2 (Normal Mode)
  CLEARBITS(rV, CR1_ASLP_DR_MASK);                   

  DataRateValue &= (CR1_ASLP_DR_MASK >> CR1_ASLP_RATE0); // Ensure user does not affect other bits in CR1 
  
  rV |= DataRateValue << CR1_ASLP_RATE0;

  // Return with previous value of System Control 1 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG1, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}//End of setSleepDataRate
/*----------------------------------------------------------------------------*/


/******************************************************************************/

void MMA8452Q_setWakeOn(boolean enable, int8u events) {
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set "wake on" events
  if (events & evFreeFall)
    registerSetBit(MMA8452Q_RA_CTRL_REG3, CR3_WAKE_FF_MT_BIT, enable);
  
  if (events & evPulse)
    registerSetBit(MMA8452Q_RA_CTRL_REG3, CR3_WAKE_PULSE_BIT, enable);
  
  if (events & evOrientation)
    registerSetBit(MMA8452Q_RA_CTRL_REG3, CR3_WAKE_LNDPRT_BIT, enable);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}// End of wakeOn
/*----------------------------------------------------------------------------*/

void MMA8452Q_setOffset(int8s off_x, int8s off_y, int8s off_z) {
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set calibration values
  registerWrite(MMA8452Q_RA_OFF_X, off_x);
  registerWrite(MMA8452Q_RA_OFF_Y, off_y);
  registerWrite(MMA8452Q_RA_OFF_Z, off_z);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}
/*----------------------------------------------------------------------------*/
 
boolean MMA8452Q_isLowNoise( void ){
  int8u cr1;
  boolean tf;
  
  cr1 = registerRead( MMA8452Q_RA_CTRL_REG1 );
  tf = READBIT(cr1, CR1_LNOISE_BIT)?TRUE:FALSE;
  
  return tf;
}// End of isLowNoise
/*----------------------------------------------------------------------------*/

void MMA8452Q_setLowNoise(boolean tf) {
//  int8u devModeOnEntry;
//
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set/Clear Low Noise Mode
  registerSetBit(MMA8452Q_RA_CTRL_REG1, CR1_LNOISE_BIT, tf);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
}//End of setLowNoise
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* Device related functions                                                   */
/******************************************************************************/

void MMA8452Q_reset(void) {
  /* Reset the device
  When the reset bit is enabled, all registers are rest and are loaded with 
  default values. Writing 1 to the RST bit immediately resets the device, no 
  matter whether it is in ACTIVE/WAKE, ACTIVE/SLEEP, or STANDBY mode.*/
  
  registerWrite(MMA8452Q_RA_CTRL_REG2, BIT(CR2_RST_BIT));
  //wait for Turn-On time ~ 2/ODR + 1 ms
  waitBusy((MMA8452Q_getTurnOnTimeUS(0) + 512L) >> 10);
  
//  registerSetBit(MMA8452Q_RA_CTRL_REG2, CR2_RST_BIT, TRUE);
}//End of reset
/*----------------------------------------------------------------------------*/

void MMA8452Q_initDevice(void){ 
  MMA8452Q_reset();
}
/*----------------------------------------------------------------------------*/

void MMA8452Q_selfTest(boolean init) {
//  int8u devModeOnEntry;
//
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Initiate/Finish self test Mode
    registerSetBit(MMA8452Q_RA_CTRL_REG2, CR2_ST_BIT, init);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
}//End of selfTest
/*----------------------------------------------------------------------------*/

 
void MMA8452Q_setAutoSleep(boolean enable) {
//  int8u devModeOnEntry;
//
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set/Clear auto sleep Mode
  registerSetBit(MMA8452Q_RA_CTRL_REG2, CR2_SLPE_BIT, enable);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
}
 
/******************************************************************************/
/* Data reading functions                                                   */
/******************************************************************************/
boolean MMA8452Q_isFastRead( void ){
  int8u cr1;
  boolean tf;
  
  cr1 = registerRead( MMA8452Q_RA_CTRL_REG1 );
  tf = (cr1 & BIT(CR1_F_READ_BIT)) >> CR1_F_READ_BIT;
  
  return tf;
}//End of isFastRead
/*----------------------------------------------------------------------------*/

void MMA8452Q_setFastRead(boolean tf) {
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set/Clear Fast Mode
  registerSetBit(MMA8452Q_RA_CTRL_REG1, CR1_F_READ_BIT, tf);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}//End of setFastRead
/*----------------------------------------------------------------------------*/

void MMA8452Q_getAxes(int16s axes[]) {
  int8u data[6];
  int8u read_count;
  int8u FastReadMode;

  FastReadMode = READBIT(registerRead(MMA8452Q_RA_CTRL_REG1), CR1_F_READ_BIT);
  
  if (FastReadMode == 0)
    read_count = 6;
  else
    read_count = 3;
  
  registersRead(MMA8452Q_RA_OUT_X_MSB, data, read_count);
  
  for (int i = 0; i < 3; i++) {
    axes[i]  = data[i * (read_count / 3)] << 8;
    
    if (FastReadMode == 0)
      axes[i] |= data[(i * 2) + 1];
    
    axes[i] >>= 4;
    
  }
  
}//End of getAxes
/*----------------------------------------------------------------------------*/

void readAccel(int16s* x, int16s* y, int16s* z){ //compatibility with Tristan's drivers
  int16s axes[3];
  MMA8452Q_getAxes(axes);
  *x = axes[0];
  *y = axes[1];
  *z = axes[2];
}
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* Orientation related functions                                              */
/******************************************************************************/
void MMA8452Q_detectOrientation(boolean enable) {
  registerSetBit(MMA8452Q_RA_PL_CFG, PL_CFG_PL_EN_BIT, enable);
}//End of detectOrientation
/*----------------------------------------------------------------------------*/  

boolean MMA8452Q_getOrientation(int8u *oStatus) {
  int8u rV;
  /* this func follows recommendations from freescale application note AN4068 */
  /* read PL_STATUS register.*/
  rV = registerRead(MMA8452Q_RA_PL_STATUS);
  *oStatus = rV & 0x7F;
  
  /* return if a change in orientation was detected */
  return (READBIT(rV, PL_ST_NEWLP_BIT) >> PL_ST_NEWLP_BIT);
  
}//End of getOrientation
/*----------------------------------------------------------------------------*/  
 
int16s MMA8452Q_getPortrait(int8u orient) {
  
  if (READBIT(orient, PL_ST_LAPO1_BIT) == 0)
    return READBITS(orient, PL_ST_LAPO_MASK);
  else return -1;
  
}//End of getPortrait
/*----------------------------------------------------------------------------*/  
 
int16s MMA8452Q_getLandscape(int8u orient) {
  
  if ( READBIT(orient, PL_ST_LAPO1_BIT) )
    return READBITS(orient, PL_ST_LAPO_MASK);
  else return -1;
  
}//End of getLandscape
/*----------------------------------------------------------------------------*/  
 
int16s MMA8452Q_getBackFront(int8u orient) {
  
  return READBIT(orient, PL_ST_BAFRO_BIT);
  
}//End of getBackFront
/*----------------------------------------------------------------------------*/  

/******************************************************************************/
/* Printing/Debbuging information functions                                   */
/******************************************************************************/
void MMA8452Q_showRegister (int8u rV, int8u rA){
  
  logPrintln(  "+------------+----------+-------------+--------------+-------------+-------------+-----+------------+");
  switch (rA){
  case MMA8452Q_RA_CTRL_REG1:
    logPrintln("|  ASLPDR1   |   ASLDR0 |     DR2     |      DR1     |     DR0     |   LNOISE    |F_RD |    ACTV    |");
    break;
  case MMA8452Q_RA_CTRL_REG2:
    logPrintln("|     ST     |    RST   |      0      |    SMODS1    |    SMODS0   |    SLPE     |MODS1|    MODS0   |");
    break;
  case MMA8452Q_RA_CTRL_REG3:
    logPrintln("|     0      |WAKE_TRANS| WAKE_LNDPRT |  WAKE_PULSE  |  WAKE_FF_MT |    IPOL     |  0  |    PP_OD   |");
    break;
  case MMA8452Q_RA_CTRL_REG4:
    logPrintln("|INT_EN_ASLP |     0    |INT_EN_TRANS |INT_EN_LNDPRT |INT_EN_PULSE |INT_EN_FF_MT |  0  |INT_EN_DRDY |");
    break;
  case MMA8452Q_RA_CTRL_REG5:
    logPrintln("|INT_CFG_ASLP|     0    |INT_CFG_TRANS|INT_CFG_LNDPRT|INT_CFG_PULSE|INT_CFG_FF_MT|  0  |INT_CFG_DRDY|");
    break;
  case MMA8452Q_RA_XYZ_DATA_CFG:
    logPrintln("|     0      |     0    |      0      |    HPF_OUT   |      0      |      0      | FS1 |     FS0    |");
    break;
  case MMA8452Q_RA_SYSMOD:
    logPrintln("|     0      |     0    |      0      |       0      |      0      |      0      |SLEEP|     WAKE   |");
    break;
        break;
  default:
    logPrintln("|   ZYX_OW   |    Z_OW  |     Y_OW    |     X_OW     |   ZYX_DR    |    Z_DR     |Y_DR |    X_DR    |");
  }
  
    logPrintln("|     %d      |     %d    |      %d      |       %d      |      %d      |      %d      |  %d  |      %d     |",
                     READBIT(rV,7) > 0, READBIT(rV,6) > 0, READBIT(rV,5) > 0,
                     READBIT(rV,4) > 0, READBIT(rV,3) > 0, READBIT(rV,2) > 0,
                     READBIT(rV,1) > 0, READBIT(rV,0) > 0
                     );
 // emberAfCorePrintln("+-----------------------------------------------------------------------------------------------------------------+");
  
}//End of showRegister

void MMA8452Q_showStatus (int8u st){
  
  MMA8452Q_showRegister(st, MMA8452Q_RA_STATUS);
  
}//End of showStatus


static int8u RegisterMap [0x32];

void printRegisterMap (int ini, int last){
  int8u rV;
   
  logPrintln("+------+------+------+------+");
  logPrintln("|-ADDR-|-CURR-|-PREV-|-DIFF-|");
  logPrintln("+------+------+------+------+");
  
  for(int8u i = ini; i <= last; i++){
    rV = I2CREAD08(MMA8452Q_I2C_ADDRESS, i);//registerRead(i);
    logPrintln("| 0x%x | 0x%x | 0x%x | 0x%x |", i, rV , RegisterMap[i], rV ^ RegisterMap[i]);
    RegisterMap[i] = rV;
  }
}

//void getRegisterMap (int8u map[]){
//  halCommonMemCopy(map, RegisterMap, 50);
//}

void getRegisterMap ( int8u map[] ){
//  registersRead(0, map, 50); 
  for(int8u i = 0; i <= 0x31; i++){
     map[i] = registerRead(i);//I2CREAD08(MMA8452Q_I2C_ADDRESS, i);//
  }
}


void checkDevice(void){
  int8u reg;
  
  do {
    reg = registerRead(MMA8452Q_RA_WHO_AM_I); 
      if (reg == WHO_AM_I_SIGNATURE) {
        logPrintln("MMA8452Q found.  Device Signature: 0x%x", reg);
          break;
      } else {
        logPrintln("Could not connect to MMA8452Q: WHO_AM_I reg == 0x%x ", reg);
        waitSleeping(500);//i2cRetryPeriod);
      }
  } while (TRUE);
}



void printAxes(int16s axes[], int8u Scale){
  int32u fraction;
  int16s val;
  
  int8u detScale = MMA8452Q_getScale();
  
  logPrintln("Scale provided by user: ±%dG", Scale);
  logPrintln("Scale read from device: ±%dG", detScale);
  logPrint("[X, Y, Z]: Dec = [%d, %d, %d]; Hex = [0x%2x, 0x%2x, 0x%2x]; Real = [", 
           axes[0], axes[1], axes[2], axes[0]<<4, axes[1]<<4, axes[2]<<4);
  for (int i = 0; i< 3; i++){
    val = axes[i];
    if (val < 0) {
      val = -val;
      logPrint("-");
    }
    logPrint("%d.", getAxisIntegerPart( val, Scale ));
    fraction = getAxisFractionPart( val, Scale );
//    if (fraction > 0){
      if (fraction < 1000) logPrint("0");
      if (fraction < 100) logPrint("0");
      if (fraction < 10) logPrint("0");
//    }
    logPrint("%d ",fraction);
  }
  logPrintln("]");
}

void printAxisValue (int16s axisVal, int8u Scale){
  int32s val;
  
  val = axisVal;
  if (val < 0) {
    val = -val;
    logPrint("-");
  }
  logPrint("%d.", getAxisIntegerPart( val, Scale ));
  val = getAxisFractionPart( val, Scale );
  if (val < 1000) logPrint("0");
  if (val < 100) logPrint("0");
  if (val < 10) logPrint("0");
  logPrint("%d ",val);
 
}

void printOrientation (int8u plStatus){

  if (plStatus & PL_ST_LOCKOUT_MASK){ // Z-tilt lockout
    logPrint(" Flat.");
    if (plStatus & PL_ST_BAFR_MASK)
      logPrint(" Upside-Down.");
  }
  else
    switch (plStatus & PL_ST_LAPO_MASK) {
    case PL_ST_PO_UP:
      logPrint("Portrait Up.");
      break;
    case PL_ST_PO_DOWN:
      logPrint("Portrait Down.");
      break;
    case PL_ST_LA_RIGHT:
      logPrint("Landscape Right.");
      break;
    case PL_ST_LA_LEFT:
      logPrint("Landscape Left.");
      break;
    }
}//End of printOrientation


int16u getHiPassFilterCutoffmHz(tOSMode osMode, int8u dr, int8u sel){
  int16u HighestCutoffmHz;
  int8u divBits []={0, 0, 1, 2, 3, 3, 3, 3};
  int16u cutoff;
  
  HighestCutoffmHz = HP_FILTER_CUTOFF_DEFAULT * 1000L ;
  
 /* Normal Mode */ 
  cutoff = (HighestCutoffmHz >> sel) >> divBits[dr];
  
  switch (osMode){
  case LoPower:   
    if (dr > ACTIVE_DR_800HZ) cutoff >>= 1;  
  case LoNoiseLoPower: 
    if (dr > ACTIVE_DR_50HZ) cutoff >>= 2; break;
  case HiRes:
    cutoff = HighestCutoffmHz >>=sel; break;
  }
  return cutoff;
}//End of getHiPassFilterCutoffmHz
/*----------------------------------------------------------------------------*/

PGM_P haldrvAcclSensor_getOSstr(int8u os){ // gets the Oversampling Mode string
    
  return strOSModes[os&0x03];
  
}//End of getOSstr
/*----------------------------------------------------------------------------*/

PGM_P haldrvAcclSensor_getOMstr(int8u sel){ // gets Operatin Mode string (WAKE,STANDBY, SLEEP)
    
  if (sel>2) sel = 0;
  return strSysModes[sel];
  
}//End of getOMstr
/*----------------------------------------------------------------------------*/
    
PGM_P haldrvAcclSensor_getDRstr(int8u dr, boolean sleep){ // gets the active or sleep data rate string
  int8u sel;
  
  sel = (dr<8)? (~dr) & (sleep?3:7):8;
  
  return strDataRates[sel];
  
}//End of getDRstr
/*----------------------------------------------------------------------------*/

int32u getCounterRegInUS(int8u count){
  tOSMode osMode;
  int8u dr;
  int32u timeusec;
  
/*  DR    N     LNLP  HR    LP
    800   1250  1250  1250 1250
    400   2500  2500  2500 2500
    200   5000  5000  2500 5000
    100   10000 10000 2500 10000
     50   20000 20000 2500 20000
    12.5  20000 80000 2500 80000
     6.25 20000 80000 2500 160000
     1.56 20000 80000 2500 160000
*/                    
  if (count > 0){
    dr = MMA8452Q_getActiveDataRate();
    
    osMode = MMA8452Q_getOSMode(); 
    switch (osMode){
    case HiRes:
      if (dr != ACTIVE_DR_800HZ) timeusec = 2500L; break;
    case Normal:
      if (dr > ACTIVE_DR_50HZ) timeusec = 20000L; break;
    case LoNoiseLoPower: 
      if (dr > ACTIVE_DR_12P5HZ) timeusec = 80000L; break;
    case LoPower:   
      if (dr == ACTIVE_DR_1P56HZ) timeusec = 160000L; break;  
    default:
      timeusec = MMA8452Q_getDataRateInUS( dr );
    }
  }
  
  timeusec *= count;
  return  timeusec;
}

void MMA8452Q_showConfig(void){
  int8u rV; 
  int8u mode;
  int8u cr2, cr3, cr4, cr5; /* control registers */
  int8u scale;
  int8u adr, sdr;           /* data rates: active/sleep */
  int8u aos, sos;           /* oversampling modes: active/sleep */
  int8u l2indent;           /* level 2 printing indentation */
  int16u cutoff;
  
  l2indent = 2;
  
  /* Read current device configuration as in control registers */
  mode  = registerRead(MMA8452Q_RA_SYSMOD);
  scale = MMA8452Q_getScale();
  cr2 = registerRead(MMA8452Q_RA_CTRL_REG2);
  cr3 = registerRead(MMA8452Q_RA_CTRL_REG3);
  cr4 = registerRead(MMA8452Q_RA_CTRL_REG4);
  cr5 = registerRead(MMA8452Q_RA_CTRL_REG5);
  adr = MMA8452Q_getActiveDataRate();
  sdr = MMA8452Q_getSleepDataRate();
  aos = MMA8452Q_getOSMode();
  sos = MMA8452Q_getSOSMode();
  
  rV = registerRead(MMA8452Q_RA_HP_FILTER_CUTOFF);
  rV = READBITS( rV, HP_FILTER_CUTOFF_SEL_MASK ) >>  HP_FILTER_CUTOFF_SEL0_BIT;
  cutoff = getHiPassFilterCutoffmHz(aos, adr,  rV);

  logPrintln("");
  logPrintln("Status");{ //Show general status information
    incIndent(l2indent);
    
    logPrintln("Mode -> %p",                strSysModes[mode] );
    logPrintln("Resolution ->%d bits",      (MMA8452Q_isFastRead()?8:12) );
    logPrintln("Dynamic Range -> ±%dg",     scale);
    logPrintln("Self-test running -> %p",   getStrYESNO(READBIT(cr2, CR2_ST_BIT)));//(READBIT(cr2, CR2_ST_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("Calibration offsets [X,Y,Z] counts -> [%d,%d,%d]", 
               registerRead(MMA8452Q_RA_OFF_X),
               registerRead(MMA8452Q_RA_OFF_Y),
               registerRead(MMA8452Q_RA_OFF_Z));
    
    decIndent(l2indent);
  }
  
  logPrintln("");
  logPrintln("NonSleep Configuration");{ // Show configuration in wake mode
    incIndent(l2indent);
    
    rV = (~adr) & 7; // data rate in ascending order
    logPrintln("Sample rate -> %p HZ",    strDataRates[rV] );
    
    
    logPrintln("HP Filter, %d.%2X Hz", cutoff/1000, toBCD(cutoff%1000) << 4); 
    logPrintln("Enable Low Noise (Up to 5.5g) -> %p",  getStrYESNO( MMA8452Q_isLowNoise() ));//(MMA8452Q_isLowNoise()?strYesNo[1]:strYesNo[0]));
    
    rV = registerRead(MMA8452Q_RA_XYZ_DATA_CFG);
    logPrintln("HPF Data Out -> %p",  getStrYESNO( READBIT(rV, XYZ_HPF_OUT_BIT) ));//(READBIT(rV, XYZ_HPF_OUT_BIT)?strYesNo[1]:strYesNo[0]));
    logPrintln("Oversampling Mode -> %p",  strOSModes[aos]);
    
    decIndent(l2indent);
  }
  
  logPrintln("");
  logPrintln("Sleep Configuration");{
    incIndent(l2indent);
    
    rV = (~sdr) & 3;
    logPrintln("Enable Auto Sleep - %p", getStrYESNO( READBIT(cr2, CR2_SLPE_BIT) ));
               //(READBIT(cr2, CR2_SLPE_BIT)?strYesNo[1]:strYesNo[0]));
    logPrintln("Sleep Sample rate -> %p HZ",  
               strDataRates[rV]);
    logPrintln("Oversampling Mode - %p",  strOSModes[sos]);
    logPrintln("Sleep timer (ms)"); //FIXME
    logPrintln("Wake from sleep on condition:");{
      incIndent(l2indent);
      logPrintln("TRANSIENT EVENT-> %p", getStrYESNO( READBIT(cr3, CR3_WAKE_TRANS_BIT) ));
                 //(READBIT(cr3, CR3_WAKE_TRANS_BIT)?strYesNo[1]:strYesNo[0]) );
      logPrintln("ORIENTATION EVENT -> %p", getStrYESNO( READBIT(cr3, CR3_WAKE_LNDPRT_BIT) ));
                 //(READBIT(cr3, CR3_WAKE_LNDPRT_BIT)?strYesNo[1]:strYesNo[0]) );
      logPrintln("PULSE EVENT -> %p", getStrYESNO( READBIT(cr3, CR3_WAKE_PULSE_BIT) ));
                 //(READBIT(cr3, CR3_WAKE_PULSE_BIT)?strYesNo[1]:strYesNo[0]) );
      logPrintln("MOTION/FREEFALL EVENT -> %p", getStrYESNO( READBIT(cr3, CR3_WAKE_FF_MT_BIT) ));
                 //(READBIT(cr3, CR3_WAKE_FF_MT_BIT)?strYesNo[1]:strYesNo[0]) );
      decIndent(l2indent);
    }
    
    decIndent(l2indent);
  }
  
  logPrintln("");
  logPrintln("Interrupt configuration");{
    incIndent(l2indent);
    
    logPrintln("INT pin -> INT2 %p",
               (cr5?"WARNING: INT1 CONFIGURED in CR5!":"") );
    logPrintln("INT Polarity Active Low/High -> %p", 
               (READBIT(cr3, CR3_IPOL_BIT)?"HIGH":"LOW"));
    logPrintln("INT Push/Pull or Open Drain -> %p", 
               (READBIT(cr3, CR3_PP_OD_BIT)?"OPEN DRAIN":"PUSH/PULL") );
    logPrintln("INT on DATA READY -> %p", 
               getStrYESNO( READBIT(cr4, CR4_INT_EN_DRDY_BIT) ));
               //(READBIT(cr4, CR4_INT_EN_DRDY_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("INT on TRANSIENT -> %p", 
               getStrYESNO( READBIT(cr4, CR4_INT_EN_TRANS_BIT) ));
               //(READBIT(cr4, CR4_INT_EN_TRANS_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("INT on PULSE -> %p", 
               getStrYESNO( READBIT(cr4, CR4_INT_EN_PULSE_BIT) ));
               //(READBIT(cr4, CR4_INT_EN_PULSE_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("INT on MOTION/FF -> %p", 
               getStrYESNO( READBIT(cr4, CR4_INT_EN_FF_MT_BIT) ));
               //(READBIT(cr4, CR4_INT_EN_FF_MT_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("INT on ORIENTATION -> %p", 
               getStrYESNO( READBIT(cr4, CR4_INT_EN_LNDPRT_BIT) ));
               //(READBIT(cr4, CR4_INT_EN_LNDPRT_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("INT on AUTO SLEEP -> %p", 
               getStrYESNO( READBIT(cr4, CR4_INT_EN_ASLP_BIT) ));
               //(READBIT(cr4, CR4_INT_EN_ASLP_BIT)?strYesNo[1]:strYesNo[0]) );
    
    decIndent(l2indent);
  }
  
  logPrintln("");
  logPrintln("Motion/Freefall (M/FF) detection");{
    incIndent(l2indent);
    
    rV = registerRead(MMA8452Q_RA_FF_MT_CFG); 
    logPrintln("OR/AND -> %p", 
               (READBIT(rV, FF_MT_CFG_OAE_BIT)?"OR":"AND") );
    logPrintln("Enable event detection on [X][Y][Z] axis -> [%p][%p][%p]",
               getStrYESNO( READBIT(rV, FF_MT_CFG_ZEFE_BIT) ),
               getStrYESNO( READBIT(rV, FF_MT_CFG_YEFE_BIT) ),
               getStrYESNO( READBIT(rV, FF_MT_CFG_XEFE_BIT) ) );
               //(READBIT(rV, FF_MT_CFG_ZEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, FF_MT_CFG_YEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, FF_MT_CFG_XEFE_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("Enable Latch - %p", 
               getStrYESNO( READBIT(rV, FF_MT_CFG_ELE_BIT) ) );
               //(READBIT(rV, FF_MT_CFG_ELE_BIT)?strYesNo[1]:strYesNo[0]));
    
    rV = registerRead(MMA8452Q_RA_FF_MT_THS);
    logPrintln("Threshold (mg) -> %d", 
               (int32u)(rV & FF_MT_THS_MASK) * 63L );
    logPrintln("Decrement Debounce option set -> %p", 
               getStrYESNO( READBIT(rV, FF_MT_THS_DBCNTM_BIT) ) );
               //(READBIT(rV, FF_MT_THS_DBCNTM_BIT)?strYesNo[1]:strYesNo[0]) );
    
    rV = registerRead(MMA8452Q_RA_FF_MT_COUNT);
       logPrintln("Debounce (usec) -> %d", 
               getCounterRegInUS(rV) );
       
    decIndent(l2indent);
  }
  
  logPrintln("");
  logPrintln("Orientation detection");{
    incIndent(l2indent);

    rV = registerRead(MMA8452Q_RA_PL_CFG); 
    logPrintln("Enable -> %p", 
               getStrYESNO( READBIT(rV, PL_CFG_PL_EN_BIT) ) );
               //(READBIT(rV, PL_CFG_PL_EN_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("Decrement Debounce option set -> %p", 
               getStrYESNO( READBIT(rV, PL_CFG_DBCNTM_BIT) ) );
               //(READBIT(rV, PL_CFG_DBCNTM_BIT)?strYesNo[1]:strYesNo[0]) );

    rV = registerRead(MMA8452Q_RA_PL_COUNT);
    logPrintln("Debounce (usec) -> %d", 
               getCounterRegInUS(rV) );
    
    rV = registerRead(MMA8452Q_RA_PL_BF_ZCOMP);   
    logPrintln("Z-Lock Angle Threshold -> %d°", ( 14 +(rV & PL_BF_ZCOMP_ZLOCK_MASK) >> PL_BF_ZCOMP_ZLOCK0_BIT) * 4 );
    
    logPrintln("Back/Front Trip Angle Threshold -> ±%d°", (80 - (rV & PL_BF_ZCOMP_BKFR_MASK) >> PL_BF_ZCOMP_BKFR0_BIT) * 5 );
    
    rV = registerRead(MMA8452Q_RA_P_L_THS_REG);
    logPrintln("Portrait/Landscape Trip Angle -> 45°");
    logPrintln("Hysteresis Angle -> ±14°");

    decIndent(l2indent);
  }

  logPrintln("");
  logPrintln("Transient (Shake) detection");{
    incIndent(l2indent);

    rV = registerRead(MMA8452Q_RA_TRANSIENT_CFG); 
    logPrintln("HPF Bypass -> %p", 
               getStrYESNO( READBIT(rV, TRANSIENT_CFG_HPF_BYP_BIT) ) );
               //(READBIT(rV, TRANSIENT_CFG_HPF_BYP_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("Enable event detection on [X][Y][Z] axis -> [%p][%p][%p]",
               getStrYESNO( READBIT(rV, TRANSIENT_CFG_ZTEFE_BIT) ),
               getStrYESNO( READBIT(rV, TRANSIENT_CFG_YTEFE_BIT) ),
               getStrYESNO( READBIT(rV, TRANSIENT_CFG_XTEFE_BIT) ) );
               //(READBIT(rV, TRANSIENT_CFG_ZTEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, TRANSIENT_CFG_YTEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, TRANSIENT_CFG_XTEFE_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("Enable Latch -> %p", 
               getStrYESNO( READBIT(rV, TRANSIENT_CFG_ELE_BIT) ) );
               //(READBIT(rV, TRANSIENT_CFG_ELE_BIT)?strYesNo[1]:strYesNo[0]));
    
    rV = registerRead(MMA8452Q_RA_TRANSIENT_THS);
    logPrintln("Threshold (mg) -> %d", 
               (int32u)(rV & TRANSIENT_THS_MASK) * 63L );
    logPrintln("Decrement Debounce option set -> %p", 
               getStrYESNO( READBIT(rV, TRANSIENT_THS_DBCNTM_BIT) ) );
               //(READBIT(rV, TRANSIENT_THS_DBCNTM_BIT)?strYesNo[1]:strYesNo[0]) );

    rV = registerRead(MMA8452Q_RA_TRANSIENT_COUNT);
    logPrintln("Debounce (usec) -> %d", 
               getCounterRegInUS(rV) );

    decIndent(l2indent);
  }

  logPrintln("");
  logPrintln("Pulse detection");{
    incIndent(l2indent);

    rV = registerRead(MMA8452Q_RA_PULSE_CFG); 
    logPrintln("Enable Single Pulse (Tap) [X][Y][Z] -> [%p][%p][%p]",
               getStrYESNO( READBIT(rV, PULSE_CFG_ZSPEFE_BIT) ),
               getStrYESNO( READBIT(rV, PULSE_CFG_YSPEFE_BIT) ),
               getStrYESNO( READBIT(rV, PULSE_CFG_XSPEFE_BIT) ) );
               //(READBIT(rV, PULSE_CFG_ZSPEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, PULSE_CFG_YSPEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, PULSE_CFG_XSPEFE_BIT)?strYesNo[1]:strYesNo[0]) );
    logPrintln("Enable Latch -> %p", 
               getStrYESNO( READBIT(rV, PULSE_CFG_ELE_BIT) ) );
               //(READBIT(rV, PULSE_CFG_ELE_BIT)?strYesNo[1]:strYesNo[0]));
    
    rV = registerRead(MMA8452Q_RA_PULSE_TMLT);
    logPrintln("Pulse Time Limit (usec) -> %d", 
               getCounterRegInUS(rV) );
    
    rV = registerRead(MMA8452Q_RA_PULSE_THSX);
    logPrintln("X Threshold (mg) -> %d", 
               (int32u)rV  * 63L );

    rV = registerRead(MMA8452Q_RA_PULSE_THSY);
    logPrintln("Y Threshold (mg) -> %d", 
               (int32u)rV  * 63L );

    rV = registerRead(MMA8452Q_RA_PULSE_THSZ);
    logPrintln("Z Threshold (mg) -> %d", 
               (int32u)rV  * 63L );
    
    rV = registerRead(MMA8452Q_RA_HP_FILTER_CUTOFF);
    logPrintln("LPF Enable -> %p", 
               getStrYESNO( READBIT(rV, HP_FILTER_CUTOFF_PULSE_LPF_EN_BIT) ) );
               //(READBIT(rV, HP_FILTER_CUTOFF_PULSE_LPF_EN_BIT)?strYesNo[1]:strYesNo[0]));
    logPrintln("HPF Bypass -> %p", 
               getStrYESNO( READBIT(rV, HP_FILTER_CUTOFF_PULSE_HPF_BYP_BIT) ) );
               //(READBIT(rV, HP_FILTER_CUTOFF_PULSE_HPF_BYP_BIT)?strYesNo[1]:strYesNo[0]));
    
    rV = registerRead(MMA8452Q_RA_PULSE_CFG); 
    logPrintln("Enable Double Pulse (Tap) [X][Y][Z] -> [%p][%p][%p]",
               getStrYESNO( READBIT(rV, PULSE_CFG_ZDPEFE_BIT) ),
               getStrYESNO( READBIT(rV, PULSE_CFG_YDPEFE_BIT) ),
               getStrYESNO( READBIT(rV, PULSE_CFG_XDPEFE_BIT) ) );
               //(READBIT(rV, PULSE_CFG_ZDPEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, PULSE_CFG_YDPEFE_BIT)?strYesNo[1]:strYesNo[0]),
               //(READBIT(rV, PULSE_CFG_XDPEFE_BIT)?strYesNo[1]:strYesNo[0]) );
    
    rV = registerRead(MMA8452Q_RA_PULSE_LTCY);
    logPrintln("Pulse Latency (usec) -> %d", 
               getCounterRegInUS(rV) << 1 );
    
    rV = registerRead(MMA8452Q_RA_PULSE_WIND);
    logPrintln("2nd Pulse Window (usec) -> %d", 
               getCounterRegInUS(rV) << 1 );//FIXME

    decIndent(l2indent);
  }
}

/*  Examples */
int8u exSelfTest(int16s differences []){
  int8u i;
  int8u devModeOnEntry, devScalOnEntry;
  int16s axes[3];
  int32s stAxes[3], mAxes[3];
  int8u status;
  
  /* Step 1. Store device operating mode */
  
  devModeOnEntry = registerRead(MMA8452Q_RA_SYSMOD); // Read the current device operating mode. 
  devScalOnEntry = MMA8452Q_getScale();  // Read the current device scale.
  
  /* Step 2. Put the device into standBy mode and select ±4g scale*/
  
  if (devModeOnEntry != STANDBY_MODE){
    // Put the device into Standby Mode
    MMA8452Q_setActive(FALSE); 
  }
  MMA8452Q_setScale( SCALE_SELFTEST );
  
  /* Step 4. Measure XYZ in Active Mode. Take several (16) values and average */
  
//  MMA8452Q_setActive(TRUE);
  MMA8452Q_activate();
  
  mAxes[0] = mAxes[1] = mAxes[2] = 0;
  for (i=0; i<16; i++){
    while ( !MMA8452Q_XYZDataReady() );
    MMA8452Q_getAxes( axes );
    mAxes[0] += axes[0]; mAxes[1] += axes[1]; mAxes[2] += axes[2];
  } 
  for(i=0; i<3; i++){
    mAxes[i] >>= 4; 
  }
  /* Step 5. Configure device in selftest mode*/
  
  MMA8452Q_setActive(FALSE); 
  MMA8452Q_selfTest(TRUE);
//  MMA8452Q_setActive(TRUE); 
  MMA8452Q_activate();

  /* Step 6. Measure XYZ in selftest Mode. Take several (16) values and average */
  
  stAxes[0] = stAxes[1] = stAxes[2] = 0;
  for (i=0; i<16; i++){
    while ( !MMA8452Q_XYZDataReady() );
    MMA8452Q_getAxes( axes );
    stAxes[0] += axes[0]; stAxes[1] += axes[1]; stAxes[2] += axes[2];
  } 
  for(i=0; i<3; i++){
    stAxes[i] >>= 4; 
  }
  
  /* Step 7. Compute differences XST = |XST_ON - XST_OFF|, YST = |YST_ON - YST_OFF|, ZST = |ZST_ON - ZST_OFF|*/
  
  for(i=0; i<3; i++){
    differences[i] = stAxes[i] - mAxes[i]; 
    stAxes[i] = differences[i];
    if (stAxes[i] < 0) stAxes[i]= -stAxes[i];
  }
  
  /* Step 5. Deactivate selftest */
  
  MMA8452Q_setActive(FALSE); 
  MMA8452Q_selfTest(FALSE);
  
  /* Step 8. Check results. If these self-test changes are higher than 50 counts 
   * at ±4g mode, the test is considered to pass.
   * The datasheet indicates that when self test is activated, the XYZ axes will 
   * be deflected by +44, +61, and +392 LSBs off of the current acceleration 
   * acting on the sensor respectively.
   */
  status =  (differences[0] > (SELFTEST_X_TYP_OUTPUT_CHANGE - 5) );
  status |= (differences[1] > (SELFTEST_Y_TYP_OUTPUT_CHANGE - 10) ) << 1;
  status |= (differences[2] > (SELFTEST_Z_TYP_OUTPUT_CHANGE - 20) ) << 2;
  
  /* Step 9. Back to normal. Put device in the mode it had when entering this function*/
  MMA8452Q_setScale( devScalOnEntry );
  if (devModeOnEntry != STANDBY_MODE){
    //  Put the device back into Active Mode
    MMA8452Q_setActive(TRUE);
  }
  
  return status;
}//End of exSelfTest
/*----------------------------------------------------------------------------*/
