/** @file hal/ece/tempSensor.h
 * @brief Header for TMP100 temperature sensor.
 *
 * See @ref temperature for documentation.
 *
 * <!-- University of New Brunswick. 2014 -->
 */
#ifndef __DRIVER_UTILS_H__
#define __DRIVER_UTILS_H__

/** @brief Some aliases for the i2c functions defines in "i2c.h".
* @param dA: Device Address
* @param rA: register Address  
* @param rV: register Value    
* @see i2c.h
*/
#define I2CREAD08(dA,rA)       halI2CReadint8u ((int8u)dA,(int8u)rA) 
#define I2CWRITE08(dA,rA,rV)   halI2CWriteint8u((int8u)dA,(int8u)rA,(int8u)rV)
#define I2CREAD16(dA,rA)       halI2CReadint16u((int8u)dA,(int8u)rA)
#define I2CWRITE16(dA,rA,rV)   halI2CWriteint16u((int8u)dA,(int8u)rA,(int16u)rV)
#define I2CREADBUFF(dA,rA,buf,buflen)  \
                 halI2CRead((int8u)dA, (int8u)rA, (int8u*)buf, (int8u) buflen)  
#define I2CWRITEBUFF(dA,rA,buf,buflen)  \
                 halI2CWrite((int8u)dA, (int8u)rA, (int8u*)buf, (int8u) buflen)  

//#if defined(EMBER_AF_PRINT_OUTPUT)
//#define logPrintln(...) emberSerialPrintfLine(EMBER_AF_PRINT_OUTPUT,  __VA_ARGS__)
//#define logPrint(...)   emberSerialPrintf(EMBER_AF_PRINT_OUTPUT,  __VA_ARGS__)
//#else
//#define logPrintln(...) 
//#define logPrint(...)   
//#endif

EmberStatus logPrint(PGM_P formatString, ...);
EmberStatus logPrintln(PGM_P formatString, ...);

void resetIndent(void);
void setIndent(int8s newIndent);
void incIndent(int8u by);
void decIndent(int8u by);
int8u getIndent(void);

/** @brief returns a "YES" or "NO" string.
 * 
 * @param yes  1 retrives "YES"; 0 retrives "NO".
 * 
 * @return selected string. 
 */
PGM_P getStrYESNO(int8u yes);

void waitBusy(int32u ms);

void waitSleeping(int32u ms);


/******************************************************************************/
/*   Utility Functions for converti Temperature values from raw measuerements */
/******************************************************************************/
/** @brief Converts from TMP100 temperature format to a floating point number.
 * 
 * @param rawT  Temperature as read from the TMP100 data register.
 * 
 * @return The temperature as a floating point number. 
 */
float rawT2float ( int16s rawT );

/** @brief Converts from TMP100 temperature format in a number in units of 1/100 oC. 
 *  
 * @param rawT  Temperature as read from the TMP100 data register.
 * 
 * @return The temperature in units of 1/100 oC . 
 */
int16s rawT2100sC(int16s rawT);

/** @brief Converts from TMP100 temperature format to an easy to print fixed point number.
 * 
 * @param rawT  Temperature as read from the TMP100 data register.
 * 
 * @return The temperature as a fixed point number 
 *  I.F, Where I is the Integral part of the number and F is its fractional part. 
 *  The High byte (MSB) holds the integral part of the number. 
 *  The low Byte (LSB) holds the fractional part of the number. 
 *  The fractional part is presented as 1/100 oC while the integral part is 
 *  presented as the temperature in oC. Tis arrangement avoids using the divide 
 *  and modulus (%) functions while presenting the results.
 *
 *  The fractional part is rounded 0ne of the 15 possible TMP100 values:
 * 
 *  TMP100 low byte value 
 * 0   1   2    3    4    5    6    7    8    9    A    B    C    D    E    F
 *  Function returns      
 * 0   6   13   19   25   31   38   44   50   56   63   69   75   81   88   94
 * For example, the value 6640 (0x19f0) read from the TMP100 data register 
 * returns the value 6494 (0x195E) which is interpreted as 0x19.0x5E = 25.94 oC 
 */
int16s rawT2fpoint ( int16s rawT );

/** @brief Converts from TMP100 temperature format to an 8-bit integer. 
 *  
 * This function rounds the measured value to the nearest integer.
 * 
 * @param rawT  Temperature as read from the TMP100 data register.
 * 
 * @return The temperature rounded to the nearest integer. 
 */
int8s rawT2int8s( int16s rawT );

/** @brief Extracts the fractional part from TMP100 temperature format. 
 *  
 * @param rawT  Temperature as read from the TMP100 data register.
 *
 * @param Resolution  TMP100 Resolution in bits (9, 10, 11 or 12 bits).
 * 
 * @return The fractional part depending on device's resolution. 
 */
int16u rawTFrac( int16s rawT, int8s Resolution );

/** @brief Extracts the integral part from TMP100 temperature format. 
 *  
 * @param rawT  Temperature as read from the TMP100 data register.
 * 
 * @return The integral part of the temperature value extracted from a TMP100 device. 
 */
int8s rawTInt ( int16s rawT );

/** @brief Converts a 16-bit unsigned binary number to a BCD number format. 
 *  
 * @param number  number to be converted.
 * 
 * @return number in BCD format. 
 */
int16u toBCD(int16u number);
/******************************************************************************/
/*   Utility Functions for converting Accelerometer values from raw measuerements */
/******************************************************************************/
float rawAxis2float(int16s raw, int8u Scale);

void rawAxes2float(const int16s rawAxes[], int8u Scale, float Axes[]);

int16s getAxisIntegerPart (int16s raw, int8u Scale);

int16u getAxisFractionPart(int16s raw, int8u Scale);

/**/
/** @brief prints a value as a fractional number
 *
 * @param value Value to print
 * 
 * @param partsOf indicate the multiplier usedin the original representation
 *
 *  Example 
 *   int32s value_in_mV = 10;
 *   int32u parts_of_Volt = 1000;
 *   print_uXtoX (value_in_mV, parts_of_Volt);
 *   prints 0.01
 */
void print_uXtoX (int32s value, int32s partsOf);
#endif
