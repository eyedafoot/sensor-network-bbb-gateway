/** @file hal/ece/haldrvRelayI2C.h
 * @brief Header for BV4627-X relay I2C driver
 *
 * Author: Erik Hatfield
 *
 * <!-- University of New Brunswick. 2015 -->
 */
#ifndef __ELECTRIC_SENSOR_H__
#define __ELECTRIC_SENSOR_H__

/*

Note 1 - The BV4627-X can use only one of the following at a time:

  1. Binary interface
  2. I2C interface
  3. USB Interface
  4. IR Interface
  
  In order to chose I2C, remove the connecting cable and connect
  ground to ground, SDA to V+, then V+ to V+ manually for a couple seconds. 
  This is done as SDA must be set high during a reset (power on) to select I2C. 
  If this is not done, then the interface will remain disabled. 
  Once I2C is selected, it will stay selected until other interface
  is selected. Thus the device can be power off or on and reset without
  need to perform the interface selection procedure again.


Note 2 - The BV4627-X has the following relay layout:

  ______________________________________________
  | ByVac                                       |
  |      ___       ___       ___       ___      |
  |     | A |     | B |     | C |     | D |     |
  |     |___|     |___|     |___|     |___|     |
  |      ___       ___       ___       ___      |
  |     | E |     | F |     | G |     | H |     |
  |     |___|     |___|     |___|     |___|     |
  |_____________________________________________|


Note 3 - The pin layout for relay connection is different for the BV4627-f.
  This is the version which was bought in summer 2015 for the UNB nodes. 
  Refer to the documentation before connecting.

*/


typedef enum {
  relayA=1, relayB=2, relayC=3, relayD=4,
  relayE=5, relayF=6, relayG=7, relayH=8
} multRelay;


// @brief: Change the specified relay to the given state after a delay value
void setRelay(multRelay relay, boolean turnOn, int16u delayValue);


// @brief: Disables all the relays on the BV4627-X
void disableAllRelays(void);


// @brief: Enables all the relays on the BV4627-X
void enableAllRelays(void);


// @brief: Reset to default state. If relay is non-responsive, see note in the header file 
void resetRelay(void);


// @brief: Turns on the given set of relays
void turnOnRelays(multRelay *relays, int8u numRelays);


#endif // __ELECTRIC_SENSOR_H__