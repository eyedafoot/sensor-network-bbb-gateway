/** @file hal/ece/halDrvBattGauge.h
 * @brief Header for LC709203F Battery gauge.
 *
 * See @ref Battery Gauge for documentation.
 *
 * <!-- University of New Brunswick. 2014 -->
 */

#ifndef __BATTGAUGE_SENSOR_H__
#define __BATTGAUGE_SENSOR_H__
   
#ifndef LC709203F_I2C_ADDRESS
#define LC709203F_I2C_ADDRESS 0x16 // This is the address of the device
#endif


/******************************************************************************/
/* LC709203F Register Address (RA) Map                                        */
/******************************************************************************/
#define LC709203F_MAX_REGS		        0x1A  // Maximum register Address
#define LC709203F_DELAY			       (30*HZ)// Maximum delay accessig registers

#define LC709203F_RA_BEFORE_RSOC		0x04  // Executes RSOC initialization with sampled maximum voltage when 0xAA55 is set
#define LC709203F_RA_THERMISTOR_B		0x06  // Assigns thermistor constant value �
#define LC709203F_RA_INITIAL_RSOC		0x07  // Sets Relatve State of Charge (RSOC) to its initial value
#define LC709203F_RA_TEMPERATURE		0x08  // Displays the value of the Cell Temperature
#define LC709203F_RA_VOLTAGE		        0x09  // Displays the value of the Cell Voltage

#define LC709203F_RA_CURRENT_DIRECTION  	0x0A  // Function that allows Fuel Gauge to report RSOC in various conditions
#define LC709203F_RA_ADJUSTMENT_PACK_APPLI	0x0B  // Compensate for total impedance between the Battery and Fuel Gauge
#define LC709203F_RA_ADJUSTMENT_PACK_THERM	0x0C  // Delay time to compensate thermistor value due to parallel capacitor & Pull-Up Resistor
#define LC709203F_RA_RSOC			0x0D  // Displays RSOC value based on a 0-100 scale

#define LC709203F_RA_INDICATOR_TO_EMPTY	        0x0F  // Displays remaining capacity of battery based on a 0-1000 scale

#define LC709203F_RA_IC_VERSION		        0x11  // Displays the IC version
#define LC709203F_RA_CHANGE_OF_THE_PARAM	0x12  // Allows to choose between stored battery profiles
#define LC709203F_RA_ALARM_LOW_CELL_RSOC	0x13  // Allows the assignment of an alarm with a given RSOC value
#define LC709203F_RA_ALARM_LOW_CELL_VOLT	0x14  // Allows the assignment of an alarm with a given voltage value (mV)
#define LC709203F_RA_IC_POWER_MODE		0x15  // Allows the assignment of IC Power Mode
#define LC709203F_RA_THERMISTOR_STATUS		0x16  // Displays whether thermistor is enabled or disabled

#define LC709203F_RA_NUM_OF_THE_PARAM	        0x1A  // Displays battery profiles uploaded 301 or 504

#define LC709203F_BATTERY_FULL		        100   // 100% of RSOC
#define LC709203F_BATTERY_LOW		        15    // 15% of relative state of Charge
#define LC709203F_BATTERY_LOW_ALARM		8     // 8% RSOC generates an Alarm (if Alarm pin connected to IRQ)
#define LC709203F_BATTERY_LOW_VOLTAGE_ALARM	0     // 0 mV generates an alarm (if Alarm pin connected to IRQ)

/******************************************************************************/
/* LC709203F Initialization routines                                          */
/******************************************************************************/

/* It is crucial to flow chart in Figure 4 to insure LC709203F has been properly 
 * initialized to ensure accurate measurements. Upon powering on LC709203F, IC 
 * enters �Sleep Mode� and initializing is required to return LC709203F back in 
 * to �Operational Mode�.
 */
void LC709203F_initBasic(int16u battImpedance, int16u profile, boolean noLoadnoCharge, boolean useThermistor, int16u thermistorB);

/**
  * The LC709203F can be Re-Scaled to alter either a different high end voltage 
  * or low end voltage. In most cases Re-Scaling is not required due to 
  * LC709203F�s four commonly used lithium-ion battery types. Additional 
  * information on Re-Scaling can be made available by contacting ON Semiconductor. 
  */
void LC709203F_initWithRescaling(int16u battImpedance, int16u profile, boolean noLoadnoCharge, boolean useThermistor, int16u thermistorB, int16u ITELow, int16u ITEHigh);


/******************************************************************************/
/* LC709203F Before RSOC (Relative State of Charge)Register                  */
/******************************************************************************/
/* (Register Address: 0x04. Access:Write Only. Default value: -)

  Executes RSOCinitialization with sampled maximum voltage when 0xAA55 is set
Possible values: 0xAA55

Related functions:
void LC709203F_beforeRSOC(void);

*/

void LC709203F_beforeRSOC(void);
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* LC709203F Thermistor � Register                                            */
/******************************************************************************/
/* (Register Address: 0x06. Access:Read/Write. Default value: 0x0D34)

 Sets selected thermistor constant value �.
Possible values: 0x0000 to 0xFFFF

Related functions:
int16u LC709203F_getThermistorB(void);
void LC709203F_setThermistorB(int16u B);

*/
#define LC709203F_THERMISTOR_BETA_DEFAULT 0x0D34

int16u LC709203F_getThermistorB(void);
void   LC709203F_setThermistorB(int16u B);

/******************************************************************************/
/* LC709203F Initial RSOC (Relative State of Charge)Register                  */
/******************************************************************************/
/* (Register Address: 0x07. Access:Write Only. Default value: -)

Sets RSOC (Relative State of Charge) to its initial value 0xAA55.
Possible values: 0xAA55

Related functions:
void LC709203F_initRSOC(void);

*/
#define LC709203F_INIT_RSOC 0xAA55

void LC709203F_initRSOC(void);

/******************************************************************************/
/* LC709203F Cell temperature Register                                        */
/******************************************************************************/
/* (Register Address: 0x08. Access:Read/Write. Default value: 0x0BA6 = 25�C)

Displays the value of the Cell Temperature in 0.1�K steps 
(0.0�C = 0x0AAC = 273.2 �K) 

Possible values: 0x09E4 to 0x0D04 = -20�C to 60�C

In thermistor Mode (reading from the device), values can range from 0x0000 to 0xFFFF

Convertion  Kelvin <-> Celsius
Tc = Tk - 273.2  // Convert Kelvin to celsius
Tk = Tc + 273.2  // Convert Celsius to Kevin

Related functions:
int16u LC709203F_getBattTemperature(void);
void   LC709203F_setBattTemperature(int16u value);

*/

#define CELL_TEMPERATURE_0C  0x0AAC
#define CELL_TEMPERATURE_25C 0x0BA6

#define CELSIUS_TO_KELVIN(Tc) (Tc + CELL_TEMPERATURE_0C)
#define KELVIN_TO_CELSIUS(Tk) (Tk - CELL_TEMPERATURE_0C)

#define CELL_TEMPERATURE_DEFAULT CELL_TEMPERATURE_25C

int16u LC709203F_getBattTemperature(void);
void   LC709203F_setBattTemperature(int16u celsius);

/******************************************************************************/
/* LC709203F Cell Voltage Register                                            */
/******************************************************************************/
/* (Register Address: 0x09. Access:Read Only. Default value: -)

Displays the value of the Cell Voltage in mVolts 

Possible values: 0x0000 to 0xFFFF 

Related functions:
int16u LC709203F_getBattVoltage(void);
*/

int16u LC709203F_getBattVoltage(void);

/******************************************************************************/
/* LC709203F Current Direction Register                                       */
/******************************************************************************/
/* (Register Address: 0x0A. Access:Read/Write. Default value: 0x0000 = Auto)

Register that allows Fuel Gauge to report RSOC in various conditions 
0x0000 ( 0): Auto
0x0001 (+1): Charge
0xFFFF (-1): Discharge

Possible values: 0x0000, 0x0001, 0xFFFF

Related functions:
int16s LC709203F_getCurrentDirection(void);
void LC709203F_setCurrentDirection(int16s value);
*/

int16s LC709203F_getCurrentDirection(void);
void   LC709203F_setCurrentDirection(int16s value);

/******************************************************************************/
/* LC709203F APA (Adjustment Pack Application) Register                       */
/******************************************************************************/
/* (Register Address: 0x0B. Access:Read/Write. Default value: -)

Register that allows to compensate for total impedance between the Battery and
Fuel Gauge  

Possible values: 0x0000 to 0xFFFF

Related functions:
int16u LC709203F_getAPA(void);
void  LC709203F_setAPA(int16u value);
*/

int16u LC709203F_getAPA(void);
void   LC709203F_setAPA(int16u value);

/******************************************************************************/
/* LC709203F APT (Adjustment Pack Thermistor) Register                        */
/******************************************************************************/
/* (Register Address: 0x0C. Access:Read/Write. Default value: 0x001E)

Delay time to compensate thermistor value due to parallel capacitor & Pull-Up Resistor 

Possible values: 0x0000 to 0xFFFF

Related functions:
int16u LC709203F_getAPT(void);
void  LC709203F_setAPT(int16u value);
*/

int16u LC709203F_getAPT(void);
void   LC709203F_setAPT(int16u value);

/******************************************************************************/
/* LC709203F RSOC (Relative State of Charge) Register                         */
/******************************************************************************/
/* (Register Address: 0x0D. Access:Read Only. Default value: -)

Displays RSOC value based on a 0-100 scale 

Possible values: 0x0000 to 0x0064 (0%-100%) 

Related functions:
int16u LC709203F_getRSOC(void);
*/

int16u LC709203F_getRSOC(void);

/******************************************************************************/
/* LC709203F Indicator to Empty (FG unit) Register                            */
/******************************************************************************/
/* (Register Address: 0x0F. Access:Read Only. Default value: -)

Displays remaining capacity of battery based on a 0-1000 scale 

Possible values: 0x0000 to 0x03E8 (0-1000) 

Related functions:
int16u LC709203F_getIndicatorEmpty(void);
*/

int16u LC709203F_getIndicatorEmpty(void);

/******************************************************************************/
/* LC709203F IC Version Register                                              */
/******************************************************************************/
/* (Register Address: 0x11. Access:Read Only. Default value: from 0x2717)

Displays the IC version 

Possible values: 0x2717 to 0xFFFF  

Related functions:
int16u LC709203F_getICVersion(void);
*/

int16u LC709203F_getICVersion(void);

/******************************************************************************/
/* LC709203F Default or Alternative Profile Select Register                   */
/******************************************************************************/
/* (Register Address: 0x12. Access:Read/Write. Default value: 0x0001)

Possible values: 0x0000 or 0x0001
Allows to choose between stored battery profiles  

 The LC709203F currently has the options of four battery chemistries. 
 Batteries come in several variations with different characteristics. It is 
 critical to assign the correct Battery Type in order to observe accurate results. 

 The LC709203F has the option of having either battery profile 301 or 504 
 pre-loaded (see register 0x1A -  Profile Number -). Each battery profile has
 two optional battery types to select from. 

 The following table contains the four possible battery profiles.Each profile is 
 unique and correlates to a specific battery chemistry

*-----------+--------------+-----------+-----------+---------+---------------+
|IC-Type     | Nominal/    | Charging  | Battery   | Battery | Change of the |
|            |Rated Voltage| Voltage   | Type      | Profile |  parameter    |
+------------+-------------+-----------+-----------+---------+---------------+
|            | 3.7 V       |   4.2V    | Type 01   |         |     0x0000    |
|            | 3.8 V       |   4.35V   | Type 03   |         |     0x0001    |  
|LC709203F-01|-------------+-----------+-----------|   301   +---------------+
|            |          Any Type       | Re-Scaling|         |               |
|            |                         | Required  |         |               |
+------------+-------------------------+-----------+---------+---------------+
|            | Use only for UR-18560ZY | Type 04   |         |     0x0000    | 
|            |        (Panasonic)      |           |         |               |
|            |Use only for ICR18650-26H| Type 05   |         |     0x0001    |
|            |         (Samsung)       |           |   504   |               |
|LC709203F-04|-------------+-----------+-----------|         +---------------+
|            |          Any Type       | Re-Scaling|         |               |
|            |                         | Required  |         |               |
+------------+-------------------------+-----------+---------+---------------+

 The LC709203F can be Re-Scaled to alter either a different high end voltage or 
 low end voltage. In most cases Re-Scaling is not required due to LC709203F�s 
 four commonly used lithium-ion battery types. Additional information on 
 Re-Scaling can be made available by contacting ON Semiconductor. 

Related functions:
int16u LC709203F_getProfileSelect(void);
void  LC709203F_setProfileSelect(int16u value);
*/

int16u LC709203F_getProfileSelect(void);
void  LC709203F_setProfileSelect(int16u value);

/******************************************************************************/
/* LC709203F Alarm Low RSOC Register                                          */
/******************************************************************************/
/* (Register Address: 0x13. Access:Read/Write. Default value: 0x0008)

Allows the assignment of an alarm with a given RSOC value  

Possible values: 0x0000 to 0x0064

The LC709203F has two possible alarm points that can be set by user.
1) Low RSOC value
2) Low Voltage value
 Alarm on Low RSOC value allows you to select a given RSOC value as an alarm 
 point. RSOC is based on a 0-100 scale which can also be expressed as percentage 
(0%-100%). Default alarm setting is 8%. 

 If the Battery�s remaining charge drops lower than the set �Low RSOC Value�, 
 the output at AlarmB will be pulled low by the open drain internal FET. 

Related functions:
int16u LC709203F_getAlarmLowRSOC(void);
void   LC709203F_setAlarmLowRSOC(int16u value);
*/


int16u LC709203F_getAlarmLowRSOC(void);
void   LC709203F_setAlarmLowRSOC(int16u value);

/******************************************************************************/
/* LC709203F Alarm Low Voltage Register                                       */
/******************************************************************************/
/* (Register Address: 0x14. Access:Read/Write. Default value: 0x0000)

Allows the assignment of an alarm with a given voltage value

Possible values: 0x0000 to 0xFFFF  (mVolts)

 The LC709203F has two possible alarm points that can be set by user.
 1) Low RSOC value
 2) Low Voltage value
 Alarm onLow Voltage value allows you to select a low voltage level as an alarm 
 point. Alarm point units are mV. Default alarm setting is 0mV.

 If the Battery�s remaining charge drops lower than the set �Low Voltage Value�, 
 the output at AlarmB will be pulled low by the open drain internal FET. 

Related functions:
int16u LC709203F_getAlarmLowVoltage(void);
void   LC709203F_setAlarmLowVoltage(int16u value);
*/

int16u LC709203F_getAlarmLowVoltage(void);
void   LC709203F_setAlarmLowVoltage(int16u value);

/******************************************************************************/
/* LC709203F IC Power Mode Register                                           */
/******************************************************************************/
/* (Register Address: 0x15. Access:Read/Write. Default value: 0x0001)

Allows the assignment of an alarm with a given voltage value

Possible values: 0x0000 to 0x0002

The LC709203F has three possible power modes in which it can operate in.
0x0000 : Testing Mode
0x0001 : Operational Mode
0x0002 : Sleep Mode
*Test Mode and Sleep Mode is used for testing and should not be used for 
 ordinary operation. Operational Mode is recommended. 

 Upon powering on LC709203F, IC enters �Sleep Mode� and initializing is required 
 to return LC709203F back in to �Operational Mode�. 

 Power consumption is different depending on which mode is selected. Fuel Gauge 
 is recommended to be assigned to Operational Mode. In this mode LC709203F goes 
 through a series of decisions to optimize power consumption. 
 

Related functions:
int16u LC709203F_getPowerMode(void);
void   LC709203F_setPowerMode(int16u mode);
*/
#define LC709203F_TEST_MODE 		0x0000
#define LC709203F_OPERATIONAL_MODE	0x0001
#define LC709203F_SLEEP_MODE 		0x0002
#define LC709203F_POWER_MODE_DEFAULT	LC709203F_OPERATIONAL_MODE
#define LC709203F_POWER_MODE_ON_POWERUP	LC709203F_OPERATIONAL_MODE

typedef enum t_ICGaugeMode{
  eTest,
  eOperational,
  eSleep
} tICGaugeMode;

tICGaugeMode LC709203F_getPowerMode(void);
void         LC709203F_setPowerMode(tICGaugeMode mode);

/******************************************************************************/
/* LC709203F Thermistor Status Bit Register                                   */
/******************************************************************************/
/* (Register Address: 0x16. Access:Read/Write. Default value: 0x0000)

 Displays whether thermistor is enabled or disabled  
 Selects temperature obtaining method
   
Possible values: 0x0000 or 0x0001
bit 0 : Thermistor Mode
bit 1 to 15 : Reserved (fix 0)

Thermistor Mode
0 : disable (I2C Mode)          Temperature provided by the host procesor
1 : enable  (Thermistor Mode)   Temperature provided by the battery


Related functions:
boolean LC709203F_getThermistorMode(void);
void   LC709203F_setThermistorMode(boolean enable);
*/
#define LC709203F_THERMISTOR_STATUS_BIT  0


boolean LC709203F_getThermistorMode(void);
void    LC709203F_setThermistorMode(boolean enable);

/******************************************************************************/
/* LC709203F Profile Number Register                                          */
/******************************************************************************/
/* (Register Address: 0x1A. Access:Read Only. Default value: -)

Displays battery profiles uploaded 301 or 504 (Please refer to list on register 
0x12 description) 

Possible values: 0x0301 or 0x0504  

Related functions:
int16u LC709203F_getProfileNumber(void);
*/

int16u LC709203F_getProfileNumber(void);

#endif