#include "app/framework/include/af.h"
#include <haldrvI2C.h>

#define WAIT_FIN_SC1(FLAG)  while( !(SC1_TWISTAT & FLAG) ) {}
#define WAIT_CMD_FIN()      WAIT_FIN_SC1(SC_TWICMDFIN)
#define WAIT_SEND_FIN()     WAIT_FIN_SC1(SC_TWITXFIN)
#define WAIT_RECV_FIN()     WAIT_FIN_SC1(SC_TWIRXFIN)

#define ACK 1
#define NAK 0



void i2c_disable (void){
  SC1_MODE = SC1_MODE_DISABLED; // Configure serial controller in disabled mode
}//End of i2c_disable
/*----------------------------------------------------------------------------*/

void i2c_ack_enable(boolean on){
  if ( on )
    SC1_TWICTRL2 |= SC_TWIACK;  // Enable ACK generation after current received byte 
  else
    SC1_TWICTRL2 &= ~SC_TWIACK; // Disable ACK generation after current received byte 
}//End of i2c_ack_enable
/*----------------------------------------------------------------------------*/

void i2c_start (void){
   SC1_TWICTRL1 |= SC_TWISTART; // Generate I2C START condition
   WAIT_CMD_FIN();              // Wait for the command to complete 
}//Endo of i2c_start

void i2c_stop (void){
  SC1_TWICTRL1 |= SC_TWISTOP;   // Generate I2C STOP condition
  WAIT_CMD_FIN();               // Wait for the command to complete 
}//End of i2c_stop
/*----------------------------------------------------------------------------*/

/**
 * Send a byte to I2C bus
 * @param data the byte to be transmitted
 */
void i2c_send(int8u data){
  SC1_DATA = data;              // Put data into buffer
  SC1_TWICTRL1 |= SC_TWISEND;   // Generate SEND command
  WAIT_SEND_FIN();              // Wait for the command to complete 
}//End of i2c_tx
/*----------------------------------------------------------------------------*/

/**
 * Receive a byte from I2C bus
 * @param ack_en If true enable ACK generation after byte reception
 * @return The received byte
 */
int8u i2c_recv(boolean ack_en){
  i2c_ack_enable(ack_en);       // Enable/Disable ACK generation after current received byte
  SC1_TWICTRL1 |= SC_TWIRECV;   // Configure control register 1 for byte reception 
  WAIT_RECV_FIN();              // Wait for the command to complete 
  return SC1_DATA;              // Read data from buffer
}//End of i2c_rx
/*----------------------------------------------------------------------------*/

void halI2CInit(void) {
  /* Configure serial controller to I2C mode */
  SC1_MODE = SC1_MODE_I2C;

  /*
   * The SCL is produced by dividing down 12MHz according to
   * this equation:
   *    Rate = 12 MHz / ( (LIN + 1) * (2^EXP) )
   *
   * Configure rate registers for Fast Mode operation (400 kbps)
   */
  SC1_RATELIN = 14;
  SC1_RATEEXP = 1;

  /* Reset control registers */
  SC1_TWICTRL1 = SC1_TWICTRL1_RESET;
  SC1_TWICTRL2 = SC1_TWICTRL2_RESET;
  
}//End of halI2CInit
/*----------------------------------------------------------------------------*/

void halI2CWriteint8u(int8u address, int8u regester, int8u data){
  i2c_start();
  i2c_send(address);
  i2c_send(regester);
  i2c_send(data);
  i2c_stop();
}//End of halI2CWriteint8u
/*----------------------------------------------------------------------------*/

void halI2CWriteint16u(int8u address, int8u regester, int16u data){
  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  i2c_send( HIGH_BYTE(data) );
  i2c_send( LOW_BYTE (data) );
  i2c_stop();
}//End of halI2CWriteint16u
/*----------------------------------------------------------------------------*/

int8u halI2CReadint8u(int8u address, int8u regester){
  int8u data;

  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  i2c_start();
  i2c_send( address | 1 );
  data = i2c_recv( NAK );
  i2c_stop();

  return data;
}//End of halI2CReadint8u
/*----------------------------------------------------------------------------*/

int16u halI2CReadint16u(int8u address, int8u regester){
  int16u data;

  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  i2c_start();
  i2c_send( address | 1 );
  data = i2c_recv( ACK ) << 8;
  data |= i2c_recv( NAK );
  i2c_stop();

  return data;
}//End of halI2CReadint16u
/*----------------------------------------------------------------------------*/

void halI2CRead(int8u address, int8u regester, int8u * buffer, int8u count){
  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  i2c_start();
  i2c_send( address | 1 );
 
  for (int8u i = 0; i < count-1; i++) 
    buffer[i] = i2c_recv( ACK );
  buffer[count-1] = i2c_recv( NAK );
  
  i2c_stop();
 
}//End of halI2CRead
/*----------------------------------------------------------------------------*/

void halI2CWrite(int8u address, int8u regester, const int8u * buffer, int8u count){
  
  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  for (int8u i = 0; i< count; i++) 
    i2c_send( buffer[i] );
  
  i2c_stop();
  
 }//End of halI2CWrite
/*----------------------------------------------------------------------------*/

/*
 * x^8 + x^2 + x + 1 - used in ATM
 */
#define POLY_CONST      0x07    /* x^8 + x^2 + x + 1 */

static const unsigned char poly_crc8 [256] = {
          0,  7, 14,  9, 28, 27, 18, 21, 56, 63, 54, 49, 36, 35, 42, 45,
        112,119,126,121,108,107, 98,101, 72, 79, 70, 65, 84, 83, 90, 93,
        224,231,238,233,252,251,242,245,216,223,214,209,196,195,202,205,
        144,151,158,153,140,139,130,133,168,175,166,161,180,179,186,189,
        199,192,201,206,219,220,213,210,255,248,241,246,227,228,237,234,
        183,176,185,190,171,172,165,162,143,136,129,134,147,148,157,154,
         39, 32, 41, 46, 59, 60, 53, 50, 31, 24, 17, 22,  3,  4, 13, 10,
         87, 80, 89, 94, 75, 76, 69, 66,111,104, 97,102,115,116,125,122,
        137,142,135,128,149,146,155,156,177,182,191,184,173,170,163,164,
        249,254,247,240,229,226,235,236,193,198,207,200,221,218,211,212,
        105,110,103, 96,117,114,123,124, 81, 86, 95, 88, 77, 74, 67, 68,
         25, 30, 23, 16,  5,  2, 11, 12, 33, 38, 47, 40, 61, 58, 51, 52,
         78, 73, 64, 71, 82, 85, 92, 91,118,113,120,127,106,109,100, 99,
         62, 57, 48, 55, 34, 37, 44, 43,  6,  1,  8, 15, 26, 29, 20, 19,
        174,169,160,167,178,181,188,187,150,145,152,159,138,141,132,131,
        222,217,208,215,194,197,204,203,230,225,232,239,250,253,244,243,
};

int8u halCommonCRC8 ( int8u newByte, int8u prevResult  ){
  return poly_crc8 [newByte ^ prevResult] ;
}//End of halCommonCRC8
/*----------------------------------------------------------------------------*/

static int8u crc8_atm (int8u *buf, int8u len, int8u icrc){
  int8u crc;
  crc = icrc;
  if (len) do {
    crc ^= *buf++;
    crc = poly_crc8 [crc];
  }while (--len);
  
  return crc;
}//End of crc8_atm
/*----------------------------------------------------------------------------*/

int8u halI2CReadWithCRC(int8u address, int8u regester, int8u * buffer, int8u count){
  int8u crc_rcv;
  int8u crc_cal;
  int8u buf;
  int8u err;
  
  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  i2c_start();
  i2c_send( address | 1 );
 
  for (int8u i = 0; i < count; i++) 
    buffer[i] = i2c_recv( ACK );
  
  crc_rcv = i2c_recv( NAK );
  
  i2c_stop();
  
  // Check crc
  crc_cal = 0;
  crc_cal = crc8_atm (&address,  1, crc_cal);
  crc_cal = crc8_atm (&regester, 1, crc_cal);
  buf = address | 1;
  crc_cal = crc8_atm (&buf,  1, crc_cal);
  crc_cal = crc8_atm (buffer,  count, crc_cal);
  
  
  err = 0;
  if (crc_cal-crc_rcv) 
    err =1;

  return err;
}//End of halI2CReadWithCRC
/*----------------------------------------------------------------------------*/


void halI2CWriteWithCRC(int8u address, int8u regester, const int8u * buffer, int8u count){
  int8u crc;
  int8u buf;
  
  crc = 0;
  crc = crc8_atm (&address,  1, crc);
  crc = crc8_atm (&regester, 1, crc);

  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  for (int8u i = 0; i< count; i++){ 
    // send valu over the bus
    i2c_send( buffer[i] );
    
    // update crc
    buf = buffer[i];
    crc = crc8_atm (&buf, 1, crc);
  }
  
  i2c_send( crc );
  
  i2c_stop();
  
 }//End of halI2CWriteWithCRC
/*----------------------------------------------------------------------------*/

/* These are the original functions from Tristan 

// Macros for common I2C operations to help improve readability of this file
#define halInternalI2CWait(mask)  while( !(SC1_TWISTAT & mask) )
#define halInternalI2CStart()     SC1_TWICTRL1 = SC_TWISTART;       \
                                  halInternalI2CWait(SC_TWICMDFIN)
                                  
#define halInternalI2CStop()      SC1_TWICTRL1 = SC_TWISTOP;        \
                                  halInternalI2CWait(SC_TWICMDFIN)
                                  
#define halInternalI2CWrite(data) SC1_DATA = data;                  \
                                  SC1_TWICTRL1 = SC_TWISEND;        \
                                  halInternalI2CWait(SC_TWITXFIN)
int8u halInternalI2CRead(void)
{
  SC1_TWICTRL1 = SC_TWIRECV;
  halInternalI2CWait(SC_TWIRXFIN);
  return SC1_DATA;
}

void halI2CInit(void) 
{
  // Set the mode of SC1 to Two Wire Serial (I2C)
  SC1_MODE = SC1_MODE_I2C;
  
  // Send ACK after recieved frames
  SC1_TWICTRL2 = SC_TWIACK;
  
  // Configure baud rate to 375 kbps
  SC1_RATEEXP = 1;
  SC1_RATELIN = 15;
}

 void halI2CWriteint8u(int8u address, int8u regester, int8u data)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  halInternalI2CWrite(data);
  halInternalI2CStop();
}

void halI2CWriteint16u(int8u address, int8u regester, int16u data)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  halInternalI2CWrite((int8u)(0xFF & (data >> 8)));
  halInternalI2CWrite((int8u)(0xFF & data));
  halInternalI2CStop();
}

int8u halI2CReadint8u(int8u address, int8u regester)
{
  int8u data;
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();
  halInternalI2CWrite(address | 1);
  data = halInternalI2CRead();
  halInternalI2CStop();
  return data;
}

int16u halI2CReadint16u(int8u address, int8u regester)
{
  int16u data;
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();			// Send Restart to I2C Bus
  halInternalI2CWrite(address | 1); // Write  address with RW bit set to 1 to set up reading
  
  // Read data from the bus
  data = halInternalI2CRead() << 8;	// Get most significant Byte
  data |= halInternalI2CRead();		// get less significant Byte 
  
  halInternalI2CStop();				// send Stop to I2C Bus
  return data;
}

void halI2CRead(int8u address, int8u regester, int8u * buffer, int8u count)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();
  halInternalI2CWrite(address | 1);

  int8u i;
  for (i = 0; i < count; i++) buffer[i] = halInternalI2CRead();
  halInternalI2CStop();
}
 
void halI2CWrite(int8u address, int8u regester, const int8u * buffer, int8u count)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  int8u i;
  for (i = 0; i < count; i++) halInternalI2CWrite(buffer[i]);
  halInternalI2CStop();
}

*/