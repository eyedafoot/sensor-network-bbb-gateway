#include "app/framework/include/af.h"
#include "i2c.h"
#include <driverUtils.h>
#include "AcclSensor.h"

/** @brief Some aliases for the i2c functions defines in "i2c.h".
  * @param dA: Device Address
  * @param rA: register Address  
  * @param rV: register Value    
  * @see i2c.h
*/

/******************************************************************************/
/* MMA8452Q STATUS Register                                              */
/******************************************************************************/
/* (Register Address: 0x00. Access:Read Only. Default value: 00)

 The MMA8452Q has 12-bit XYZ data. The event flag can be monitored by reading the 
 STATUS register (0x00). This can be done by using either a polling or interrupt 
 technique. It is not absolutely necessary to read the STATUS register to clear 
 it. Reading the data clears the STATUS register. 
*/
#define ST_ZYX_OW_BIT 7 /* X, Y, Z-axis Data Overwrite. */
#define ST_Z_OW_BIT   6 /* Z-axis Data Overwrite. */
#define ST_Y_OW_BIT   5 /* Y-axis Data Overwrite. */
#define ST_X_OW_BIT   4 /* X-axis Data Overwrite. */
#define ST_ZYX_DR_BIT 3 /* X, Y, Z-axis new Data Ready. */
#define ST_Z_DR_BIT   2 /* Z-axis new Data Available. */
#define ST_Y_DR_BIT   1 /* Y-axis new Data Available. */
#define ST_X_DR_BIT   0 /* X-axis new Data Available. */

#define ST_ZYX_OW BIT(7) /* X, Y, Z-axis Data Overwrite. Default value: 0. 
                            0: No data overwrite has occurred 
                            1: Previous X, Y, or Z data was overwritten by new X, Y, or Z data before it was read*/
#define ST_Z_OW   BIT(6) /* Z-axis Data Overwrite. Default value: 0
                            0: No data overwrite has occurred
                            1: Previous Z-axis data was overwritten by new Z-axis data before it was read*/
#define ST_Y_OW   BIT(5) /* Y-axis Data Overwrite. Default value: 0
                            0: No data overwrite has occurred
                            1: Previous Y-axis data was overwritten by new Y-axis data before it was read */
#define ST_X_OW   BIT(4) /* X-axis Data Overwrite. Default value: 0
                            0: No data overwrite has occurred
                            1: Previous X-axis data was overwritten by new X-axis data before it was read */
#define ST_ZYX_DR BIT(3) /* X, Y, Z-axis new Data Ready. Default value: 0
                            0: No new set of data ready
                            1: A new set of data is ready */
#define ST_Z_DR   BIT(2) /* Z-axis new Data Available. Default value: 0
                            0: No new Z-axis data is ready
                            1: A new Z-axis data is ready */
#define ST_Y_DR   BIT(1) /* Y-axis new Data Available. Default value: 0
                            0: No new Y-axis data ready
                            1: A new Y-axis data is ready */
#define ST_X_DR   BIT(0) /* X-axis new Data Available. Default value: 0
                            0: No new X-axis data ready
                            1: A new X-axis data is ready */


/******************************************************************************/
/* MMA8452Q XYZ_DATA_CFG Register                                             */
/******************************************************************************/
/* (Register Address: 0x0E. Access:Read Only. Default value: 00)

 The XYZ_DATA_CFG register sets the dynamic range and sets the high pass filter 
 for the output data. When the HPF_OUT bit is set. The data registers 
 (RA 0x01 - 0x06) will contain high pass filtered data when this bit is set. 
*/
#define XYZ_HPF_OUT_BIT 4 /* Enable High pass output data 1 = output data high pass filtered. Default value: 0 */
#define XYZ_FS1_BIT     1 /* Output buffer data format full scale. Default value: 00 (2g).*/
#define XYZ_FS0_BIT     0

#define XYZ_HPF_OUT  BIT(4) /* Enable High pass output data. */
#define XYZ_FSMASK  (BIT(0) | BIT(1)) /* There are 3 different dynamic ranges that can be set(2g, 4g, 8g). 
                                         The dynamic range is changeable only in the Standby Mode.
                                         FS1   FS0   g Range
                                          0      0     �2g
                                          0      1     �4g
                                          1      0     �8g
                                          1      1      �                    */

/******************************************************************************/
/* MMA8452Q Control Register 1 (CTRL_REG_1)                                   */
/******************************************************************************/
#define CR1_ACTIVE_BIT  0
#define CR1_F_READ_BIT  1
#define CR1_LNOISE_BIT  2
#define CR1_DR0_BIT     3
#define CR1_DR1_BIT     4
#define CR1_DR2_BIT     5
#define CR1_ASLP_RATE0  6 
#define CR1_ASLP_RATE1  7

#define ACTIVE_MASK      (BIT(0))
#define CR1_DR_MASK      (BIT(5)|BIT(4)|BIT(3))
#define CR1_ASLP_DR_MASK (BIT(7)|BIT(6))
/******************************************************************************/
/* MMA8452Q Control Register 2 (CTRL_REG_2)                                   */
/******************************************************************************/
#define CR2_MODS0_BIT   0
#define CR2_MODS1_BIT   1
#define CR2_SLPE_BIT    2
#define CR2_SMODS0_BIT  3
#define CR2_SMODS1_BIT  4
#define CR2_RST_BIT     6
#define CR2_ST_BIT      7

#define RESET_MASK      (BIT(6))
#define CR2_MODS_MASK   (BIT(1)|BIT(0))
#define CR2_SMODS_MASK  (BIT(4)|BIT(3))
/******************************************************************************/
/* MMA8452Q Control Register 3 (CTRL_REG_3)                                   */
/******************************************************************************/
#define CR3_PP_OD_BIT        0
#define CR3_IPOL_BIT         1
#define CR3_WAKE_FF_MT_BIT   3
#define CR3_WAKE_PULSE_BIT   4
#define CR3_WAKE_LNDPRT_BIT  5
#define CR3_WAKE_TRANS_BIT   6

/******************************************************************************/
/* MMA8452Q Control Register 4 (CTRL_REG_4)                                   */
/******************************************************************************/
#define CR4_INT_EN_DRDY_BIT    0
#define CR4_INT_EN_FF_MT_BIT   2
#define CR4_INT_EN_PULSE_BIT   3
#define CR4_INT_EN_LNDPRT_BIT  4
#define CR4_INT_EN_TRANS_BIT   5
#define CR4_INT_EN_ASLP_BIT    7

/******************************************************************************/
/* MMA8452Q Control Register 5 (CTRL_REG_5)                                   */
/******************************************************************************/
#define CR5_INT_CFG_DRDY_BIT    0
#define CR5_INT_CFG_FF_MT_BIT   2
#define CR5_INT_CFG_PULSE_BIT   3
#define CR5_INT_CFG_LNDPRT_BIT  4
#define CR5_INT_CFG_TRANS_BIT   5
#define CR5_INT_CFG_ASLP_BIT    7

/******************************************************************************/
/* MMA8452Q Portrait/Landscape Status Register  (PL_STATUS)                   */
/******************************************************************************/
/* (Register Address: 0x10. Access:Read Only. Default value: 00)

 This status register can be read to get updated information on any change in 
 orientation by reading Bit 7, or on the specifics of the orientation by reading 
 the other bits. For further understanding of Portrait Up, Portrait Down, 
 Landscape Left, Landscape Right, Back and Front orientations please refer to 
 AN4068 application note. The interrupt is cleared when reading the PL_STATUS 
 register.
*/
                                            
/*Back or Front orientation. Default value: 0
0: Front: Equipment is in the front facing orientation.
1: Back: Equipment is in the back facing orientation.*/                                            
#define PL_ST_BAFRO_BIT  0

 /*Landscape/Portrait orientation. Default value: 00
00: Portrait Up: Equipment standing vertically in the normal orientation
01: Portrait Down: Equipment standing vertically in the inverted orientation
10: Landscape Right: Equipment is in landscape mode to the right
11: Landscape Left: Equipment is in landscape mode to the left.*/
#define PL_ST_LAPO0_BIT  1   
#define PL_ST_LAPO1_BIT  2 

/*Z-Tilt Angle Lockout. Default value: 0.
0: Lockout condition has not been detected.
1: Z-Tilt lockout trip angle has been exceeded. Lockout has been detected.*/                                            
#define PL_ST_LO_BIT     6   
                                            
/*Landscape/Portrait status change flag. Default value: 0.
  0: No change, 
  1: BAFRO and/or LAPO and/or Z-Tilt lockout value has changed*/
#define PL_ST_NEWLP_BIT  7  

/* PL_ST MASKS */
#define PL_ST_BAFR_MASK     BIT(PL_ST_BAFRO_BIT)
#define PL_ST_LAPO_MASK     BIT(PL_ST_LAPO0_BIT) | BIT(PL_ST_LAPO1_BIT)
#define PL_ST_LOCKOUT_MASK  BIT(PL_ST_LO_BIT)
                                            
/* PL_ST MASKS */
#define PL_ST_PO_UP    0 // 00:Portrait Up: Equipment standing vertically in the normal orientation
#define PL_ST_PO_DOWN  2 // 01:Portrait Down: Equipment standing vertically in the inverted orientation
#define PL_ST_LA_RIGHT 4 // 10:Landscape Right: Equipment is in landscape mode to the right
#define PL_ST_LA_LEFT  6 // 11:Landscape Left: Equipment is in landscape mode to the left.

/******************************************************************************/
/* MMA8452Q PL_CFG Register  (PL_CFG)                                         */
/******************************************************************************/
#define PL_CFG_PL_EN_BIT   6
#define PL_CFG_DBCNTM_BIT  7

/******************************************************************************/
/* I2C Related Functions to access the MMA8452Q device                        */
/******************************************************************************/
int8u registerRead(int8u rA) {
  return I2CREAD08 (MMA8452Q_I2C_ADDRESS, rA); 
}

void registersRead(int8u rA, int8u* rVs, int8u count) {
  I2CREADBUFF(MMA8452Q_I2C_ADDRESS, rA, rVs,  count);
}

void registerWrite(int8u rA, int8u rV) {
  I2CWRITE08(MMA8452Q_I2C_ADDRESS, rA, rV);
}

void registersWrite(int8u rA, int8u* rVs, int8u count) {
  I2CWRITEBUFF(MMA8452Q_I2C_ADDRESS, rA, rVs, count);
}

void registerSetBit(int8u rA, int8u bitPos, int8u value) {
  int8u rV; 
  
  // Read current value of System Control 1 Register.
  rV = I2CREAD08(MMA8452Q_I2C_ADDRESS, rA); //registerRead(rA);
  
  if (value == TRUE)
    SETBIT(rV, bitPos);
  else
    CLEARBIT(rV, bitPos);
  
  // Return with previous value of the register.
  I2CWRITE08(MMA8452Q_I2C_ADDRESS, rA, rV);//registerWrite(rA, rV);
}


/******************************************************************************/
/* MMA8452Q Status related functions                                          */
/******************************************************************************/
int8u MMA8452Q_getStatus (void){
  return registerRead(MMA8452Q_RA_STATUS);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_STATUS); //
}//End of getStatus

int8u MMA8452Q_getSysMod(void) {
  return registerRead(MMA8452Q_RA_SYSMOD);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_SYSMOD); //
}
 
int8u MMA8452Q_getIntSrc(void) {
  return registerRead(MMA8452Q_RA_INT_SOURCE);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_INT_SOURCE);//
}// End of getWhoIam

int8u MMA8452Q_getWhoAmI(void){
  return registerRead(MMA8452Q_RA_WHO_AM_I);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_WHO_AM_I);//
}// End of getIntSrc

boolean MMA8452Q_isPresent(void) {
  
int8u whoami = registerRead(MMA8452Q_RA_WHO_AM_I);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_WHO_AM_I);//

if (whoami != WHO_AM_I_SIGNATURE) {
  return FALSE;
} else
  return TRUE;

}// End of isPresent
 
int8u MMA8452Q_getRegister(int8u rA){
  return registerRead(rA);//I2CREAD08(MMA8452Q_I2C_ADDRESS, rA);//
}//End of getRegister

void MMA8452Q_getControlRegisters(int8u regs[]){
  for (int8u i = MMA8452Q_RA_CTRL_REG1; i <= MMA8452Q_RA_CTRL_REG5; i++)
    regs[i] = registerRead(i);//I2CREAD08(MMA8452Q_I2C_ADDRESS, i);//
}//End of getControlRegisters

boolean MMA8452Q_isActive (void){
  int8u val = registerRead(MMA8452Q_RA_CTRL_REG1);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1);//
  val = READBIT(val, CR1_ACTIVE_BIT);
  val >>= CR1_ACTIVE_BIT;
  return val;
}//End of isActive

/* Most of the functions have been customized from examples in the Application Note AN4076*/
/***
 Most, although not quite all changes to the registers must be done while the 
 accelerometer is in Standby Mode. Current consumption in Standby Mode is 
 typically 1 - 2 �A. To be in Standby Mode the last bit of CTRL_REG1 must be 
 cleared (Active = 0). When Active = 1 the device is in the active mode.
*/


int32u MMA8452Q_getDataRateInUS (int8u odr){
  int32u timeInUS; // in microseconds
  switch (odr){
  case ACTIVE_DR_800HZ:  //0 0 0 800 Hz 1.25 ms
    timeInUS  = 1250; break;
  case ACTIVE_DR_400HZ:  //0 0 1 400 Hz 2.5 ms
    timeInUS  = 2500; break;
  case ACTIVE_DR_200HZ:  //0 1 0 200 Hz 5 ms
    timeInUS  = 5000; break;
  case ACTIVE_DR_100HZ:  //0 1 1 100 Hz 10 ms
    timeInUS  = 10000; break;
  case ACTIVE_DR_50HZ:   //1 0 0 50 Hz 20 ms
    timeInUS  = 20000; break;
  case ACTIVE_DR_12P5HZ: //1 0 1 12.5 Hz 80 ms
    timeInUS  = 80000; break;
  case ACTIVE_DR_6P25HZ: //1 1 0 6.25 Hz 160 ms
    timeInUS  = 160000; break;
  case ACTIVE_DR_1P56HZ: //1 1 1 1.56 Hz 640 ms
    timeInUS  = 640000;
  }
  return timeInUS;
}//End of getDataRateInUS

int32u MMA8452Q_getTurnOnTimeUS(int8u odr){
  // TurOnTime = 2/ODR + 2ms
  return ( (MMA8452Q_getDataRateInUS(odr) << 1) + 2000L );
}//End of getTurnOnTime

int8u MMA8452Q_XYZDataReady(void){
  int8u rV = registerRead(MMA8452Q_RA_STATUS);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_STATUS);//
  return (READBIT(rV, ST_ZYX_DR_BIT) >> ST_ZYX_DR_BIT);
}

void MMA8452Q_setActive(boolean enable){
  // Put sensor into Active/StandBy Mode by setting/clearing the Active bit
  registerSetBit(MMA8452Q_RA_CTRL_REG1, CR1_ACTIVE_BIT, enable);
}// End of Active

int8u MMA8452Q_getFSx(void){
  //get Full Scale  from XYZ_DATA_CFG Register
  int8u rV;
  

  // Read the XYZ_DATA_REG
  rV = registerRead(MMA8452Q_RA_XYZ_DATA_CFG);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_XYZ_DATA_CFG);//
  
  // Read the Scale bits in XYZ_DATA_CFG register 
  return  (READBITS(rV, XYZ_FSMASK)); 

}//End getFSx

int8u MMA8452Q_getScale(void){
  int8u Scale = MMA8452Q_getFSx() << 2;
  if (Scale == 0) Scale = 2;
  return Scale;
}//End of getScale

void MMA8452Q_setScale(int8u scale) {
 // int8u devModeOnEntry;
  int8u rV;
  
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//  
//  /* If previously defined the StandBy mode, means that the device is under 
//     configuration and it will be activated outside this function. If the device 
//     is in another mode, then it needs to be deactivated before programming and 
//     activating it afterwards in order to apply the changes. */
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Read the XYZ_DATA_REG
  rV = registerRead(MMA8452Q_RA_XYZ_DATA_CFG);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_XYZ_DATA_CFG);//
  
  // Clear the Scale bits in XYZ_DATA_CFG register (setting �2g scale by default)
  CLEARBITS(rV, XYZ_FSMASK); // Set �2g as the default mode
  
  switch (scale) {
  case 4: SETBIT(rV, 0); break; // FSX = 01 ==> �4g
  case 8: SETBIT(rV, 1); break; // FSX = 10 ==> �8g
  }
  
  registerWrite(MMA8452Q_RA_XYZ_DATA_CFG, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_XYZ_DATA_CFG, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device back to Active Mode
//    MMA8452Q_setActive(TRUE); 
//  }
}

/** Setting the Oversampling Mode
There are four different oversampling modes. There is a normal mode, a low noise + power mode, a high-resolution mode and
a low-power mode. The difference between these are the amount of averaging of the sampled data, which is done internal to the
device. The following chart shows the amount of averaging at each data rate, which is the OSRatio (oversampling ratio). There
is a trade-off between the oversampling and the current consumption at each ODR value.
*/
#define MODS_MASK 0x3
#define MODS_NORMAL 0x00
#define MODS_LO_NOISE_LO_POWER 0x01
#define MODS_HI_RESOLUTION 0x02
#define MODS_LO_POWER 0x03

void MMA8452Q_setOSMode (t_osMode osMode){
//  int8u devModeOnEntry;
  int8u rV;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG2 );//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2);// 
  
  // Clear the Mode bits in CTRL_REG2 (Normal Mode)
  CLEARBITS(rV, MODS_MASK);                   // MODS = 10
  
  rV |= osMode;
  switch (osMode) {
  case HiRes: 
    SETBIT(rV, CR2_MODS1_BIT); break;           // MODS = 10
  case LoPower: 
    SETBIT(rV, CR2_MODS1_BIT);                  // MODS = 11
  case LoNoiseLoPower: 
    SETBIT(rV, CR2_MODS0_BIT); break;           // MODS = 01
  }

  // Return with previous value of System Control 2 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG2, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}// End of setOSMode

void MMA8452Q_setSOSMode (t_osMode osMode){
  int8u rV;
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG2 ); //I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2);//
  
  // Clear the Mode bits in CTRL_REG2 (Normal Mode)
  CLEARBITS(rV, MODS_MASK);                   // MODS = 10
  
  rV |= osMode;
  switch (osMode) {
  case HiRes: 
    SETBIT(rV, CR2_SMODS1_BIT); break;           // MODS = 10
  case LoPower: 
    SETBIT(rV, CR2_SMODS1_BIT);                  // MODS = 11
  case LoNoiseLoPower: 
    SETBIT(rV, CR2_SMODS0_BIT); break;           // MODS = 01
  }

  // Return with previous value of System Control 2 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG2, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}// End of setSOSMode

int8u  MMA8452Q_getActiveDataRate (void){
  
  int8u rV = registerRead( MMA8452Q_RA_CTRL_REG1 );//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1);//
  
  return (rV >> 3) & 7;

}//End of getActiveDataRate

void MMA8452Q_setActiveDataRate (int8u DataRateValue){
  int8u rV;
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }
    
  rV = registerRead( MMA8452Q_RA_CTRL_REG1 );//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1);// 
  
  // Clear the Mode bits in CTRL_REG1 
  CLEARBITS(rV, CR1_DR_MASK);                   

  DataRateValue &= 7; // Ensure user does not affect other bits in CR1 
  
  rV |= (DataRateValue << 3);

  // Return with previous value of System Control 1 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG1, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1, rV);//

//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}//End of setActiveDataRate

int8u  MMA8452Q_getSleepDataRate (void){
  
  int8u rV = registerRead( MMA8452Q_RA_CTRL_REG1 );//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1 );//
  
  return (rV >> 6) & 3;

}//End of getSleepDataRate


void MMA8452Q_setSleepDataRate (int8u DataRateValue){
  int8u rV;
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  rV = registerRead( MMA8452Q_RA_CTRL_REG1 ); //I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1);//
  
  // Clear the Mode bits in CTRL_REG2 (Normal Mode)
  CLEARBITS(rV, CR1_ASLP_DR_MASK);                   

  DataRateValue &= 3; // Ensure user does not affect other bits in CR1 
  
  rV |= DataRateValue << 6;

  // Return with previous value of System Control 1 Register.
  registerWrite( MMA8452Q_RA_CTRL_REG1, rV);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1, rV);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}//End of setSleepDataRate


/* MMA8452Q_EVENTS */
#define 	FREEFALL_MOTION  0x01 /**< Freefall/motion event */
#define 	PULSE            0x02 /**< Pulse (tap) event */
#define 	ORIENTATION      0x04 /**< Orientation change event */

void MMA8452Q_setWakeOn(boolean enable, int8u events) {
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set "wake on" events
  if (events & FREEFALL_MOTION)
    registerSetBit(MMA8452Q_RA_CTRL_REG3, CR3_WAKE_FF_MT_BIT, enable);
  
  if (events & PULSE)
    registerSetBit(MMA8452Q_RA_CTRL_REG3, CR3_WAKE_PULSE_BIT, enable);
  
  if (events & ORIENTATION)
    registerSetBit(MMA8452Q_RA_CTRL_REG3, CR3_WAKE_LNDPRT_BIT, enable);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}// End of wakeOn

void MMA8452Q_setOffset(int8s off_x, int8s off_y, int8s off_z) {
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set calibration values
  registerWrite(MMA8452Q_RA_OFF_X, off_x);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_OFF_X, off_x);//
  registerWrite(MMA8452Q_RA_OFF_Y, off_y);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_OFF_Y, off_y);//
  registerWrite(MMA8452Q_RA_OFF_Z, off_z);//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_OFF_Z, off_z);//
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}
 
void MMA8452Q_setFastRead(boolean tf) {
//  int8u devModeOnEntry;

//    // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set/Clear Fast Mode
  registerSetBit(MMA8452Q_RA_CTRL_REG1, CR1_F_READ_BIT, tf);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
  
}//End of setFastRead
 
void MMA8452Q_setLowNoise(boolean tf) {
//  int8u devModeOnEntry;
//
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set/Clear Low Noise Mode
  registerSetBit(MMA8452Q_RA_CTRL_REG1, CR1_LNOISE_BIT, tf);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
}//End of setLowNoise
 
void MMA8452Q_reset(void) {
  /* Reset the device
  When the reset bit is enabled, all registers are rest and are loaded with 
  default values. Writing �1� to the RST bit immediately resets the device, no 
  matter whether it is in ACTIVE/WAKE, ACTIVE/SLEEP, or STANDBY mode.*/
  
  registerWrite(MMA8452Q_RA_CTRL_REG2, BIT(CR2_RST_BIT));//I2CWRITE08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG2, BIT(CR2_RST_BIT));
  //wait for Turn-On time ~ 2/ODR + 1 ms
  waitBusy((MMA8452Q_getTurnOnTimeUS(0) + 512L) >> 10);
//  registerSetBit(MMA8452Q_RA_CTRL_REG2, CR2_RST_BIT, TRUE);
}//End of reset
 
void MMA8452Q_selfTest(boolean init) {
//  int8u devModeOnEntry;
//
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Initiate/Finish self test Mode
    registerSetBit(MMA8452Q_RA_CTRL_REG2, CR2_ST_BIT, init);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
}//End of selfTest
 
void MMA8452Q_setAutoSleep(boolean enable) {
//  int8u devModeOnEntry;
//
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  // Set/Clear auto sleep Mode
  registerSetBit(MMA8452Q_RA_CTRL_REG2, CR2_SLPE_BIT, enable);
  
//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
}
 
void MMA8452Q_detectOrientation(boolean enable) {
//  int8u devModeOnEntry;
//
//  // Read the current device operating mode. 
//  devModeOnEntry = MMA8452Q_getSysMod();
//
//  if (devModeOnEntry != STANDBY_MODE){
//    // Put the device into Standby Mode
//    MMA8452Q_setActive(FALSE); 
//  }

  registerSetBit(MMA8452Q_RA_PL_CFG, PL_CFG_PL_EN_BIT, enable);

//  if (devModeOnEntry != STANDBY_MODE){
//    //  Put the device back into Active Mode
//    MMA8452Q_setActive(TRUE);
//  }
}

void readAccel(int16s* x, int16s* y, int16s* z){
  int16s axes[3];
  MMA8452Q_getAxes(axes);
  *x = axes[0];
  *y = axes[1];
  *z = axes[2];
}

void MMA8452Q_getAxes(int16s axes[]) {
  int8u data[6];
  int8u read_count;
  int8u FastReadMode;

//  FastReadMode = I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_CTRL_REG1);
//  FastReadMode = READBIT(FastReadMode, CR1_F_READ_BIT);

    FastReadMode = READBIT(registerRead(MMA8452Q_RA_CTRL_REG1), CR1_F_READ_BIT);
  
  if (FastReadMode == 0)
    read_count = 6;
  else
    read_count = 3;
  
  registersRead(MMA8452Q_RA_OUT_X_MSB, data, read_count);
  
  for (int i = 0; i < 3; i++) {
    axes[i]  = data[i * (read_count / 3)] << 8;
    
    if (FastReadMode == 0)
      axes[i] |= data[(i * 2) + 1];
    
    axes[i] >>= 4;
    
  }
  
}

boolean MMA8452Q_getOrientation(int8u *value) {
  
  *value = registerRead(MMA8452Q_RA_PL_STATUS);//I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_PL_STATUS);//
  return (READBIT(*value, PL_ST_NEWLP_BIT) >> PL_ST_NEWLP_BIT);
  
}//End of getOrientation
 
int16s MMA8452Q_getPortrait(int8u orient) {
  
  if (READBIT(orient, PL_ST_LAPO1_BIT) == 0)
    return READBITS(orient, PL_ST_LAPO_MASK);
  else return -1;
  
}//End of getPortrait
 
int16s MMA8452Q_getLandscape(int8u orient) {
  
  if ( READBIT(orient, PL_ST_LAPO1_BIT) )
    return READBITS(orient, PL_ST_LAPO_MASK);
  else return -1;
  
}//End of getLandscape
 
int16s MMA8452Q_getBackFront(int8u orient) {
  
  return READBIT(orient, PL_ST_BAFRO_BIT);
  
}//End of getBackFront


void MMA8452Q_showRegister (int8u rV, int8u rA){
  
  logPrintln(  "+------------+----------+-------------+--------------+-------------+-------------+-----+------------+");
  switch (rA){
  case MMA8452Q_RA_CTRL_REG1:
    logPrintln("|  ASLPDR1   |   ASLDR0 |     DR2     |      DR1     |     DR0     |   LNOISE    |F_RD |    ACTV    |");
    break;
  case MMA8452Q_RA_CTRL_REG2:
    logPrintln("|     ST     |    RST   |      0      |    SMODS1    |    SMODS0   |    SLPE     |MODS1|    MODS0   |");
    break;
  case MMA8452Q_RA_CTRL_REG3:
    logPrintln("|     0      |WAKE_TRANS| WAKE_LNDPRT |  WAKE_PULSE  |  WAKE_FF_MT |    IPOL     |  0  |    PP_OD   |");
    break;
  case MMA8452Q_RA_CTRL_REG4:
    logPrintln("|INT_EN_ASLP |     0    |INT_EN_TRANS |INT_EN_LNDPRT |INT_EN_PULSE |INT_EN_FF_MT |  0  |INT_EN_DRDY |");
    break;
  case MMA8452Q_RA_CTRL_REG5:
    logPrintln("|INT_CFG_ASLP|     0    |INT_CFG_TRANS|INT_CFG_LNDPRT|INT_CFG_PULSE|INT_CFG_FF_MT|  0  |INT_CFG_DRDY|");
    break;
  case MMA8452Q_RA_XYZ_DATA_CFG:
    logPrintln("|     0      |     0    |      0      |    HPF_OUT   |      0      |      0      | FS1 |     FS0    |");
    break;
  case MMA8452Q_RA_SYSMOD:
    logPrintln("|     0      |     0    |      0      |       0      |      0      |      0      |SLEEP|     WAKE   |");
    break;
        break;
  default:
    logPrintln("|   ZYX_OW   |    Z_OW  |     Y_OW    |     X_OW     |   ZYX_DR    |    Z_DR     |Y_DR |    X_DR    |");
  }
  
    logPrintln("|     %d      |     %d    |      %d      |       %d      |      %d      |      %d      |  %d  |      %d     |",
                     READBIT(rV,7) > 0, READBIT(rV,6) > 0, READBIT(rV,5) > 0,
                     READBIT(rV,4) > 0, READBIT(rV,3) > 0, READBIT(rV,2) > 0,
                     READBIT(rV,1) > 0, READBIT(rV,0) > 0
                     );
 // emberAfCorePrintln("+-----------------------------------------------------------------------------------------------------------------+");
  
}//End of showRegister

void MMA8452Q_showStatus (int8u st){
  
  MMA8452Q_showRegister(st, MMA8452Q_RA_STATUS);
  
}//End of showStatus


int8u RegisterMap [0x32];

void printRegisterMap (int ini, int last){
  int8u rV;
   
  logPrintln("+------+------+------+------+");
  logPrintln("|-ADDR-|-CURR-|-PREV-|-DIFF-|");
  logPrintln("+------+------+------+------+");
  
  for(int8u i = ini; i <= last; i++){
    rV = I2CREAD08(MMA8452Q_I2C_ADDRESS, i);//registerRead(i);
    logPrintln("| 0x%x | 0x%x | 0x%x | 0x%x |", i, rV , RegisterMap[i], rV ^ RegisterMap[i]);
    RegisterMap[i] = rV;
  }
}

//void getRegisterMap (int8u map[]){
//  halCommonMemCopy(map, RegisterMap, 50);
//}

void getRegisterMap ( int8u map[] ){
//  registersRead(0, map, 50); 
  for(int8u i = 0; i <= 0x31; i++){
     map[i] = registerRead(i);//I2CREAD08(MMA8452Q_I2C_ADDRESS, i);//
  }
}


void checkDevice(void){
  int8u reg;
  
  do {
    reg = registerRead(MMA8452Q_RA_WHO_AM_I); //I2CREAD08(MMA8452Q_I2C_ADDRESS, MMA8452Q_RA_WHO_AM_I);// // Read WHO_AM_I register
      if (reg == WHO_AM_I_SIGNATURE) {
        logPrintln("MMA8452Q found.  Device Signature: 0x%x", reg);
          break;
      } else {
        logPrintln("Could not connect to MMA8452Q: WHO_AM_I reg == 0x%x ", reg);
        waitSleeping(500);//i2cRetryPeriod);
      }
  } while (TRUE);
}



void printAxes(int16s axes[], int8u Scale){
  int32u fraction;
  int16s val;
  
  int8u detScale = MMA8452Q_getScale();
  
  logPrintln("Scale provided by user: �%dG", Scale);
  logPrintln("Scale read from device: �%dG", detScale);
  logPrint("[X, Y, Z]: Dec = [%d, %d, %d]; Hex = [0x%2x, 0x%2x, 0x%2x]; Real = [", 
           axes[0], axes[1], axes[2], axes[0]<<4, axes[1]<<4, axes[2]<<4);
  for (int i = 0; i< 3; i++){
    val = axes[i];
    if (val < 0) {
      val = -val;
      logPrint("-");
    }
    logPrint("%d.", getAxisIntegerPart( val, Scale ));
    fraction = getAxisFractionPart( val, Scale );
//    if (fraction > 0){
      if (fraction < 1000) logPrint("0");
      if (fraction < 100) logPrint("0");
      if (fraction < 10) logPrint("0");
//    }
    logPrint("%d ",fraction);
  }
  logPrintln("]");
}

void printAxisValue (int16s axisVal, int8u Scale){
  int32s val;
  
  val = axisVal;
  if (val < 0) {
    val = -val;
    logPrint("-");
  }
  logPrint("%d.", getAxisIntegerPart( val, Scale ));
  val = getAxisFractionPart( val, Scale );
  if (val < 1000) logPrint("0");
  if (val < 100) logPrint("0");
  if (val < 10) logPrint("0");
  logPrint("%d ",val);
 
}

void printOrientation (int8u plStatus){

  if (plStatus & PL_ST_LOCKOUT_MASK){ // Z-tilt lockout
    logPrint(" Flat.");
    if (plStatus & PL_ST_BAFR_MASK)
      logPrint(" Upside-Down.");
  }
  else
    switch (plStatus & PL_ST_LAPO_MASK) {
    case PL_ST_PO_UP:
      logPrint(" Portrait Up.");
      break;
    case PL_ST_PO_DOWN:
      logPrint("Portrait Down.");
      break;
    case PL_ST_LA_RIGHT:
      logPrint("Landscape Right.");
      break;
    case PL_ST_LA_LEFT:
      logPrint("Landscape Left.");
      break;
    }
}//End of printOrientation
