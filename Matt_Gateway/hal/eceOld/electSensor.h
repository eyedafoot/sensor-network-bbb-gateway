/** @file hal/ece/electSensor.h
 * @brief Header for electrical current sensor.
 *
 * See @ref electrical for documentation.
 *
 * <!-- University of New Brunswick. 2014 -->
 */
#ifndef __ELECTRIC_SENSOR_H__
#define __ELECTRIC_SENSOR_H__


#define BURDEN_RESISTOR_VALUE    297L

#define PERIOD_US_60HZ           16667  // Period of a 60Hz sine wave in usec
#define N_CYCLES                 16      // 4 cycles of a 60 Hz AC wave

#define ADC_SLOW_CLK 1L
#define ADC_HIGH_CLK 6L
#define ADC_MIN_RESOL 7
   
#define ADC_CLOCK_MHZ            ADC_SLOW_CLK
#define ADC_CLK_SELECTED         ((ADC_CLOCK_MHZ == ADC_SLOW_CLK) << ADC_1MHZCLK_BIT)

#define ADC_SAMPLE_CLOCKS_SELECTED \
   ADC_SAMPLE_CLOCKS_512

#define SAMPLING_PERIOD \
   BIT32(ADC_SAMPLE_CLOCKS_SELECTED + 5)

#define ADC_SOURCE_BATT_VOLTAGE ADC_SOURCE_ADC2_VREF2
     
/** @brief Initialize EM357 ADC
 *
 * Configure and calibrate the internal ADC.
 */
void halInternalInitAdc(void);

/** @brief Read Battery Voltage
 *
 * Returns the battery voltage by measuring it via a voltage divider circuit
 * attached to PB7 (ADC2). The resistor values for the voltage divider are
 *   R1 = 9.83 kOHM
 *   R2 = 5.14 kOHM
 *
 * Return value is in units of mV
 */
int16s readBattVoltage(void);
int32s readBoardBattery(void);
   
/** @brief Read AC Current
 *
 * Measure an AC current. For this function to work properly a current
 * transformer needs to be connected to ADC4 and ADC5. Voltage measurements
 * are taken diffrentially across the CT's burden resistor and then converted
 * to Irms.
 *
 * Because the time required to take all of the measurements will most likely
 * exceed the rundown time of the watchdog timer, this function returns
 * immidiatly. When the measurement operation is complete, the callback function
 * provided by the user will be called and passed the value.
 *
 * @param callback
 */
void readACCurrent(void (*callback)(int16u));

/** @brief Integer Square Root
 *
 * Calculated the square root of the provided value using integer arithmetic.
 * The return value will be rounded to the nearest integer.
 *
 * @param value
 */
int32u squareRoot(int32u value);

//Added by Julian Cardenas
void readTokenVrefs(int16s* mfg_1v8_reg, int16s* mfg_vref);
void readRawVRefs(int16s* vdd, int16s* vRef2, int16s* vReg2);
int16s getADCncVref2(void);
int16s getADCncVreg2(void);
int16s getADCoffset(void);
int16s getADCgain(void);
int16s ADCraw2mv(int16s rawSample);

int16s convertTo100uVolts(int16u adcdata);

int16s getADCNvss(void);
int16s getADCNvdd(void);

int16s getADCraw_gnd_Vref2(void);
int16s getADCraw_gnd_gnd(void);
int16s getADCraw_vref_vref2(void);
int16s getADCraw_vref_gnd(void);
int16s getADCraw_vreg2_vref2(void);
int16s getADCraw_vdd_gnd(void);

int16s getADCPeriod(void);
int16s getADCbitRes(void);
int32s getADCClkHz (void);
int32s getADCFsHz (void);

boolean adcIsCalibrated(void);
#endif // __ELECTRIC_SENSOR_H__
