#include "app/framework/include/af.h"
#include <hal/hal.h>
#include "driverUtils.h"
#include "electSensor.h"

#define ADC_INPUT_PB5 0x0   /* ADC0 */
#define ADC_INPUT_PB6 0x1   /* ADC1 */
#define ADC_INPUT_PB7 0x2   /* ADC2 */
#define ADC_INPUT_PC1 0x3   /* ADC3 */
#define ADC_INPUT_PA4 0x4   /* ADC4 */
#define ADC_INPUT_PA5 0x5   /* ADC5 */
#define ADC_INPUT_GND 0x8   /* GND, 0V */
#define ADC_INPUT_VREF2 0x9 /* VREF/2, 0.6V */
#define ADC_INPUT_VREF  0xA /* VREF, 1.2V */
#define ADC_INPUT_VREG2 0xB /* VDD_PADSA/2, 0.9 */



// Define a channel field that combines ADC_MUXP and ADC_MUXN
#define ADC_CHAN        (ADC_MUXP | ADC_MUXN)
#define ADC_CHAN_MASK   (ADC_MUXP_MASK | ADC_MUXN_MASK)
#define ADC_CHAN_BIT    ADC_MUXN_BIT
#define ADC_CHAN_BITS   (ADC_MUXP_BITS + ADC_MUXN_BITS)

// Globals
#define DMA_BUFFER_SZ \
   ( (N_CYCLES *  ADC_CLOCK_MHZ * PERIOD_US_60HZ) / SAMPLING_PERIOD )

static int16s buffer1[DMA_BUFFER_SZ];
static int16s buffer2[DMA_BUFFER_SZ];
static int16s* dmaBuffer;
static int16s* processBuffer;

// for calibration purposes
int8u adcVoltageValid;          // bitmap of valid reference voltage readings
enum{
  nVREG2valid,
  VREG2valid,
  nVREFvalid,
  VREFvalid,
  nGNDvalid,
  GNDvalid,
  ftVREGvalid,
  ftVREFvalid
};

static int16s factoryTrimmedVREG;
static int16s nVREG2;  // VREG2_VREF2;
static int16s  VREG2;  // VREG2_GND
static int16s nVDD;    // nVREG2      
static int16s  VDD;    // VREG2

static int16s factoryTrimmedVREF;
static int16s nVREF;   // VREF_VREF2
static int16s  VREF;   // VREF_GND

static int16s nGND;    // GND_VREF2
static int16s  GND;    // GND_GND
static int16s nVSS;    // nGND

static int16s offset;
static int16s gain;

static boolean adcCalibrated;


static int16s hiVreg2;            // read with hal Internalfunctions

void adcEnable(void)              { SETBIT(ADC_CFG, ADC_ENABLE_BIT); }  
void adcDisable(void)             { CLEARBIT(ADC_CFG, ADC_ENABLE_BIT); }  
void adcInterruptDisable(void)    { INT_ADCCFG = 0; }
void adcDMAReset(void)            { SETBIT(ADC_DMACFG,ADC_DMARST_BIT); }
void adcResetCalibration(void)    { ADC_OFFSET = ADC_OFFSET_RESET;  ADC_GAIN = ADC_GAIN_RESET; adcCalibrated = FALSE; }
void adcCalibrate(void);
boolean adcIsCalibrated(void)     { return adcCalibrated; }

int16s getADCraw_gnd_Vref2(void)  { return nGND; }
int16s getADCraw_gnd_gnd(void)    { return GND; }
int16s getADCraw_vref_vref2(void) { return nVREF; }
int16s getADCraw_vref_gnd(void)   { return VREF; }
int16s getADCraw_vreg2_vref2(void){ return nVREG2; }
int16s getADCraw_vdd_gnd(void)    { return VDD; }
int16s getADCraw_vreg2_gnd(void)  { return VREG2; }

int16s getADCNvss(void)           { return nVSS; }
int16s getADCNvdd(void)           { return nVDD; }

boolean getADCClock(void)         { return (boolean)(ADC_CFG & ADC_1MHZCLK_MASK); }
int16s getADCPeriod(void)         { return (int16s)((ADC_CFG_REG & ADC_PERIOD_MASK) >> ADC_PERIOD_BIT); }
int16s getADCbitRes(void)         { return (getADCPeriod() + ADC_MIN_RESOL); }
int32s getADCClkHz (void)         { return ((ADC_CFG_REG & ADC_1MHZCLK_MASK)?1000000:6000000);}
int32s getADCFsHz (void)          { return (getADCClkHz() >> (getADCPeriod() + 5));}

int16s getADCoffset(void)         { return (ADC_OFFSET & ADC_OFFSET_FIELD_MASK); }
int16s getADCgain(void)           { return (ADC_GAIN & ADC_GAIN_FIELD_MASK); }
int16u getADCChannel (void)       { return ((ADC_CFG & ADC_CHAN) >> ADC_CHAN_BIT); }

void setADCChannel(int32u ch)     { ADC_CFG &= ~ADC_CHAN_MASK; ADC_CFG |= (ch & ADC_CHAN_MASK) << ADC_CHAN_BIT; }
void setDMABuffer(int16s buf[], int16s sz){ ADC_DMABEG = (int32u)buf; ADC_DMASIZE = (sz & ADC_DMASIZE_FIELD_MASK);}

int16s ADCraw2mv(int16s rawSample)        { return (rawSample * 1200L ) >> 14; }
int16s convertToMillivolts(int16s adcdata){ return ((adcdata * 1200L) / 16384L);}// if calibrated, just scale by nominal vref (1200mV)

int16s readADCsampleBlocking(void){
  // Clear the interrupt flags
  INT_ADCFLAG = 0xFFFF;
  
  // Wait for a new sample
  while (!(INT_ADCFLAG & INT_ADCDATA));
  
  return (int16s)ADC_DATA;
} //End of readADCsampleBlocking

void readTokenVrefs(int16s* mfg_1v8_reg, int16s* mfg_vref){
  // Read factory trimmed voltage values from tokens
  halCommonGetToken(mfg_1v8_reg, TOKEN_MFG_1V8_REG_VOLTAGE);
  halCommonGetToken(mfg_vref, TOKEN_MFG_VREF_VOLTAGE);
} //End of readTokenVrefs

// Forward declerations to private functions
void internalCalibrateADC(void);
void internalReadDiffSeries(void);
int16s internalReadADC(void);


void (*dmaCallback)(int16u);

// Ember event for delayed processing of ACD data, requires a custom event to
// be defined in the AppBuilder
//   Control: adcEventControl
//   Function: adcEvent
EmberEventControl adcEventControl;

// Function called by the stack's event processing mechenism after a successful
// DMA operation. It processes the ADC measurements to calculate the Irms,
// and then returnes the value to the callback function provided by the user
// to readACCurrent(). Note: All of the calculations are done using integer
// arithmatic, so there will be rounding errors.

void adcEvent(void){
  int32u irms = 0xFFFFFFFF;
  if (processBuffer != NULL){
    int i;
    int32s avg = 0;
    int16s max = -32768;
    int16s min = 32767;
    
    logPrint("-A- ADC Measurement: Calculating RMS Value -A-");                                 // ERASE ME
    for (i = 0; i < DMA_BUFFER_SZ; i++) {
      avg += processBuffer[i];
//      logPrint("0x%2x ",processBuffer[i]);                                                    // ERASE ME
      logPrint("%d ",processBuffer[i]);                                                       // ERASE ME
//      print_uXtoX (convertTo100uVolts(processBuffer[i]), 10000L); logPrint(", ");             // ERASE ME
      if (max < processBuffer[i]) max = processBuffer[i];                                     // ERASE ME
      if (min > processBuffer[i]) min = processBuffer[i];                                     // ERASE ME
    }
    avg /= DMA_BUFFER_SZ;
    logPrintln("");                                                                           // ERASE ME
    int16s vpp = max-min;                                                                     // ERASE ME
    logPrint("Vpp = %d adcunits (",vpp); print_uXtoX (convertTo100uVolts(vpp), 10000L); logPrintln(" volts)"); // ERASE ME
    
    avg = convertTo100uVolts(avg);

    // Calculate the RMS value from the AC measurements
    irms = 0;
    for (i = 0; i < DMA_BUFFER_SZ; i++) {
      // Offset corretion to help prevent overflow errors
      processBuffer[i] = convertTo100uVolts( processBuffer[i] ) - avg;
      
      irms += processBuffer[i] * processBuffer[i];
    }
    irms = squareRoot(irms / DMA_BUFFER_SZ);


    // ERASE or COMMENT OUT the following lines                                                      // ERASE ME
    logPrint("-V- ADC Measurement:Internally before conversion to full scale Amps (0x%4x), ", irms); // ERASE ME
    print_uXtoX (irms, 10000L); logPrintln(" volts -V-");                                            // ERASE ME
    // ERASE or COMMENT OUT the previous lines                                                       // ERASE ME
    
    irms = (irms * 2000L) / BURDEN_RESISTOR_VALUE;
    
    // ERASE or COMMENT OUT the following lines                                                      // ERASE ME
    logPrint("-A- ADC Measurement:Internally after conversion to full scale Amps (0x%4x), ", irms);  // ERASE ME
    print_uXtoX (irms, 10000L); logPrintln(" A -A-");                                                // ERASE ME
    // ERASE or COMMENT OUT the previous lines                                                       // ERASE ME
  }
  // Finished, clean up and call the user-provided callback function
  emberEventControlSetInactive(adcEventControl);
  
  if (dmaCallback != NULL) dmaCallback((int16u)irms);
}//End of adcEvent


boolean adcOneShotPoll(int16s* adcdata, int32u ADC_SOURCE, int32s precision){

  adcdata[0] = 0;
  if (ADC_DMASTAT & ADC_DMAACT) 
    return FALSE;
   
  if ( (precision < 7) || ( precision > 14 )) 
    return FALSE;

  adcDisable();
  
  precision -= 7;
  ADC_CFG = ADC_CLK_SELECTED
            | (precision << ADC_PERIOD_BIT)
            | (ADC_SOURCE << ADC_CHAN_BIT);
  
  // Configure interrupts for polling
  INT_CFGCLR = INT_ADC;
  INT_ADCCFG = INT_ADCDATA;

  // Start the ADC
  adcEnable();
  
  adcdata[0] = readADCsampleBlocking();
  
  // Stop the ADC
  adcDisable();
  
  return TRUE;
}//End of adcOneShotPoll

void adcCalibrate(void){
  //try to read voltages
  int16s ngnd, nvdd;
  int8u valuesRead = 0; 

  valuesRead  = adcOneShotPoll(&ngnd, ADC_SOURCE_GND_VREF2, 14) ;
  valuesRead &= adcOneShotPoll(&nvdd, ADC_SOURCE_VREG2_VREF2, 14) ;
  
  if ( valuesRead ){
    adcResetCalibration();
    nVSS = ngnd;
    nVDD = nvdd - nVSS;
    adcCalibrated = TRUE;
    
    offset = -2 * nVSS;
    gain = (int16u)(32768L * (16384L * factoryTrimmedVREG / 24000L) / nvdd);
  }
  
  
  // Calculate also offset and gain to use with differential readings
  
}//End of adcCalibrate

void halInternalInitAdc(void){
  boolean valueRead;
  int16s value; 
  
  // First thing to do is to STOP the ADC
  adcDisable();
  
  // Initialize DMA and process buffer pointers;
  dmaBuffer = buffer1;
  processBuffer = buffer2;
  adcResetCalibration();
  
  // Read reference voltages
  valueRead = adcOneShotPoll( &value, ADC_SOURCE_GND_VREF2, 14 ) ;
  if ( valueRead ) { nGND = nVSS = value; SETBIT(adcVoltageValid, nGNDvalid); }
  
  valueRead = adcOneShotPoll( &value, ADC_SOURCE_VREG2_VREF2, 14 );
  if ( valueRead ) { nVREG2 = nVDD = value; SETBIT(adcVoltageValid, nVREG2valid); }
  
  valueRead = adcOneShotPoll( &value, ADC_SOURCE_VREF_VREF2, 14 );
  if ( valueRead ) { nVREF = value; SETBIT(adcVoltageValid, nVREFvalid); }
  
  valueRead = adcOneShotPoll( &value, ADC_SOURCE_VGND, 14 );
  if ( valueRead ) { GND = value; SETBIT(adcVoltageValid, GNDvalid); }
  
  valueRead = adcOneShotPoll( &value,  ADC_SOURCE_VREF, 14 );
  if ( valueRead ) { VREF = value; SETBIT(adcVoltageValid, VREFvalid); }
  
  valueRead = adcOneShotPoll( &value, ADC_SOURCE_VDD_GND, 14);
  if ( valueRead ) { VDD = VREG2 = value; SETBIT(adcVoltageValid, VREG2valid); }

  halCommonGetToken( &factoryTrimmedVREG, TOKEN_MFG_1V8_REG_VOLTAGE );
  SETBIT(adcVoltageValid, ftVREGvalid);
  
  halCommonGetToken( &factoryTrimmedVREF, TOKEN_MFG_VREF_VOLTAGE );
  SETBIT(adcVoltageValid, ftVREFvalid);

  hiVreg2 = halInternalGetVreg();

  // Configure interrupts for polling
  INT_CFGCLR = INT_ADC;
  INT_ADCCFG = INT_ADCDATA;
  
  // Start the ADC
  adcEnable();
  
  // Calibrate the ADC 
  adcCalibrate();
  
  // Stop the ADC
  adcDisable();
  if ( adcIsCalibrated() ){                                                                     // ERASE ME
    logPrintln("nVSS= %d (0x%2x), nVDD= %d (0x%2x)", nVSS, nVSS, nVDD, nVDD);                   // ERASE ME
    logPrint("nVSS= "); print_uXtoX (convertTo100uVolts( nVSS ), 10000L); logPrint("V");        // ERASE ME
    logPrint("nVDD= "); print_uXtoX (convertTo100uVolts( nVDD ), 10000L); logPrintln(" V");     // ERASE ME
    
    logPrintln("hal Internal NVreg2= %d mV(0x%2x)", hiVreg2, hiVreg2);                                        // ERASE ME
  }else logPrintln("ADC could not be calibrated!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");               // ERASE ME
  

}//End of halInternalInitAdc


int16s convertTo100uVolts(int16u value){
  int32s N;
  int16s V;
  int32s nvalue;

  if (!adcCalibrated) {
    adcCalibrate();
  }
  if (adcCalibrated) {
    assert(nVDD);
    nvalue = value - nVSS;
    // Convert input value (minus ground) to a fraction of VDD/2.
    N = ((nvalue << 16) + nVDD/2) / nVDD;
    // Calculate voltage with: V = (N * Vreg/2) / (2^16)
    // Mutiplying by Vreg/2*10 makes the result units of 100 uVolts
    // (in fixed point E-4 which allows for 13.5 bits vs millivolts
    // which is only 10.2 bits).
    // halInternalGetVreg returns VREG_OUT in millivolts
    V = (int16s)((N*((int32s)halInternalGetVreg())*5) >> 16);
  } else {
     V = -32768;
  }
  return V;
}//End of halConvertValueToVolts

int32s readBoardBattery(void){
  
  int32s V = 0; 
  int16s adcdata;
  
  if (adcOneShotPoll( &adcdata, ADC_SOURCE_BATT_VOLTAGE, 14)){
 
    logPrint("Battery: 0x%4x: ", adcdata); //ERASE ME
    // Convert to 100s of uVolts
    adcdata = convertTo100uVolts( adcdata );
    print_uXtoX (adcdata,10000);//ERASE ME
 
    // Adjust for the voltage divider circuit
    // R1 = 9.83 kOHM
    // R2 = 5.14 kOHM
    V = (adcdata) * (983L + 514L) / 514L;
    logPrint(" volts => ");print_uXtoX (V,10000);logPrintln(" volts.");//ERASE ME
  } 
  else 
    logPrint("Battery: 0 volts. VALUE COULD NOT BE READ FROM DEVICE!!!!!!!!!!!!!!!!! "); //ERASE ME
   
  return V;
   
}// End of readBoardBattery

int16s readBattVoltage(void){ return readBoardBattery(); }// End of readBattVoltage

void readRawVRefs(int16s* vdd, int16s* vRef2, int16s* vReg2){

  adcOneShotPoll( vdd, ADC_SOURCE_VDD_GND, 14);
  
  adcOneShotPoll( vRef2, ((ADC_MUX_VREF2   <<ADC_MUXN_BITS) + ADC_MUX_GND), 14);
    
  // Read 1.8V regulator/2 (nvreg)
  adcOneShotPoll(vReg2, ((ADC_MUX_VREG2   <<ADC_MUXN_BITS) + ADC_MUX_GND), 14);
   
}//End of readRawVRefs

void readACCurrent(void (*callback)(int16u))
{
    /* Calibrate ADC if needed */
  adcCalibrate();
  internalCalibrateADC();

  logPrintln("OFFSET: 0x%2x, GAIN: 0x%2x",ADC_OFFSET, ADC_GAIN); //ERASE ME
  logPrintln("OFFSET: %d; GAIN: %d;",ADC_OFFSET, ADC_GAIN); //ERASE ME
  // Read from the ADC inputs connected to a current transformer (diffrential)
  // and reduce the sample time to improve the measurement resolution
  ADC_CFG = ADC_CLK_SELECTED
            | (ADC_SAMPLE_CLOCKS_SELECTED << ADC_PERIOD_BIT)
            | (ADC_SOURCE_ADC5_ADC4       << ADC_MUXN_BIT);
  
  
  // Depending on the number of samples being taken, it can easilly take longer
  // for the DMA buffer to fill then the watchdog timer to run down. So instead
  // of returning the result immidiatly we will give it to the user-specified
  // callback function when it's ready.
  dmaCallback = callback;
  
  internalReadDiffSeries();
//  adcEnable();
  
  logPrintln("ADCPeriod: %d, ADC bit depth: %d bits, ADC Clock: %d Hz, ADC Sampling frequency, %d Hz, BufferSize: %d",
               getADCPeriod(), getADCbitRes(), getADCClkHz (), getADCFsHz (), DMA_BUFFER_SZ);  //ERASE ME
}

// This function is only intended to be used internally by this file. It polls
// the ADC for a new sample and returns it. This function is independent of
// ADC configuration and input selection.
int16s internalReadADC(void)
{
  // Clear the interrupt flags
  INT_ADCFLAG = 0xFFFF;
  
  // Wait for a new sample
  while (!(INT_ADCFLAG & INT_ADCDATA));
  
  return (int16s)ADC_DATA;
}

// This function is only intended to be used internally by this file. It
// configures the ADC to use it's DMA functionality to take a series of
// readings without tieing up the main program thred. The main application
// will be notified when the DMA operation is finished
void internalReadDiffSeries(void)
{
  // if buffers have not been initialized yet, then this is the moment to do it
  if ((dmaBuffer != buffer1) && (dmaBuffer != buffer2)){
    dmaBuffer= buffer1; processBuffer = buffer2;
  }
  
  // Disable all ADC interrupts
  adcInterruptDisable();
  
  /*Set up DMA to output conversion results:
    a. Reset the DMA
    b. Define the sample Buffer*/
  adcDMAReset();
  setDMABuffer(dmaBuffer, DMA_BUFFER_SZ);
  

  /* Start the ADC and the DMA*/
  
  // Enable the ADC. ADC has been previouls configured (channel and sampling frequency/resolution)
  adcEnable();
  
  // Clear the ADC buffer full flag:
  INT_ADCCFG = INT_ADCULDFULL;
  
  // Start the DMA in autowrap mode:
//  ADC_DMACFG = ADC_DMALOAD | ADC_DMAAUTOWRAP;
  ADC_DMACFG = ADC_DMALOAD;
  
  // Clear the interrupt flags, and enable the ADC top-level interrupt
  INT_ADCFLAG = 0xFFFF;
  INT_CFGSET = INT_ADC;
  
  /* Now we should Wait until the INT_ADCULDFULL bit is set in INT_ADCFLAG, 
     then we read results from dmaBuffer */
 
}//End of internalReadDiffSeries

// This function is only intended to be used internally by this file. It
// calibrates the ADC by providing providing appropriate offset and gain values.
//   Calibrated operating range: 0 V - 1.2 V
void internalCalibrateADC(void)
{
  int16s vgnd, vref;
  
//  adcOneShotPoll( &vgnd, ADC_SOURCE_VGND, 14 );
//  adcOneShotPoll( &vref, ADC_SOURCE_VREF, 14 );
  adcOneShotPoll( &vgnd, ADC_SOURCE_GND_VREF2, 14 );
  adcOneShotPoll( &vref, ADC_SOURCE_VREF_VREF2, 14 );

  ADC_GAIN  = (32768L * 16384L )/ (vref - vgnd);
  
//  adcOneShotPoll( &vgnd, ADC_SOURCE_VGND, 14 );
  adcOneShotPoll( &vgnd, ADC_SOURCE_GND_VREF2, 14 );

  ADC_OFFSET = 2*(0xE000-vgnd);
    
}

// ADC top-level ISR defined by the HAL
// If the active interrupt is "DMA buffer full" then this function will schedule
// the ADC event to run the next time events are run. Afterwords it returns the
// ADC configuration to polling mode.
void halAdcIsr(void)
{
  // WARNING: ISR Context!
  
  // We only want to respond to the DMA full interrupt
  if (!(INT_ADCFLAG & INT_ADCULDFULL)) {
    INT_ADCFLAG = 0xFFFF;
    return;
  }
  // Disable the ADC interrupt
  INT_CFGCLR = INT_ADC;
  
  adcDisable();
  
  // Interchange dma and process buffers
  int16s* tmpbuffer = processBuffer;
  processBuffer = dmaBuffer;
  dmaBuffer = tmpbuffer;
  
  //Reset Differential calibration
  ADC_OFFSET = ADC_OFFSET_RESET;
  ADC_GAIN = ADC_GAIN_RESET;
  
  // Run the event that processes the DMA data
  emberEventControlSetActive(adcEventControl);
  
  // Reset to polling configuration
//  INT_ADCCFG = INT_ADCDATA;
//  ADC_CFG = ADC_1MHZCLK
//            | ADC_ENABLE
//            | (0x7 << ADC_PERIOD_BIT)
//            | (ADC_INPUT_PB7 << ADC_MUXP_BIT)
//            | (ADC_INPUT_VREF2 << ADC_MUXN_BIT);
}

// Integer square root algorithem for calculating RMS value
int32u squareRoot(int32u value)
{
    int32u op  = value;
    int32u res = 0;
    int32u one = 1uL << 30; // The second-to-top bit is set: use 1u << 14 for int16u type; use 1uL<<30 for int32u type


    // "one" starts at the highest power of four <= than the argument
    while (one > op) one >>= 2;

    while (one != 0) {
        if (op >= res + one) {
            op = op - (res + one);
            res = res +  2 * one;
        }
        res >>= 1;
        one >>= 2;
    }

    // Do arithmetic rounding to nearest integer
    if (op > res) res++;
    
    return res;
}
