/** @file hal/ece/AcclSensor.h
 * @brief Header for MMA8452Q accelerator sensor.
 *
 * See @ref Accelerator for documentation.
 *
 * <!-- University of New Brunswick. 2014 -->
 */
#ifndef __ACCELERATOR_SENSOR_H__
#define __ACCELERATOR_SENSOR_H__

#define MMA8452Q_I2C_ADDRESS (0x1C << 1) // This is the address of the device

/******************************************************************************/
/* MMA8452Q Register Address (RA) Map                                         */
/******************************************************************************/
#define MMA8452Q_RA_STATUS           0x00 // (RO) Real time status
 
#define MMA8452Q_RA_OUT_X_MSB        0x01 // (RO) [7:0] are 8 MSBs of 12-bit sample.
#define MMA8452Q_RA_OUT_X_LSB        0x02 // (RO) [7:4] are 4 LSBs of 12-bit sample.
 
#define MMA8452Q_RA_OUT_Y_MSB        0x03 // (RO) [7:0] are 8 MSBs of 12-bit sample.
#define MMA8452Q_RA_OUT_Y_LSB        0x04 // (RO) [7:4] are 4 LSBs of 12-bit sample.
 
#define MMA8452Q_RA_OUT_Z_MSB        0x05 // (RO) [7:0] are 8 MSBs of 12-bit sample.
#define MMA8452Q_RA_OUT_Z_LSB        0x06 // (RO) [7:4] are 4 LSBs of 12-bit sample.
   
#define MMA8452Q_RA_RESERVED00       0x07 // (RO) Reserved. Read return 0x00.
#define MMA8452Q_RA_RESERVED01       0x08 // (RO) Reserved. Read return 0x00.
 
#define MMA8452Q_RA_SYSMOD           0x0B // (RO) Current System Mode
#define MMA8452Q_RA_INT_SOURCE       0x0C // (RO) Interrupt status
#define MMA8452Q_RA_WHO_AM_I         0x0D // (RO) Device ID (0x2A)
#define MMA8452Q_RA_XYZ_DATA_CFG     0x0E // (RW) HPF Data Out and Dynamic Range Settings
#define MMA8452Q_RA_HP_FILTER_CUTOFF 0x0E // (RW) Cut-off frequency is set to 16 Hz @ 800 Hz
 
#define MMA8452Q_RA_PL_STATUS        0x10 // (RO) Landscape/Portrait orientation status
#define MMA8452Q_RA_PL_CFG           0x11 // (RW) Landscape/Portrait configuration.
#define MMA8452Q_RA_PL_COUNT         0x12 // (RO) Landscape/Portrait debounce counter
#define MMA8452Q_RA_PL_BF_ZCOMP      0x13 // (RO) Back-Front, Z-Lock Trip threshold
#define MMA8452Q_RA_P_L_THS_REG      0x14 // (RW) Portrait to Landscape Trip Angle is 29�

#define MMA8452Q_RA_FF_MT_CFG        0x15 // (RW) Freefall/Motion functional block configuration
#define MMA8452Q_RA_FF_MT_SRC        0x16 // (RO) Freefall/Motion event source register
#define MMA8452Q_RA_FF_MT_THS        0x17 // (RW) Freefall/Motion threshold register
#define MMA8452Q_RA_FF_MT_COUNT      0x18 // (RW) Freefall/Motion debounce counter

#define MMA8452Q_RA_RESERVED02       0x19 // (RO) Reserved. Read return 0x00.
#define MMA8452Q_RA_RESERVED03       0x1A // (RO) Reserved. Read return 0x00.
#define MMA8452Q_RA_RESERVED04       0x1B // (RO) Reserved. Read return 0x00.
#define MMA8452Q_RA_RESERVED05       0x1C // (RO) Reserved. Read return 0x00.

#define MMA8452Q_RA_TRANSIENT_CFG    0x1D // (RW) Transient functional block configuration
#define MMA8452Q_RA_TRANSIENT_SRC    0x1E // (RO) Transient event status register
#define MMA8452Q_RA_TRANSIENT_THS    0x1F // (RW) Transient event threshold
#define MMA8452Q_RA_TRANSIENT_COUNT  0x20 // (RW) Transient debounce counter

#define MMA8452Q_RA_PULSE_CFG        0x21 // (RW) ELE, Double_XYZ or Single_XYZ
#define MMA8452Q_RA_PULSE_SRC        0x22 // (RO) EA, Double_XYZ or Single_XYZ
#define MMA8452Q_RA_PULSE_THSX       0x23 // (RW) X pulse threshold
#define MMA8452Q_RA_PULSE_THSY       0x24 // (RW) Y pulse threshold
#define MMA8452Q_RA_PULSE_THSZ       0x25 // (RW) Z pulse threshold
#define MMA8452Q_RA_PULSE_TMLT       0x26 // (RW) Time limit for pulse
#define MMA8452Q_RA_PULSE_LTCY       0x27 // (RW) Latency time for 2nd pulse
#define MMA8452Q_RA_PULSE_WIND       0x28 // (RW) Window time for 2nd pulse

#define MMA8452Q_RA_ASLP_COUNT       0x29 // (RW) Counter setting for Auto-SLEEP
 
#define MMA8452Q_RA_CTRL_REG1        0x2A // (RW) ODR = 800 Hz, STANDBY Mode.
#define MMA8452Q_RA_CTRL_REG2        0x2B // (RW) Sleep Enable, OS Modes, RST, ST
#define MMA8452Q_RA_CTRL_REG3        0x2C // (RW) Wake from Sleep, IPOL, PP_OD
#define MMA8452Q_RA_CTRL_REG4        0x2D // (RW) Interrupt enable register
#define MMA8452Q_RA_CTRL_REG5        0x2E // (RW) Interrupt pin (INT1/INT2) map
 
#define MMA8452Q_RA_OFF_X            0x2F // (RW) X-axis offset adjust
#define MMA8452Q_RA_OFF_Y            0x30 // (RW) Y-axis offset adjust
#define MMA8452Q_RA_OFF_Z            0x31 // (RW) Z-axis offset adjust

#define MMA8452Q_RA_RESERVED06       0x40 // (RO) Reserved. Read return 0x00.
#define MMA8452Q_RA_RESERVED07       0x41 // (RO) Reserved. Read return 0x00.
#define MMA8452Q_RA_RESERVED08       0x42 // (RO) Reserved. Read return 0x00.
#define MMA8452Q_RA_RESERVED09       0x43 // (RO) Reserved. Read return 0x00.
/******************************************************************************/



#define WHO_AM_I_SIGNATURE 0x2A  


/*******************************************************************************
MMA8452Q System Mode (SYSMOD) Register 
   (Register Address: 0x0B. Access:Read Only. Default value: 00)

 The system mode register indicates the current device operating mode. 
 Applications using the Auto-SLEEP/WAKE mechanism should use this register to 
 synchronize the application with the device operating mode transitions.
*/

#define STANDBY_MODE 0x00 // In Standby Mode the device responds to I2C communication but doesn�t allow for updated data
#define WAKE_MODE 0x01
#define SLEEP_MODE 0x02
 
   //Available Scales
#define SCALE_8G 8
#define SCALE_4G 4
#define SCALE_2G 0


// Available data rates when device is in Wake Mode
#define ACTIVE_DR_800HZ  0 //0 0 0 800 Hz 1.25 ms
#define ACTIVE_DR_400HZ  1 //0 0 1 400 Hz 2.5 ms
#define ACTIVE_DR_200HZ  2 //0 1 0 200 Hz 5 ms
#define ACTIVE_DR_100HZ  3 //0 1 1 100 Hz 10 ms
#define ACTIVE_DR_50HZ   4 //1 0 0 50 Hz 20 ms
#define ACTIVE_DR_12P5HZ 5 //1 0 1 12.5 Hz 80 ms
#define ACTIVE_DR_6P25HZ 6 //1 1 0 6.25 Hz 160 ms
#define ACTIVE_DR_1P56HZ 7 //1 1 1 1.56 Hz 640 ms

// Available data rates when device is in Sleep Mode
#define SLEEP_DR_50HZ   0 // 0 0 50 Hz 20 ms
#define SLEEP_DR_12P5HZ 1 // 0 1 12.5 Hz 80 ms
#define SLEEP_DR_6P25HZ 2 // 1 0 6.25 Hz 160 ms
#define SLEEP_DR_1P56HZ 3 // 1 1 1.56 Hz 640 ms

/** Oversampling Modes:  */
typedef enum tOsMode {
  Normal = 0,           // MODS = 00     Normal
  LoNoiseLoPower = 1,   // MODS = 01     Low Noise Low Power
  HiRes = 2,            // MODS = 10     Hi Resolution
  LoPower = 3           // MODS = 11     Low Power
} t_osMode;

#define PORTRAIT_U 0
#define PORTRAIT_D 1
#define LANDSCAPE_R 2
#define LANDSCAPE_L 3
#define LOCKOUT 0x40

/*Reset*/
void MMA8452Q_reset(void);
 

/*Set Modes*/
void MMA8452Q_setActive(boolean enable);

void MMA8452Q_setAutoSleep(boolean enable);

void MMA8452Q_setWakeOn(boolean enable, int8u events);

void MMA8452Q_selfTest(boolean init); 
 
/*Status*/
int8u MMA8452Q_getStatus (void);

int8u MMA8452Q_getSysMod(void);
 
int8u MMA8452Q_getIntSrc(void);

int8u MMA8452Q_getWhoAmI(void);

boolean MMA8452Q_isPresent(void);

boolean MMA8452Q_isActive (void);

void MMA8452Q_getControlRegisters(int8u regs[]);

int8u MMA8452Q_getRegister(int8u rA);

int8u  MMA8452Q_getActiveDataRate (void);

int8u  MMA8452Q_getSleepDataRate (void);

int32u MMA8452Q_getDataRateInUS (int8u odr);

int32u MMA8452Q_getTurnOnTimeUS(int8u odr);

int8u MMA8452Q_XYZDataReady(void);


/*Configuration*/
int8u MMA8452Q_getScale(void);

void MMA8452Q_setScale(int8u scale);

void MMA8452Q_setOSMode (t_osMode osMode);

void MMA8452Q_setSOSMode (t_osMode osMode);

void MMA8452Q_setFastRead(boolean tf);
 
void MMA8452Q_setLowNoise(boolean tf);

void MMA8452Q_setActiveDataRate (int8u DataRateValue);

void MMA8452Q_setSleepDataRate (int8u DataRateValue);


/* Calibration */
void MMA8452Q_setOffset(int8s off_x, int8s off_y, int8s off_z);
 
/* Orientation*/
void MMA8452Q_detectOrientation(boolean enable);
 
boolean MMA8452Q_getOrientation(int8u *value);
 
int16s MMA8452Q_getPortrait(int8u orient);
 
int16s MMA8452Q_getLandscape(int8u orient);
 
int16s MMA8452Q_getBackFront(int8u orient);

/* Axes*/
void MMA8452Q_getAxes(int16s axes[]);

void readAccel(int16s* x, int16s* y, int16s* z); 

/* Debug */

void MMA8452Q_showStatus (int8u st);

void MMA8452Q_showRegister (int8u rV, int8u rA);

void printAxisValue (int16s axisVal, int8u Scale);

void printAxes(int16s axes[], int8u Scale);

void printOrientation (int8u plStatus);

//void printRegisterMap (void);
void printRegisterMap (int ini, int last);
void getRegisterMap ( int8u map[] );
void checkDevice(void);
#endif
   