#ifndef _TEMPERATURE_SENSOR_H
#define _TEMPERATURE_SENSOR_H

#define TEMP_SENSOR_TOLERANCE 200
#define TEMP_SENSOR_MIN_VALUE 0xea84
#define TEMP_SENSOR_MAX_VALUE 0x30d4

/** @brief Initialize Temprature Sensor
 *
 * Configure the onboard temprature sensor via the I2C bus.
 * This function should be called when the Temprature Sensor cluster is being
 * initialized.
 */
void initTemprature(void);

/** @brief Read Temprature
 *
 * This function returns the node's temprature in units of 1/100 C
 */
int16s readTemprature(void);

#endif
