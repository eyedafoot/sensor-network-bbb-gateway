#ifndef _HAL_I2C_H
#define _HAL_I2C_H

/** @brief Initialize I2C
 *
 * Configure SC1 to operate as a I2C (Two Wire Serial) bus master. This
 * function should be called automatically by the stack on power-up
 * (see eznode.h)
 */
void halI2CInit(void);

/** @brief I2C Write 8 Bits
 *
 * Write "data" to the I2C device at "address".
 *
 * @param address
 * @param regester
 * @param data
 */
void halI2CWriteint8u(int8u address, int8u regester, int8u data);

/** @brief I2C Write 16 Bits
 *
 * Write "data" to the I2C device at "address".
 *
 * @param address
 * @param regester
 * @param data
 */
void halI2CWriteint16u(int8u address, int8u regester, int16u data);

/** @brief I2C Read 8 Bits
 *
 * Return the value in "regester" from the I2C device at "address".
 *
 * @param address
 * @param regester
 */
int8u halI2CReadint8u(int8u address, int8u regester);

/** @brief I2C Read 16 Bits
 *
 * Return the value in "regester" from the I2C device at "address".
 *
 * @param address
 * @param regester
 */
int16u halI2CReadint16u(int8u address, int8u regester);

/** @brief I2C Read
 *
 * Read data will be placed into the provided buffer.
 *
 * @param address
 * @param regester
 * @param buffer
 * @param count
 */
void halI2CRead(int8u address, int8u regester, int8u * buffer, int8u count);

/** @brief I2C Write
 *
 * Write the provided data buffer to the specified address.
 *
 * @param address
 * @param regester
 * @param buffer
 * @param count
 */
void halI2CWrite(int8u address, int8u regester, const int8u * buffer, int8u count);

#endif
