#ifndef _ACCELERATION_SENSOR_H
#define _ACCELERATION_SENSOR_H

// The acceleration.c/.h files define an interface for accessing the MMA8452
// acceleration sensor on the UNB ZigBee node. It assumes the I2C interface
// has allready been configured.

#define ACCEL_SENSOR_TOLERANCE 102

/** @brief Initialize Accelerometer
 *
 * Configure the onboard acceleration sensor via the I2C bus.
 * This function should be called when the Acceleration Sensor cluster is being
 * initialized.
 */
void initAcceleration(void);

/** @brief Read Acceleration
 *
 * Take an acceleration measurement from the Accelerometer.
 * Values are returned via the pointer arguments, pass a value of NULL if you
 * aren't interested in that value. Units are 1/1000 g
 */
EmberStatus readAcceleration(int16s * x, int16s * y, int16s * z);

#endif
