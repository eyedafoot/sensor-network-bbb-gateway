#include "app/framework/include/af.h"
#include "i2c.h"

#define WAIT_FIN_SC1(FLAG)  while( !(SC1_TWISTAT & FLAG) ) {}
#define WAIT_CMD_FIN()      WAIT_FIN_SC1(SC_TWICMDFIN)
#define WAIT_SEND_FIN()     WAIT_FIN_SC1(SC_TWITXFIN)
#define WAIT_RECV_FIN()     WAIT_FIN_SC1(SC_TWIRXFIN)

#define ACK 1
#define NAK 0



void i2c_disable (void){
  SC1_MODE = SC1_MODE_DISABLED; // Configure serial controller in disabled mode
}//End of i2c_disable

void i2c_ack_enable(boolean on){
  if ( on )
    SC1_TWICTRL2 |= SC_TWIACK;  // Enable ACK generation after current received byte 
  else
    SC1_TWICTRL2 &= ~SC_TWIACK; // Disable ACK generation after current received byte 
}//End of i2c_ack_enable

void i2c_start (void){
   SC1_TWICTRL1 |= SC_TWISTART; // Generate I2C START condition
   WAIT_CMD_FIN();              // Wait for the command to complete 
}//Endo of i2c_start

void i2c_stop (void){
  SC1_TWICTRL1 |= SC_TWISTOP;   // Generate I2C STOP condition
  WAIT_CMD_FIN();               // Wait for the command to complete 
}//End of i2c_stop

/**
 * Send a byte to I2C bus
 * @param data the byte to be transmitted
 */
void i2c_send(int8u data){
  SC1_DATA = data;              // Put data into buffer
  SC1_TWICTRL1 |= SC_TWISEND;   // Generate SEND command
  WAIT_SEND_FIN();              // Wait for the command to complete 
}//End of i2c_tx

/**
 * Receive a byte from I2C bus
 * @param ack_en If true enable ACK generation after byte reception
 * @return The received byte
 */
int8u i2c_recv(boolean ack_en){
  i2c_ack_enable(ack_en);       // Enable/Disable ACK generation after current received byte
  SC1_TWICTRL1 |= SC_TWIRECV;   // Configure control register 1 for byte reception 
  WAIT_RECV_FIN();              // Wait for the command to complete 
  return SC1_DATA;              // Read data from buffer
}//End of i2c_rx


void halI2CInit(void) {
  /* Configure serial controller to I2C mode */
  SC1_MODE = SC1_MODE_I2C;

  /*
   * The SCL is produced by dividing down 12MHz according to
   * this equation:
   *    Rate = 12 MHz / ( (LIN + 1) * (2^EXP) )
   *
   * Configure rate registers for Fast Mode operation (400 kbps)
   */
  SC1_RATELIN = 14;
  SC1_RATEEXP = 1;

  /* Reset control registers */
  SC1_TWICTRL1 = SC1_TWICTRL1_RESET;
  SC1_TWICTRL2 = SC1_TWICTRL2_RESET;
  
}//End of halI2CInit

void halI2CWriteint8u(int8u address, int8u regester, int8u data){
  i2c_start();
  i2c_send(address);
  i2c_send(regester);
  i2c_send(data);
  i2c_stop();
}//End of halI2CWriteint8u

void halI2CWriteint16u(int8u address, int8u regester, int16u data){
  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  i2c_send( HIGH_BYTE(data) );
  i2c_send( LOW_BYTE (data) );
  i2c_stop();
}//End of halI2CWriteint16u

int8u halI2CReadint8u(int8u address, int8u regester){
  int8u data;

  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  i2c_start();
  i2c_send( address | 1 );
  data = i2c_recv( NAK );
  i2c_stop();

  return data;
}//End of halI2CReadint8u

int16u halI2CReadint16u(int8u address, int8u regester){
  int16u data;

  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  i2c_start();
  i2c_send( address | 1 );
  data = i2c_recv( ACK ) << 8;
  data |= i2c_recv( NAK );
  i2c_stop();

  return data;
}//End of halI2CReadint16u

void halI2CRead(int8u address, int8u regester, int8u * buffer, int8u count){
  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  i2c_start();
  i2c_send( address | 1 );
 
  for (int8u i = 0; i < count-1; i++) 
    buffer[i] = i2c_recv( ACK );
  buffer[count-1] = i2c_recv( NAK );
  
  i2c_stop();
 
}//End of halI2CRead

void halI2CWrite(int8u address, int8u regester, const int8u * buffer, int8u count){
  
  i2c_start();
  i2c_send( address );
  i2c_send( regester );
  
  for (int8u i = 0; i< count; i++) 
    i2c_send( buffer[i] );
  
  i2c_stop();
  
 }//End of halI2CWrite


/* These are the original functions from Tristan 

// Macros for common I2C operations to help improve readability of this file
#define halInternalI2CWait(mask)  while( !(SC1_TWISTAT & mask) )
#define halInternalI2CStart()     SC1_TWICTRL1 = SC_TWISTART;       \
                                  halInternalI2CWait(SC_TWICMDFIN)
                                  
#define halInternalI2CStop()      SC1_TWICTRL1 = SC_TWISTOP;        \
                                  halInternalI2CWait(SC_TWICMDFIN)
                                  
#define halInternalI2CWrite(data) SC1_DATA = data;                  \
                                  SC1_TWICTRL1 = SC_TWISEND;        \
                                  halInternalI2CWait(SC_TWITXFIN)
int8u halInternalI2CRead(void)
{
  SC1_TWICTRL1 = SC_TWIRECV;
  halInternalI2CWait(SC_TWIRXFIN);
  return SC1_DATA;
}

void halI2CInit(void) 
{
  // Set the mode of SC1 to Two Wire Serial (I2C)
  SC1_MODE = SC1_MODE_I2C;
  
  // Send ACK after recieved frames
  SC1_TWICTRL2 = SC_TWIACK;
  
  // Configure baud rate to 375 kbps
  SC1_RATEEXP = 1;
  SC1_RATELIN = 15;
}

 void halI2CWriteint8u(int8u address, int8u regester, int8u data)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  halInternalI2CWrite(data);
  halInternalI2CStop();
}

void halI2CWriteint16u(int8u address, int8u regester, int16u data)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  halInternalI2CWrite((int8u)(0xFF & (data >> 8)));
  halInternalI2CWrite((int8u)(0xFF & data));
  halInternalI2CStop();
}

int8u halI2CReadint8u(int8u address, int8u regester)
{
  int8u data;
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();
  halInternalI2CWrite(address | 1);
  data = halInternalI2CRead();
  halInternalI2CStop();
  return data;
}

int16u halI2CReadint16u(int8u address, int8u regester)
{
  int16u data;
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();			// Send Restart to I2C Bus
  halInternalI2CWrite(address | 1); // Write  address with RW bit set to 1 to set up reading
  
  // Read data from the bus
  data = halInternalI2CRead() << 8;	// Get most significant Byte
  data |= halInternalI2CRead();		// get less significant Byte 
  
  halInternalI2CStop();				// send Stop to I2C Bus
  return data;
}

void halI2CRead(int8u address, int8u regester, int8u * buffer, int8u count)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  
  halInternalI2CStart();
  halInternalI2CWrite(address | 1);

  int8u i;
  for (i = 0; i < count; i++) buffer[i] = halInternalI2CRead();
  halInternalI2CStop();
}
 
void halI2CWrite(int8u address, int8u regester, const int8u * buffer, int8u count)
{
  halInternalI2CStart();
  halInternalI2CWrite(address);
  halInternalI2CWrite(regester);
  int8u i;
  for (i = 0; i < count; i++) halInternalI2CWrite(buffer[i]);
  halInternalI2CStop();
}

*/