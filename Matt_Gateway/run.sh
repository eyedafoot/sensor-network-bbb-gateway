#!/bin/bash
if [ ! -z $1 ]; then
    if [ $1 = "clean" ]; then
        echo "Cleanup Started"
        rm /dev/shm/sem.sem_data1_v2
        rm /dev/shm/sem.sem_data2_v2

        rm data1.csv
        rm data2.csv
        echo "Cleanup Finished"
    elif [ $1 = "make" ]; then
        if [ ! -z $2 ] && [ $2 = "clean" ]; then
            echo "Make Clean"
            make -f Matt_Beagle_Gateway.mak clean
        else
            echo "Making Project"
            make -f Matt_Beagle_Gateway.mak
        fi
    elif [ $1 = "touch" ]; then
        echo "Touch Files"
        find -exec touch \{\} \;
    fi
else
    echo "Running Gateway"
    ./build/Matt_Beagle_Gateway/Matt_Beagle_Gateway -n 1 -p /dev/ttyO2
fi

