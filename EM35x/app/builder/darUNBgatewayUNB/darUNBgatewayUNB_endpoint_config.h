// This file is generated by AppBuilder. Please do not edit manually.
// 
//

// Enclosing macro to prevent multiple inclusion
#ifndef __AF_ENDPOINT_CONFIG__
#define __AF_ENDPOINT_CONFIG__


// Fixed number of defined endpoints
#define FIXED_ENDPOINT_COUNT 2




// Generated attributes
#define GENERATED_ATTRIBUTES { \
    { 0x0000, ZCL_INT8U_ATTRIBUTE_TYPE, 1, (ATTRIBUTE_MASK_SINGLETON), { (int8u*)0x01 } }, /* 0 / Basic / ZCL version*/\
    { 0x0007, ZCL_ENUM8_ATTRIBUTE_TYPE, 1, (ATTRIBUTE_MASK_SINGLETON), { (int8u*)0x00 } }, /* 1 / Basic / power source*/\
    { 0x0000, ZCL_INT16U_ATTRIBUTE_TYPE, 2, (ATTRIBUTE_MASK_WRITABLE), { (int8u*)0x0000 } }, /* 2 / Identify / identify time*/\
    { 0x0000, ZCL_INT16U_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x00 } }, /* 3 / Illuminance Measurement / measured value*/\
    { 0x0001, ZCL_INT16U_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000UL } }, /* 4 / Illuminance Measurement / min measured value*/\
    { 0x0002, ZCL_INT16U_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000UL } }, /* 5 / Illuminance Measurement / max measured value*/\
    { 0x0000, ZCL_INT16S_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000 } }, /* 6 / Temperature Measurement / measured value*/\
    { 0x0001, ZCL_INT16S_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000L } }, /* 7 / Temperature Measurement / min measured value*/\
    { 0x0002, ZCL_INT16S_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000L } }, /* 8 / Temperature Measurement / max measured value*/\
    { 0x0003, ZCL_INT16U_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000UL } }, /* 9 / Temperature Measurement / tolerance*/\
    { 0x0000, ZCL_INT16S_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000L } }, /* 10 / Acceleration Measurement / Instantaneous X Acceleration*/\
    { 0x0001, ZCL_INT16S_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000L } }, /* 11 / Acceleration Measurement / Instantaneous Y Acceleration*/\
    { 0x0002, ZCL_INT16S_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000L } }, /* 12 / Acceleration Measurement / Instantaneous Z Acceleration*/\
    { 0x0003, ZCL_INT16U_ATTRIBUTE_TYPE, 2, (0x00), { (int8u*)0x0000UL } }, /* 13 / Acceleration Measurement / Tolerance*/\
  }


// Cluster function static arrays
#define GENERATED_FUNCTION_ARRAYS \
const EmberAfGenericClusterFunction emberAfFuncArrayIdentifyClusterServer[] = { (EmberAfGenericClusterFunction)emberAfIdentifyClusterServerInitCallback,(EmberAfGenericClusterFunction)emberAfIdentifyClusterServerAttributeChangedCallback}; \


// Clusters defitions
#define GENERATED_CLUSTERS { \
    { 0x0000, (EmberAfAttributeMetadata*)&(generatedAttributes[0]), 2, 0, (CLUSTER_MASK_SERVER), NULL,  },    \
    { 0x0001, (EmberAfAttributeMetadata*)&(generatedAttributes[2]), 0, 0, (CLUSTER_MASK_SERVER), NULL,  },    \
    { 0x0003, (EmberAfAttributeMetadata*)&(generatedAttributes[2]), 1, 2, (CLUSTER_MASK_SERVER| CLUSTER_MASK_INIT_FUNCTION| CLUSTER_MASK_ATTRIBUTE_CHANGED_FUNCTION), emberAfFuncArrayIdentifyClusterServer, },    \
    { 0x0400, (EmberAfAttributeMetadata*)&(generatedAttributes[3]), 0, 0, (CLUSTER_MASK_CLIENT), NULL,  },    \
    { 0x0400, (EmberAfAttributeMetadata*)&(generatedAttributes[3]), 3, 6, (CLUSTER_MASK_SERVER), NULL,  },    \
    { 0x0402, (EmberAfAttributeMetadata*)&(generatedAttributes[6]), 0, 0, (CLUSTER_MASK_CLIENT), NULL,  },    \
    { 0x0402, (EmberAfAttributeMetadata*)&(generatedAttributes[6]), 4, 8, (CLUSTER_MASK_SERVER), NULL,  },    \
    { 0xFE00, (EmberAfAttributeMetadata*)&(generatedAttributes[10]), 0, 0, (CLUSTER_MASK_CLIENT), NULL,  },    \
    { 0xFE00, (EmberAfAttributeMetadata*)&(generatedAttributes[10]), 4, 8, (CLUSTER_MASK_SERVER), NULL,  },    \
  }


// Endpoint types
#define GENERATED_ENDPOINT_TYPES {        \
    { (EmberAfCluster*)&(generatedClusters[0]), 9, 24 }, \
  }


// Networks
#define EMBER_AF_GENERATED_NETWORKS { \
  {ZA_COORDINATOR, EMBER_AF_SECURITY_PROFILE_HA}, \
}
#define EMBER_AF_GENERATED_NETWORK_STRINGS  \
  "Primary", \


// Cluster manufacturer codes
#define GENERATED_CLUSTER_MANUFACTURER_CODES {      \
{0x00, 0x00} \
  }
#define GENERATED_CLUSTER_MANUFACTURER_CODE_COUNT 0

// Attribute manufacturer codes
#define GENERATED_ATTRIBUTE_MANUFACTURER_CODES {      \
{0x00, 0x00} \
  }
#define GENERATED_ATTRIBUTE_MANUFACTURER_CODE_COUNT 0


// Largest attribute size is needed for various buffers
#define ATTRIBUTE_LARGEST 2
// Total size of singleton attributes
#define ATTRIBUTE_SINGLETONS_SIZE 2

// Total size of attribute storage
#define ATTRIBUTE_MAX_SIZE 48

// Array of endpoints that are supported
#define FIXED_ENDPOINT_ARRAY { 1, 2 }

// Array of profile ids
#define FIXED_PROFILE_IDS { 260, 260 }

// Array of profile ids
#define FIXED_DEVICE_IDS { 0, 0 }

// Array of profile ids
#define FIXED_DEVICE_VERSIONS { 0, 0 }

// Array of endpoint types supported on each endpoint
#define FIXED_ENDPOINT_TYPES { 0, 0 }

// Array of networks supported on each endpoint
#define FIXED_NETWORKS { 0, 0 }


// Code used to configure the cluster event mechanism
#define EMBER_AF_GENERATED_EVENT_CODE \
  EmberEventControl emberAfIdentifyClusterServerTickCallbackControl1; \
  EmberEventControl emberAfIdentifyClusterServerTickCallbackControl2; \
  extern EmberEventControl emberAfPluginConcentratorUpdateEventControl; \
  extern void emberAfPluginConcentratorUpdateEventHandler(void); \
  extern EmberEventControl emberAfPluginNetworkFindTickEventControl; \
  extern void emberAfPluginNetworkFindTickEventHandler(void); \
  extern EmberEventControl emberAfPluginIdentifyFeedbackProvideFeedbackEventControl; \
  extern void emberAfPluginIdentifyFeedbackProvideFeedbackEventHandler(void); \
  extern EmberEventControl emberAfPluginReportingTickEventControl; \
  extern void emberAfPluginReportingTickEventHandler(void); \
  extern EmberEventControl buttonEventControl; \
  extern void buttonEventHandler(void); \
  static void clusterTickWrapper(EmberEventControl *control, EmberAfTickFunction callback, int8u endpoint) \
  { \
    emberAfPushEndpointNetworkIndex(endpoint); \
    emberEventControlSetInactive(*control); \
    (*callback)(endpoint); \
    emberAfPopNetworkIndex(); \
  } \
  void emberAfIdentifyClusterServerTickCallbackWrapperFunction1(void) { clusterTickWrapper(&emberAfIdentifyClusterServerTickCallbackControl1, &emberAfIdentifyClusterServerTickCallback, 1); } \
  void emberAfIdentifyClusterServerTickCallbackWrapperFunction2(void) { clusterTickWrapper(&emberAfIdentifyClusterServerTickCallbackControl2, &emberAfIdentifyClusterServerTickCallback, 2); } \


// EmberEventData structs used to populate the EmberEventData table
#define EMBER_AF_GENERATED_EVENTS   \
  { &emberAfIdentifyClusterServerTickCallbackControl1, emberAfIdentifyClusterServerTickCallbackWrapperFunction1 }, \
  { &emberAfIdentifyClusterServerTickCallbackControl2, emberAfIdentifyClusterServerTickCallbackWrapperFunction2 }, \
  { &emberAfPluginConcentratorUpdateEventControl, emberAfPluginConcentratorUpdateEventHandler }, \
  { &emberAfPluginNetworkFindTickEventControl, emberAfPluginNetworkFindTickEventHandler }, \
  { &emberAfPluginIdentifyFeedbackProvideFeedbackEventControl, emberAfPluginIdentifyFeedbackProvideFeedbackEventHandler }, \
  { &emberAfPluginReportingTickEventControl, emberAfPluginReportingTickEventHandler }, \
  { &buttonEventControl, buttonEventHandler }, \


#define EMBER_AF_GENERATED_EVENT_STRINGS   \
  "Identify Cluster Server EP 1",  \
  "Identify Cluster Server EP 2",  \
  "Concentrator Support Plugin Update",  \
  "Network Find Plugin Tick",  \
  "Identify Feedback Plugin ProvideFeedback",  \
  "Reporting Plugin Tick",  \
  "button Custom",  \


// The length of the event context table used to track and retrieve cluster events
#define EMBER_AF_EVENT_CONTEXT_LENGTH 2

// EmberAfEventContext structs used to populate the EmberAfEventContext table
#define EMBER_AF_GENERATED_EVENT_CONTEXT { 0x1, 0x3, FALSE, EMBER_AF_OK_TO_HIBERNATE, &emberAfIdentifyClusterServerTickCallbackControl1}, \
{ 0x2, 0x3, FALSE, EMBER_AF_OK_TO_HIBERNATE, &emberAfIdentifyClusterServerTickCallbackControl2}


#define EMBER_AF_GENERATED_PLUGIN_INIT_FUNCTION_DECLARATIONS \
  void emberAfPluginConcentratorInitCallback(void); \
  void emberAfPluginNetworkFindInitCallback(void); \
  void emberAfPluginAddressTableInitCallback(void); \
  void emberAfPluginReportingInitCallback(void); \


#define EMBER_AF_GENERATED_PLUGIN_INIT_FUNCTION_CALLS \
  emberAfPluginConcentratorInitCallback(); \
  emberAfPluginNetworkFindInitCallback(); \
  emberAfPluginAddressTableInitCallback(); \
  emberAfPluginReportingInitCallback(); \


#define EMBER_AF_GENERATED_PLUGIN_NCP_INIT_FUNCTION_DECLARATIONS \
  void emberAfPluginConcentratorNcpInitCallback(boolean memoryAllocation); \
  void emberAfPluginAddressTableNcpInitCallback(boolean memoryAllocation); \


#define EMBER_AF_GENERATED_PLUGIN_NCP_INIT_FUNCTION_CALLS \
  emberAfPluginConcentratorNcpInitCallback(memoryAllocation); \
  emberAfPluginAddressTableNcpInitCallback(memoryAllocation); \


#define EMBER_AF_GENERATED_PLUGIN_STACK_STATUS_FUNCTION_DECLARATIONS \
  void emberAfPluginNetworkFindStackStatusCallback(EmberStatus status); \


#define EMBER_AF_GENERATED_PLUGIN_STACK_STATUS_FUNCTION_CALLS \
  emberAfPluginNetworkFindStackStatusCallback(status); \


#define EMBER_AF_GENERATED_PLUGIN_MESSAGE_SENT_FUNCTION_DECLARATIONS \
  void emberAfPluginConcentratorMessageSentCallback(EmberOutgoingMessageType type, \
                    int16u indexOrDestination, \
                    EmberApsFrame *apsFrame, \
                    EmberStatus status, \
                    int16u messageLength, \
                    int8u *messageContents); \


#define EMBER_AF_GENERATED_PLUGIN_MESSAGE_SENT_FUNCTION_CALLS \
  emberAfPluginConcentratorMessageSentCallback(type, \
                    indexOrDestination, \
                    apsFrame, \
                    status, \
                    messageLength, \
                    messageContents); \

// Generated data for the command discovery
#define GENERATED_COMMANDS { \
    { 0x0003, 0x00, COMMAND_MASK_OUTGOING_SERVER }, /* Identify / IdentifyQueryResponse */ \
    { 0x0003, 0x00, COMMAND_MASK_INCOMING_SERVER }, /* Identify / Identify */ \
    { 0x0003, 0x01, COMMAND_MASK_INCOMING_SERVER }, /* Identify / IdentifyQuery */ \
  }
#define EMBER_AF_GENERATED_COMMAND_COUNT 3
#endif // __AF_ENDPOINT_CONFIG__
